<?php
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use App\Models\Student_consultant;
if (App::environment('production')) {
    URL::forceScheme('https');
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('authentication/login'); });
Route::get('/home', function () { return redirect('integration/mystore'); });

/* Authentication  */
Route::get('authentication', function ()        { return redirect('authentication/login'); });



Route::get('changelang','CommonController@changelang')->name('changelang');

if(Cache::has('lang')){
    App::setLocale(Cache::get('lang'));
}


Route::get('authentication/login',              'AuthenticationController@login')->name('authentication.login');
Route::get('authentication/logout',              'AuthenticationController@logout')->name('authentication.logout');
Route::post('authentication/validatelogin',     'AuthenticationController@validatelogin')->name('authentication.validatelogin');
Route::get('authentication/login2',           'AuthenticationController@login2')->name('authentication.login2');
Route::get('authentication/register',           'AuthenticationController@register')->name('authentication.register');
Route::post('authentication/register',          'AuthenticationController@store')->name('authentication.register');
Route::get('authentication/forgotpassword',     'AuthenticationController@forgotpassword')->name('authentication.forgotpassword');
Route::post('authentication/forgotpasswordemail',     'AuthenticationController@forgotpasswordemail')->name('authentication.forgotpasswordemail');
Route::get('authentication/page404',            'AuthenticationController@page404')->name('authentication.page404');
Route::get('authentication/maintenance',        'AuthenticationController@maintenance')->name('authentication.maintenance');
Route::get('authentication/comingsoon',         'AuthenticationController@comingsoon')->name('authentication.comingsoon');
Route::get('authentication/adminvalidatelogin/{user}',     'AuthenticationController@adminvalidatelogin')->name('authentication.adminvalidatelogin');
Route::get('authentication/adminlogout',              'AuthenticationController@adminlogout')->name('authentication.adminlogout');
Route::get('authentication/consvalidatelogin/{user}',     'AuthenticationController@consvalidatelogin')->name('authentication.consvalidatelogin');
Route::get('authentication/conslogout',              'AuthenticationController@conslogout')->name('authentication.conslogout');
Route::get('authentication/confirmemail/{user}',   function($user) {
    try{
        $decrypted = Crypt::decryptString($user);
    } catch (Throwable $e) {
        return abort(404);
    }

    $user=App\User::find($decrypted);
    $loginURL=config('app.url').'/public/authentication/login/';
    //echo $user;
    //echo $user->email;

    if($user->email_verified_at==null) {

        $result = Student_consultant::where('email', '=', $user->email)
        ->update(['status' => 3]);
        echo $result;

        $user->email_verified_at=now();
        $user->save();
        return response('Your email verified successfully.  <a href="'.$loginURL.'">Click here to login</a>','400');
    } else {
        return response('Your email already verified. <a href="'.$loginURL.'">Click here to login</a>','400');
    }
});



/**Batch jobs */
Route::get('batch/suppliers',                    'BatchController@suppliers')->name('batch.suppliers');
Route::get('batch/processSuppliers',                    'BatchController@processSuppliers')->name('batch.processSuppliers');
Route::get('batch/pushtoamazon',                    'BatchController@pushtoamazon')->name('batch.pushtoamazon');
Route::get('batch/pushtoamazoninventory',                    'BatchController@pushtoamazoninventory')->name('batch.pushtoamazoninventory');
Route::get('batch/pushtoamazonprice',                    'BatchController@pushtoamazonprice')->name('batch.pushtoamazonprice');
Route::get('batch/updateinventory',                    'BatchController@updateinventory')->name('batch.updateinventory');
Route::get('batch/updateinventorydoc',                    'BatchController@updateinventorydoc')->name('batch.updateinventorydoc');
Route::get('batch/processinventorydoc',                    'BatchController@processInventoryDoc')->name('batch.processInventoryDoc');
Route::get('batch/updateinventorytable',                    'BatchController@updateinventorytable')->name('batch.updateinventorytable');
Route::get('batch/updateexchangerate',                    'BatchController@updateexchangerate')->name('batch.updateexchangerate');
Route::get('batch/updateorders',                    'BatchController@updateorders')->name('batch.updateorders');
Route::get('batch/updateordersS',                    'BatchController@updateordersS')->name('batch.updateordersS');
Route::get('batch/updatecate',                    'BatchController@updatecate')->name('batch.updatecate');
Route::get('batch/updateexchangeratetry',                    'BatchController@updateexchangeratetry')->name('batch.updateexchangeratetry');
Route::get('batch/updateinventories_qty',                    'BatchController@updateinventories_qty')->name('batch.updateinventories_qty');
Route::get('batch/updateinventories_price',                    'BatchController@updateinventories_price')->name('batch.updateinventories_price');


Route::post('payment/callback',                    'PaymentController@callback')->name('payment.callback');


Route::group(['middleware' => 'auth'], function () {
/* Subscription*/
Route::get('subscription', function () { return redirect('subscription/addnew'); });
Route::get('subscription/addnew',                    'SubscriptionController@index')->name('subscription.addnew');
Route::get('subscription/process/{package}',                    'SubscriptionController@process')->name('subscription.process');
Route::post('subscription/confirm',                    'SubscriptionController@confirm')->name('subscription.confirm');
Route::post('subscription/applydiscount',                    'SubscriptionController@applydiscount')->name('subscription.applydiscount');
Route::get('subscription/postprocess',                    'SubscriptionController@postprocess')->name('subscription.postprocess');
Route::get('subscription/postprocess1',                    'SubscriptionController@postprocess1')->name('subscription.postprocess1');
Route::post('subscription/getbin',                    'SubscriptionController@getbin')->name('subscription.getbin');
Route::get('subscription/success_payment',                    'SubscriptionController@success_payment')->name('subscription.success_payment');
Route::post('subscription/failure_payment',                    'SubscriptionController@failure_payment')->name('subscription.failure_payment');
Route::post('subscription/getinstallment',                    'SubscriptionController@getinstallment')->name('subscription.getinstallment');
Route::get('subscription/tokenprocess',                    'SubscriptionController@tokenprocess')->name('subscription.tokenprocess');
Route::post('subscription/upfinalamount',                    'SubscriptionController@upfinalamount')->name('subscription.upfinalamount');
Route::get('subscription/success_payment1',                    'SubscriptionController@success_payment1')->name('subscription.success_payment1');
Route::get('subscription/addto_invoice_payments',                    'SubscriptionController@addto_invoice_payments')->name('subscription.addto_invoice_payments');
Route::get('subscription/success_payment2',                    'SubscriptionController@success_payment2')->name('subscription.success_payment2');
Route::get('subscription/addto_wallet_payments',                    'SubscriptionController@addto_wallet_payments')->name('subscription.addto_wallet_payments');


/**Common */
Route::get('common/demolish',                    'CommonController@demolish')->name('common.demolish');
Route::get('common/actconsultant',                    'CommonController@actconsultant')->name('common.actconsultant');

/*Common API*/
Route::post('api/fetch-states',                    'ApiController@states')->name('api.states');
Route::get('api/updateuser',                    'ApiController@updateuser')->name('api.updateuser');
Route::post('api/savefeed',                    'ApiController@savefeed')->name('api.savefeed');
Route::get('api/loadstore',                    'ApiController@loadstore')->name('api.loadstore');

/* Integration */
Route::get('integration', function ()                { return redirect('integration/addstore'); });
Route::get('integration/addstore',                    'IntegrationController@addstore')->name('integration.addstore');
Route::post('integration/addstore',                    'IntegrationController@savestore')->name('integration.savestore');
Route::get('integration/connectspapi',                    'IntegrationController@successReturnSpapi')->name('integration.successReturnSpapi');
Route::get('integration/mystore',                     'IntegrationController@mystore')->name('integration.mystore');
Route::get('integration/callback',                     'IntegrationController@callback')->name('integration.callback');
Route::get('integration/changestore/{storeid}',                     'IntegrationController@changestore')->name('integration.changestore');
Route::get('integration/feedbackstatus/{feedbk}',                     'IntegrationController@feedbackstatus')->name('integration.feedbackstatus');
Route::get('integration/deletestore',                     'IntegrationController@deletestore')->name('integration.deletestore');


/** Dashboards */
Route::get('dashboard', function ()                { return redirect('dashboard/dashboard'); });
Route::get('dashboard/userdash',                    'DashboardController@userdash')->name('dashboard.userdash');
Route::get('dashboard/profitloss',                    'DashboardController@profitloss')->name('dashboard.profitloss');
Route::get('dashboard/consettings',                    'DashboardController@consettings')->name('dashboard.consettings');
Route::get('dashboard/consdetails',                    'DashboardController@consdetails')->name('dashboard.consdetails');
Route::get('dashboard/order',                    'DashboardController@dashboard')->name('dashboard.dashboard');
Route::get('dashboard/con_student_list',                    'DashboardController@con_student_list')->name('dashboard.con_student_list');
Route::post('dashboard/add_upload',                    'DashboardController@add_upload')->name('dashboard.add_upload');
Route::get('dashboard/classroom',                    'DashboardController@classroom')->name('dashboard.classroom');
Route::post('dashboard/add_class',                    'DashboardController@add_class')->name('dashboard.add_class');
Route::post('dashboard/update_class',                    'DashboardController@update_class')->name('dashboard.update_class');
Route::get('dashboard/get_class',                    'DashboardController@get_class')->name('dashboard.get_class');
Route::get('dashboard/teacher',                    'DashboardController@teacher')->name('dashboard.teacher');
Route::post('dashboard/add_teacher',                    'DashboardController@add_teacher')->name('dashboard.add_teacher');
Route::get('dashboard/get_teacher',                    'DashboardController@get_teacher')->name('dashboard.get_teacher');
Route::post('dashboard/update_teacher',                    'DashboardController@update_teacher')->name('dashboard.update_teacher');
Route::get('dashboard/banstudent',                    'DashboardController@banstudent')->name('dashboard.banstudent');
Route::post('dashboard/addlogo',                    'DashboardController@addlogo')->name('dashboard.addlogo');
Route::get('dashboard/generaldashboard',                    'DashboardController@generaldashboard')->name('dashboard.generaldashboard');


/* Drop shipping */
Route::get('dropshipping', function ()                { return redirect('dropshipping/blacklist'); });
Route::get('dropshipping/productsearch',                    'DropShippingController@productsearch')->name('dropshipping.productsearch');
Route::post('dropshipping/productsearchApi',                    'DropShippingController@productsearchApi')->name('dropshipping.productsearchApi');
Route::get('dropshipping/bestselling',                    'DropShippingController@bestselling')->name('dropshipping.bestselling');
Route::get('dropshipping/blacklistcategory',                    'DropShippingController@blacklistcategory')->name('dropshipping.blacklistcategory');
Route::get('dropshipping/blacklist',                    'DropShippingController@blacklist')->name('dropshipping.blacklist');
Route::get('dropshipping/whitelist',                    'DropShippingController@whitelist')->name('dropshipping.whitelist');
Route::post('dropshipping/addnew',                    'DropShippingController@addnew')->name('dropshipping.addnew');
Route::get('dropshipping/listsearch',                    'DropShippingController@listsearch')->name('dropshipping.listsearch');
Route::get('dropshipping/listsearch2',                    'DropShippingController@listsearch2')->name('dropshipping.listsearch2');
Route::get('dropshipping/listsearchpool',                    'DropShippingController@listsearchpool')->name('dropshipping.listsearchpool');
Route::get('dropshipping/listsearch5',                    'DropShippingController@listsearch5')->name('dropshipping.listsearch5');
Route::get('dropshipping/listpendingapp',                    'DropShippingController@listpendingapp')->name('dropshipping.listpendingapp');
Route::get('dropshipping/restrictedproducts',                    'DropShippingController@restrictedproducts')->name('dropshipping.restrictedproducts');
Route::get('dropshipping/commonbrandpool',                    'DropShippingController@commonbrandpool')->name('dropshipping.commonbrandpool');
Route::get('dropshipping/commonproductpool',                    'DropShippingController@commonproductpool')->name('dropshipping.commonproductpool');
Route::get('dropshipping/addnewproduct',                    'DropShippingController@addnewproduct')->name('dropshipping.addnewproduct');
Route::post('dropshipping/addnewproduct',                    'DropShippingController@savenewproduct')->name('dropshipping.addnewproduct');
Route::get('dropshipping/restrictedkeywords',                    'DropShippingController@restrictedkeywords')->name('dropshipping.restrictedkeywords');
Route::post('dropshipping/restrictedkeywords',                    'DropShippingController@saverestrictedkeywords')->name('dropshipping.restrictedkeywords');
Route::get('dropshipping/bulkupload',                    'DropShippingController@bulkupload')->name('dropshipping.bulkupload');
Route::get('dropshipping/deleteitem',                    'DropShippingController@deleteitem')->name('dropshipping.deleteitem');
Route::get('dropshipping/deletekeyword',                    'DropShippingController@deletekeyword')->name('dropshipping.deletekeyword');
Route::get('dropshipping/deleteproduct',                    'DropShippingController@deleteproduct')->name('dropshipping.deleteproduct');
Route::get('dropshipping/pooltobrandlist',                    'DropShippingController@pooltobrandlist')->name('dropshipping.pooltobrandlist');
Route::get('dropshipping/pooltoasin',                    'DropShippingController@pooltoasin')->name('dropshipping.pooltoasin');
Route::get('dropshipping/pendingapproval',                    'DropShippingController@pendingapproval')->name('dropshipping.pendingapproval');
Route::get('dropshipping/duplicatelistings',                    'DropShippingController@duplicatelistings')->name('dropshipping.duplicatelistings');
Route::get('dropshipping/productdetails',                    'DropShippingController@productdetails')->name('dropshipping.productdetails');
Route::get('dropshipping/suppressed',                    'DropShippingController@suppressed')->name('dropshipping.suppressed');
Route::get('dropshipping/buyboxreport',                    'DropShippingController@buyboxreport')->name('dropshipping.buyboxreport');
Route::get('dropshipping/salesreports',                    'DropShippingController@salesreports')->name('dropshipping.salesreports');
Route::get('dropshipping/updateStatus',                    'DropShippingController@updateStatus')->name('dropshipping.updateStatus');
Route::get('dropshipping/searchhistory',                    'DropShippingController@searchhistory')->name('dropshipping.searchhistory');
Route::post('dropshipping/advancedsearch',                    'DropShippingController@advancedsearch')->name('dropshipping.advancedsearch');
Route::get('dropshipping/addtorestricated',                    'DropShippingController@addtorestricated')->name('dropshipping.addtorestricated');
Route::get('dropshipping/update_searchrequest',                    'DropShippingController@update_searchrequest')->name('dropshipping.update_searchrequest');
Route::get('dropshipping/categorysearch',                    'DropShippingController@categorysearch')->name('dropshipping.categorysearch');
Route::post('dropshipping/addcategory',                    'DropShippingController@addcategory')->name('dropshipping.addcategory');
Route::get('dropshipping/deletecate',                    'DropShippingController@deletecate')->name('dropshipping.deletecate');
Route::get('dropshipping/clonesearch',                    'DropShippingController@clonesearch')->name('dropshipping.clonesearch');
Route::get('dropshipping/approvedproducts',                    'DropShippingController@approvedproducts')->name('dropshipping.approvedproducts');


/* Amazon */
Route::get('amazon', function ()                { return redirect('amazon/orders'); });
Route::get('amazon/orders',                    'AmazonController@orders')->name('amazon.orders');
Route::get('amazon/listing',                    'AmazonController@listing')->name('amazon.listing');
Route::get('amazon/feedreport',                    'AmazonController@feedreport')->name('amazon.feedreport');
Route::get('amazon/vieworder/{orderid}',                    'AmazonController@vieworder')->name('amazon.vieworder');
Route::get('amazon/inventory',                    'AmazonController@inventory')->name('amazon.inventory');
Route::get('amazon/ordersPage',                    'AmazonController@ordersPage')->name('amazon.ordersPage');
Route::get('amazon/reports',                    'AmazonController@reports')->name('amazon.reports');
Route::get('amazon/viewreport/{reportid}',                    'AmazonController@viewreport')->name('amazon.viewreport');
Route::get('amazon/viewdoc',                    'AmazonController@viewdoc')->name('amazon.viewdoc');
Route::get('amazon/deletereport',                    'AmazonController@deletereport')->name('amazon.deletereport');
Route::post('amazon/addnewreport',                    'AmazonController@addnewreport')->name('amazon.addnewreport');
Route::get('amazon/healthreport',                    'AmazonController@healthreport')->name('amazon.healthreport');
Route::get('amazon/addhealthreport',                    'AmazonController@addhealthreport')->name('amazon.addhealthreport');
Route::get('amazon/new_vieworder',                    'AmazonController@new_vieworder')->name('amazon.new_vieworder');
Route::get('amazon/new_ordersPage',                    'AmazonController@new_ordersPage')->name('amazon.new_ordersPage');
Route::get('amazon/add_loadmsg',                    'AmazonController@add_loadmsg')->name('amazon.add_loadmsg');
Route::get('amazon/load_filters',                    'AmazonController@load_filters')->name('amazon.load_filters');
Route::get('amazon/saveconfig',                    'AmazonController@saveconfig')->name('amazon.saveconfig');
Route::get('amazon/savefilters',                    'AmazonController@savefilters')->name('amazon.savefilters');
Route::get('amazon/loadcols',                    'AmazonController@loadcols')->name('amazon.loadcols');
Route::get('amazon/inventoryhistory',                    'AmazonController@inventoryhistory')->name('amazon.inventoryhistory');

/* Options */
Route::get('options', function ()                { return redirect('options/suppliers'); });
Route::get('options/suppliers',                    'OptionsController@suppliers')->name('options.suppliers');
Route::get('options/listsearch',                    'OptionsController@listsearch')->name('options.listsearch');
Route::get('options/listsearch2',                    'OptionsController@listsearch2')->name('options.listsearch2');
Route::post('options/suppliers',                    'OptionsController@savesuppliers')->name('options.suppliers');
Route::get('options/suppliersdetails',                    'OptionsController@suppliersdetails')->name('options.suppliersdetails');
Route::get('options/fileupload',                    'OptionsController@fileupload')->name('options.fileupload');
Route::post('options/fileupload',                    'OptionsController@uploadfiletoserver')->name('options.fileupload');
Route::get('options/download',                    'OptionsController@download')->name('options.download');
Route::get('options/profitloss',                    'OptionsController@profitloss')->name('options.profitloss');

/** FBA */
Route::get('fba', function ()                { return redirect('fba/fbalist'); });
Route::get('fba/fbalist',                    'FbaController@fbalist')->name('fba.fbalist');
Route::get('fba/fbashipment',                    'FbaController@fbashipment')->name('fba.fbashipment');
Route::get('fba/addnewplan',                    'FbaController@addnewplan')->name('fba.addnewplan');
Route::get('fba/giveaway',                    'FbaController@giveaway')->name('fba.giveaway');
Route::post('fba/saveplan',                    'FbaController@saveplan')->name('fba.saveplan');
Route::get('fba/addproduct',                    'FbaController@addproduct')->name('fba.addproduct');
Route::get('fba/load_listings',                    'FbaController@load_listings')->name('fba.load_listings');
Route::get('fba/load_selectprod',                    'FbaController@load_selectprod')->name('fba.load_selectprod');
Route::get('fba/changeqty',                    'FbaController@changeqty')->name('fba.changeqty');
Route::get('fba/toestimate',                    'FbaController@toestimate')->name('fba.toestimate');
Route::get('fba/removeprod',                    'FbaController@removeprod')->name('fba.removeprod');
Route::get('fba/loadstore_list',                    'FbaController@loadstore_list')->name('fba.loadstore_list');
Route::get('fba/load_fbashipment',                    'FbaController@load_fbashipment')->name('fba.load_fbashipment');

/** Feedbacks */
Route::get('feedbacks', function ()                { return redirect('feedbacks/campaign'); });
Route::get('feedbacks/campaign',                    'FeedbacksController@campaign')->name('feedbacks.campaign');
Route::get('feedbacks/newcampaign',                    'FeedbacksController@newcampaign')->name('feedbacks.newcampaign');


/** Transaction */
Route::get('transaction', function ()                { return redirect('transaction/walletlist'); });
Route::get('transaction/walletlist',                    'TransactionController@walletlist')->name('transaction.walletlist');
Route::post('transaction/add_credits',                    'TransactionController@add_credits')->name('transaction.add_credits');
Route::post('transaction/add_cclist',                    'TransactionController@add_cclist')->name('transaction.add_cclist');
Route::get('transaction/view_cclist',                    'TransactionController@view_cclist')->name('transaction.view_cclist');
Route::get('transaction/delete_cc',                    'TransactionController@delete_cc')->name('transaction.delete_cc');
Route::get('transaction/update_default_cc',                    'TransactionController@update_default_cc')->name('transaction.update_default_cc');


/** Settings */
Route::get('settings', function ()                { return redirect('settings/shipping'); });
Route::get('settings/shipping',                    'SettingsController@shipping')->name('settings.shipping');
Route::post('settings/shipping',                    'SettingsController@shippingsave')->name('settings.shippingsave');
Route::get('settings/showshipping',                    'SettingsController@showshipping')->name('settings.showshipping');
Route::get('settings/generalsettings',                    'SettingsController@generalsettings')->name('settings.generalsettings');
Route::get('settings/invoicedetails',                    'SettingsController@invoicedetails')->name('settings.invoicedetails');
Route::get('settings/userdetails',                    'SettingsController@userdetails')->name('settings.userdetails');
Route::get('settings/updatetable',                    'SettingsController@updatetable')->name('settings.updatetable');
Route::get('settings/filterbystore',                    'SettingsController@filterbystore')->name('settings.filterbystore');
Route::post('settings/storeinfo_update',                    'SettingsController@storeinfo_update')->name('settings.storeinfo_update');
Route::get('settings/filterbystoreinfo',                    'SettingsController@filterbystoreinfo')->name('settings.filterbystoreinfo');
Route::get('settings/update_user_main_config',                    'SettingsController@update_user_main_config')->name('settings.update_user_main_config');
Route::get('settings/update_repricer',                    'SettingsController@update_repricer')->name('settings.update_repricer');
Route::get('settings/pricerloadstore',                    'SettingsController@pricerloadstore')->name('settings.pricerloadstore');
Route::post('settings/update_pricingdetails',                    'SettingsController@update_pricingdetails')->name('settings.update_pricingdetails');
Route::post('settings/add_cclist',                    'SettingsController@add_cclist')->name('settings.add_cclist');
Route::get('settings/view_cclist',                    'SettingsController@view_cclist')->name('settings.view_cclist');
Route::get('settings/delete_cc',                    'SettingsController@delete_cc')->name('settings.delete_cc');
Route::get('settings/update_default_cc',                    'SettingsController@update_default_cc')->name('settings.update_default_cc');
Route::post('settings/add_credits',                    'SettingsController@add_credits')->name('settings.add_credits');
Route::get('settings/view_credits',                    'SettingsController@view_credits')->name('settings.view_credits');
Route::get('settings/view_credit_type',                    'SettingsController@view_credit_type')->name('settings.view_credit_type');
Route::get('settings/bill_details_list',                    'SettingsController@bill_details_list')->name('settings.bill_details_list');
Route::get('settings/ccp_list',                    'SettingsController@ccp_list')->name('settings.ccp_list');
Route::get('settings/totalpayment',                    'SettingsController@totalpayment')->name('settings.totalpayment');
Route::post('settings/cc_pay',                    'SettingsController@cc_pay')->name('settings.cc_pay');
Route::get('settings/shipfilterbystore',                    'SettingsController@shipfilterbystore')->name('settings.shipfilterbystore');

Route::get('settings/loadstore_list',                    'SettingsController@loadstore_list')->name('settings.loadstore_list');
Route::post('settings/changepwd',                    'SettingsController@changepwd')->name('settings.changepwd');
Route::post('settings/updatelocal',                    'SettingsController@updatelocal')->name('settings.updatelocal');
Route::get('settings/loadstate_list',                    'SettingsController@loadstate_list')->name('settings.loadstate_list');
Route::post('settings/updatecompany',                    'SettingsController@updatecompany')->name('settings.updatecompany');
Route::get('settings/updateusertable',                    'SettingsController@updateusertable')->name('settings.updateusertable');
Route::get('settings/filters_update',                    'SettingsController@filters_update')->name('settings.filters_update');
Route::get('settings/updatephone',                    'SettingsController@updatephone')->name('settings.updatephone');
Route::post('settings/updatestore',                    'SettingsController@updatestore')->name('settings.updatestore');
Route::post('settings/postpone',                    'SettingsController@postpone')->name('settings.postpone');
Route::get('settings/getqueue',                    'SettingsController@getqueue')->name('settings.getqueue');
Route::get('settings/postpone_details',                    'SettingsController@postpone_details')->name('settings.postpone_details');
Route::get('settings/rollback',                    'SettingsController@rollback')->name('settings.rollback');
Route::get('settings/getorderids',                    'SettingsController@getorderids')->name('settings.getorderids');
Route::get('settings/getpaylist',                    'SettingsController@getpaylist')->name('settings.getpaylist');
Route::post('settings/addtopaylist',                    'SettingsController@addtopaylist')->name('settings.addtopaylist');
Route::post('settings/addto_invoice_payments',                    'SettingsController@addto_invoice_payments')->name('settings.addto_invoice_payments');
Route::post('settings/add_payoneer',                    'SettingsController@add_payoneer')->name('settings.add_payoneer');
Route::get('settings/viewpayoneer',                    'SettingsController@viewpayoneer')->name('settings.viewpayoneer');
Route::get('settings/delete_payoneer',                    'SettingsController@delete_payoneer')->name('settings.delete_payoneer');
Route::get('settings/update_default_payoneer',                    'SettingsController@update_default_payoneer')->name('settings.update_default_payoneer');
Route::get('settings/updatepayoneerlist',                    'SettingsController@updatepayoneerlist')->name('settings.updatepayoneerlist');
Route::get('settings/creditbal',                    'SettingsController@creditbal')->name('settings.creditbal');
Route::get('settings/formulalist',                    'SettingsController@formulalist')->name('settings.formulalist');
Route::get('settings/loadprice_opt',                    'SettingsController@loadprice_opt')->name('settings.loadprice_opt');
Route::get('settings/getwallet',                    'SettingsController@getwallet')->name('settings.getwallet');
Route::get('settings/getwalletdetails',                    'SettingsController@getwalletdetails')->name('settings.getwalletdetails');
Route::get('settings/takefromwallet',                    'SettingsController@takefromwallet')->name('settings.takefromwallet');
Route::get('settings/freeze',                    'SettingsController@freeze')->name('settings.freeze');
Route::get('settings/extraprofitlist',                    'SettingsController@extraprofitlist')->name('settings.extraprofitlist');


/** Help Desk*/
Route::get('helpdesk', function ()                { return redirect('helpdesk/createticket'); });
Route::get('helpdesk/createticket',                      'HelpdeskController@createticket')->name('helpdesk.createticket');
Route::get('helpdesk/ticketlist',                     'HelpdeskController@ticketlist')->name('helpdesk.ticketlist');
Route::get('helpdesk/ticket_message',                     'HelpdeskController@ticket_message')->name('helpdesk.ticket_message');
Route::post('helpdesk/add_ticket',                     'HelpdeskController@add_ticket')->name('helpdesk.add_ticket');
Route::get('helpdesk/priority_list',                     'HelpdeskController@priority_list')->name('helpdesk.priority_list');
Route::post('helpdesk/add_msg',            'HelpdeskController@add_msg')->name('helpdesk.add_msg');
Route::post('helpdesk/add_url',                     'HelpdeskController@add_url')->name('helpdesk.add_url');








#Route::get('integration/subscribe',                     'IntegrationController@users_can_subscribe_plans')->name('integration.subscribe');
#Route::get('integration/pay',                     'IntegrationController@we_must_pay_user_when_charge_date_has_come')->name('integration.pay');

/* My Page */
Route::get('mypage', function ()                { return redirect('mypage/index'); });
Route::get('mypage/index',                      'MypageController@index')->name('mypage.index');
Route::get('mypage/index4',                     'MypageController@index4')->name('mypage.index4');
Route::get('mypage/index5',                     'MypageController@index5')->name('mypage.index5');
Route::get('mypage/index6',                     'MypageController@index6')->name('mypage.index6');
Route::get('mypage/index7',                     'MypageController@index7')->name('mypage.index7');
Route::get('mypage/index8',                     'MypageController@index8')->name('mypage.index8');
Route::get('mypage/index9',                     'MypageController@index9')->name('mypage.index9');
Route::get('mypage/index10',                    'MypageController@index10')->name('mypage.index10');

/* My Page */
Route::get('dashboard', function ()             { return redirect('dashboard/index2'); });
Route::get('dashboard/index2',                  'DashboardController@index2')->name('dashboard.index2');
Route::get('dashboard/index3',                  'DashboardController@index3')->name('dashboard.index3');


/* App */
Route::get('contact', function ()               { return redirect('contact/contact'); });
Route::get('contact/contact',                   'ContactController@contact')->name('contact.contact');
Route::get('contact/contact2',                  'ContactController@contact2')->name('contact.contact2');

/* App */
Route::get('app', function ()                   { return redirect('app/contact'); });
Route::get('app/calendar',                      'AppController@calendar')->name('app.calendar');

/* email */
Route::get('email', function ()                 { return redirect('email/inbox'); });
Route::get('email/inbox',                       'EmailController@inbox')->name('email.inbox');
Route::get('email/compose',                     'EmailController@compose')->name('email.compose');
Route::get('email/inboxdetail',                 'EmailController@inboxdetail')->name('email.inboxdetail');

/* messenger */
Route::get('messenger', function ()             { return redirect('messenger/chat'); });
Route::get('messenger/chat',                    'MessengerController@chat')->name('messenger.chat');

/* Icons */
Route::get('icon', function ()                  { return redirect('icon/fontawesome'); });
Route::get('icon/fontawesome',                  'IconController@fontawesome')->name('icon.fontawesome');
Route::get('icon/iconsline',                    'IconController@iconsline')->name('icon.iconsline');
Route::get('icon/themify',                      'IconController@themify')->name('icon.themify');

/* Components  */
Route::get('components', function ()            { return redirect('components/bootstrap'); });
Route::get('components/bootstrap',              'ComponentsController@bootstrap')->name('components.bootstrap');
Route::get('components/typography',             'ComponentsController@typography')->name('components.typography');
Route::get('components/colors',                 'ComponentsController@colors')->name('components.colors');
Route::get('components/buttons',                'ComponentsController@buttons')->name('components.buttons');
Route::get('components/tabs',                   'ComponentsController@tabs')->name('components.tabs');
Route::get('components/progressbars',           'ComponentsController@progressbars')->name('components.progressbars');
Route::get('components/modals',                 'ComponentsController@modals')->name('components.modals');
Route::get('components/notifications',          'ComponentsController@notifications')->name('components.notifications');
Route::get('components/dialogs',                'ComponentsController@dialogs')->name('components.dialogs');
Route::get('components/listgroup',              'ComponentsController@listgroup')->name('components.listgroup');
Route::get('components/mediaobject',            'ComponentsController@mediaobject')->name('components.mediaobject');
Route::get('components/nestable',               'ComponentsController@nestable')->name('components.nestable');
Route::get('components/rangesliders',           'ComponentsController@rangesliders')->name('components.rangesliders');
Route::get('components/helperclass',            'ComponentsController@helperclass')->name('components.helperclass');

/* Forms  */
Route::get('forms', function ()                 { return redirect('forms/basic'); });
Route::get('forms/basic',                       'FormsController@basic')->name('forms.basic');
Route::get('forms/advanced',                    'FormsController@advanced')->name('forms.advanced');
Route::get('forms/validation',                  'FormsController@validation')->name('forms.validation');
Route::get('forms/wizard',                      'FormsController@wizard')->name('forms.wizard');
Route::get('forms/summernote',                  'FormsController@summernote')->name('forms.summernote');
Route::get('forms/dragdropupload',              'FormsController@dragdropupload')->name('forms.dragdropupload');
Route::get('forms/editors',                     'FormsController@editors')->name('forms.editors');
Route::get('forms/markdown',                    'FormsController@markdown')->name('forms.markdown');
Route::get('forms/cropping',                    'FormsController@cropping')->name('forms.cropping');
Route::get('forms/userdetails',                    'FormsController@userdetails')->name('forms.userdetails');


/* Table  */
Route::get('tables', function ()                { return redirect('tables/normal'); });
Route::get('tables/normal',                     'TablesController@normal')->name('tables.normal');
Route::get('tables/color',                      'TablesController@color')->name('tables.color');
Route::get('tables/datatable',                  'TablesController@datatable')->name('tables.datatable');
Route::get('tables/editable',                   'TablesController@editable')->name('tables.editable');
Route::get('tables/filter',                     'TablesController@filter')->name('tables.filter');
Route::get('tables/dragger',                    'TablesController@dragger')->name('tables.dragger');

/* Table  */
Route::get('charts', function ()                { return redirect('charts/c3'); });
Route::get('charts/c3',                         'ChartsController@c3')->name('charts.c3');
Route::get('charts/chartjs',                    'ChartsController@chartjs')->name('charts.chartjs');
Route::get('charts/morris',                     'ChartsController@morris')->name('charts.morris');
Route::get('charts/flot',                       'ChartsController@flot')->name('charts.flot');
Route::get('charts/sparkline',                  'ChartsController@sparkline')->name('charts.sparkline');
Route::get('charts/knob',                       'ChartsController@knob')->name('charts.knob');

/* Maps  */
Route::get('maps', function ()                  { return redirect('maps/jvectormap'); });
Route::get('maps/jvectormap',                   'MapsController@jvectormap')->name('maps.jvectormap');

/* Maps  */
Route::get('widget', function ()                { return redirect('widget/widgets'); });
Route::get('widget/widgets',                    'WidgetController@widgets')->name('widget.widgets');

/* Pages  */
Route::get('pages', function ()                 { return redirect('pages/blank'); });
Route::get('pages/blank',                       'PagesController@blank')->name('pages.blank');
Route::get('pages/profile',                     'PagesController@profile')->name('pages.profile');
Route::get('pages/userlist',                    'PagesController@userlist')->name('pages.userlist');
Route::get('pages/testimonials',                'PagesController@testimonials')->name('pages.testimonials');
Route::get('pages/invoices',                    'PagesController@invoices')->name('pages.invoices');
Route::get('pages/invoicedetails',              'PagesController@invoicedetails')->name('pages.invoicedetails');
Route::get('pages/timeline',                    'PagesController@timeline')->name('pages.timeline');
Route::get('pages/searchresults',               'PagesController@searchresults')->name('pages.searchresults');
Route::get('pages/gallery',                     'PagesController@gallery')->name('pages.gallery');
Route::get('pages/pricing',                     'PagesController@pricing')->name('pages.pricing');
Route::post('pages/updateprofileimg',                     'PagesController@updateprofileimg')->name('pages.updateprofileimg');

/* Extra  */
Route::get('extra', function ()                 { return redirect('extra/social'); });
Route::get('extra/social',                      'ExtraController@social')->name('extra.social');
Route::get('extra/news',                        'ExtraController@news')->name('extra.news');

Route::get('extra/settings',                    'ExtraController@settings')->name('extra.settings');

/* Admin   */
Route::get('admin', function ()              { return redirect('admin/ticketdetails'); });
Route::get('admin/ticket',                   'AdminController@ticket')->name('admin.ticket');
Route::get('admin/ticketdetails',            'AdminController@ticketdetails')->name('admin.ticketdetails');
Route::post('admin/add_msg',            'AdminController@add_msg')->name('admin.add_msg');
Route::post('admin/add_url',            'AdminController@add_url')->name('admin.add_url');
Route::get('admin/manageusers',                   'AdminController@manageusers')->name('admin.manageusers');
Route::get('admin/userfeedback',                   'AdminController@userfeedback')->name('admin.userfeedback');
Route::get('admin/change_userstatus',                   'AdminController@change_userstatus')->name('admin.change_userstatus');
Route::get('admin/change_usertype',                   'AdminController@change_usertype')->name('admin.change_usertype');
Route::get('admin/userprofile',                   'AdminController@userprofile')->name('admin.userprofile');
Route::get('admin/viewfeedback',                   'AdminController@viewfeedback')->name('admin.viewfeedback');
Route::get('admin/feedback',                   'AdminController@feedback')->name('admin.feedback');
Route::get('admin/get_question',                   'AdminController@get_question')->name('admin.get_question');
Route::get('admin/feedbk_status',                   'AdminController@feedbk_status')->name('admin.feedbk_status');
Route::post('admin/update_feedbkquest',                   'AdminController@update_feedbkquest')->name('admin.update_feedbkquest');
Route::get('admin/del_question',                   'AdminController@del_question')->name('admin.del_question');
Route::post('admin/add_question',                   'AdminController@add_question')->name('admin.add_question');
Route::get('admin/userpayment',                   'AdminController@userpayment')->name('admin.userpayment');
Route::get('admin/approvepayment',                   'AdminController@approvepayment')->name('admin.approvepayment');

/* Project   */
Route::get('projects', function ()              { return redirect('projects/taskboard'); });
Route::get('projects/taskboard',                'ProjectsController@taskboard')->name('projects.taskboard');
Route::get('projects/projectlist',              'ProjectsController@projectlist')->name('projects.projectlist');
Route::get('projects/addproject',               'ProjectsController@addproject')->name('projects.addproject');
Route::get('projects/ticket',                   'ProjectsController@ticket')->name('projects.ticket');
Route::get('projects/ticketdetails',            'ProjectsController@ticketdetails')->name('projects.ticketdetails');
Route::get('projects/clients',                  'ProjectsController@clients')->name('projects.clients');
Route::get('projects/todo',                     'ProjectsController@todo')->name('projects.todo');

/* HR   */
Route::get('hr', function ()                    { return redirect('hr/hrdashboard'); });
Route::get('hr/hrdashboard',                    'HrController@hrdashboard')->name('hr.hrdashboard');
Route::get('hr/users',                          'HrController@users')->name('hr.users');
Route::get('hr/departments',                    'HrController@departments')->name('hr.departments');
Route::get('hr/employee',                       'HrController@employee')->name('hr.employee');
Route::get('hr/activities',                     'HrController@activities')->name('hr.activities');
Route::get('hr/holidays',                       'HrController@holidays')->name('hr.holidays');
Route::get('hr/events',                         'HrController@events')->name('hr.events');
Route::get('hr/payroll',                        'HrController@payroll')->name('hr.payroll');
Route::get('hr/accounts',                       'HrController@accounts')->name('hr.accounts');
Route::get('hr/report',                         'HrController@report')->name('hr.report');

/* Card   */
Route::get('cardlayout', function ()            { return redirect('cardlayout/cards'); });
Route::get('cardlayout/cards',                  'CardlayoutController@cards')->name('cardlayout.cards');


/* Card   */
Route::get('job', function ()                   { return redirect('job/jobdashboard'); });
Route::get('job/jobdashboard',                  'JobController@jobdashboard')->name('job.jobdashboard');
Route::get('job/positions',                     'JobController@positions')->name('job.positions');
Route::get('job/applicants',                    'JobController@applicants')->name('job.applicants');
Route::get('job/resumes',                       'JobController@resumes')->name('job.resumes');
Route::get('job/jobsettings',                   'JobController@jobsettings')->name('job.jobsettings');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
