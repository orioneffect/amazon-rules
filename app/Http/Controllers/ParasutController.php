<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parasut_integration;
use DB;
use Auth;
use App\User;


class ParasutController extends Controller
{
    function invoiceintegration()
    {
        $table = 'parasut_integrations';
        $list = DB::table($table)
        ->where($table.'.userid', '=', Auth::user()->id)->get();
        
        return view('settings.invoiceintegration',compact('list'));
    }

    public function parasut_infos(Request $request)
    {

            date_default_timezone_set('Europe/Istanbul');
         Mail_account::create([
            'userid' => Auth::user()->id,
            'client_id' => $request->parclientid,
            'client_secret' => $request->parclientsecret,
            'callback_urls'=> $request->parcallbackurls,
            'parusername'=> $request->parusername,
            'paruserpwd'=> $request->parusername,
            'invoicetype'=> $request->acctype,
            'pardescription'=> $request->parainvdesc,
            'status'=> 0
        ]);

        return redirect('settings/mailaccounts');
    }

   
}
