<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\Models\Ticket_detail;
use App\Models\Notification;
use App\Models\Ticket_activity;
use App\Models\Store;
use App\Models\Fbk_user_answer;
use App\Models\Fbk_user_master;
use App\Models\Fbk_ans_master;
use App\Models\Fbk_quest_master;
use App\Models\Invoice_payment;
use App\Models\Invoice_wallet;
use App\User;
use App\Models\User_type;
use Illuminate\Support\Facades\Storage;
use App\Models\Helpdesk_config;
use App\Models\CommonFunction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController
{
    function ticket(Request $request){
        $table='ticket_details';
        $ticketlist=DB::table($table)
        ->join('helpdesk_configs',$table.'.priority', '=', 'helpdesk_configs.typeid')
        ->join('users',$table.'.user_id', '=', 'users.id')
        ->select($table.'.*', 'helpdesk_configs.visible', 'users.name');

        if($request->search_id!='') {
            $id_val = substr($request->search_id, 0, 2);
            if($id_val=='or' || $id_val=='OR') {
                $id = substr($request->search_id, 2);
                $ticketlist=$ticketlist->where ($table.'.id', 'LIKE', '%' . $id .'%');
            } else {
                $ticketlist=$ticketlist->where ($table.'.id', 'LIKE', '%' . $request->search_id .'%');
            }
        }
        if($request->search_priority!='') {
            $ticketlist=$ticketlist->where($table.'.priority', '=', $request->search_priority);
        };

        if($request->search_from!='' && $request->search_to!='') {

            $search_from = Carbon::parse($request->search_from)
            ->toDateTimeString();

            $search_to = Carbon::parse($request->search_to)
            ->toDateTimeString();

            $ticketlist=$ticketlist->whereBetween($table.'.created_at',[$search_from,$search_to]);
        };
        if($request->search_status!='') {
            $ticketlist=$ticketlist->where($table.'.status', '=', $request->search_status);
        };
        $ticketlist=$ticketlist->orderBy($table.'.id', 'desc')->get();

        $ticketresponded = Ticket_detail::where('status', '=', 'a2')->get();
        $ticketresolve = Ticket_detail::where('status', '=', 'a3')->get();
        $ticketpending = Ticket_detail::where('status', '=', 'a1')->get();

        $table='ticket_details';
        $ticketfull=DB::table($table)->get();

        $statusNav = Helpdesk_config::where('type', '=', 'status')->get();
        $priorityNav = Helpdesk_config::where('type', '=', 'priority')->get();
        
        $no_of_ticket = $ticketfull->count();
        $no_of_responded = $ticketresponded->count();
        $no_of_resolve = $ticketresolve->count();
        $no_of_pending = $ticketpending->count();

        $data =  array();
        $data['ticketlist']  =  $ticketlist;
        $data['no_of_ticket']  =  $no_of_ticket;
        $data['no_of_responded']  =  $no_of_responded;
        $data['no_of_resolve']  =  $no_of_resolve;
        $data['no_of_pending']  =  $no_of_pending;
        $data['priorityNav']  =  $priorityNav;
        $data['statusNav']  =  $statusNav;


        return view('admin.ticket',compact('data'));
    }
    function ticketdetails(Request $request) {
        $t_id = $request->t_id;

        $table='ticket_details';
        $ticketdetails=DB::table($table)
        ->join('helpdesk_configs',$table.'.status', '=', 'helpdesk_configs.typeid')
        ->join('users',$table.'.user_id', '=', 'users.id')
        ->where('ticket_details.id', '=', $t_id)
        ->select($table.'.*', 'helpdesk_configs.visible', 'users.name', 'users.phone', 'users.email', 'users.logo')
        ->first();

        $table = 'ticket_activities';
        $ticketactivity = DB::table($table)
        ->join('helpdesk_configs',$table.'.message_by', '=', 'helpdesk_configs.typeid')
        ->where('ticket_activities.ticket_id', '=', $t_id)
        ->select($table.'.*', 'helpdesk_configs.visible')
        ->orderBy('ticket_activities.id', 'desc')
        ->get();

        $statusNav = Helpdesk_config::where('type', '=', 'status')->get();

        $data =  array();
        $data['ticketdetails']  =  $ticketdetails;
        $data['ticketactivity']  =  $ticketactivity;
        $data['statusNav']  =  $statusNav;

        return view('admin.ticketdetails',compact('data'));
    }    

    function add_msg(Request $request) {
        Ticket_detail::where('id', '=', $request->ticket_id,)
        ->update(['status' => $request->setstatus]);
        

        Ticket_activity::create([
        'ticket_id' => $request->ticket_id,
        'user_id' => $request->user_id,
        'message' => $request->msg,
        'message_by' => 'b2',//for admin pass - b2
        ]);
        return redirect()->back();

    }

    function add_url(Request $request) {
        if(isset($request->file)) {
            $request->validate([
                'file' => 'required|mimes:PNG,png,tiff,jpg,jpeg'
            ]);
            $fileName = $request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');
        } else {
            $filePath="";
        }


        Ticket_activity::create([
        'ticket_id' => $request->ticket_id,
        'user_id' => $request->user_id,
        'message' => 'Details in File',
        'message_by' => 'b2',//for admin pass - b2
        'url' => $filePath
        ]);
        return redirect()->back();


        /*
        Notification::create([
            'user_id' => Auth::user()->id,
            'type_id' => 1,
            'head' => 'Ticket',
            'message' => 'We received your Request',
            'sent_by' => 1,
            'is_read' => 0,
            'status' => 0
            ]);

        */
        //return redirect('admin/ticketdetails')->with("success","success");
    }    
    function manageusers() {
        $user = User::select('*')
        ->selectRaw('(select count(1) from stores where stores.user_id=users.id) as store_count')
        ->selectRaw('(select country_name from countries where countries.id=users.country) as country')
        ->orderBy('id', 'desc')->get();

        $type = User_type::orderBy('id', 'desc')->get();
        
        $user_c = User::orderBy('id', 'desc')->get();
        $user_count = $user_c->count();

        $userActive_c = User::orderBy('id', 'desc')
        ->where('is_active', '=', 1)
        ->get();
        $userActive_count = $userActive_c->count();

        $store = Store::orderBy('id', 'desc')->get();
        $store_count = $store->count();

        $data =  array();
        $data['user']  =  $user;
        $data['type']  =  $type;
        $data['user_count']  =  $user_count;
        $data['store_count']  =  $store_count;
        $data['userActive_count']  =  $userActive_count;
        return view('admin.manageusers',compact('data'));
    }    

    function userfeedback() {
        $user = DB::table('fbk_user_masters')
        ->join('users','fbk_user_masters.user_id', '=', 'users.id')
        ->select('users.*', 'fbk_user_masters.*')
        ->selectRaw('(select country_name from countries where countries.id=users.country) as country')
        ->orderBy('fbk_user_masters.id', 'desc')->get();

        $type = User_type::orderBy('id', 'desc')->get();
        
        $user_c = User::orderBy('id', 'desc')->get();
        $user_count = $user_c->count();

        $userActive_c = User::orderBy('id', 'desc')
        ->where('is_active', '=', 1)
        ->get();
        
        $feedbk_count = Fbk_user_master::where('status', '=', 0)->count();
        $feedbk_satisfied = Fbk_user_master::where('satisfied', '=', 1)->count();
        $feedbk_unsatisfied = Fbk_user_master::where('satisfied', '=', 0)->count();
        $feedbk_questcount = Fbk_quest_master::where('status', '=', 0)->count();
        $feedbk_questcount = $feedbk_questcount + 1;

        $userActive_count = $userActive_c->count();


        $store = Store::orderBy('id', 'desc')->get();
        $store_count = $store->count();



        $data =  array();
        $data['user']  =  $user;
        $data['type']  =  $type;
        $data['user_count']  =  $user_count;
        $data['store_count']  =  $store_count;
        $data['feedbk_satisfied']  =  $feedbk_satisfied;
        $data['feedbk_unsatisfied']  =  $feedbk_unsatisfied;
        $data['feedbk_count']  =  $feedbk_count;
        $data['feedbk_questcount']  =  $feedbk_questcount;
        $data['userActive_count']  =  $userActive_count;
        return view('admin.userfeedback',compact('data'));
    }        
    function change_userstatus(Request $request) {
        User::where('id', '=', $request->id)
        ->update(['is_active' => $request->val]);
        return redirect('admin/manageusers')->with("success","success");
    }

    function change_usertype(Request $request) {
        User::where('id', '=', $request->id)
        ->update(['type' => $request->val]);
        return redirect('admin/manageusers')->with("success","success");
    }

    function userprofile(Request $request) {
        $user = User::where('id', '=', $request->id)->first();

        $country = User::where('id', '=', $request->id)
        ->selectRaw('(select country_name from countries where countries.id=users.country) as country')
        ->first();

        $stores = User::where('id', '=', $request->id)
        ->selectRaw('(select count(1) from stores where stores.user_id=users.id) as store_count')
        ->first();

        $store_name = User::where('id', '=', $request->id)
        ->selectRaw('(select store_name from stores where stores.id=users.default_store_id) as default_store')
        ->first();

        $lan_name = User::where('id', '=', $request->id)
        ->selectRaw('(select language from languages where languages.id=users.default_language_id) as default_lan')
        ->first();

        $ship_name = User::where('id', '=', $request->id)
        ->selectRaw('(select company_name from shipping_companies where shipping_companies.id=users.shipping) as default_ship')
        ->first();

        $default_country_name = User::where('id', '=', $request->id)
        ->selectRaw('(select country_name from countries where countries.id=users.default_country_id) as default_country')
        ->first();

        $state_name = User::where('id', '=', $request->id)
        ->selectRaw('(select state_name from states where states.id=users.state) as state')
        ->first();

        $data =  array();
        $data['user']  =  $user;
        $data['country']  =  $country;
        $data['stores']  =  $stores;
        $data['store_name']  =  $store_name;
        $data['lan_name']  =  $lan_name;
        $data['ship_name']  =  $ship_name;
        $data['default_country_name']  =  $default_country_name;
        $data['state_name']  =  $state_name;

        return view('admin.userprofile',compact('data'));
    }

    function viewfeedback(Request $request) {
        $fbk = DB::table('fbk_user_masters')
        ->join('users','fbk_user_masters.user_id', '=', 'users.id')
        ->join('fbk_user_answers','fbk_user_masters.id', '=', 'fbk_user_answers.feedbk_id')
        ->join('fbk_quest_masters','fbk_user_answers.qid', '=', 'fbk_quest_masters.id')
        ->leftjoin('fbk_ans_masters','fbk_user_answers.user_ans', '=', 'fbk_ans_masters.id')
        ->select('fbk_user_masters.*','fbk_user_answers.*', 'fbk_quest_masters.*', 'fbk_ans_masters.*','users.name','users.country','users.profileimg')
        ->where('fbk_user_masters.id', '=', $request->id)
        ->get();
        $user = DB::table('users')
        ->join('fbk_user_masters','users.id', '=', 'fbk_user_masters.user_id')
        ->where('fbk_user_masters.id', '=', $request->id)
        ->first();
        return view('admin.viewfeedback',compact('fbk'),compact('user'));
    }
    function feedback() {
        $feedbk = DB::table('fbk_quest_masters')
        ->select('fbk_quest_masters.*')
//        ->where('fbk_quest_masters.status', '=', 0)
        ->get();

        $data =  array();
        $data['feedbk']  =  $feedbk;
        return view('admin.feedback',compact('data'));
    }
    function get_question(Request $request)
    {
        $feedbackq = DB::table('fbk_quest_masters')
            ->where('id', '=', $request->id)
            ->first();
        return $feedbackq;
    }
    function feedbk_status(Request $request) {
        Fbk_quest_master::where('id', '=', $request->id)
        ->update(['status' => $request->val]);
        return redirect('admin/feedback')->with("success","success");
    }
    function del_question(Request $request) {
        Fbk_quest_master::where('id', '=', $request->id)
        ->delete();
        return redirect('admin/feedback')->with("success","success");
    }
    function update_feedbkquest(Request $request)
    {
        $result = Fbk_quest_master::where('id', '=', $request->u_id)
            ->update([
                'quest' => $request->quest
            ]);
        return redirect('admin/feedback')->with("message_updt", ' ');
    }
    function add_question(Request $request) {
        $quest = Fbk_quest_master::create([
        'quest' => $request->quest,
        'type' => $request->qtype,
        ])->id;
        $quest;
        if ($request->qtype == 'OPTION') {
            for ($i = 1; $i<=6; $i++) {
                $ans_no = 'ans'.$i;
                if ($request->$ans_no != '') {
                    Fbk_ans_master::create([
                        'qid' => $quest,
                        'answers' => $request->$ans_no,
                        ]);                
                }
            }            
        }
    return redirect('admin/feedback')->with("message", ' ');

    }

    function userpayment(Request $request) {
        $usermail = $request->search_mail;
        if ($request->duration != '') {
            $duration = $request->duration;
        } else {
            $duration = 0;
        }

        $paypend = DB::table('invoice_payments')
            ->join('payoneers','invoice_payments.from_account_name', '=', 'payoneers.id')
            ->join('users','invoice_payments.user_id', '=', 'users.id')
            ->select('invoice_payments.*','payoneers.payoneer','users.email')
            ->where('invoice_payments.status', '=', 0)
            ->orderBy('invoice_payments.id', 'desc');

            if($usermail!='') {
                $paypend = $paypend->where('users.email', 'LIKE', '%' . $usermail .'%');
            };
    
            $paypend = $paypend->whereDate('invoice_payments.created_at', '>=', now()->subDays($duration)->setTime(0, 0, 0)->toDateTimeString());
            
            $paypend = $paypend->get();

            
        $payhist = DB::table('invoice_payments')
            ->join('payoneers','invoice_payments.from_account_name', '=', 'payoneers.id')
            ->join('users','invoice_payments.user_id', '=', 'users.id')
            ->select('invoice_payments.*','payoneers.payoneer','users.email')
            ->where('invoice_payments.status', '=', 1)
            ->orderBy('invoice_payments.id', 'desc');

            if($usermail!='') {
                $payhist = $payhist->where('users.email', 'LIKE', '%' . $usermail .'%');
            };
    
            $payhist = $payhist->whereDate('invoice_payments.created_at', '>=', now()->subDays($duration)->setTime(0, 0, 0)->toDateTimeString());
            
            $payhist = $payhist->get();

        $data =  array();
        $data['paypend']  =  $paypend;
        $data['payhist']  =  $payhist;
        $data['usermail']  =  $usermail;
        $data['duration']  =  $duration;

        return view('admin.userpayment',compact('data'));
    }        

    function approvepayment(Request $request) {
        if ($request->val == 1) {
            Invoice_payment::where('id', '=', $request->id)
            ->update(['status' => 1]);

            $paytype = Invoice_payment::where('id', '=', $request->id)->first();
            if ($paytype->payment_type==3) {
                Invoice_wallet::where('trans_id', '=', $request->id)
                ->update(['paymt_status' => 0]);    
            }

            return 1;
        }
        if ($request->val == 0) {
            Invoice_payment::where('id', '=', $request->id)
            ->update(['status' => 0]);

            $paytype = Invoice_payment::where('id', '=', $request->id)->first();
            if ($paytype->payment_type==3) {
                Invoice_wallet::where('trans_id', '=', $request->id)
                ->update(['paymt_status' => 1]);
            }

            return 1;
        }

    }        

}