<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Dropshipping_package;
use App\Models\Country;
use App\Models\Discount;
use App\Models\Transaction_detail;
use App\Models\State;
use App\Models\User_cc;
use App\Models\Invoice_payment;
use App\Models\Invoice_credit;
use App\Models\Invoice_wallet;
use App\Models\Invoice;
use App\Rules\ExpDateValidate;
use App\Rules\CcValidate;
use App\Models\Exchange_rate;
use Carbon\Carbon;
use App\User;
use Auth;
use Session;
use Cache;
use Crypt;
use DB;
use Redirect;
/*
use Iyzico\IyzipayLaravel\Exceptions\Transaction\TransactionSaveException;
use Iyzico\IyzipayLaravel\Exceptions\Card\CardSaveException;
use Iyzico\IyzipayLaravel\IyzipayLaravel;
use Iyzico\IyzipayLaravel\StorableClasses\Address;
use Iyzico\IyzipayLaravel\StorableClasses\BillFields;
use Iyzico\IyzipayLaravel\IyzipayLaravelServiceProvider;
*/
class SubscriptionController extends Controller
{
    protected $maxtry = 0;

    function index(Request $request) {
        if ($request->upgrade=='') {
            $packages =  Dropshipping_package::where('status',0)->orderBy('package_amount')->get();
        } else {
            $packages =  Dropshipping_package::where('status',0)
            ->where('package_amount','>', $request->upgrade)
            ->orderBy('package_amount')->get();
        }
        return view('subscription.addnew',compact('packages'));
    }


    function applydiscount(Request $request) {

        $coupon =  Discount::where('status',0)->where('coupon_code',$request->discount)->where('expiry_date',">=",now())->first();
        if(isset($coupon)){
           $userCount =  User::where('coupon_code',$coupon->id)->get();
           if(isset($userCount) && $userCount->count()<$coupon->max_usage) {
               return '{"status":"success","value":"'.$coupon->value.'","type":"'.$coupon->type.'","id":"'.$coupon->id.'"}';
           } else {
                return '{"status":"failed","message":"Expired"}';
           }
        } else {
            return '{"status":"failed","message":"Invalid"}';
        }
    }


    function process($package) {
        $packages=Dropshipping_package::findOrFail($package);
        //$user=Auth::user();
        //$countries =  Country::all('country_name','id');

        $data =  array();
        $data['packages']  =  $packages;
        $data['user']  =  Auth::user();
        $data['countries']     =  Country::all('country_name','id');

        return view('subscription.process',compact('data'));
    }


    function confirm(Request $request) {

        $request->validate([
            'email' => 'required|string|email',
            'name' => 'required|string',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'phone' => 'required|string',
            'address1' => 'required|string',
            'country' => 'required',
            'state' => 'required',
            //'cc' => ['required',new CcValidate],
            //'exp_mon' => ['required',new ExpDateValidate],
            //'cvv' => 'required|integer',
        ]);
        $user = User::findOrFail(Auth::user()->id);
        $user->name = $request->name;
        $user->first_name = $request->firstname;
        $user->last_name = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address1 = $request->address1;
        $user->address2 = $request->address2;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->pincode = $request->pincode;
        $user->coupon_code = $request->couponid;
        $user->save();

        $curr_value = Exchange_rate::where('curr_code', '=', 'TRY')
        ->orderBy('created_at', 'desc')
        ->first();
        //echo $request->tobepaid . "<br>";
        //echo $curr_value->curr_value;
        //exit;

        $tranid = Transaction_detail::create([
            'user_id' => Auth::user()->id,
            'plan_id' => $request->packageid,
            'discount_code' => $request->couponid,
            'discount_amt' => $request->discountamt,
            'price_u' => $request->tobepaid,
            'price_t' => round($request->tobepaid * $curr_value->curr_value, 2),
            'final_price_u' => round($request->tobepaid, 2),
            'final_price_t' => round($request->tobepaid * $curr_value->curr_value, 2)
            ])->id;

            Cache::flush();
            Session::put('transid', $tranid);

/*
        $countryName=Country::findOrFail($request->country)->country_name;
        $stateName=State::findOrFail($request->state)->state_name;

        $packageid = $request->packageid;

        $user_cc = User_cc::where('user_id', $user->id)->where('package_id', $packageid)->first();

        if(isset($user_cc)) {
            $idnumber = $user_cc->id;
            $user_cc->cc = Crypt::encryptString(str_replace(" ","",$request->cc));
            $user_cc->exp_mon = Crypt::encryptString($request->exp_mon);
            $user_cc->cvv = Crypt::encryptString($request->cvv);
            $user->save();
        } else {
            $idnumber = User_cc::Create([
                'user_id' => $user->id,
                'package_id' => $packageid,
                'cc' => Crypt::encryptString(str_replace(" ","",$request->cc)),
                'exp_mon' => Crypt::encryptString($request->exp_mon),
                'cvv' => Crypt::encryptString($request->cvv),

            ])->id;
        }
        $user->bill_fields = new BillFields([
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'email' => $request->email,
            'shipping_address' => new Address([
                'city' => $stateName,
                'country' => $countryName,
                'address' => ($request->address1 . " " . $request->address2)
            ]),
            'billing_address' => new Address([
                'city' => $stateName,
                'country' => $countryName,
                'address' => ($request->address1 . " " . $request->address2)
            ]),
            'identity_number' => $idnumber,
            'mobile_number' => $request->phone
        ]);

        $monarr=explode("-", $request->exp_mon);
        $ccyear=$monarr[0];
        $ccmonth=$monarr[1];

        $package = Dropshipping_package::where('id',$packageid);
        $this->createPlans($package->id, $package->package_name,$package->package_amount);
        try {
            $user->addCreditCard(array(
                'alias' => $request->name,
                'holder' => $request->name,
                'number' => str_replace(" ","",$request->cc),
                'month' => $ccmonth,
                'year' => $ccyear)
            );
        } catch (CardSaveException $e) {
            return redirect()->back()->with('error', __('Subscription.invalidCreditCard'));
            exit;
        }


        try {
            $plan = IyzipayLaravel::monthlyPlans()->first();
            $user->subscribe($plan);
        }
        catch (TransactionSaveException $e) {
            if (str_contains('System error', $e->getMessage())) { // Its weird but we face this error sometimes.
                $user->subscribe($plan);
            }
        }

        $status=false;
        $errormsg="";

        for($i=1; $i < 10; $i++) {
            try {
                $user->paySubscription();
                //echo $user->transactions->count()."<br>";
                //echo $user->subscriptions->first()->transactions->count()."<br>";
                //echo $user->subscriptions->first()->next_charge_at."<br>";
                $status=true;
                break;
            } catch (TransactionSaveException $e) {
                $errormsg=$e->getMessage();
                if (str_contains('System error', $e->getMessage())) { // Its weird but we face this error sometimes.
                    continue;
                }
            }
        }*/

        //if($status) {

            //$user = User::findOrFail(Auth::user()->id);
            //$user->is_active = 1;
            //$user->save();
            Cache::flush();
            Session::put('package', $request->packageid);

            return redirect('subscription/postprocess');
            //return redirect('integration/addstore');
        //} else {
          //   return redirect()->back()->withErrors($errormsg);
        //}
    }


    /*protected function createPlans($id,$name,$price): void
    {
        IyzipayLaravel::plan($id, $name)->trialDays(0)->price($price)->currency('USD');

    }*/

    function postprocess(Request $request) {

        $packages=Dropshipping_package::findOrFail(Session::get('package'));
        //$user=Auth::user();
        //$countries =  Country::all('country_name','id');
        $total = Transaction_detail::where('user_id','=', Auth::user()->id)
        ->where('id', Session::get('transid'))->first();
        $curryear = now()->format('Y');
        $currmonth = now()->format('m');

        $data =  array();
        $data['total']  =  $total;
        $data['type']  =  "1";
        $data['packages']  =  $packages;
        $data['user']  =  Auth::user();
        $data['countries'] = Country::all('country_name','id');
        $data['curryear']  =  $curryear;
        $data['currmonth']  =  $currmonth;

        return view('subscription.postprocess',compact('data'));

    }

    function postprocess1(Request $request) {

        Cache::flush();
        Session::put('pgamount', $request->amount);

        Cache::flush();
        Session::put('pginvoices', $request->paycc_id);

        $curr_value = Exchange_rate::where('curr_code', '=', 'TRY')
        ->orderBy('created_at', 'desc')
        ->first();
        //echo $request->tobepaid . "<br>";
        //echo $curr_value->curr_value;
        //exit;

        $total = Transaction_detail::create([
            'user_id' => Auth::user()->id,
            'plan_id' => 0,
            'discount_code' => 0,
            'discount_amt' => 0,
            'price_u' => $request->amount,
            'price_t' => round($request->amount * $curr_value->curr_value, 2),
            'final_price_u' => round($request->amount, 2),
            'final_price_t' => round($request->amount * $curr_value->curr_value, 2)
            ]);

            //Cache::flush();
            Session::put('transid', $total->id);


        //$user=Auth::user();
        //$countries =  Country::all('country_name','id');
        $curryear = now()->format('Y');
        $currmonth = now()->format('m');

        $data =  array();

        $data["package_name"]="Charges";
        $data["package_amount"]=$request->amount;
        $data['total']  =  $total;
        if ($request->paycc_id==0) {
            $data['type']  =  "3";
        } else {
            $data['type']  =  "2";
        }
        $data['user']  =  Auth::user();
        $data['countries'] = Country::all('country_name','id');
        $data['curryear']  =  $curryear;
        $data['currmonth']  =  $currmonth;


        return view('subscription.postprocess',compact('data'));
    }

    function tokenprocess(Request $request) {

        $total = Transaction_detail::where('user_id','=', Auth::user()->id)
        ->where('id', Session::get('transid'))->first();
        

        $merchant_id = env('MERCHANT_ID');
        $merchant_key = env('MERCHANT_KEY');
        $merchant_salt = env('MERCHANT_SALT');


        if( isset( $_SERVER["HTTP_CLIENT_IP"] ) ) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        $user_ip            = $ip;
        $merchant_oid       = rand();
        $email              = Auth::user()->email;
        $payment_type       = "card";        // card or card_points

        if ($request->opt == '1') {//intallment
            if ($request->val!='0') {
                $instatables = Session::get('instatables');
                foreach($instatables as $x => $x_value) {
                    if ($x == $request->val) {
                        $interest = $x_value;
                    };
                }

                //echo Session::get('transid');
                //exit;
                $paymentamount     = $total->final_price_t * ($interest/100);

                $in_interest          = round($paymentamount,2);
                $payment_amount     = round(($paymentamount + $total->final_price_t),2);
            } else {
                $payment_amount     = round($total->final_price_t,2);
                $interest = 0;
                $in_interest          = 0;
            }
        } else if ($request->opt == '2') {//pay in usd/TL No installment
            $interest = 0;
            $in_interest = 0;
            $installment_count = 0;            
            if ($request->val == 'TL') { 
                $payment_amount = round($total->final_price_t,2);
            } else {
                $payment_amount = round($total->final_price_u,2);
            }

        }
        
        
        if ($request->opt == '1') { $currency = "TL"; }//intallment
        else if ($request->opt == '2') { 
            if ($request->val == 'TL') { 
                $currency = "TL"; 
            } else {
                $currency = "USD"; 
            }
        }
        //TL, EUR, USD, GBP, RUB (TL is assumed if not sent)


        $test_mode          = "0";
        $non_3d             = "0";
        
        if ($request->opt == '1') { 
            $installment_count  = str_replace('taksit_','',$request->val);           // 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12            
        }//intallment
               

        $hash_str = $merchant_id . $user_ip . $merchant_oid . $email . $payment_amount . $payment_type . $installment_count. $currency. $test_mode. $non_3d;
        $token = base64_encode(hash_hmac('sha256',$hash_str.$merchant_salt,$merchant_key,true));
        return response()->json('{"token":"'.$token.'","merchant_oid":"'.$merchant_oid.'","installment_count":"'.$installment_count.'","in_interest":"'.$in_interest.'","interest_rate":"'.$interest.'","payment_amount":"'.$payment_amount.'","currency":"'.$currency.'"}');

    }

    function success_payment() {

        $transid = Session::get('transid');

        $transuser = DB::table('transaction_details')
        ->where('transaction_details.id','=',$transid)
        ->join('dropshipping_packages', 'transaction_details.plan_id', '=', 'dropshipping_packages.id')
        ->select('transaction_details.*', 'dropshipping_packages.package_name', 'dropshipping_packages.id as plan_id')
        ->first();

        Invoice_payment::create([
            'user_id' => Auth::user()->id,
            'payment_type' => "0",
            'cc_type' => $transuser->cc_type,
            'from_account_name' => '',
            'to_account_name' => '',
            'amount' => $transuser->final_price_u,
            'description' => '',
            'status' => 1
        ]);

        $usertype = Auth::user()->is_active;

        Transaction_detail::where('id', '=', $transid)
        ->update(['status' => 1]);
        
        $current = Carbon::now();

        $user = User::findOrFail($transuser->user_id);
        $user->is_active = 1;
        $user->plan_id = $transuser->plan_id;
        $user->plan_fromdate = Carbon::now();
        $user->plan_todate = $current->addMonth(1);
        $user->plan_freeze = 1;
        $user->plan_freezedt = null;
        $user->save();

        $data =  array();
        $data['transuser'] = $transuser;
        $data['user'] = $user;
        $data['usertype'] = $usertype;

        //return redirect('integration/addstore');
        return view('subscription.success_payment', compact('data'));
    }
    function failure_payment() {
        $transid = Session::get('transid');

        $transuser = Transaction_detail::where('id','=',$transid)
        ->select('transaction_details.*')
        ->first();

        Transaction_detail::where('id', '=', $transid)
        ->update(['status' => 2]);

        $user = User::findOrFail($transuser->user_id);
        $user->is_active = 0;
        $user->save();

        $data =  array();
        $data['transuser'] = $transuser;
        $data['user'] = $user;
        $data['package'] = Session::get('package');
        //return redirect('subscription/process');
        return view('subscription.failure_payment', compact('data'));
    }

    function getbin(Request $request) {
        $merchant_id 	= env('MERCHANT_ID');
        $merchant_key 	= env('MERCHANT_KEY');
        $merchant_salt	= env('MERCHANT_SALT');
        $bin_number	= $request->bin_number;
        $hash_str = $bin_number . $merchant_id . $merchant_salt;
        $paytr_token=base64_encode(hash_hmac('sha256', $hash_str, $merchant_key, true));
        $post_vals=array(
            'merchant_id'=>$merchant_id,
            'bin_number'=>$bin_number,
            'paytr_token'=>$paytr_token
        );
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.paytr.com/odeme/api/bin-detail");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1) ;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $result = @curl_exec($ch);

        if(curl_errno($ch))
            die("PAYTR BIN detail request timeout. err:".curl_error($ch));

        curl_close($ch);

        $result=json_decode($result,1);

        if($result['status']=='error')
            die("PAYTR BIN detail request error. Error:".$result['err_msg']);
        elseif($result['status']=='failed')
            die("BIN is not defined. (For example, a foreign card)");
        else
            //print_r($result);
        return $result;

    }

    public function getinstallment(Request $request) {
        $merchant_id 	= env('MERCHANT_ID');
        $merchant_key 	= env('MERCHANT_KEY');
        $merchant_salt	= env('MERCHANT_SALT');
        $request_id=time();

        $paytr_token=base64_encode(hash_hmac('sha256',$merchant_id.$request_id.$merchant_salt,$merchant_key,true));

        $post_vals=array(
            'merchant_id'=>$merchant_id,
            'request_id'=>$request_id,
            'paytr_token'=>$paytr_token
        );

        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.paytr.com/odeme/taksit-oranlari");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1) ;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);

        // XXX: ATTENTION: if you get "SSL certificate problem: unable to get local issuer certificate" on your local machine,
        // you can open the code below and try it. BUT, it is very important that this code remains off on your server (in your real environment) for security reasons!
        // curl_setopt ($ ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = @curl_exec($ch);

        if(curl_errno($ch))
        {
            echo curl_error($ch);
            curl_close($ch);
            exit;
        }

        curl_close($ch);
        $result=json_decode($result,1);

        if($result['status']=='success')
        {
            if($result['oranlar'][$request->cname]!='') {
                //print_r($result['oranlar'][$request->cname]);
                    $strOutput = '';
                    $instatables = $result['oranlar'][$request->cname];

                    Cache::flush();
                    Session::put('instatables', $instatables);
                    foreach($result['oranlar'][$request->cname] as $x => $x_value) {
                        $inst = str_replace('taksit_','',$x);
                        $strOutput .= '<option value="'.$x.'">'.$inst.'</option>';
                    }
                    //$strOutput .='';

                    return $strOutput;
            }
        }
        else
        {
            echo $result['err_msg'];
        }
    }

    function upfinalamount(Request $request) {
        Session::put('cctype', $request->cc_type);
        $updatetrans = Transaction_detail::where('id','=', $request->tid)
            ->update([
                'final_price_t' => $request->finalpricet,
                'final_price_u' => $request->finalpriceu,
                'cc_type' => $request->cc_type
            ]);

        return $updatetrans;
    }

    function addto_invoice_payments(Request $request) {
        
        $resultid = Invoice_payment::create([
             'user_id' => Auth::user()->id,
             'payment_type' => "0",
             'cc_type' => Session::get('cctype'),
             'from_account_name' => '',
             'to_account_name' => '',
             'amount' => Session::get('pgamount'),
             'description' => '',
             'status' => 1
         ])->id;

         $result = Invoice::whereIn('id', explode(",", Session::get('pginvoices')))
         ->where('user_id', '=', Auth::user()->id)
         ->update([
             'status' => '1',
             'paid_dt' => Carbon::now(),
             'ref_no' => $resultid,
         ]);

         /*$credit_bal = DB::table('invoice_credits')
             ->where('user_id', '=', Auth::user()->id)
             ->select('credit_amt')
             ->first();

         $new_credit_bal = $credit_bal->credit_amt + Session::get('pgamount');

         Invoice_credit::where('user_id', '=', Auth::user()->id)->update(['credit_amt' => $new_credit_bal]);*/

         return redirect('subscription/success_payment1');
     }
     
     function success_payment1(Request $request) {
        return view('subscription.success_payment1');
     }

     function addto_wallet_payments(Request $request) {

        $resultid = Invoice_payment::create([
            'user_id' => Auth::user()->id,
            'payment_type' => "3",
            'cc_type' => Session::get('cctype'),
            'from_account_name' => '',
            'to_account_name' => '',
            'amount' => Session::get('pgamount'),
            'description' => '',
            'status' => 1,
            'trans_id' => Session::get('transid')
        ])->id;


        $result = Invoice_wallet::create([
             'user_id' => Auth::user()->id,
             'wallet_amt' => Session::get('pgamount'),
             'paymt_status' => 0,
             'paymt_type' => 0,
             'trans_id' => $resultid
         ]);
 
         return redirect('subscription/success_payment2');
     }

     function success_payment2(Request $request) {
        return view('subscription.success_payment2');
     }

}
