<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use App\Models\CommonFunction;
use Auth;
use App\User;
use App\Models\Shipping_companie;
use App\Models\User_config;
use App\Models\User_main_config;
use App\Models\User_product;
use App\Models\Pricing_detail;
use App\Models\Order_detail;
use App\Models\Store;
use App\Models\User_cc;
use App\Models\Credit;
use App\Models\Credit_type;
use App\Models\Payoneer;
use App\Models\Invoice;
use App\Models\Invoice_credit;
use App\Models\Invoice_payment;
use App\Models\Invoice_wallet;
use App\Models\Dropshipping_package;
use App\Rules\ExpDateValidate;
use App\Rules\CcValidate;
use Carbon\Carbon;
use Crypt;
use Hash;

class SettingsController extends Controller
{
    function shipping()
    {
        $shippings = DB::table('shipping_servers')
            ->join('shipping_companies', 'shipping_servers.id', '=', 'shipping_companies.server_id')
            ->select('shipping_servers.server_name', 'shipping_companies.id', 'shipping_companies.company_name', 'shipping_companies.company_name', 'shipping_companies.company_logo', 'shipping_servers.logo_path', 'shipping_companies.delivery_timeline')
            ->where('shipping_servers.status', 0)
            ->where('shipping_companies.status', 0)
            ->orderBy('shipping_servers.id', 'asc')->get();
        $user = User::findOrFail(Auth::user()->id);
        return view('settings.shipping', compact('shippings'), compact('user'));
    }
    function showshipping(Request $request)
    {
        $orderid = $request->orderid;
        $store_id = $request->store_id;
        $orderInstance = new \App\Models\OrderModel;
        $vieworder = $orderInstance->vieworder($orderid,$store_id);

        $user = User::findOrFail(Auth::user()->id);

        $company = Shipping_companie::where('id', $user->shipping)->first();

        //print_r($vieworder["order_details"]);
        return view('settings.shippingview', compact('vieworder'), compact('company'));
    }
    function shippingsave(Request $request)
    {
        $request->validate([
            'shipping' => 'required',
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->shipping = $request->shipping;
        $user->save();
        return redirect('settings/shipping')->with('success', 'success');
    }


    function generalsettings(Request $request)
    {
        $table = 'user_configs';

        $shippings = DB::table('shipping_servers')
            ->join('shipping_companies', 'shipping_servers.id', '=', 'shipping_companies.server_id')
            ->select('shipping_servers.server_name', 'shipping_companies.id', 'shipping_companies.company_name', 'shipping_companies.company_name', 'shipping_companies.company_logo', 'shipping_servers.logo_path', 'shipping_companies.delivery_timeline')
            ->where('shipping_servers.status', 0)
            ->where('shipping_companies.status', 0)
            ->orderBy('shipping_servers.id', 'asc')->get();

        $config = DB::table($table)
            ->where('user_configs.user_id', '=', Auth::user()->id)
            ->first();

        $load_user_main_config = User_main_config::where('user_id', '=', Auth::user()->id)
            ->first();

        $default_cc = User_cc::where('user_id', '=', Auth::user()->id)
            ->where('default_cc', '=', 1)
            ->first();

        $store = Store::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->storeid)
            ->first();

        $invoice_ship = DB::table('invoices')
            ->join('credit_types', 'invoices.invoice_type', '=', 'credit_types.id')
            ->where('invoices.user_id', '=', Auth::user()->id)
            ->where('invoices.invoice_type', '=', 2)
            ->select('invoices.*', 'credit_types.credit_type_text')
            ->orderBy('invoices.created_at', 'asc')
            ->get();

        $credittype = DB::table('credit_types')
            ->orderBy('id', 'asc')
            ->get();



        $wallet_credit = Invoice_wallet::where('user_id', Auth::user()->id)
            ->where('paymt_type', '=', 0)
            ->where('paymt_status', '=', 0)
            ->sum('wallet_amt');
        $wallet_debit = Invoice_wallet::where('user_id', Auth::user()->id)
            ->where('paymt_type', '=', 1)
            ->sum('wallet_amt');
        $wallet_available = $wallet_credit - $wallet_debit;

        $sum_credit = Invoice_credit::where('user_id', Auth::user()->id)->sum('credit_amt');
        $sum_pending = Invoice::where('user_id', Auth::user()->id)
                        ->where('invoice_type', '=', 2)
                        ->where('status', '=', 0)
                        ->sum('amount');
        if ($sum_pending == '') {
            $sum_pending = 0;
        }
        $credit_bal = $sum_credit - $sum_pending + $wallet_available;


        $to = \Carbon\Carbon::parse(Auth::user()->plan_todate);
        $from = \Carbon\Carbon::now();
        $daysleft = $to->diffInDays($from);

        $package = DB::table('dropshipping_packages')
        ->where('id','=', Auth::user()->plan_id)
        ->first();

        $maxpackage = Dropshipping_package::max('package_amount');


        $data =  array();
        $data['shippings']  =  $shippings;
        $data['store']  =  $store;
        $data['user']  =  Auth::user();
        $data['config']     =  $config;
        $data['load_user_main_config'] = $load_user_main_config;
        $data['default_cc']  =  $default_cc;
        $data['invoice_ship']  =  $invoice_ship;
        $data['credittype']  =  $credittype;
        $data['credit_bal']  =  $credit_bal;
        $data['daysleft']  =  $daysleft;
        $data['package']  =  $package;
        $data['maxpackage']  =  $maxpackage;

        return view('settings.generalsettings', compact('data'));
        //        return view('settings.generalsettings',compact('shippings'),compact('user'));
    }
    function invoicedetails()
    {
        return view('settings.invoicedetails');
    }
    function userdetails()
    {
        $user  =  Auth::user();
        return view('settings.userdetails', compact('user'));
    }

    function updateusertable(Request $request)
    {
        $optionid = $request->optionid;
        $optionid_val = $request->optionid_val;
        $result = User_main_config::where('user_id', '=', Auth::user()->id)
            ->update([$optionid => $optionid_val]);
        echo $result;
    }

    function updatetable(Request $request)
    {
        $optionid = $request->optionid;
        $optionid_val = $request->optionid_val;
        $result = Store::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->storeid)
            ->update([$optionid => $optionid_val]);
        echo $result;
    }

    function filterbystore(Request $request)
    {
        $result = Store::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->storeid)
            ->first();
        return $result;
    }

    function shipfilterbystore(Request $request)
    {
        $result = DB::table('stores')
            ->where('user_id', '=', Auth::user()->id)
            ->join('shipping_companies', 'stores.shipping', '=', 'shipping_companies.id')
            ->join('shipping_servers', 'shipping_companies.server_id', '=', 'shipping_servers.id')
            ->join('countries', 'shipping_servers.country', '=', 'countries.id')
            ->join('states', 'shipping_servers.state', '=', 'states.id')
            ->select('stores.*', 'shipping_servers.*', 'countries.country_name', 'states.state_name')
            ->where('stores.id', '=', $request->storeid)
            ->first();
        return $result;
    }

    function filters_update(Request $request)
    {
        $setstore = Store::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->store_id)->first();

        $result = Store::where('user_id', '=', Auth::user()->id)
            ->update([
                'only_fba_amazon_products' => $setstore->only_fba_amazon_products,
                'chinese_sellers_in_the_sale_store' => $setstore->chinese_sellers_in_the_sale_store,
                'amazon_sellers_in_the_sale_store' => $setstore->amazon_sellers_in_the_sale_store,
                'products_from_my_own_stores' => $setstore->products_from_my_own_stores,
                'stores_i_ve_added' => $setstore->stores_i_ve_added,
                'trademark_protected_products' => $setstore->trademark_protected_products,
                'discounted_products' => $setstore->discounted_products,
                'cannot_be_sent_to_the_address' => $setstore->cannot_be_sent_to_the_address,
                'missing_shipping_info_products' => $setstore->missing_shipping_info_products,
                'unavailable_products' => $setstore->unavailable_products,
                'no_import_fee' => $setstore->no_import_fee,
                'same_brand_and_seller_name' => $setstore->same_brand_and_seller_name,
                'prime_sellers_in_the_sale_store' => $setstore->prime_sellers_in_the_sale_store,
                'max_number_of_sellers' => $setstore->max_number_of_sellers,
                'min_number_of_sellers' => $setstore->min_number_of_sellers
            ]);
        echo $result;
    }

    function storeinfo_update(Request $request)
    {
        $result = Store::where('user_id', '=', Auth::user()->id);

        if ($request->updateallstores == '') {
            $result = $result->where('id', '=', $request->updatestore);
        }

        $result = $result->update([
            'country_same_asin' => ($request->country_same_asin == null) ? '0' : '1',
            
            'stock' => $request->maxstock,
            'amazon_commission' => $request->amazon_commission,
            'enable_tax' => $request->enable_tax,
            'enable_seller_central_tax' => $request->enable_seller_central_tax,
            //'add_fix_profit' => $request->add_fix_profit,

            'currency_update_o' => $request->currency_update_o,
            'currency_update_t' => ($request->currency_update_o == 'F') ? $request->currency_update_t : '0.00',

            'sales_tax_c' => ($request->sales_tax_c == null) ? '0' : '1',
            'sales_tax_t' => ($request->sales_tax_c == null) ? '0' : $request->sales_tax_t,

            'shippingfee_c' => ($request->shippingfee_c == null) ? '0' : '1',
            'shippingfee_t' => ($request->shippingfee_c == null) ? '0' : $request->shippingfee_t,
            'shippingfee_o' => ($request->shippingfee_c == null) ? '0' : $request->shippingfee_o,

            'import_fee_c' => ($request->import_fee_c == null) ? '0' : '1',
            'import_fee_t' => ($request->import_fee_c == null) ? '0' : $request->import_fee_t,
            'import_fee_o' => ($request->import_fee_c == null) ? '0' : $request->import_fee_o,

            'tax1_c' => ($request->tax1_c == null) ? '0' : '1',
            'tax1_t' => ($request->tax1_c == null) ? '0' : $request->tax1_t,
            'tax1_o' => ($request->tax1_c == null) ? '0' : $request->tax1_o,

            'tax2_c' => ($request->tax2_c == null) ? '0' : '1',
            'tax2_t' => ($request->tax2_c == null) ? '0' : $request->tax2_t,
            'tax2_o' => ($request->tax2_c == null) ? '0' : $request->tax2_o,

            'tax3_c' => ($request->tax3_c == null) ? '0' : '1',
            'tax3_t' => ($request->tax3_c == null) ? '0' : $request->tax3_t,
            'tax3_o' => ($request->tax3_c == null) ? '0' : $request->tax3_o,

            'black_seller' => ($request->black_seller == null) ? '0' : '1',
            'norepricer_black_seller' => ($request->norepricer_black_seller == null) ? '0' : '1',
            'cate_black' => ($request->cate_black == null) ? '0' : '1',

            'restricted_keywords' => ($request->restricted_keywords == null) ? '0' : '1',
            'restricted_products' => ($request->restricted_products == null) ? '0' : '1',
            'blacklist' => ($request->blacklist == null) ? '0' : '1',
            'whitelist' => ($request->whitelist == null) ? '0' : '1',
            'common_pools_for_brand' => ($request->common_pools_for_brand == null) ? '0' : '1',
            'common_pools_for_product' => ($request->common_pools_for_product == null) ? '0' : '1',

            'handling_time_c' => ($request->handling_time_c == null) ? '0' : '1',
            'handling_time_t' => ($request->handling_time_c == null) ? '0' : $request->handling_time_t,

            'order_confirmation' => ($request->order_confirmation == null) ? '0' : '1',

            'pre_order_confirmation_c' => ($request->pre_order_confirmation_c == null) ? '0' : '1',
            'pre_order_confirmation_t' => ($request->pre_order_confirmation_c == null) ? '0' : $request->pre_order_confirmation_t,

            'price_update_flag' => ($request->priceupdate == null) ? '0' : '1',
            'stock_update_flag' => ($request->stockupdate == null) ? '0' : '1',
            'product_removal_flag' => ($request->autodeletepr == null) ? '0' : '1',
            'pre_order_confirmation' => ($request->pre_order_confirmation == null) ? '0' : '1'
        ]);
        echo $result;
    }
    function filterbystoreinfo(Request $request)
    {
        $table = 'stores';
        if ($request->storeid > 0) {
            $result = DB::table($table)
                ->join('countries', $table . '.country_id', '=', 'countries.id')
                ->where('stores.user_id', '=', Auth::user()->id)
                ->where('stores.id', '=', $request->storeid)
                ->select($table . '.*', 'countries.currency_code')
                ->selectRaw('(select round(curr_value,2) from exchange_rates exc where exc.curr_code=countries.currency_code order by created_at desc limit 1) as currency_update_a')
                ->first();
            return $result;
        } else if ($request->storeid == 0) {
            $result = DB::table($table)
                ->join('users', $table . '.id', '=', 'users.default_store_id')
                ->join('countries', $table . '.country_id', '=', 'countries.id')
                ->where('stores.user_id', '=', Auth::user()->id)
                ->select($table . '.*', 'countries.currency_code')
                ->selectRaw('(select round(curr_value,2) from exchange_rates exc where exc.curr_code=countries.currency_code order by created_at desc limit 1) as currency_update_a')
                ->first();
            return $result;
        }
    }
    //phone_number update - message tab pane
    function update_user_main_config(Request $request)
    {
        $optionid = $request->optionid;
        $optionid_val = $request->optionid_val;
        $result = User_main_config::where('user_id', '=', Auth::user()->id)
            ->update([$optionid => $optionid_val]);
        echo $result;
    }

    function update_repricer(Request $request)
    {
        $optionid = $request->optionid;
        $optionid_val = $request->optionid_val;
        $result = Store::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->storeid)
            ->update([$optionid => $optionid_val]);
        echo $result;
    }

    function pricerloadstore(Request $request)
    {
        $table = 'stores';
        $repricer = DB::table($table)
            ->join('countries', $table . '.country_id', '=', 'countries.id')
            ->where($table . '.user_id', '=', Auth::user()->id)
            ->select($table . '.repricer', $table . '.pricing_method as pricemethod', $table . '.repricer_interval','countries.currency_code as curr')
            ->where($table . '.id', '=', $request->storeid)
            ->first();

            
        $pricing_details = Pricing_detail::where('user_id', '=', Auth::user()->id)
            ->where('store_id', '=', $request->storeid)
            ->get();

        $data =  array();
        $data['repricer']  =  $repricer;
        $data['pricing_details']  =  $pricing_details;
        return $data;
    }

    function update_pricingdetails(Request $request)
    {

        $totalrows = $request->totalRows;
        $result = Pricing_detail::where('user_id', '=', Auth::user()->id)
            ->where('store_id', $request->store_id)
            ->delete();

        for ($i = 0; $i < $totalrows; $i++) {

            if (isset($request["price_a" . $i])) {
                $result = Pricing_detail::create([
                    'user_id' => Auth::user()->id,
                    'store_id' => $request->store_id,
                    'price_1' => $request["price_a" . $i],
                    'price_2' => $request["price_b" . $i],
                    'min_profit' => $request["min" . $i],
                    'standard' => $request["standard" . $i],
                    'maximum' => $request["max" . $i],
                    'one_seller' => $request["oneseller" . $i],
                    'critic_stock' => $request["stock" . $i],
                    'repricer_interval' => $request["interval" . $i],
                    'formula' => $request["formula" . $i],
                    'extraprofit' => $request["extraprofit" . $i]
                ]);
            }
        }
        echo $result;
    }

    function add_cclist(Request $request)
    {
        $set_default = User_cc::where('user_id', '=', Auth::user()->id)->first();
        if ($set_default != '') {
            $set_default = '0';
        } else {
            $set_default = '1';
        }

        /* $request->validate([
            'cardno' => ['required',new CcValidate],
            'expdt' => ['required',new ExpDateValidate],
            'cvv' => 'required|integer',
        ]);

        $result = User_cc::where('user_id', '=', Auth::user()->id)
        //->where('cc',Crypt::encryptString(str_replace(" ","",$request->cardno)))
        ->first();

        if($result!=null && $result->id>0) {
            return response()->json('{"message":"Card already exist."}');
        }
        */
        $cno = $request->cardno;
        $cno = str_replace(' ', '', $cno);
        $resultid = User_cc::create([
            'user_id' => Auth::user()->id,
            'cc' => $cno,
            'exp_mon' => $request->expdt,
            //        'cc' => Crypt::encryptString(str_replace(" ","",$request->cardno)),
            //        'exp_mon' => Crypt::encryptString($request->expdt),
            'cvv' => Crypt::encryptString($request->cvv),
            'default_cc' => $set_default,
            'package_id' => 0
        ]);
        /*
        if($resultid>0) {
            return response()->json('{"status":"ok"}');
        } else {
            return response()->json('{"message":"Invalid request"}');
        }
*/
    }

    function view_cclist()
    {
        $list = User_cc::where('user_id', '=', Auth::user()->id)->get();
        $strOutput = '';
        foreach ($list as $item) {
            $number =  $item->cc;
            $cno =  str_pad(substr($number, -4), strlen($number), '*', STR_PAD_LEFT);

            $strOutput .= '<tr>
                <td><div class="avtar-pic w35" data-toggle="tooltip" data-placement="top" title="Subscription Payment"><span><img src="../assets/images/visa-card.png" data-toggle="tooltip" data-placement="top" title="Payment Logo" class="w35 h35 rounded"></span></div></td>
                <td>' . $cno . '</td>
                <td>' . $item->exp_mon . '</td>
                <td><button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="del_cc(' . $item->id . ')"><i class="fa fa-trash-o text-danger"></i></button></td>
                <td>
                ';
            if ($item->default_cc == '0') {
                $strOutput .= '<button type="button" class="btn btn-sm btn-default js-sweetalert" title="Set As Default" data-type="confirm" onclick="if(confirm(\'Are you sure to make it deafult?\')){set_default_cc(' . $item->id . ')}"><i class="fa fa-usd text-danger"></i></button>';
            }
            $strOutput .= '
                </td>
                </tr>';
        }
        return $strOutput;
    }

    function delete_cc(Request $request)
    {
        $list = User_cc::where('user_id', '=', Auth::user()->id)->get();
        $no_of_cc = $list->count();
        if ($no_of_cc > 1) {
            $default_cc = User_cc::where('user_id', '=', Auth::user()->id)
                ->where('default_cc', '=', 1)
                ->first();
            if ($default_cc->id != $request->id) {
                $item = User_cc::where('user_id', '=', Auth::user()->id)
                    ->where('id', '=', $request->id)
                    ->delete();
                return 1;
            } else {
                return 0;
            }
        } else if ($no_of_cc == 1) {
            $item = User_cc::where('user_id', '=', Auth::user()->id)
                ->where('id', '=', $request->id)
                ->delete();
            return 1;
        }
    }

    function update_default_cc(Request $request)
    {
        $default_cc = User_cc::where('user_id', '=', Auth::user()->id)
            ->where('default_cc', '=', 1)
            ->first();
        $old_id = $default_cc->id;
        $new_id = $request->id;
        $this->set_default_cc($old_id, 0);
        $this->set_default_cc($new_id, 1);
    }

    private function set_default_cc($id, $val)
    {
        $result = User_cc::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $id)
            ->update(['default_cc' => $val]);
        echo $result;
    }

    function add_credits(Request $request)
    {
        $resultid = Credit::create([
            'user_id' => Auth::user()->id,
            'add_credit' => $request->addbalance,
            'credit_type' => $request->credits_feetype,
        ]);
    }

    function view_credits()
    {
        $table = 'credits';
        $list = DB::table($table)
            ->join('credit_types', $table . '.credit_type', '=', 'credit_types.id')
            ->select('credits.add_credit', 'credit_types.credit_type_text')
            ->where('credits.user_id', '=', Auth::user()->id)
            ->orderBy('credits.id', 'desc')
            ->get();
        return response()->json($list);
    }

    function loadstore_list(Request $request)
    {
        $list = DB::table('countries')
            ->join('stores', 'countries.id', '=', 'stores.country_id')
            ->join('users', 'stores.user_id', '=', 'users.id')
            ->select('stores.*')
            ->where('stores.user_id', Auth::user()->id)
            ->where('countries.id', $request->country_id)
            ->orderBy('stores.created_at', 'desc')
            ->get();
        return response()->json($list);
    }

    function view_credit_type()
    {
        $list = Credit_type::where('status', '=', 0)
            ->orderBy('id', 'asc')
            ->get();
        $strOutput = '<select name="credits_feetype" id="credits_feetype" class="form-control show-tick" required>';
        foreach ($list as $item) {
            $strOutput .= '<option value="' . $item->id . '">' . $item->credit_type_text . '</option>';
        }
        $strOutput .= '</select>';

        return $strOutput;
    }
    function bill_details_list(Request $request)
    {
        $norec = 0;

        $table = 'invoices';
        $list = DB::table($table)
            ->join('credit_types', $table . '.invoice_type', '=', 'credit_types.id')
            ->where('invoices.user_id', '=', Auth::user()->id)
            ->where('invoices.status', '=', $request->status)
            ->select($table . '.*', 'credit_types.credit_type_text')
            ->get();
        $strOutput = '';
        foreach ($list as $item) {
            $norec++;
            $strOutput .= '<tr>';

            if ($item->status == '0') {
                $strOutput .= '<td>
                <label class="switch">
                    <input type="checkbox" class="exclude_item" name="chkbox" checked onclick="pending_payment()" value="' . $item->id . '">
                    <span class="slider round"></span>
                </label>
                </td>';
            } else {
                $strOutput .= '<td>' . $norec . '</td>';
            }

            $strOutput .= '<td>
            <div class="d-flex align-items-center"><div class="avtar-pic w35 bg-indigo" data-toggle="tooltip" data-placement="top" title="Give Away Payment"><span>GA</span>
            </div><div class="ml-3">' . $item->credit_type_text . '</div></div></td>
            <td><strong>' . $item->invoice_no . '</strong></td>
            <td>$ ' . $item->amount . '</td>';

            if ($item->status == '0') {
                $strOutput .= '<td><span class="badge badge-warning ml-0 mr-0">Pending</span></td>
                <td>' . $item->created_at . '</td>';
            } else {
                $strOutput .= '<td><span class="badge badge-success ml-0 mr-0">Paid</span></td><td>' . $item->paid_dt . '</td>
                <td><button type="button" class="btn btn-sm btn-default " title="Print" data-toggle="tooltip" data-placement="top"><i class="icon-printer"></i></button></td>';
            }
            $strOutput .= '</tr>';
        }
        if ($list != '') {
            return $strOutput;
        }
    }

    function ccp_list()
    {
        $list = User_cc::where('user_id', '=', Auth::user()->id)
            ->get();

        $strOutput = 'Select Credit Card<br><select name="paycc_ccno" id="paycc_ccno" class="form-control show-tick" required>';
        foreach ($list as $item) {
            $number =  $item->cc;
            $cno =  str_pad(substr($number, -4), strlen($number), '*', STR_PAD_LEFT);

            $strOutput .= '<option value="' . $item->id . '">' . $cno . '</option>';
        }
        $strOutput .= '</select>';

        return $strOutput;
    }
    function totalpayment(Request $request)
    {
        $result = Invoice::whereIn('id', explode(",", $request->id))
            ->where('user_id', '=', Auth::user()->id)
            ->sum('amount');

        return $result;
    }
    function cc_pay(Request $request)
    {
        $resultid = Invoice_payment::create([
            'user_id' => Auth::user()->id,
            //'cc_id' => $request->paycc_ccno,
            'amount' => $request->paycc_totalamt,
        ])->id;
        $resultid;

        $result = Invoice::whereIn('id', explode(",", $request->paycc_id))
            ->where('user_id', '=', Auth::user()->id)
            ->update([
                'status' => '1',
                'paid_dt' => Carbon::now(),
                'ref_no' => $resultid,
            ]);


        /*$credit_bal = DB::table('invoice_credits')
            ->where('user_id', '=', Auth::user()->id)
            ->select('credit_amt')
            ->first();

        $new_credit_bal = $credit_bal->credit_amt + $request->paycc_totalamt;

        Invoice_credit::where('user_id', '=', Auth::user()->id)->update(['credit_amt' => $new_credit_bal]);*/

        echo $result;
    }
    function changepwd(Request $request)
    {

        $request->validate([
            'password' => 'confirmed|min:6'
        ]);
        User::where('id', '=', Auth::user()->id)
            ->update(['password' => Hash::make($request->password)]);
        $success = 53; //changed successfully
        return redirect('settings/userdetails')->with("success", $success);
    }
    function updatelocal(Request $request)
    {
        $result = User::where('id', '=', Auth::user()->id)
            ->update([
                'default_store_id' => $request->store_main,
                'currency' => $request->currency_info,
                'default_language_id' => $request->language_info,
                'default_country_id' => $request->country_info
            ]);
        $success = 54; //updated successfully
        return redirect('settings/userdetails')->with("success", $success);
    }
    function loadstate_list(Request $request)
    {
        $list = DB::table('countries')
            ->join('states', 'countries.id', '=', 'states.country_id')
            ->select('states.*')
            ->where('countries.id', $request->country_id)
            ->get();
        return response()->json($list);
    }
    function updatecompany(Request $request)
    {
        $result = User::where('id', '=', Auth::user()->id)
            ->update([
                'company_name' => $request->company_name,
                'contact_person' => $request->contact_person,
                'mobile_numb' => $request->mobile_numb,
                'address1' => $request->address1,
                'email' => $request->email,
                'website_url' => $request->website_url,
                'country' => $request->country,
                'state' => $request->state,
                'city' => $request->city,
                'pincode' => $request->pincode,
                'phone' => $request->phone,
                'fax' => $request->fax
            ]);
        $success = 55; //updated successfully
        return redirect('settings/userdetails')->with("success", $success);
    }

    function updatestore(Request $request)
    {
        $result = Store::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->ustore_id)
            ->update([
                'company' => $request->company,
                'contact' => $request->contact,
                'mobile_number' => $request->mobile_number,
                'address' => $request->address,
                'email' => $request->email,
                'weburl' => $request->weburl,
                'country_id' => $request->country_id,
                'state_id' => $request->state_id,
                'city' => $request->city,
                'postalcode' => $request->postalcode,
                'phone_number' => $request->phone_number,
                'fax' => $request->fax
            ]);
        $success = 55; //updated successfully
        return redirect()->back();
    }

    function postpone(Request $request)
    {
        print_r(User_config::where('user_id', '=', Auth::user()->id)
            ->update(['postpone' => $request->postsel]));
    }

    function getqueue()
    {
        $table = 'user_products';
        $list = DB::table($table)
            ->where('user_id', '=', Auth::user()->id)
            ->where('status', '=', 1)
            ->orderBy('updated_at', 'desc')
            ->get();
        return response()->json($list);
    }

    function postpone_details(Request $request)
    {
        $result = User_config::where('user_id', '=', Auth::user()->id)->first();
        return response()->json($result);
    }

    function rollback(Request $request)
    {
        $num = $request->num;
        $result = User_product::where('user_id', '=', Auth::user()->id);

        if ($num == '1') {
            $result = $result->where('id', '=', $request->id);
        } else if ($num == '2') {
            $result = $result->where('status', '=', 1);
        }

        $result = $result->update(['status' => '3']);
        echo $result;
    }

    function getorderids(Request $request)
    {
        $vieworder = DB::table('orders')
            ->join('order_details', 'order_details.order_id', '=', 'orders.id')
            ->select('orders.id', 'orders.amazon_order_id', 'order_details.asin', 'order_details.producttitle', 'order_details.orion_ship_cost')
            ->where('orders.user_id', Auth::user()->id)
            ->where('orders.store_id', '=', $request->storeid)
            ->where('order_details.orion_pay_status', '=', '0')
            ->get();
        return response()->json($vieworder);
    }


    function addtopaylist(Request $request)
    {
        if ($request->id != '') {
            Order_detail::where('order_id', '=', $request->id)
                ->update([
                    'orion_pay_status' => 1,//1 - Credit Payment
                    'orion_ship_cost' => $request->price]);

            $resultid = Invoice::create([
                'user_id' => Auth::user()->id,
                'invoice_type' => 2,
                'amount' => $request->price
            ])->id;

            Invoice::where('id', '=', $resultid)->update(['invoice_no' => $resultid]);

            /*$credit_bal = DB::table('invoice_credits')
                ->where('user_id', '=', Auth::user()->id)
                ->select('credit_amt')
                ->first();

            $new_credit_bal = $credit_bal->credit_amt - $request->price;

            Invoice_credit::where('user_id', '=', Auth::user()->id)->update(['credit_amt' => $new_credit_bal]);*/
        }
    }

    function getpaylist(Request $request)
    {

        $list = DB::table('orders')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('order_details', 'order_details.order_id', '=', 'orders.id')
            ->select('orders.*', 'order_details.*', 'stores.store_name', 'orders.amazon_order_id', 'order_details.asin', 'order_details.producttitle', 'order_details.orion_ship_cost')
            ->where('orders.user_id', Auth::user()->id)
            ->where('order_details.orion_pay_status', '=', 1)
            ->get();

        $strOutput = '';
        foreach ($list as $item) {
            $strOutput .= '<tr>';

            $strOutput .= '<td>' . $item->store_name . '</td>
                <td>' . $item->amazon_order_id . '</td>
                <td>' . $item->asin . '</td>
                <td>' . $item->orion_ship_cost . '</td>
                <td class="text-wrap">' . $item->producttitle . '</td>';

            $strOutput .= '</tr>';
        }

        $wallet_credit = Invoice_wallet::where('user_id', Auth::user()->id)
            ->where('paymt_type', '=', 0)
            ->where('paymt_status', '=', 0)
            ->sum('wallet_amt');
        $wallet_debit = Invoice_wallet::where('user_id', Auth::user()->id)
            ->where('paymt_type', '=', 1)
            ->sum('wallet_amt');
        $wallet_available = $wallet_credit - $wallet_debit;

        $sum_credit = Invoice_credit::where('user_id', Auth::user()->id)->sum('credit_amt');
        $sum_pending = Invoice::where('user_id', Auth::user()->id)
                        ->where('invoice_type', '=', 2)
                        ->where('status', '=', 0)
                        ->sum('amount');
        if ($sum_pending == '') {
            $sum_pending = 0;
        }
        $credit_bal = $sum_credit - $sum_pending + $wallet_available;


        $data =  array();
        $data['strOutput']  =  $strOutput;
        $data['credit_bal']  =  $credit_bal;
        return $data;
    }

    function add_payoneer(Request $request)
    {
        $set_default = Payoneer::where('user_id', '=', Auth::user()->id)->first();
        if ($set_default == '') {
            $default = 1;
        } else {
            $default = 0;
        }

        $duplicate = Payoneer::where('payoneer', '=', $request->payoneer)
        ->where('user_id', '=', Auth::user()->id)->first();

        if (is_null($duplicate)) {
            $resultid = Payoneer::create([
                'user_id' => Auth::user()->id,
                'payoneer' => $request->payoneer,
                'default' => $default
            ]);
            return 'ok';
        } else {
            return 'duplicate';
        }      

    }


    function viewpayoneer()
    {
        $list = Payoneer::where('user_id', '=', Auth::user()->id)->get();
        $strOutput = '';
        foreach ($list as $item) {
            $strOutput .= '<tr>
                <td>' . $item->payoneer . '</td>
                <td><button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="del_payoneer(' . $item->id . ')"><i class="fa fa-trash-o text-danger"></i></button></td>
                <td>
                ';
            if ($item->default == '0') {
                $strOutput .= '<button type="button" class="btn btn-sm btn-default js-sweetalert" title="Set As Default" data-type="confirm" onclick="if(confirm(\'Are you sure to make it deafult?\')){set_default_payoneer(' . $item->id . ')}"><i class="fa fa-usd text-danger"></i></button>';
            }
            $strOutput .= '
                </td>
                </tr>';
        }
        return $strOutput;
    }
    function updatepayoneerlist()
    {
        $list = Payoneer::where('user_id', '=', Auth::user()->id)->get();
        $strOutput = '';
        foreach ($list as $item) {
            $strOutput .= '<option value="' . $item->id . '">' . $item->payoneer . '</option>';
        }
        return $strOutput;
    }

    function delete_payoneer(Request $request)
    {
        $list = Payoneer::where('user_id', '=', Auth::user()->id)->get();
        $no_of_payoneer = $list->count();
        if ($no_of_payoneer > 1) {
            $default_payoneer = Payoneer::where('user_id', '=', Auth::user()->id)
                ->where('default', '=', 1)
                ->first();
            if ($default_payoneer->id != $request->id) {
                $item = Payoneer::where('user_id', '=', Auth::user()->id)
                    ->where('id', '=', $request->id)
                    ->delete();
                return 1;
            } else {
                return 0;
            }
        } else if ($no_of_payoneer == 1) {
            $item = Payoneer::where('user_id', '=', Auth::user()->id)
                ->where('id', '=', $request->id)
                ->delete();
            return 1;
        }
    }

    function update_default_payoneer(Request $request)
    {
        $default_payoneer = Payoneer::where('user_id', '=', Auth::user()->id)
            ->where('default', '=', 1)
            ->first();
        $old_id = $default_payoneer->id;
        $new_id = $request->id;
        $this->set_default_payoneer($old_id, 0);
        $this->set_default_payoneer($new_id, 1);
    }

    private function set_default_payoneer($id, $val)
    {
        $result = Payoneer::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $id)
            ->update(['default' => $val]);
        echo $result;
    }

    function creditbal(Request $request)
    {
        $credit_bal = DB::table('invoice_credits')
            ->where('user_id', '=', Auth::user()->id)
            ->select('credit_amt')
            ->first();
        return $credit_bal;
    }

    function addto_invoice_payments(Request $request) {
        $payment_type = '';
        if ($request->paidfor == 'wallet') {$payment_type = 3;}
        else if  ($request->paidfor == 'others') {$payment_type = 1;}

       $resultid = Invoice_payment::create([
            'user_id' => Auth::user()->id,
            'payment_type' => $payment_type,
            'from_account_name' => $request->fromaccounttype,
            'to_account_name' => $request->payontype,
            'amount' => $request->addbalance,
            'description' => $request->descpayoneer,
            'status' => 0
        ])->id;        

        if  ($request->paidfor == 'others') {
            $result = Invoice::whereIn('id', explode(",", $request->paycc_id))
            ->where('user_id', '=', Auth::user()->id)
            ->update([
                'status' => '1',
                'paid_dt' => Carbon::now(),
                'ref_no' => $resultid,
            ]);
    
            /*$credit_bal = DB::table('invoice_credits')
                ->where('user_id', '=', Auth::user()->id)
                ->select('credit_amt')
                ->first();
    
            $new_credit_bal = $credit_bal->credit_amt + $request->addbalance;
    
            Invoice_credit::where('user_id', '=', Auth::user()->id)->update(['credit_amt' => $new_credit_bal]);*/
        }

        if ($request->paidfor == 'wallet') {
            $result = Invoice_wallet::create([
                'user_id' => Auth::user()->id,
                'wallet_amt' => $request->addbalance,
                'paymt_status' => 1,
                'paymt_type' => 0,
                'trans_id' => $resultid
            ]);   
        }
    }
    function formulalist(Request $request)
    {
        $list = DB::table('formulas')
            ->orderBy('id', 'asc')->get();
        return response()->json($list);
    }

    function getwallet(Request $request)
    {
        $credit = Invoice_wallet::where('user_id', Auth::user()->id)
            ->where('paymt_type', '=', 0)
            ->where('paymt_status', '=', 0)
            ->sum('wallet_amt');
        $debit = Invoice_wallet::where('user_id', Auth::user()->id)
            ->where('paymt_type', '=', 1)
            ->sum('wallet_amt');
        $available = $credit - $debit;
        return $available;
    }

    function getwalletdetails(Request $request)
    {
        $norec = 0;

        $list = Invoice_wallet::where('user_id', Auth::user()->id)
            ->orderBy('id', 'desc')
            ->get();

   
        $strOutput = '';
        
        foreach ($list as $item) {
            $norec++;

            if ($item->paymt_status == 0) {
                $paymt_status = '<span class="badge badge-success">Approved</span>';
            } else {
                $paymt_status = '<span class="badge badge-danger">Pending</span>';
            }
    
            if ($item->paymt_type == 0) {
                $paymt_type = 'Credited';
            } else {
                $paymt_type = 'Debited';
            }
    
            $transdate=CommonFunction::time_elapsed_string($item->created_at);

            $strOutput .= '<tr>';

            $strOutput .=   '<td>' . $norec . '</td>
                            <td><span class="text-info">$&nbsp;</span>' . $item->wallet_amt . '</td>
                            <td>' . $paymt_type . '</td>
                            <td>' . $transdate . '</td>
                            <td>' . $paymt_status . '</td>
                            <td>' . $item->trans_id . '</td>
                            </tr>';
        }
        if ($list != '') {
            return $strOutput;
        }

    }

    function takefromwallet (Request $request)
    {
        
        $resultid = Invoice_payment::create([
            'user_id' => Auth::user()->id,
            'payment_type' => "2",
            'from_account_name' => '',
            'to_account_name' => '',
            'amount' => $request->amount,
            'description' => '',
            'status' => 1
        ])->id;

        Invoice::whereIn('id', explode(",", $request->paycc_id))
        ->where('user_id', '=', Auth::user()->id)
        ->update([
            'status' => '1',
            'paid_dt' => Carbon::now(),
            'ref_no' => $resultid,
        ]);

        $paid = Invoice_wallet::create([
             'user_id' => Auth::user()->id,
             'wallet_amt' => $request->amount,
             'paymt_status' => 0,
             'paymt_type' => 1,
             'trans_id' => $resultid
         ]);
 
         return redirect()->back();
    }

    function freeze (Request $request)    {
        $result = User::where('id', '=', Auth::user()->id)
            ->update(['plan_freeze' => $request->freeze,
            'plan_freezedt' => Carbon::now()
        ]);
        return 1;
    }

    function extraprofitlist(Request $request)
    {
        $list = DB::table('extraprofit_lists')
            ->orderBy('calc_val', 'asc')->get();
        return response()->json($list);
    }


}
