<?php

namespace App\Http\Controllers;
use App\Models\State;
use App\Models\Fbk_user_answer;
use App\Models\Fbk_user_master;
use Auth;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function states(Request $request)
    {
        $data['states'] = State::where("country_id",$request->country_id)->get(["state_name", "id"]);
        return response()->json($data);
    }

    function updateuser(Request $request) {
        $optionid = $request->optionid;
        $optionid_val = $request->optionid_val;
        $result = User::where('id', '=', Auth::user()->id)
        ->update([$optionid => $optionid_val]);
        echo $result;
    }

    function savefeed(Request $request)
    {
        $anscount = 1; //satisfied question is mandatory so statrt with 1
        $resultid = Fbk_user_master::create([
            'user_id' => Auth::user()->id,
            'satisfied' => $request->satisfied,
            ])->id;
            $resultid;
        for ($i=1; $i<=$request->totalquest;$i++) {
            $qid = $request["qid".$i];
            if ($request["qtype".$i] == 1) {
                $answer_field = "user_ans";
                $answer = $request["user_ans".$qid];
                if ($answer != '') {
                $anscount++;
                }
            } else {
                $answer_field = "user_ans_text";
                $answer = $request->user_ans_text;
                if ($answer != '') {
                    $anscount++;
                }
            }
            $save = Fbk_user_answer::create([
            'feedbk_id' => $resultid,
            'qid' => $request["qid".$i],
            $answer_field => $answer
            ]);
        }
        Fbk_user_master::where('id', '=', $resultid)
        ->update(['answer_count' => $anscount]);

        return response()->json('{"status":"success"}');
    }

    function nav_loadstore() {
        $store_id = Session::get('storeid');
        $stores =  DB::table('stores')
        ->join('countries','stores.country_id', '=', 'countries.id')
        ->select('countries.flag_path','stores.store_name')
        ->where('stores.id','=',$store_id)
        ->first();

        $strOutput = '<img src="../assets/images/flag/store/128/'.$stores->flag_path.'" alt="Avatar" class="w30 mr-1 rounded-circle"> <span>'.$stores->store_name.'</span>';
        return $strOutput;

    }

    
}
