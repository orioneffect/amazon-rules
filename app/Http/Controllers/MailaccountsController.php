<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mail_account;
use DB;
use Auth;
use App\User;

class MailaccountsController extends Controller
{
    function mailaccounts()
    {
        $table = 'mail_accounts';
        $list = DB::table($table)
        ->where($table.'.userid', '=', Auth::user()->id)->get();
        
        return view('settings.mailaccounts',compact('list'));
    }

    public function mailaccountscreate(Request $request)
    {

            date_default_timezone_set('Europe/Istanbul');
            Mail_account::create([
            'userid' => Auth::user()->id,
            'buyermail' => $request->buyeremail,
            'sellermail' => $request->selleremail,
            'stores'=> $request->store
        ]);

        return redirect('settings/mailaccounts');
    }

}