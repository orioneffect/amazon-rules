<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use App\User;
use App\Models\Student_consultant;
use App\Models\Classroom;
use App\Models\Teacher;
use App\Models\User_main_config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;


class DashboardController extends BaseController
{
    function userdash()
    {
        return view('dashboard.userdash');
    }
    function profitloss()
    {
        return view('dashboard.profitloss');
    }
    function consettings()
    {
        $table = 'student_consultants';
        $student = DB::table($table)
            ->join('classrooms', $table . '.class_id', '=', 'classrooms.id')
            ->select($table . '.*', 'classrooms.classroom_name')
            ->where('student_consultants.user_id', '=', Auth::user()->id)
            ->orderBy('student_consultants.id', 'desc')
            ->get();

        $classroom = Classroom::where('classrooms.user_id', '=', Auth::user()->id)
            ->orderBy('classrooms.id', 'desc')
            ->get();

        $tocons =  array();
        $tocons['student']  =  $student;
        $tocons['classroom']  =  $classroom;

        return view('dashboard.consultant', compact('tocons'));
    }
    function consdetails()
    {
        return view('dashboard.consdetails');
    }
    function dashboard()
    {
        return view('dashboard.dashboard');
    }
    function teacher()
    {
        $teacher = Teacher::where('user_id', '=', Auth::user()->id)
            ->get();

        return view('dashboard.teacher', compact('teacher'));
    }
    function classroom()
    {
        $table = 'classrooms';
        $classroom = DB::table($table)
            ->where('classrooms.user_id', '=', Auth::user()->id)
            ->orderBy('classrooms.id', 'desc')
            ->get();

        $teachers = Teacher::where('user_id', '=', Auth::user()->id)->get();

        $teacherarr = [];

        foreach ($teachers as $teacher) {
            $teacherarr[$teacher["id"]] = $teacher["teacher"];
        }

        $toclass =  array();
        $toclass['classroom']  =  $classroom;
        $toclass['teacher']  =  $teacherarr;

        return view('dashboard.classroom', compact('toclass'));
    }
    function add_upload(Request $request)
    {
        if (isset($request->file)) {
            $request->validate([
                'file' => 'mimes:txt|max:5120'
            ]);
            $classroom=$request->classroom;
            $fileName = $request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');
        } else {
            $filePath = "Invalid request";
            exit;
        }

        $result = User_main_config::where('user_id', '=', Auth::user()->id)
            ->update(['url' => $filePath]);

        //echo $result;
        $file_path = public_path('../storage/app/public/' . $filePath);
        $file = fopen($file_path, 'r');
        $count = 0;
        $duplicate_row = 0;
        $str1 = '';
        while (!feof($file)) {
            $count++;
            $line = fgets($file);
            if ($line != '') {
                $value = explode(",", $line);
                if (count($value) == 4) {
                    $email = $value[1];
                    $duplicate  = Student_consultant::where('email', '=', $email)
                        ->where('user_id', '=', Auth::user()->id)->first();

                    if (is_null($duplicate)) {
                        Student_consultant::create([
                            'user_id' => Auth::user()->id,
                            'class_id' => $classroom,
                            'studname' => $value[0],
                            'email' => $value[1],
                            'company_name' => $value[2],
                            'notes' => $value[3],
                        ]);
                    } else {
                        $duplicate_row++;
                        $str1 = $str1 . ', ' . $count;
                    }
                } else {
                    $duplicate_row++;
                    $str1 = $str1 . ', ' . $count;
                }
            }
        }
        if ($duplicate_row > 0) {
            $count = $count - $duplicate_row;
            return redirect('dashboard/consettings')->with("err", $str1);
        }
        return redirect('dashboard/consettings')->with("message", $count);
    }
    function add_class(Request $request)
    {
        $name = implode(',', $request->teacherlist);
        $resultid = Classroom::create([
            'user_id' => Auth::user()->id,
            'classroom_name' => $request->classname,
            'teacher' => $name,
            'features' => $request->features,
            'notes' => $request->class_notes,
            'edu_st_date' => $request->edu_dt_start,
            'edu_end_date' => $request->edu_dt_end
        ]);
        return redirect('dashboard/classroom')->with("message", ' ');
    }
    function update_class(Request $request)
    {
        $result = Classroom::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->u_id)
            ->update([
                'classroom_name' => $request->u_classname,
                'teacher' => implode(',', $request->u_teacherlist),
                'features' => $request->u_features,
                'notes' => $request->u_class_notes,
                'edu_st_date' => $request->u_edu_dt_start,
                'edu_end_date' => $request->u_edu_dt_end
            ]);
        return redirect('dashboard/classroom')->with("message_updt", ' ');
    }
    function get_class(Request $request)
    {
        $table = 'classrooms';
        $classroom = DB::table($table)
            ->where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->id)
            ->first();
        return $classroom;
    }
    function add_teacher(Request $request)
    {
        $resultid = Teacher::create([
            'user_id' => Auth::user()->id,
            'teacher' => $request->teacher_name,
            'email' => $request->email
        ]);
        $count = '';
        return redirect('dashboard/teacher')->with("message", $count);
    }
    function get_teacher(Request $request)
    {
        $table = 'teachers';
        $classroom = DB::table($table)
            ->where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->id)
            ->first();
        return $classroom;
    }
    function update_teacher(Request $request)
    {
        $result = Teacher::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->u_id)
            ->update([
                'teacher' => $request->u_teacher_name,
                'email' => $request->u_email
            ]);
        return redirect('dashboard/teacher')->with("message_updt", ' ');
    }
    function banstudent(Request $request)
    {
        $result = Student_consultant::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->id)
            ->update([
                'status' => 1,
                'impersonate' => 2,
            ]);
        $count = '';
        return 1;
    }
    function addlogo(Request $request)
    {
        if (isset($request->file)) {
            $request->validate([
                'file' => 'required|mimes:PNG,png,tiff,jpg,jpeg|max:5120'
            ]);
            $fileName = $request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('logo', $fileName, 'public');
        } else {
            $filePath = "";
        }

        $result = User::where('id', '=', Auth::user()->id)
            ->update(['logo' => $filePath]);
        return redirect('dashboard/consettings')->with("message", '');
    }
    function generaldashboard(Request $request)
    {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        $store = Session::get('storeid');
        $userid = Auth::user()->id;

        $listings = DB::table('listings')->count();

        $orders = DB::table('orders')
            ->join('order_details','orders.id', '=', 'order_details.order_id')
            ->join('stores','orders.store_id', '=', 'stores.id')
            ->join('countries','stores.country_id', '=', 'countries.id')
            ->where('orders.user_id', '=', $userid)
            ->where('orders.store_id', '=', $store)
            ->select('orders.*', 'order_details.*','stores.store_name as store', 'countries.flag_path as countryflag')
            ->orderBy('orders.id', 'desc')
        ->get();

        $counts = DB::table('orders')
            ->join('order_details','orders.id', '=', 'order_details.order_id')
            ->join('stores','orders.store_id', '=', 'stores.id')
            ->join('countries','stores.country_id', '=', 'countries.id')
            ->where('orders.user_id', '=', $userid)
            ->where('orders.store_id', '=', $store)
            ->selectraw("(select currency_code from orders where order_details.order_id=orders.id) as curcode ")
            ->selectraw("(select sum(price) from order_details a join orders b on a.order_id=b.id where b.store_id=$store) as prodsales ")
            ->selectraw("(select sum(price_usd) from order_details a join orders b on a.order_id=b.id where b.store_id=$store) as prodsales_usd ")
            ->selectraw("(select sum(profit) from order_details a join orders b on a.order_id=b.id where b.store_id=$store) as totprofit ")
            ->selectraw("(select sum(profit_usd) from order_details a join orders b on a.order_id=b.id where b.store_id=$store) as totprofit_usd ")
            ->selectraw("(select count(1) from orders where orderstatus!='return' and store_id=$store) as totorders_count ")
            ->selectraw("(select sum(ordertotal) from orders where orderstatus!='return' and store_id=$store) as totorders ")
            ->selectraw("(select sum(ordertotal_usd) from orders where orderstatus!='return' and store_id=$store) as totorders_usd ")
            ->selectraw("(select count(1) from orders where orderstatus='return' and store_id=$store) as tot_ret_orders_count ")
            ->selectraw("(select sum(ordertotal) from orders where orderstatus='return' and store_id=$store) as tot_ret_orders ")
            ->selectraw("(select sum(ordertotal_usd) from orders where orderstatus='return' and store_id=$store) as tot_ret_orders_usd ")
        ->first();

        $bestseller = DB::select("SELECT asin,
            count(1) as total_sales,
            sum(quantity) as total_quantity,
            cost_usd as estcost,
            profit_usd as estprofit,
            producttitle as descrp,
            category as categ,
            profit_percent as percent,
            c.store_name as store,
            d.flag_path as countryflag
            FROM `order_details` a
            join orders b on b.id = a.order_id
            JOIN stores c on b.store_id=c.id
            JOIN countries d on c.country_id=d.id
        WHERE b.user_id=$userid and 1 group by asin, cost_usd, profit_usd, producttitle, category, profit_percent, c.store_name, d.flag_path order by 2 desc");

        $topstores = DB::select("SELECT
            sum(a.profit_usd) as store_revenue,
            c.store_name as store,
            d.flag_path as countryflag
            FROM `order_details` a
            join orders b on b.id = a.order_id
            JOIN stores c on b.store_id=c.id
            JOIN countries d on c.country_id=d.id
        WHERE b.user_id=$userid and 1 group by c.store_name, d.flag_path order by 2 desc");

        $topcountries = DB::select("SELECT
            sum(a.profit_usd) as country_revenue,
            d.country_name as country_name,
            d.flag_path as countryflag
            FROM `order_details` a
            join orders b on b.id = a.order_id
            JOIN stores c on b.store_id=c.id
            JOIN countries d on c.country_id=d.id
        WHERE b.user_id=$userid and 1 group by d.country_name, d.flag_path order by 2 desc");

        $data =  array();
        $data['listings']  =  $listings;
        $data['orders']  =  $orders;
        $data['counts']  =  $counts;
        $data['bestseller']  =  $bestseller;
        $data['topstores']  =  $topstores;
        $data['topcountries']  =  $topcountries;
        return view('dashboard.generaldashboard',compact('data'));
    }

}
