<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parasut_integration;

class Parasut extends Controller
{
    public function addto_parasut_intgration(Request $request)
    {
        $parclientid=$request->parclientid;
        $parclientsecret=$request->parclientsecret;
        $parcallbackurls=$request->parcallbackurls;
        $parusername=$request->parusername;
        $paruserpwd=$request->paruserpwd;
        
        Parasut_integration::create([
            "client_id"=>$parclientid,
            "client_secret"=>$parclientsecret,
            "callback_urls"=>$parcallbackurls,
            "parusername"=>$parusername,
            "paruserpwd"=>$paruserpwd,
            
        ]);
        echo ("Bilgileriniz kaydedilmiştir...");

    }
        
    
}
