<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\Models\Ticket_detail;
use App\Models\Notification;
use App\Models\Ticket_activity;
use Illuminate\Support\Facades\Storage;
use App\Models\Helpdesk_config;
use App\Models\CommonFunction;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class HelpdeskController extends BaseController
{
    function createticket(){
        return view('helpdesk.createticket');
    }
    function ticketlist(Request $request){
        $table='ticket_details';
        $ticketlist=DB::table($table)
        ->join('helpdesk_configs',$table.'.priority', '=', 'helpdesk_configs.typeid')

        ->where('ticket_details.user_id', '=', Auth::user()->id)
        ->select($table.'.*', 'helpdesk_configs.visible');

        if($request->search_id!='') {
            $id_val = substr($request->search_id, 0, 2);
            if($id_val=='or' || $id_val=='OR') {
                $id = substr($request->search_id, 2);
                $ticketlist=$ticketlist->where ($table.'.id', 'LIKE', '%' . $id .'%');
            } else {
                $ticketlist=$ticketlist->where ($table.'.id', 'LIKE', '%' . $request->search_id .'%');
            }
        }
        if($request->search_priority!='') {
            $ticketlist=$ticketlist->where($table.'.priority', '=', $request->search_priority);
        };

        if($request->search_from!='' && $request->search_to!='') {

            $search_from = Carbon::parse($request->search_from)
            ->toDateTimeString();

            $search_to = Carbon::parse($request->search_to)
            ->toDateTimeString();

            $ticketlist=$ticketlist->whereBetween($table.'.created_at',[$search_from,$search_to]);
        };
        if($request->search_status!='') {
            $ticketlist=$ticketlist->where($table.'.status', '=', $request->search_status);
        };
        $ticketlist=$ticketlist->orderBy($table.'.id', 'desc')->get();

        $ticketresponded = Ticket_detail::where('status', '=', 'a2')->get();
        $ticketresolve = Ticket_detail::where('status', '=', 'a3')->get();
        $ticketpending = Ticket_detail::where('status', '=', 'a1')->get();

        $table='ticket_details';
        $ticketfull=DB::table($table)->get();

        $statusNav = Helpdesk_config::where('type', '=', 'status')->get();
        $priorityNav = Helpdesk_config::where('type', '=', 'priority')->get();
        
        $no_of_ticket = $ticketfull->count();
        $no_of_responded = $ticketresponded->count();
        $no_of_resolve = $ticketresolve->count();
        $no_of_pending = $ticketpending->count();

        $data =  array();
        $data['ticketlist']  =  $ticketlist;
        $data['no_of_ticket']  =  $no_of_ticket;
        $data['no_of_responded']  =  $no_of_responded;
        $data['no_of_resolve']  =  $no_of_resolve;
        $data['no_of_pending']  =  $no_of_pending;
        $data['priorityNav']  =  $priorityNav;
        $data['statusNav']  =  $statusNav;


        return view('helpdesk.ticketlist',compact('data'));
    }
    function ticket_message(Request $request){
        $t_id = $request->t_id;

        $table='ticket_details';
        $ticketdetails=DB::table($table)
        ->join('helpdesk_configs',$table.'.status', '=', 'helpdesk_configs.typeid')
        ->join('users',$table.'.user_id', '=', 'users.id')
        ->where('ticket_details.id', '=', $t_id)
        ->select($table.'.*', 'helpdesk_configs.visible', 'users.name', 'users.phone', 'users.email', 'users.logo')
        ->first();

        $table = 'ticket_activities';
        $ticketactivity = DB::table($table)
        ->join('helpdesk_configs',$table.'.message_by', '=', 'helpdesk_configs.typeid')
        ->where('ticket_activities.ticket_id', '=', $t_id)
        ->select($table.'.*', 'helpdesk_configs.visible')
        ->orderBy('ticket_activities.id', 'desc')
        ->get();

        $statusNav = Helpdesk_config::where('type', '=', 'status')->get();

        $data =  array();
        $data['ticketdetails']  =  $ticketdetails;
        $data['ticketactivity']  =  $ticketactivity;
        $data['statusNav']  =  $statusNav;

        return view('helpdesk.ticket_message',compact('data'));        
    }
    function add_msg(Request $request) {
        Ticket_detail::where('id', '=', $request->ticket_id,)
        ->update(['status' => $request->setstatus]);
        

        Ticket_activity::create([
        'ticket_id' => $request->ticket_id,
        'user_id' => $request->user_id,
        'message' => $request->msg,
        'message_by' => 'b1',//for User pass - b1
        ]);
        return redirect()->back();

    }
    function add_ticket(Request $request) {

        if(isset($request->file)) {
            $request->validate([
                'file' => 'required|mimes:pdf,doc,docx,PNG,png,tiff,jpg,jpeg|max:5120'
            ]);
            $fileName = $request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');
        } else {
            $filePath="";
        }


        $resultid = Ticket_detail::create([
        'user_id' => Auth::user()->id,
        'title' => $request->title,
        'description' => $request->description,
        'priority' => $request->priority,
        'url' => $filePath
        ]);

        Notification::create([
            'user_id' => Auth::user()->id,
            'type_id' => 1,
            'head' => 'Ticket',
            'message' => 'We received your Request',
            'sent_by' => 1,
            'is_read' => 0,
            'status' => 0
            ]);

        
        return redirect('helpdesk/ticketlist')->with("success","success");
    }
    function priority_list() {
        $check = 'priority';
        $list = Helpdesk_config::where('type', '=', $check)
        ->orderby('typeid', 'asc')
        ->get();

        $strOutput = '<select class="form-control show-tick" required id="priority" name="priority">';
        foreach ($list as $item) {
            $strOutput .= '<option value="'.$item->typeid.'">'.$item->visible.'</option>';
        }
        $strOutput .='</select>';

        return $strOutput;
    }
    function add_url(Request $request) {
        if(isset($request->file)) {
            $request->validate([
                'file' => 'required|mimes:PNG,png,tiff,jpg,jpeg'
            ]);
            $fileName = $request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');
        } else {
            $filePath="";
        }


        Ticket_activity::create([
        'ticket_id' => $request->ticket_id,
        'user_id' => $request->user_id,
        'message' => 'Details in File',
        'message_by' => 'b1',//for User pass - b1
        'url' => $filePath
        ]);
        return redirect()->back();


        /*
        Notification::create([
            'user_id' => Auth::user()->id,
            'type_id' => 1,
            'head' => 'Ticket',
            'message' => 'We received your Request',
            'sent_by' => 1,
            'is_read' => 0,
            'status' => 0
            ]);

        */
        //return redirect('admin/ticketdetails')->with("success","success");
    }    


}
