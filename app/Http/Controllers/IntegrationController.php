<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Store;
use App\Models\Integration;
use App\Models\User_amazon_integration;
use Auth;
use DB;
use Redirect;
use App\User;
use Session;
use Cache;

/*use Iyzico\IyzipayLaravel\Exceptions\Transaction\TransactionSaveException;
use Iyzico\IyzipayLaravel\IyzipayLaravel;
use Iyzico\IyzipayLaravel\StorableClasses\Address;
use Iyzico\IyzipayLaravel\StorableClasses\BillFields;
use Iyzico\IyzipayLaravel\IyzipayLaravelServiceProvider;
*/
class IntegrationController extends Controller
{


    function callback(Request $request) {
        $storeId = $request->input('state');
        $spapi_oauth_code = $request->input('spapi_oauth_code');
        $selling_partner_id = $request->input('selling_partner_id');

        $integration = Integration::where('id','1')->first();
        Store::where('id', $storeId)->update(array('merchant' => $selling_partner_id)); 
        /*print_r($integration->id);

        echo "<br>".$spapi_oauth_code;
        echo "<br>".$integration->client_id;
        echo "<br>".$integration->client_secret;
        echo "<br>".$integration->redirect_uri;*/

        //$refreshToken="Atzr|IwEBIE1CLPt78JZ90EdvN3BmBnv286kkLdqJXtMD0Tv5OBFdqRqMOoa9dg83WmcYyMe2dHbhIqStII8aes7GFIGJDBTfkItwiir4ZQXiOrusRHd0opChug4GREk5o2MBKQ7tJwcEv64urtTdBE5JbPQZJV8dKIs66XpJQp_AauZrPURX5XnztalADGSBcDkdEpABFbjUkSicm0ZKk9Pvi2oINYaxl-pvk1NteV1KntcJwnbXiBCHM9-0sBmKGcJU7aJISNhi14hFyNHSG_hXKS8bnm00fqxPhxw23v2VHuKRGJQuyv-QMNSB9WeC_uUZfQca4u8";

        $refreshToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getRefreshTokenFromLwaAuthorizationCode(
            $spapi_oauth_code,
            $integration->client_id,
            $integration->client_secret,
            $integration->redirect_uri
        );

        $id = User_amazon_integration::create([
            'user_id' => Auth::user()->id,
            'store_id' => $storeId,
            'admin_integration_id' => $integration->id,
            'refresh_token' => $refreshToken,
        ])->id;


        return redirect('integration/mystore')->with('success1','success1');

    }

    function addstore() {
        $countries =  Country::all('country_name','id','flag_path','url_path');
        return view('integration.addstore',compact('countries'));
    }

    function savestore(Request $request) {

        $request->validate([
            'store' => 'required|string|max:255',
            'country' => 'required',
            'stock' => 'required'
        ]);
        $merchantid='';
        if(isset($request->merchant)){
            $merchantid=$request->merchant;
        }
        $storeid = Store::create([
            'store_name' => $request->store,
            'stock' => $request->stock,
            'budget' => $request->budget,
            'sku' => $request->sku,
            'merchant' => $merchantid,
            'country_id' => $request->country,
            'user_id' => Auth::user()->id,
            'price_update_flag'=> $request->priceupdate,
            'stock_update_flag'=> $request->stockupdate,
            'product_removal_flag'=> $request->productremove,
            'product_insert_flag'=> $request->productinsert
        ])->id;
        return response()->json('{"success":"'.$storeid.'"}');
    }
    function successReturnSpapi(Request $request) {
       
        dd($request);
        return view('integration.storelist', ['stores' => $stores]);
    }
    function mystore() {
        //$stores = Store::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        $stores = DB::table('stores')
            ->join('countries', 'stores.country_id', '=', 'countries.id')
            ->join('users', 'stores.user_id', '=', 'users.id')
            ->leftjoin('user_amazon_integrations','stores.id', '=', 'user_amazon_integrations.store_id')
            ->select('stores.*', 'countries.flag_path', 'countries.country_name','users.name','user_amazon_integrations.admin_integration_id')
            ->where('stores.user_id',Auth::user()->id)
            ->orderBy('stores.created_at','desc')
            ->get();
           /* //dd($stores);
            //$reportId=65581018999;
            $reportId=52630018999;
            $reportInstance = new \App\Models\AmazonReportModel;
          // $reportId = $reportInstance->getReport('GET_MERCHANT_LISTINGS_ALL_DATA');
           echo "<pre>";
           print_r($reportId);
           echo "</pre>";
            $docId=$reportInstance->viewReport($reportId);
            echo "<pre>";
            print_r($docId);
            echo "</pre>";
            $DataFound=$reportInstance->viewDoc($docId);
            //dd($reportId,0);
            dd($DataFound);*/
        return view('integration.storelist', ['stores' => $stores]);
    }

    function changestore($storeid) {
        Cache::flush();
        Session::put('storeid', $storeid);

        $countryid = DB::table('stores')
        ->where('stores.id', $storeid)
        ->first();

        Session::put('countryid', $countryid->country_id);

        return redirect()->back()->with('success',Session::get('storeid'));

        //return redirect('integration/mystore')->with('success',Session::get('storeid'));

    }
    function feedbackstatus($status) {
        Cache::flush();
        Session::put('feedbk', $status);
        return redirect()->back()->with('success',Session::get('feedbk'));
    }

    function deletestore(Request $request) {
        Store::where('id', '=', $request->id)
        ->delete();
        return 1;
    }


}
