<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Cache;
class CommonController extends Controller
{

    function demolish(Request $request){
        DB::table($request->refname)
        ->where('user_id', '=', Auth::user()->id)
        ->where('id',$request->id)
        ->delete();
    }
    function actconsultant(Request $request){
        if ($request->flag==1)
        {
            $impersonate=1;
        } else if ($request->flag==0)
        {
            $impersonate=2;
        }
        DB::table('student_consultants')
        ->where('email', '=', Auth::user()->email)
        ->update(['impersonate' => $impersonate]);
    }
    function changelang(Request $request) {
        Cache::put('lang', $request->lang);
        return redirect()->back();
        /*$lang = DB::table('languages')
        ->where('id', '=', $request->lang_id;)
        ->get();*/
    }           
    
}
