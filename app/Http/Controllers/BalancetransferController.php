<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Balance_transfer;
use DB;
use Auth;
use App\User;

class BalancetransferController extends Controller
{
    function balancetransfer()
    {
        $table = 'balance_transfers';
        $list = DB::table($table)
        ->where($table.'.userid', '=', Auth::user()->id)->get();
        return view('settings.balancetransfer',compact('list'));
    }

    public function amountransfer(Request $request)
    {
        date_default_timezone_set('Europe/Istanbul');
        Balance_transfer::create([
           'userid' => Auth::user()->id,
           'targetemail' => $request->targetemail,
           'targetuserid' => Auth::user()->id,
           'currency'=> $request->currency,
           'exccurvalue'=> $request->currency,
           'amount'=> $request->amount
           
       ]);

       return redirect('settings/balancetransfer');
    }

}
