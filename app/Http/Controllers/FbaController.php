<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Country;
use App\Models\Fba_plan;
use App\Models\Fba_plan_product;
use App\Models\Listing;
use App\Models\Fba_shippingstatus_list;
use Session;
use Cache;
use App\Models\CommonFunction;

class FbaController extends Controller
{

    function fbalist() {
        $countries =  Country::all('country_name','id','flag_path','url_path');
        return view('fba.fbalist',compact('countries'));
    }


    function fbashipment() {
        $stores_nav = DB::table('stores')
        ->join('countries', 'stores.country_id', '=', 'countries.id')
        ->join('users', 'stores.user_id', '=', 'users.id')
        ->leftjoin('user_amazon_integrations','stores.id', '=', 'user_amazon_integrations.store_id')
        ->select('stores.*', 'countries.flag_path', 'countries.country_name','users.name','user_amazon_integrations.admin_integration_id')
        ->where('stores.user_id', '=', Auth::user()->id)
        ->where('user_amazon_integrations.admin_integration_id','>',0)
        ->orderBy('stores.created_at','desc')
        ->get();

        $stores_navarr = [];

        foreach ($stores_nav as $store) {
            $stores_navarr[$store->id] = $store->store_name;
        }

        $shipping_status =  Fba_shippingstatus_list::where('status', '=', 0)->get();

        $shipping_statusarr = [];

        foreach ($shipping_status as $shippingstatus) {
            $shipping_statusarr[$shippingstatus["id"]] = $shippingstatus["shipstatus"];
        }

        
        $shipment = DB::table('fba_plans')
        ->join('fba_shippingstatus_lists','fba_plans.shipping_status', '=', 'fba_shippingstatus_lists.id')
        ->join('stores','fba_plans.store_id', '=', 'stores.id')
        ->join('countries', 'stores.country_id', '=', 'countries.id')
        ->selectRaw('(select count(1) from fba_plan_products u where u.plan_id=fba_plans.id) as plan_count,
        (select sum(quantity) from fba_plan_products u where u.plan_id=fba_plans.id) as total_quantity,
        stores.store_name,fba_plans.id,fba_plans.created_at,fba_plans.seller_label,fba_plans.shipment_id,fba_shippingstatus_lists.shipstatus,countries.flag_path
        ')
        //->select('stores.store_name','fba_plans.id','fba_plans.created_at')
        ->where('fba_plans.user_id', '=', Auth::user()->id)
        ->get();

        

        $data =  array();
        $data['shipment']  =  $shipment;
        $data['stores_nav']  =  $stores_navarr;
        $data['shipping_status']  =  $shipping_statusarr;
        $data['user']  =  Auth::user();

        return view('fba.fbashipment',compact('data'));
    }

    function addnewplan() {
        $countries =  Country::all('country_name','id','flag_path','url_path');

        $listings =  Listing::where('user_id', '=', Auth::user()->id)
        ->where('quantity', '>', 0)
        ->get();


        $data =  array();
        $data['countries']  =  $countries;
        $data['listings']  =  $listings;

        return view('fba.addnewplan',compact('data'));
    }

    function giveaway() {
        $countries =  Country::all('country_name','id','flag_path','url_path');
        return view('fba.giveaway',compact('countries'));
    }

    function saveplan(Request $request)
    {
        $store_id = Session::get('storeid');
        $plan_id = Fba_plan::create([
            'user_id' => Auth::user()->id,
            'store_id' => $store_id,
            'shipment_id' => 'FBAqwere123',
            'to_country' => $request->to_country,
            'seller_label' => $request->seller_label,
            'notes' => $request->notes,
            'from_name' => $request->from_name,
            'from_address1' => $request->from_address1,
            'from_address2' => $request->from_address2,
            'from_city' => $request->from_city,
            'from_country' => $request->from_country,
            'from_state' => $request->from_state,
            'from_pincode' => $request->from_pincode
            ])->id;
            $plan_id;
        return response()->json($plan_id);
    }

    function load_listings(Request $request) {
        $listings =  DB::table('listings')
        ->where('user_id', '=', Auth::user()->id);
        //->where('status', '=', "Active");
        if($request->name!='') {
            $listings =  $listings->where('listings.sku', 'LIKE', '%' . $request->name .'%')
                        ->orwhere('listings.asin', 'LIKE', '%' . $request->name .'%')
                        ->orwhere('listings.product_name', 'LIKE', '%' . $request->name .'%');
        };
        if($request->id!='') {
            $listings =  $listings->whereRaw(' NOT EXISTS (SELECT 1 FROM fba_plan_products plan where plan.sku=listings.sku and plan_id ='.$request->id.' ) ');
        };
        $listings = $listings->get();
        return response()->json($listings);
    }

    function addproduct(Request $request) {
        
        $prods = explode(",", $request->id);

        foreach ($prods as $prod) {
            if ($prod!='') {
                $result = Listing::where('id', '=', $prod)->first();
                $addprod = Fba_plan_product::create([
                    'plan_id' => $request->planid,
                    'asin'=> $result->asin,
                    'sku'=> $result->sku,
                    'condition'=> 'New',
                    'quantity'=> '0'
                ]);
            }
        }
    }

    function load_selectprod(Request $request) {
        $listings =  Fba_plan_product::where('plan_id', '=', $request->id)->get();
        return response()->json($listings);
    }

    function changeqty(Request $request) {
        $listings =  Fba_plan_product::where('id', '=', $request->id)
        ->update(['quantity' => $request->quantity]);
        return $listings;
    }

    function removeprod(Request $request) {
        $result =  Fba_plan_product::where('id', '=', $request->id)
        ->delete();
        return $result;
    }    

    function toestimate(Request $request) {
        $listings =  Fba_plan_product::where('plan_id', '=', $request->id)
        ->where('quantity', '=', 0)
        ->get();
        return response()->json($listings);
    }

    function loadstore_list(Request $request) {
        $list = DB::table('countries')
        ->join('stores', 'countries.id', '=', 'stores.country_id')
        ->join('users', 'stores.user_id', '=', 'users.id')
        ->select('stores.*')
        ->where('stores.user_id',Auth::user()->id)
        ->where('countries.id', $request->country_id)
        ->orderBy('stores.created_at','desc')
        ->get();
        return response()->json($list);
    }

    function load_fbashipment(Request $request) {
        $shipment = DB::table('fba_plans')
        ->join('fba_shippingstatus_lists','fba_plans.shipping_status', '=', 'fba_shippingstatus_lists.id')
        ->join('stores','fba_plans.store_id', '=', 'stores.id')
        ->join('countries', 'stores.country_id', '=', 'countries.id')
        ->selectRaw('(select count(1) from fba_plan_products u where u.plan_id=fba_plans.id) as plan_count,
        (select sum(quantity) from fba_plan_products u where u.plan_id=fba_plans.id) as total_quantity,
        stores.store_name,fba_plans.id,fba_plans.created_at,fba_plans.seller_label,fba_plans.shipment_id,fba_shippingstatus_lists.shipstatus,countries.flag_path
        ')
        ->where('fba_plans.user_id', '=', Auth::user()->id);
        
        if($request->store!='') {
            $shipment =  $shipment->whereRaw('store_id in ('.$request->store.')');
        };
        if($request->shipmentid!='') {
            $shipment =  $shipment->where('fba_plans.shipment_id', '=', $request->shipmentid);
        };
        if($request->shipping_status!='') {
            $shipment =  $shipment->whereRaw('shipping_status in ('.$request->shipping_status.' ) ');
        };

        $shipment=$shipment->get();

        $strOutput = '';
        foreach ($shipment as $item) {
            $strOutput .= '<tr>
            <tr>
            <td>'.$item->id.'</td>
            <td><img src="../assets/images/flag/store/128/'.$item->flag_path.'" alt="Avatar" class="w30 mr-1 rounded-circle"> <span>'.$item->store_name.'</span></td>
            <td><h6 class="mb-0">'.$item->shipment_id.'</h6></td>
            <td><span class="badge badge-info">'.$item->shipstatus.'</span></td>
            <td><span>'.$item->plan_count.'</span></td>
            <td>'.$item->total_quantity.'</td>
            <td><span class="text-warning">'.$item->seller_label.'</span></td>
            <td>
                '.CommonFunction::time_elapsed_string($item->created_at,false).'
            </td>
            </tr>';


        }
        if ($strOutput=='')
        {
            $strOutput="Search not found";
        }
        return $strOutput;


    }


}
