<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Models\Shipping_companie;
use App\Models\User_config;
use App\Models\User_main_config;
use App\Models\User_product;
use App\Models\Pricing_detail;
use App\Models\Order_detail;
use App\Models\Store;
use App\Models\User_cc;
use App\Models\Credit;
use App\Models\Credit_type;
use App\Models\Invoice;
use App\Models\Invoice_payment;
use App\Rules\ExpDateValidate;
use App\Rules\CcValidate;
use Carbon\Carbon;
use Crypt;
use Hash;

class transactionController extends Controller
{
    function walletlist(){
        return view('transaction.walletlist');
    }

    function add_credits(Request $request) {
        $resultid = Credit::create([
        'user_id' => Auth::user()->id,
        'add_credit' => $request->addbalance,
        'credit_type' => $request->credits_feetype,
        ]);
    }
    
    function add_cclist(Request $request) {
        $set_default = User_cc::where('user_id', '=', Auth::user()->id)->first();
        if ($set_default!=''){
            $set_default='0';
        } else {
            $set_default='1';
        }

     /* $request->validate([
            'cardno' => ['required',new CcValidate],
            'expdt' => ['required',new ExpDateValidate],
            'cvv' => 'required|integer',
        ]);

        $result = User_cc::where('user_id', '=', Auth::user()->id)
        //->where('cc',Crypt::encryptString(str_replace(" ","",$request->cardno)))
        ->first();

        if($result!=null && $result->id>0) {
            return response()->json('{"message":"Card already exist."}');
        }
        */
        $cno = $request->cardno;
        $cno = str_replace(' ','',$cno);
        $resultid = User_cc::create([
        'user_id' => Auth::user()->id,
        'cc' => $cno,
        'exp_mon' => $request->expdt,
//        'cc' => Crypt::encryptString(str_replace(" ","",$request->cardno)),
//        'exp_mon' => Crypt::encryptString($request->expdt),
        'cvv' => Crypt::encryptString($request->cvv),
        'default_cc' => $set_default,
        'package_id' => 0
        ]);
/*
        if($resultid>0) {
            return response()->json('{"status":"ok"}');
        } else {
            return response()->json('{"message":"Invalid request"}');
        }
*/

    }

    function view_cclist() {
        $list = User_cc::where('user_id', '=', Auth::user()->id)->get();
        $strOutput = '';
        foreach ($list as $item) {
            $number =  $item->cc;
            $cno =  str_pad(substr($number, -4), strlen($number), '*', STR_PAD_LEFT);

            $strOutput .= '<tr>
                <td><div class="avtar-pic w35" data-toggle="tooltip" data-placement="top" title="Subscription Payment"><span><img src="../assets/images/visa-card.png" data-toggle="tooltip" data-placement="top" title="Payment Logo" class="w35 h35 rounded"></span></div></td>
                <td>'.$cno.'</td>
                <td>'.$item->exp_mon.'</td>
                <td><button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="del_cc('.$item->id.')"><i class="fa fa-trash-o text-danger"></i></button></td>
                <td>
                ';
                if ($item->default_cc=='0') {
                    $strOutput .='<button type="button" class="btn btn-sm btn-default js-sweetalert" title="Set As Default" data-type="confirm" onclick="if(confirm(\'Are you sure to make it deafult?\')){set_default_cc('.$item->id.')}"><i class="fa fa-usd text-danger"></i></button>';
                }
                $strOutput .='
                </td>
                </tr>';
        }
        return $strOutput;
    }

    function delete_cc(Request $request) {
        $list = User_cc::where('user_id', '=', Auth::user()->id)->get();
        $no_of_cc = $list->count();
        if ($no_of_cc > 1) {
            $default_cc = User_cc::where('user_id', '=', Auth::user()->id)
            ->where('default_cc', '=', 1)
            ->first();
            if ($default_cc->id!=$request->id){
                $item = User_cc::where('user_id', '=', Auth::user()->id)
                ->where('id', '=', $request->id)
                ->delete();
                return 1;
            } else {
                return 0;
            }
        } else if ($no_of_cc == 1) {
            $item = User_cc::where('user_id', '=', Auth::user()->id)
            ->where('id', '=', $request->id)
            ->delete();
            return 1;
        }
    }

    function update_default_cc(Request $request) {
        $default_cc = User_cc::where('user_id', '=', Auth::user()->id)
        ->where('default_cc', '=', 1)
        ->first();
        $old_id=$default_cc->id;
        $new_id=$request->id;
        $this->set_default_cc($old_id,0);
        $this->set_default_cc($new_id,1);

    }
    private function set_default_cc($id,$val) {
        $result = User_cc::where('user_id', '=', Auth::user()->id)
        ->where('id', '=', $id)
        ->update(['default_cc' => $val]);
        echo $result;
    }


}
