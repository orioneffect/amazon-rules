<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class FeedbacksController extends BaseController
{
    function campaign(){
        return view('feedbacks.campaign');
    }
    function newcampaign(){
        return view('feedbacks.newcampaign');
    }
}
