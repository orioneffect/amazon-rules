<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;

class PagesController extends BaseController
{
    function blank(){
        return view('pages.blank');
    }
    function profile(){
        $user = Auth::user();

        $state_name = User::where('id', '=', $user->id)
        ->selectRaw('(select state_name from states where states.id=users.state) as state')
        ->first();

        $country = User::where('id', '=', $user->id)
        ->selectRaw('(select country_name from countries where countries.id=users.country) as country')
        ->first();

        $ship_name = User::where('id', '=', $user->id)
        ->selectRaw('(select company_name from shipping_companies where shipping_companies.id=users.shipping) as default_ship')
        ->first();

        $store_name = User::where('id', '=', $user->id)
        ->selectRaw('(select store_name from stores where stores.id=users.default_store_id) as default_store')
        ->first();

        $lan_name = User::where('id', '=', $user->id)
        ->selectRaw('(select language from languages where languages.id=users.default_language_id) as default_lan')
        ->first();

        $default_country_name = User::where('id', '=', $user->id)
        ->selectRaw('(select country_name from countries where countries.id=users.default_country_id) as default_country')
        ->first();

        $country = User::where('id', '=', $user->id)
        ->selectRaw('(select country_name from countries where countries.id=users.country) as country')
        ->first();

        $stores = User::where('id', '=', $user->id)
        ->selectRaw('(select count(1) from stores where stores.user_id=users.id) as store_count')
        ->first();
        
        $data =  array();
        $data['user']  =  $user;
        $data['stores']  =  $stores;
        $data['country']  =  $country;
        $data['default_country_name']  =  $default_country_name;
        $data['store_name']  =  $store_name;
        $data['lan_name']  =  $lan_name;
        $data['ship_name']  =  $ship_name;
        $data['country']  =  $country;
        $data['state_name']  =  $state_name;

        return view('pages.profile',compact('data'));
    }
    function userlist(){
        return view('pages.userlist');
    }
    function testimonials(){
        return view('pages.testimonials');
    }
    function invoices(){
        return view('pages.invoices');
    }
    function invoicedetails(){
        return view('pages.invoicedetails');
    }
    function timeline(){
        return view('pages.timeline');
    }
    function searchresults(){
        return view('pages.searchresults');
    }
    function gallery(){
        return view('pages.gallery');
    }
    function pricing(){
        return view('pages.pricing');
    }

    function updateprofileimg(Request $request) {
        if(isset($request->file)) {
            $request->validate([
                'file' => 'required|mimes:PNG,png,tiff,jpg,jpeg'
            ]);
            $user = Auth::user()->id;
            $fileName = $request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs("user_profile_img/$user", $fileName, 'public');
        } else {
            $filePath="";
        }
        User::where('id', '=', Auth::user()->id)
        ->update(['profileimg' => $filePath]);
        return redirect()->back();
    }      

}