<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Integration;
use \App\Models\Amazon_health_report;
use Cache;
use DateTime;
use Session;
use App\Models\Amazon_reports_type;
use App\Models\Order_note;
use App\Models\Amazon_report;
use App\Models\Column_userdisplay;
use App\Models\Column_display;
use App\Models\CommonFunction;
use App\Models\User_main_config;
use App\Models\User_type;
use Auth;
use DB;
use Exeption;

class AmazonController extends Controller
{

    function vieworder($orderid) {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        $orderInstance = new \App\Models\OrderModel;
        $vieworder = $orderInstance->vieworder($orderid);

        return $vieworder;

    }
    function new_vieworder(Request $request) {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }

        $vieworder = DB::table('orders')
        ->join('order_details', 'order_details.order_id', '=', 'orders.id')
        ->select('orders.*', 'order_details.*')
        ->where('orders.amazon_order_id','=', $request->orderid)
        ->where('orders.user_id',Auth::user()->id)
        ->where('orders.store_id',Session::get('storeid'))
        ->first();
        return response()->json($vieworder);
    }

    function listing() {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        $orderInstance = new \App\Models\AmazonListingModel;
        $orderInstance->createListing();

    }
    function feedreport() {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        $orderInstance = new \App\Models\AmazonListingModel;
        $orderInstance->getFeedReport();

    }

    function orders() {
        return view('amazon.orders');

    }
    public function orderPage()
    {
        $hide =  User_main_config::where('user_id', '=', Auth::user()->id)->first();

        $cons = Session::get('consultant');
        $TotalCount = DB::table('listings')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))->count();
        $ActiveCount = DB::table('listings')
        ->where('listings.status','Active')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))->count();
        $InactiveCount = DB::table('listings')
        ->where('listings.status','!=','Active')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))->count();
        $listing = DB::table('orders')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))
        ->whereRaw('listings.id in (select max(id) from listings l1 where l1.sku=listings.sku ) ')
        ->get();
    }
    public function ordersPagebk()
    {
        
        
        $orderInstance = new \App\Models\OrderModel;
        $Orders= $orderInstance->getOrders();


        $orderStr="";

        foreach ($Orders["orders"] as $order) {
            $dt=new DateTime($order["purchase_date"]);
            $orderAmount=$order["order_total"]['amount'] ?? '';
            $orderCurr=$order["order_total"]['currency_code'] ?? '';
            $orderStr.=' <tr>
            <td>'.$order["amazon_order_id"] .'</td>
            <td>'. $dt->format('Y-m-d H:i:s').'
            </td>
            <td>'. $order["order_status"] .'</td>
            <td class="text-center">'. ($order["number_of_items_shipped"] + $order["number_of_items_unshipped"]) .'</td>
            <td class="text-center">'. $orderAmount . " " . $orderCurr .'</td>
            </tr>';
        }

        $data =  array();
        $data['orderStr']  =  $orderStr;
        $data['nexttoken']  = $Orders["next_token"];
        return $data;
    }

    public function new_ordersPage(Request $request)
    {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }

        $duration = 0;
        $orderid = $request->orderid;
        $duration = $request->duration;
        $orderlist = DB::table('orders')
        ->leftjoin('order_details', [['order_details.order_id', '=', 'orders.amazon_order_id'],['order_details.user_id', '=', 'orders.user_id'],['order_details.store_id', '=', 'orders.store_id']])
        ->leftjoin('stores', [['stores.user_id', '=', 'orders.user_id'],['stores.id', '=', 'orders.store_id']])
        ->leftjoin('countries', 'countries.id', '=', 'stores.country_id')
        ->leftjoin('webscrap_products', 'order_details.asin', '=', 'webscrap_products.asin')
        ->select('countries.url_purchase','orders.user_id','orders.store_id','orders.amazon_order_id','orders.ordertotal_usd','orders.ordertotal','orders.currency_code','orders.shipment_service_level_category','orders.order_type','orders.buyername','orders.buyeremail','orders.address1','orders.address2','orders.municipality','orders.postal','orders.city',
        'orders.country','orders.shipping_id','orders.number_of_items_shipped','orders.number_of_items_unshipped','orders.purchasedate','orders.deliverydate','orders.latest_ship_date','orders.orderstatus','orders.is_replacement_order','order_details.item_id', 'order_details.sku','order_details.asin',
         'order_details.condition as orderitemcondition','order_details.order_tracking_id', 'order_details.purchase_tracking_id','order_details.purchase_status','order_details.quantity as orderitemquantity', 'order_details.price_usd as orderitempriceusd', 
         'order_details.price as orderitemprice', 'order_details.quantity_shipped as orderitemquantity_shipped', 'order_details.quantity_ordered as orderitemquantity_ordered',
          'order_details.cost as orderitemcost', 'order_details.profit as orderitemprofit','order_details.profit_percent as orderitemprofit_percent',
          'webscrap_products.product_imgurl as img','webscrap_products.package_length','webscrap_products.package_height','webscrap_products.package_width')
          ->selectRaw("CONCAT(webscrap_products.package_length, ' X ', webscrap_products.package_width, ' X ', webscrap_products.package_height, '  ', webscrap_products.package_dimension_units) as lbh")
        ->selectRaw("CONCAT(webscrap_products.package_weight, '  ', webscrap_products.package_weight_units) as weight")
        ->where('orders.user_id',Auth::user()->id)
        ->where('orders.store_id',Session::get('storeid'));
        
        if($orderid!='') {
            $orderlist = $orderlist->where('orders.amazon_order_id', 'LIKE', '%' . $orderid .'%');
        };

        $orderlist = $orderlist->whereDate('orders.created_at', '>=', now()->subDays($duration)->setTime(0, 0, 0)->toDateTimeString());
        //$orderlist= str_replace_array('?', $orderlist->getBindings(), $orderlist->toSql());
       // dd($orderlist);
        $orderlist = $orderlist->get();
        //dd($orderlist);
        $orderStr="";

        foreach ($orderlist as $order) {
            //dd($order);
            $dt=new DateTime($order->purchasedate);     
            $badgeClass="btn-success";
            $PurchasedText="Purchased";
            if($order->purchase_status=="false")
            {
                $badgeClass="btn-danger";
                $PurchasedText="NotPurchased"; 
            }
            $orderStr.='<tr><td><div class="d-flex align-items-center"><img src="' . $order->img . '" data-toggle="tooltip" data-placement="top" title="'.$order->asin.'" alt="'.$order->asin.'" class="w35 h35 rounded">
            <span class="badge '.$badgeClass.'">'.$order->orderitemquantity_ordered.' Piece</span><div class="ml-3"><p class="mb-0">ASIN:<b>'.$order->asin.'</b></p>
            <p class="mb-0">SKU:<b>'.$order->sku.'</b></p>
            <div class="progress progress-xxs"><div class="progress-bar bg-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div></div>
            <p class="mb-0">BUY-ID:<b>'.$order->order_tracking_id.'</b></p>
            <p class="mb-0">TRK-ID:<b>'.$order->purchase_tracking_id.'</b></p></div></div></td><td><div class="d-flex align-items-center"><div class=""><p>'.$order->shipment_service_level_category.'</p><a href="javascript:void(0)" onclick="openorder(\''.$order->amazon_order_id.'\',\''. $order->orderstatus .'\',\''.$dt->format("Y-m-d H:i:s") .'\',\''. $order->ordertotal .' '. $order->currency_code .'\')">'.$order->amazon_order_id.'</a><p>SellerCentral</p</div></div></td>';
            if ($order->orderstatus!="Canceled") {
                $orderStr.='<td>
            <div class="" style="width: 173px;">
                <div class="mb-2 form-control-sm pl-0">
                    <div class="input-group '.$badgeClass.' rounded-pill">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-white pl-1 pr-1">Sel:</span>
                        </div>
                        <label type="text" class="form-control text-white pl-1" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">NotConfirmed</label>
                    </div> 
                </div> 
                <div class="form-control-sm pl-0">  
                    <div class="input-group '.$badgeClass.' rounded-pill">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-white pl-1 pr-1">Buy:</span>
                        </div>
                        <label type="text" class="form-control text-white pl-1" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">'.$PurchasedText.'</label>
                    </div>
                </div><!--<a href="'. route('settings.showshipping') .'?orderid='.$order->amazon_order_id.'&store_id='.$order->store_id.'" target="_new">Show</a>--></td>';
            }
            else {
                $orderStr.='<td>N/A</td>';
            }
            $orderStr.='<td><div class="d-flex align-items-center"><div class="ml-3"><p class="mb-0">'.$order->orderitemprice.'<b> '.$order->currency_code.'</b></p>
            <p class="mb-0">'.$order->orderitempriceusd.'<b> USD</b></p></div></div></td>
            <td>'. $order->orderitemcost .'</td><td>'. $order->orderitemprofit .'</td><td>'. $order->orderitemprofit_percent .'</td>';
            $orderStr.='<td>'. CommonFunction::time_elapsed_string($order->purchasedate) .'<p>'.$order->purchasedate .'</p></td>
                <td>'. $order->orderstatus .'</td>
                <td class="text-left">'.CommonFunction::time_elapsed_string($order->latest_ship_date).'<p>'.$order->latest_ship_date .'</p></td>';
                /*<td class="text-left">'. ($order->number_of_items_shipped + $order->number_of_items_unshipped) .'</td>
                <td class="text-left">';
            if ($order->package_length!='' && $order->package_height!='' && $order->package_width !='' && $order->package_weight !='') {
                $orderStr.= $order->package_length.'&#xd7;'.$order->package_height.'&#xd7;'.$order->package_width.'&nbsp;['.$order->package_dimension_units.']<br>'.$order->package_weight.'&nbsp['.$order->package_weight_units.']';
            } else {
                $orderStr.= '';
            }
            $orderStr.='</td>
                <td></td>
                <td></td>
                <td>'. $order->ordertotal .' '. $order->currency_code .'</td>*/
                $orderStr.='<td><a target="_blank" href="'.$order->url_purchase.'/dp/'.$order->asin.'" class="btn btn-sm btn-primary btn-block" title="">Order Now</a></td></tr>';
        }
        if ($orderStr=='')
        {
            $orderStr="No Orders to display.";
        }


        return $orderStr;
    }


    function addhealthreport() {
        $reportInstance = new \App\Models\AmazonReportModel;
        $reportId = $reportInstance->getReport('GET_V2_SELLER_PERFORMANCE_REPORT',null,null);
        $amaznRptId = Amazon_report::create([
            'report_type_id' => 'GET_V2_SELLER_PERFORMANCE_REPORT',
            'from_date' => null,
            'to_date' => null,
            'user_id' => Auth::user()->id,
            'amazon_report_id'=> $reportId,
            'store_id'=> Session::get('storeid'),
        ])->id;
        return redirect('amazon/healthreport')->with('success','success');
    }


    function healthreport()
    {
        $amazonReport = DB::table('amazon_reports')
        ->where('amazon_reports.user_id',Auth::user()->id)
        ->where('amazon_reports.store_id',Session::get('storeid'))
        ->where('amazon_reports.report_type_id','GET_V2_SELLER_PERFORMANCE_REPORT')
        ->orderBy('id', 'desc')->first();

        if (isset($amazonReport)) {
            if($amazonReport->amazon_doc_id==null) {
                $reportInstance = new \App\Models\AmazonReportModel;
                $docId = $reportInstance->viewreport($amazonReport->amazon_report_id);

                if($docId!="") {
                    $updatedoc = Amazon_report::find($amazonReport->id);
                    $updatedoc->amazon_doc_id = $docId;
                    $updatedoc->status = 1;
                    $updatedoc->save();
                    $reportInstance = new \App\Models\AmazonReportModel;
                    $response = $reportInstance->viewdoc($docId);
                    $amznHealth = Amazon_health_report::create([
                        'user_id' => Auth::user()->id,
                        'report_content' => $response,
                        'amazon_report_id'=> $amazonReport->amazon_report_id,
                        'store_id'=> Session::get('storeid'),
                    ]);
                    $json = json_decode($amznHealth->report_content);
                    return view('amazon.health', ['metrix' => $json,'lastgeenrated_on'=>$amznHealth->created_at]);
                } else {
                    return redirect('amazon/healthreport')->with('in_progress','in_progress');
                }
            } else {
                $amznHealth = DB::table('amazon_health_reports')
                ->where('amazon_health_reports.user_id',Auth::user()->id)
                ->where('amazon_health_reports.store_id',Session::get('storeid'))
                ->orderBy('id', 'desc')->first();
                if (isset($amznHealth)) {
                    $json = json_decode($amznHealth->report_content);
                    return view('amazon.health', ['metrix' => $json,'lastgeenrated_on'=>$amznHealth->created_at]);
                } else {
                    return view('amazon.health', ['metrix' => null,'lastgeenrated_on'=>null]);
                }
            }
        } else {
            return view('amazon.health', ['metrix' => null,'lastgeenrated_on'=>null]);
        }
    }

    function addnewreport(Request $request) {
        $request->validate([
            'reporttype' => 'required',
        ]);

        $reportInstance = new \App\Models\AmazonReportModel;
        $reportId = $reportInstance->getReport($request->reporttype,$request->fromdt,$request->todt);

        $amaznRptId = Amazon_report::create([
            'report_type_id' => $request->reporttype,
            'from_date' => $request->fromdt,
            'to_date' => $request->todt,
            'user_id' => Auth::user()->id,
            'amazon_report_id'=> $reportId,
            'store_id'=> Session::get('storeid'),
        ])->id;

        //echo $amaznRptId;

        return redirect('amazon/reports')->with('success','success');

    }

    function viewdoc(Request $request) {
        $reportInstance = new \App\Models\AmazonReportModel;
        $response = $reportInstance->viewdoc($request->input('doc_id'));

        $data =  array();
        $data['response']  =  $response;
        $data['report_name']  =  $request->input('report_name');
        return view('amazon.viewreport',compact('data'));
    }


    function deletereport(Request $request) {

        $amaznRpt = Amazon_report::findorfail($request->input('id'));
        try {
            $reportInstance = new \App\Models\AmazonReportModel;
            $reportInstance->deleteReport($amaznRpt->amazon_report_id);
        } catch (Exception $e) {

        }

        $amaznRpt->delete(); //returns true/false

        return redirect('amazon/reports')->with('deleted','deleted');

    }

    function viewreport($reportId) {

        $amaznRpt = Amazon_report::findorfail($reportId);
        //print_r($amaznRptId->amazon_report_id);

        $reportInstance = new \App\Models\AmazonReportModel;
        $docId = $reportInstance->viewreport($amaznRpt->amazon_report_id);

        if($docId!="") {
            $amaznRpt->amazon_doc_id = $docId;
            $amaznRpt->status = 1;
            $amaznRpt->save();
            $reportInstance = new \App\Models\AmazonReportModel;
            $response = $reportInstance->viewdoc($docId);
            $data =  array();
            $data['response']  =  $response;
            $data['report_name']  =  $amaznRpt->report_name;
            return view('amazon.viewreport',compact('data'));

        } else {
            return redirect('amazon/reports')->with('in_progress','in_progress');
        }
    }

    function reports() {

        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }

        $reports = DB::table('amazon_reports')
        ->join('amazon_reports_types', 'amazon_reports_types.amazon_name', '=', 'amazon_reports.report_type_id')
        ->select('amazon_reports.*', 'amazon_reports_types.report_name')
        ->where('amazon_reports.user_id',Auth::user()->id)
        ->where('amazon_reports.store_id',Session::get('storeid'))
        ->where('amazon_reports.report_type_id','!=','GET_V2_SELLER_PERFORMANCE_REPORT')
        ->orderBy('amazon_reports.created_at','desc')
        ->get();


        $data =  array();

        $reporttypes = DB::table('amazon_reports_types')
        ->select('amazon_reports_types.*')
        ->where('amazon_reports_types.amazon_name','!=','GET_V2_SELLER_PERFORMANCE_REPORT')
        ->get();
        $data['report_types']  =  $reporttypes;
        $data['reports']  =  $reports;

        return view('amazon.reports', compact('data'));

    }

    function inventory() {

        /*if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        $inventoryInstance = new \App\Models\InventoryModel;
        $inventoryInstance->getInventory();*/

        $hide =  User_main_config::where('user_id', '=', Auth::user()->id)->first();

        $cons = Session::get('consultant');
        $TotalCount = DB::table('listings')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))->count();
        $ActiveCount = DB::table('listings')
        ->where('listings.status','Active')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))->count();
        $InactiveCount = DB::table('listings')
        ->where('listings.status','!=','Active')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))->count();
        $listing = DB::table('listings')
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))
        ->whereRaw('listings.id in (select max(id) from listings l1 where l1.sku=listings.sku ) ')
        ->get();
        //print_r(Auth::user()->id);echo "<br>";
        //print_r(Session::get('storeid'));
        //dd($listing);
        $filter_status =  Column_display::where('status', '=', 0)->orderBy('id', 'asc');

        if ($cons == 'yes') {

            if ($hide->asin == 1) {
                $filter_status =  $filter_status->whereRaw('inventory NOT IN ("asin")');
            }
            if ($hide->product_name == 1) {
                $filter_status =  $filter_status->whereRaw('inventory NOT IN ("product_name")');
            }
            
        }


        $filter_status =  $filter_status->get();

        $filter_statusarr = [];

        foreach ($filter_status as $filterstatus) {
            $filter_statusarr[$filterstatus["id"]] = $filterstatus["inventory"];
        }

        $filter_selected =  Column_userdisplay::where('user_id', '=', Auth::user()->id)->get();

        $data =  array();
        $data['listing']  =  $listing;
        $data['filter']  =  $filter_statusarr;
        $data['sfilter']  =  $filter_selected;
        $data['h_asin']  =  $hide->asin;
        $data['h_product_name']  =  $hide->product_name;
        $data['cons']  =  $cons;
        $data['TotalCount']  =  $TotalCount;
        $data['ActiveCount']  =  $ActiveCount;
        $data['InactiveCount']  =  $InactiveCount;

        //dd($data);
        return view('amazon.inventory',compact('data'));

    }
    function add_loadmsg(Request $request) {
        $msgStrnull = "No Messages Yet";
        if ($request->notes != '') {
            Order_note::create([
                'order_id' => $request->orderid,
                'note' => $request->notes,
                ]);        
        }
        $resultid = Order_note::where('order_id','=',$request->orderid)
        ->orderBy('note', 'desc')->get();

        if ($resultid != '') {
        
            $msgStr="";
            foreach ($resultid as $msg) {
                $msgStr.= $msg->created_at.'<br>'.$msg->note.'<hr/>';
            }
        
            return $msgStr;
        } else {
            return $msgStrnull;
        }

    }

    function load_filters(Request $request) {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        
        $filters = explode(",", $request->filter_status);

        $listings = DB::table('listings')
        ->leftJoin('webscrap_products', 'listings.asin', '=', 'webscrap_products.asin')
        ->leftJoin("user_products",function($join){
            $join->on("listings.sku","=","user_products.sku")
                ->on("user_products.user_id","=","listings.user_id");
        })
        //->leftJoin('user_products', 'listings.sku', '=', 'user_products.sku')
        ->leftJoin('stores','user_products.store_id', '=', 'stores.id')
        ->leftJoin('countries','stores.country_id', '=', 'countries.id')
       //->where('user_products.user_id',Auth::user()->id)
        ->where('listings.user_id',Auth::user()->id)
        ->where('listings.store_id',Session::get('storeid'))
        
        //->whereRaw('listings.id in (select max(id) from listings l1 where l1.sku=listings.sku ) ')      
        ->select('listings.*','webscrap_products.product_imgurl as img', 'user_products.target_price','countries.currency_code')
        ->selectRaw("CONCAT(webscrap_products.package_length, ' X ', webscrap_products.package_width, ' X ', webscrap_products.package_height, '  ', webscrap_products.package_dimension_units) as lbh")
        ->selectRaw("CONCAT(webscrap_products.package_weight, '  ', webscrap_products.package_weight_units) as weight")
        ->selectRaw("CONCAT(user_products.target_price, '  ', countries.currency_code) as target_price")
        ->selectRaw("CONCAT(COALESCE(listings.price,''), '  ', COALESCE(countries.currency_code,'')) as price");
        //->selectRaw("aaa as lbh");

       if($request->filterOn=="product_name")
       {
            $listings = $listings->where('listings.product_name', $request->filterCondition, $request->filterValue);
       }
       else if($request->filterOn=="asin")
       {
            $listings = $listings->where('listings.asin', $request->filterCondition, $request->filterValue);
       }
       else if($request->filterOn=="price")
       {
            $listings = $listings->where('listings.price', $request->filterCondition, $request->filterValue);
       }
       else if($request->filterOn=="quantity")
       {
            $listings = $listings->where('listings.quantity', $request->filterCondition, $request->filterValue); 
       }
       else if($request->filterOn=="weight")
       {
            $listings = $listings->where('webscrap_products.package_weight', $request->filterCondition, $request->filterValue); 
       }
       else if($request->filterOn=="status")
       {
            $listings = $listings->where('listings.status', $request->filterCondition, $request->filterValue);  
       }
       else if($request->filterOn=="created_at")
       {
            $listings = $listings->where('listings.created_at', $request->filterCondition, $request->filterValue); 
       }
       else if($request->filterOn=="updated_at")
       {
            $listings = $listings->where('listings.created_at', $request->filterCondition, $request->filterValue);
       }



        if($request->listing_status==1) {
            $listings = $listings->where('listings.status', '=', 'Active');
        } else if($request->listing_status==2) {
            $listings = $listings->where('listings.status', '=', 'Inactive');
        }

        $listings = $listings->get();
        //dd($listings);
        $strOutput = '';

        $strOutput .= '<table class="table table-striped table-hover dataTable js-exportable"><thead><tr>';
        
        foreach ($filters as $filter) {
            
            $strOutput .= '<th class="align-top">'. trans('amazon.'.$filter) .'</th>';
            
        }
        $strOutput .= '</tr></thead>';

        $strOutput .= '<tbody id="con_student_tbl">';
        
        foreach ($listings as $listing) {

            $strOutput .= '<tr class="align-top">';
            foreach ($filters as $filter) {
                if ($filter == 'img'){
                    $strOutput .= '<td class="align-top"><div class="float-left"><img src="' . $listing->$filter . '" class="w100"></div></td>';
                } else {
                    $strOutput .= '<td class="align-top text-wrap"><div class="float-left">'. $listing->$filter .'</div></td>';
                }
            }
            $strOutput .= '</tr>';

        }            

        $strOutput .= '</tbody></table>';
        
        return $strOutput;

    }
    function savefilters(Request $request) {
        
        $item = Column_userdisplay::where('user_id', Auth::user()->id)->get();
        if ($item!='') {
            Column_userdisplay::where('user_id', Auth::user()->id)->delete();
        }
        
        $listings = explode(",", $request->filter_status);
        
        foreach ($listings as $listing) {

            Column_userdisplay::create([
                'user_id' => Auth::user()->id,
                'column_txt' => $listing
            ]);

        }
    }
    function saveconfig(Request $request) {

        User_main_config::where('user_id', '=', Auth::user()->id)
        ->update([$request->col => $request->status]);
        
        $strOutput = 'Settings Updated';

        return $strOutput;
    }

    function loadcols(Request $request) {

        $columns =  Column_userdisplay::where('user_id', '=', Auth::user()->id)->get();
        
        //$cols = implode(',', $columns->column_txt);
        $cols = '';
        foreach ($columns as $column) {
            if ($cols=='') {
                $cols = $column->column_txt;
            } else {
                $cols = $cols .','. $column->column_txt;
            }
        }
        return $cols;
    }

    function inventoryhistory(Request $request) {
        if(Session::get('storeid')==null) {
            return redirect('integration/mystore')->with('error','error');
        }
        $duration = $request->duration;
        if ($duration == '') {
            $duration = 0;
        }
        $asin = $request->asin;
        if ($asin == '') {
            $asin = '';
        }

        $invent_history = DB::table('inventory_histories')
        ->join('stores', 'inventory_histories.store_id', '=', 'stores.id')
        ->select('inventory_histories.*','stores.store_name')
        ->where('inventory_histories.user_id', Auth::user()->id)
        ->where('inventory_histories.store_id', Session::get('storeid'))
        ->orderBy('inventory_histories.id', 'asc');


        if($asin!='') {
            $invent_history = $invent_history->where('inventory_histories.asin', 'LIKE', '%' . $asin .'%');
        };

        $invent_history = $invent_history->whereDate('inventory_histories.created_at', '>=', now()->subDays($duration)->setTime(0, 0, 0)->toDateTimeString());
        
        $invent_history = $invent_history->get();


        $data =  array();
        $data['invent_history']  =  $invent_history;
        $data['duration'] = $duration;
        $data['asin'] = $asin;

        return view('amazon.inventoryhistory',compact('data'));

    }    


}
