<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Supplier;
use Illuminate\Support\Facades\Storage;
use DB;
use Auth;
use DateTime;
use App\Models\Supplier_detail;
use Exception;
use App\Models\User_product;
use App\Models\Exchange_rate;
use App\Models\Order;
use App\Models\Store;
use App\Models\Order_detail;
use App\Models\User_feed;
use App\Models\Amazon_report;
use App\Models\Listing;
use App\Models\Inventory;
use App\Models\Batch_list;
use App\Models\Inventory_history;
use App\Models\OrderModel;
use Carbon\Carbon;
use App\Models\PricingCalc;
use App\Models\Webscrap_product;
use File;
class BatchController extends Controller
{

    function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }

    function updateinventory()
    {
            $table = 'stores';
            if(date("h")>env('REUESTINVENTORYREPORT_CRON_AT'))
            {
                if(env('AND_REUESTINVENTORYREPORT_CRON_AT')>=10)
                {
                    $BegningOfToday= date("Y-m-d ".env('AND_REUESTINVENTORYREPORT_CRON_AT')."::00:00");
                    $DateNow=date("Y-m-d ".env('AND_REUESTINVENTORYREPORT_CRON_AT')."::05:00");
                }
                else
                {
                    $BegningOfToday= date("Y-m-d 0".env('AND_REUESTINVENTORYREPORT_CRON_AT')."::00:00");
                    $DateNow=date("Y-m-d 0".env('AND_REUESTINVENTORYREPORT_CRON_AT')."::05:00");
                }
            }
            else
            {
                if(env('REUESTINVENTORYREPORT_CRON_AT')>=10)
                {
                    $BegningOfToday= date("Y-m-d ".env('REUESTINVENTORYREPORT_CRON_AT')."::00:00");
                    $DateNow=date("Y-m-d ".env('REUESTINVENTORYREPORT_CRON_AT')."::05:00");
                }
                else
                {
                    $BegningOfToday= date("Y-m-d 0".env('REUESTINVENTORYREPORT_CRON_AT')."::00:00");
                    $DateNow=date("Y-m-d 0".env('REUESTINVENTORYREPORT_CRON_AT')."::05:00");
                }
            }
            
            
            $list = DB::table($table)
            ->select('stores.id', 'stores.user_id')
            ->join('users', $table . '.user_id', '=', 'users.id')
            ->join('user_amazon_integrations', $table . '.id', '=', 'user_amazon_integrations.store_id')
            ->where('users.is_active', 1)
            ->where('user_amazon_integrations.admin_integration_id','!=', '')
            ->where(function($query) use ($BegningOfToday){
                $query->where('stores.last_sync_time','<', $BegningOfToday)
                    ->orWhere('stores.last_sync_time',null);
            })
            ->whereIn('users.type', [0, 1])
            ->groupBy('stores.user_id')
            ->orderBy('stores.id', 'asc')
            ->take(env('UPDATEINVENTORY_SELECT_STORES_IN_A_GO'))
            ->get();
            //dd($list);
        foreach ($list as $item) {

            $storeid = $item->id;
            $userid = $item->user_id;

            echo "Running for " . $storeid . "<br>\n";
            $DateNow= date('Y-m-d h:i:s');
            $reportTypeToRequest="GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT";
            $reportTypeToRequest="GET_MERCHANT_LISTINGS_ALL_DATA";
            $reportInstance = new \App\Models\AmazonReportModel;
            $reportId = $reportInstance->getBatchReport($reportTypeToRequest, $storeid);
            echo "reportId " . $reportId . "<br>\n";
            if(trim($reportId)!='')
            {
                $amaznRptId = Amazon_report::create([
                    'report_type_id' => $reportTypeToRequest,
                    'user_id' => $userid,
                    'amazon_report_id' => $reportId,
                    'reportType' => 'Inventory',
                    'store_id' => $storeid,
                    'admin_report' => 1,
                ])->id;
                Store::where('id', $storeid)->update(array('last_sync_time' => $DateNow)); 
            }
            
        }
    }

    /* Get All Those Reports not yet ready from amazon */
    function updateinventorydoc()
    {

        $table = 'amazon_reports';
        $list = DB::table($table)
            ->select('amazon_report_id', 'amazon_doc_id', 'store_id', 'user_id', 'id')
            ->where($table . '.status', 0)
            ->where($table . '.reportType', 'Inventory')
            ->where($table . '.admin_report', 1)
            ->take(env('UPDATEINVENTORYDOC_SELECT_REPORTS_IN_A_GO'))
            ->get();
        foreach ($list as $item) {

            $storeid = $item->store_id;
            $userid = $item->user_id;

            echo "Running for " . $storeid . "<br>\n";

            $reportInstance = new \App\Models\AmazonReportModel;
            $docId = $reportInstance->viewBatchReport($item->amazon_report_id, $storeid);
            if ($docId != "") {
                $fileData= $this->getInventoryDoc($docId,$storeid);
                $file = time() .rand(). '_'.$storeid.'.txt';
                $destinationPath=public_path()."/InventoryFiles/";
                if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
                File::put($destinationPath.$file,$fileData);
                $updatedoc = Amazon_report::find($item->id);
                $updatedoc->amazon_doc_id = $docId;
                $updatedoc->status = 1;
                $updatedoc->filePath = $destinationPath.$file;
                $updatedoc->admin_report = 2;
                $updatedoc->save();
            }
        }
    }
    function getInventoryDoc($amazon_doc_id,$storeid,$StopDecodeFile=true)
    {
        $reportInstance = new \App\Models\AmazonReportModel;
        return $response = $reportInstance->viewBatchDoc($amazon_doc_id, $storeid, $StopDecodeFile);
    }
    //Now Import The Inventory in the database
    function processInventoryDoc()
    {
        $this->ProcessInventoryDocUsingShell();
        exit;
        $table = 'amazon_reports';
        $list = DB::table($table)
            ->select('amazon_report_id', 'amazon_doc_id', 'store_id', 'user_id', 'id','filePath')
            ->where($table . '.status', 1)
            ->where($table . '.reportType', 'Inventory')
            ->where($table . '.admin_report', 2)
            ->orderBy($table. '.id', 'asc')
            ->take(env('PROCESSINVENTORYDOC_SELECT_DOC_IN_A_GO'))
            ->get();
            foreach ($list as $item) 
            {
                $UpdateTime= date('Y-m-d h:i:s');
                $storeid = $item->store_id;
                $userid = $item->user_id;
                $filePath = $item->filePath;

                /* //for report type GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT
                $res=DB::statement("LOAD DATA LOCAL INFILE '".$filePath."' INTO TABLE listings
                FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' IGNORE 1 LINES 
                (@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10,@col11,
                @col12,@col13,@col14,@col15,@col16,@col17,@col18,@col19,@col20,@col21,
                @col22,@col23,@col24,@col25,@col26) set user_id=".$userid.",store_id=".$storeid.",product_name=@col1,listing_id=@col3,sku=@col4,price=@col5,quantity=@col6,asin=@col17,product_id=@col23,status=IF(@col6>0,'Active','Inactive'),updated_at='".$UpdateTime."'");
                */
                DB::statement("CREATE TEMPORARY TABLE temporary_table LIKE listings");
                DB::statement("ALTER TABLE temporary_table DROP id ");
                $res=DB::statement("LOAD DATA LOCAL INFILE '".$filePath."' INTO TABLE temporary_table
                FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' IGNORE 1 LINES 
                (@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10,@col11,
                @col12,@col13,@col14,@col15,@col16,@col17,@col18,@col19,@col20,@col21,
                @col22,@col23,@col24,@col25,@col26,@col27,@col28) set user_id=".$userid.",store_id=".$storeid.",product_name=@col1,listing_id=@col3,sku=@col4,price=@col5,quantity=@col6,asin=@col17,product_id=@col23,status=@col28,updated_at='".$UpdateTime."'");
                DB::statement("INSERT INTO listings (user_id,store_id,product_name,listing_id,sku,price,quantity,asin,product_id,status,updated_at) 
                SELECT user_id,store_id,product_name,listing_id,sku,price,quantity,asin,product_id,status,updated_at FROM temporary_table
                ON DUPLICATE KEY UPDATE product_name = VALUES(product_name), listing_id = VALUES(listing_id), sku = VALUES(sku), price = VALUES(price), quantity = VALUES(quantity), asin = VALUES(asin), `status` = VALUES(`status`), `updated_at` = VALUES(`updated_at`)");
                DB::statement("DROP TEMPORARY TABLE temporary_table");
               //If Process Was Successfull
                if($res)
                {
                    $updatedoc = Amazon_report::find($item->id);
                    $updatedoc->admin_report = 3;
                    $updatedoc->fileProcessStatus='Success';
                    $updatedoc->save();

                    //Delete File After Successfully Processed
                    //File::delete($filePath);
                }
                else
                {
                    $updatedoc = Amazon_report::find($item->id);
                    $updatedoc->admin_report = 3;
                    $updatedoc->fileProcessStatus='Error';
                    $updatedoc->save();
                }
                
            }
        
    }
    function ProcessInventoryDocUsingShell()
    {
        $table = 'amazon_reports';
        $list = DB::table($table)
            ->select('amazon_report_id', 'amazon_doc_id', 'store_id', 'user_id', 'id','filePath')
            ->where($table . '.status', 1)
            ->where($table . '.reportType', 'Inventory')
            ->where($table . '.admin_report', 2)
            ->orderBy($table. '.id', 'asc')
            ->take(env('PROCESSINVENTORYDOC_SELECT_DOC_IN_A_GO'))
            ->get();
            foreach ($list as $item) 
            {
                $UpdateTime= date('Y-m-d h:i:s');
                $storeid = $item->store_id;
                $userid = $item->user_id;
                $filePath = $item->filePath;
                $totalRecordsInFile= $this->parseCSVShellTotalRecords($filePath);
                //dd(env('PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO'));
                $dataInserted=false;
                for($i=1;$i<=ceil($totalRecordsInFile/env('PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO'));$i++)
                {
                    $dataReS=$this->shellParseFile($filePath,$i);
                    if($dataReS['status']=="Success")
                    {
                        
                        
                        foreach($dataReS['data'] as $keyin=>$valin)
                        {
                            $dataRe=$valin;
                            $dataInserted=true;
                            $productname=$dataRe['itemname'];
                            $listingid=$dataRe['listingid'];
                            $quantity=$dataRe['quantity'];
                            $price=$dataRe['price'];
                            $asin=$dataRe['asin1'];
                            $product_id=$dataRe['productid'];
                            $status=$dataRe['status'];
                            $sku=$dataRe['sellersku'];
                            $matchThese = ['user_id'=>$userid,'store_id'=>$storeid,"sku"=>$sku];
                            /*$newUser = Listing::updateOrCreate([
                                'user_id'   => $userid,
                                'store_id'     => $storeid,
                                'sku' => $sku,
                                'product_name'    => $productname,
                                'listing_id'   => $listingid,
                                'quantity'       => $quantity,
                                'price'   => $price,
                                'asin'    => $asin,
                                'product_id'    => $product_id,
                                'status'    => $status,
                                'updated_at'    => $UpdateTime
                            ]);*/
                            //DB::enableQueryLog();
                            Listing::updateOrCreate($matchThese,['product_name'=>$productname,'listing_id'=>$listingid,
                            'quantity'=>$quantity,'price'=>$price,'asin'=>$asin,'product_id'=>$product_id,
                            'status'=>$status,'updated_at'=>$UpdateTime]);
                            //$quries = DB::getQueryLog();
                            //dd($quries);

                        }
                    }
                    
                }
                if($dataInserted)
                {
                    $updatedoc = Amazon_report::find($item->id);
                    $updatedoc->admin_report = 3;
                    $updatedoc->fileProcessStatus='Success';
                    $updatedoc->save();
                }
                else
                {
                    $updatedoc = Amazon_report::find($item->id);
                    $updatedoc->admin_report = 3;
                    $updatedoc->fileProcessStatus='Error';
                    $updatedoc->save();
                }
                
            }
    }
    function shellParseFile($filename,$beg)
    {
        global $amazon_db_connection;
        $PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO=env('PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO');
        if($beg<=1)
        {
            $beg=2;
        }
        else
        {
            $beg=$beg-1;
            $beg= $beg*$PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO;
        }
        $end= $beg + $PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO;
        $end=$beg+$PROCESSINVENTORYDOC_SELECT_NO_ROWS_IN_A_GO;
    
        # first header row handled
        $firstRow = 'sed -n 1,1p '.$filename;
        $header = shell_exec($firstRow);
        $headerData=(explode(PHP_EOL,rtrim($header,PHP_EOL)));
        if(empty($headerData[0]))
        return array('status'=>'Error','data'=>'No data found in the file');
    
        # rest rows with respect to beg and end
        $cmd = 'sed -n '.$beg.','.$end.'p '.$filename;
        $wcoutput = shell_exec($cmd);
        $output=(explode(PHP_EOL,rtrim($wcoutput,PHP_EOL)));
        $responseArray=array();
        
        $responseArray=array();
        //$header=preg_split("/\t+/", $headerData[0]);
        $header=str_getcsv($headerData[0], "\t");
        //$header=explode('\t',$headerData[0]);
       // exit;
        //  dd($headerData[0]);

        foreach($output as $k=>$row)
        {
            $TemArray=array();
            //$data=preg_split("/\t+/", $row);
            $data=str_getcsv($row, "\t");
            foreach($header as $key=>$val)
            {
                $val=preg_replace('/[^A-Za-z0-9\-]/', '', $val);
                $TemArray[trim(str_replace("-","",$val))]=$data[$key];
            }
            $responseArray[]=$TemArray;
        }
        return array('status'=>'Success','data'=>$responseArray);
    }
    function parseCSVShellTotalRecords($FilePath){
       $command = "wc -l < '".$FilePath."'";
       $result = (int)shell_exec($command);
       return $result;
   } 
    function updateinventorytable()
    {

        $table = 'amazon_reports';
        $list = DB::table($table)
            ->select('amazon_report_id', 'amazon_doc_id', 'store_id', 'user_id', 'id')
            ->where($table . '.status', 1)
            ->where($table . '.admin_report', 2)
            ->take(5)
            ->get();
        foreach ($list as $item) {

            $storeid = $item->store_id;
            $userid = $item->user_id;

            echo "Running for " . $storeid . "<br>";

            $reportInstance = new \App\Models\AmazonReportModel;
            $response = $reportInstance->viewBatchDoc($item->amazon_doc_id, $storeid, false);

            //echo $response;

            $i = 1;

            foreach (explode('***', $response) as $info) {
                if ($i == 1) {
                } else {

                    $data = explode('^^^', $info);
                    print_r($data);
                    echo "<br>";
                    if (count($data) > 20) {

                        $productname = $data[0];
                        $listingid = $data[2];
                        $sku = $data[3];
                        $price = $data[4];
                        $quantity = $data[5];
                        $asin = $data[16];
                        $productid = $data[22];
                        if ($quantity > 0) {
                            $status = "Active";
                        } else {
                            $status = "Inactive";
                        }

                        $listing = Listing::create([
                            'user_id' => $userid,
                            'store_id' => $storeid,
                            'product_name' => $productname,
                            'listing_id' => $listingid,
                            'sku' => $sku,
                            'quantity' => $quantity,
                            'price' => $price,
                            'asin' => $asin,
                            'product_id' => $productid,
                            'status' => $status,
                        ])->id;
                    }
                }

                $i++;
            }

            if ($response != "") {
                $updatedoc = Amazon_report::find($item->id);
                $updatedoc->admin_report = 3;
                $updatedoc->save();
            }
        }
    }


    function pushtoamazon()
    {
        $table = 'inventories';
        $list = DB::table($table)
            ->join('stores', $table . '.store_id', '=', 'stores.id')
            ->join('user_amazon_integrations', $table . '.store_id', '=', 'user_amazon_integrations.store_id')
            ->select($table . '.sku', $table . '.id', $table . '.asin', $table . '.store_id', 'stores.merchant')
            ->where($table . '.status', 0)
            ->whereNotNull('stores.merchant')
            ->orderBy('stores.id', 'asc')
            ->get();

        $merchant = "";
        $prevStore = "";
        $apiconfig = "";
        $currStore = "";
        $messgaestr = "";

        foreach ($list as $item) {

            $messageid = $item->id;
            $asin = $item->asin;
            //$sku = $item->sku;
            //$next_sku = $item->last_sku + 1;
            //$finalsku = $sku . "-" . $next_sku;
            $finalsku = $item->sku;


            $currStore = $item->store_id;
            $merchant = $item->merchant;
            if ($prevStore == "" || $prevStore != $currStore) {
                //$apiconfig = \App\Models\AmazonApiconnector::getBatchConfig($currStore);

                //$this->createListing($apiconfig,$messgaestr,$merchant,'Product','POST_PRODUCT_DATA',$storeid);
                //echo "<pre>".$messgaestr."</pre>";
                //$messgaestr = "";
                //$messageid = 0;
            }

            //echo $id;

            $messgaestr .= "<Message>
                <MessageID>" . $messageid . "</MessageID>
                <OperationType>Update</OperationType>
                <Product>
                    <SKU>" . $finalsku . "</SKU>
                    <StandardProductID>
                        <Type>ASIN</Type>
                        <Value>" . $asin . "</Value>
                    </StandardProductID>
                    <Condition>
                        <ConditionType>New</ConditionType>
                    </Condition>
                </Product>
            </Message>";

            $prevStore = $currStore;

            Inventory::where('id', $item->id)
                ->update(['status' => 1]);
        }

        echo $messgaestr;
        if ($currStore != "") {

            echo "pushing...";

            $apiconfig = \App\Models\AmazonApiconnector::getBatchConfig($currStore);
            $this->createListing($apiconfig, $messgaestr, $merchant, 'Product', 'POST_PRODUCT_DATA', $currStore);
        }
    }

    function pushtoamazoninventory()
    {
        $table = 'inventories';
        $list = DB::table($table)
            ->join('stores', $table . '.store_id', '=', 'stores.id')
            ->join('user_amazon_integrations', $table . '.store_id', '=', 'user_amazon_integrations.store_id')
            ->select($table . '.id', $table . '.asin', $table . '.store_id', $table . '.sku', $table . '.stock', 'stores.merchant')
            ->where($table . '.status', 1)
            ->whereNotNull('stores.merchant')
            ->orderBy('stores.id', 'asc')
            ->get();

        $merchant = "";
        $prevStore = "";
        $apiconfig = "";
        $messgaestr = "";
        foreach ($list as $item) {

            $messageid = $item->id;
            $sku = $item->sku;
            $id = $item->id;
            $currStore = $item->store_id;
            $stock = $item->stock;

            echo "<br>Running for " . $sku;

            $messgaestr .= "<Message>
                <MessageID>" . $messageid . "</MessageID>
                <OperationType>Update</OperationType>
                <Inventory>
                      <SKU>" . $sku . "</SKU>
                      <Quantity>" . $stock . "</Quantity>
                      <FulfillmentLatency>2</FulfillmentLatency>
                    </Inventory>
            </Message>";

            $merchant = $item->merchant;
            if ($prevStore == "" || $prevStore != $currStore) {
                //$apiconfig = \App\Models\AmazonApiconnector::getBatchConfig($currStore);

                //$this->createListing($apiconfig,$messgaestr,$merchant,'Inventory','POST_INVENTORY_AVAILABILITY_DATA',$currStore);
                //echo "<pre>".$messgaestr."</pre>";
                //$messgaestr = "";
                //$messageid = 0;
            }
            $prevStore = $currStore;

            Inventory::where('id', $item->id)
                ->update(['status' => 2]);
        }

        $apiconfig = \App\Models\AmazonApiconnector::getBatchConfig($currStore);
        $this->createListing($apiconfig, $messgaestr, $merchant, 'Inventory', 'POST_INVENTORY_AVAILABILITY_DATA', $currStore);
    }


    function pushtoamazonprice()
    {
        $table = 'inventories';
        $list = DB::table($table)
            ->select($table . '.id', $table . '.asin', $table . '.store_id', $table . '.sku', 'stores.merchant', 'min_price')
            ->selectRaw('(select countries.currency_code from countries where stores.country_id=countries.id ) as currency_code ')
            ->join('stores', $table . '.store_id', '=', 'stores.id')
            ->join('user_amazon_integrations', $table . '.store_id', '=', 'user_amazon_integrations.store_id')
            //->join('stores', $table . '.country_id', '=', 'countries.id')
            ->where($table . '.status', 2)
            ->whereNotNull('stores.merchant')
            ->orderBy('stores.id', 'asc')
            ->get();

        $merchant = "";
        $prevStore = "";
        $apiconfig = "";
        $messgaestr = "";
        foreach ($list as $item) {

            $messageid = $item->id;
            $sku = $item->sku;
            $currStore = $item->store_id;
            $target_price = $item->min_price;
            $currency_code = $item->currency_code;

            //echo $currency_code;

            $messgaestr .= "<Message>
                <MessageID>" . $messageid . "</MessageID>
                <OperationType>Update</OperationType>
                     <Price>
                        <SKU>" . $sku . "</SKU>
                        <StandardPrice currency=\"" . $currency_code . "\">" . $target_price . "</StandardPrice>
                    </Price>
                </Message>";

            $merchant = $item->merchant;
            if ($prevStore == "" || $prevStore != $currStore) {
                //$apiconfig = \App\Models\AmazonApiconnector::getBatchConfig($currStore);

                //$this->createListing($apiconfig,$messgaestr,$merchant,'Inventory','POST_INVENTORY_AVAILABILITY_DATA',$currStore);
                //echo "<pre>".$messgaestr."</pre>";
                //$messgaestr = "";
                //$messageid = 0;
            }
            $prevStore = $currStore;

            Inventory::where('id', $item->id)
                ->update(['status' => 3]);
        }

        $apiconfig = \App\Models\AmazonApiconnector::getBatchConfig($currStore);
        $this->createListing($apiconfig, $messgaestr, $merchant, 'Price', 'POST_PRODUCT_PRICING_DATA', $currStore);
    }


    public function createListing($config, $content, $merchant, $feedtype, $feedapitype, $storeid)
    {


        try {
            $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\FeedsApi($config);
            $contentType = 'text/xml; charset=UTF-8'; // please pay attention here, the content_type will be used many time

            $body = '
            {
                "contentType": ' . $contentType . '
            }';

            $feedDocument = $apiInstance->createFeedDocument($body);


            $feedDocumentId = $feedDocument->getPayload()->getFeedDocumentId();

            echo $feedDocumentId;

            $xmlcontent = '<?xml version="1.0" encoding="iso-8859-1"?>
                <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
                  <Header>
                    <DocumentVersion>1.01</DocumentVersion>
                    <MerchantIdentifier>' . $merchant . '</MerchantIdentifier>
                  </Header>
                <MessageType>' . $feedtype . '</MessageType>
                ' . $content . '
                </AmazonEnvelope>';


            $url = $feedDocument->getPayload()->getUrl();
            $key = $feedDocument->getPayload()->getEncryptionDetails()->getKey();
            $key = base64_decode($key, true);
            $initializationVector = base64_decode($feedDocument->getPayload()->getEncryptionDetails()->getInitializationVector(), true);
            $encryptedFeedData = openssl_encrypt(utf8_encode(
                $xmlcontent
            ), 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $initializationVector);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_POSTFIELDS => $encryptedFeedData,
                CURLOPT_HTTPHEADER => [
                    'Accept: application/xml',
                    'Content-Type: ' . $contentType,
                ],
            ));

            $response = curl_exec($curl);
            $error = curl_error($curl);
            $httpcode = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);

            echo "httpcode>>" . $httpcode;


            if ($httpcode >= 200 && $httpcode <= 299) {

                echo $config->getMarketplaceid();

                $createFeedParams = [
                    //"feedType" => "POST_INVENTORY_AVAILABILITY_DATA",
                    //"feedType" => "POST_PRODUCT_PRICING_DATA",
                    //"feedType" => "POST_PRODUCT_DATA",
                    "feedType" => $feedapitype,
                    //"marketplaceIds" => ["A33AVAJ2PDY3EV"],
                    "marketplaceIds" => [$config->getMarketplaceid()],
                    "inputFeedDocumentId" => $feedDocumentId
                ];

                print_r($createFeedParams);

                $r = $apiInstance->createFeed(json_encode($createFeedParams));
                print_r($r);


                $feedid = $r->getPayload()->getFeedId();

                echo $feedid;

                User_feed::create([
                    'store_id' => $storeid,
                    'feed_document_id' => $feedDocumentId,
                    'feed_id' => $feedid,
                    'feed_type' => $feedapitype,
                    'xml_content' => $xmlcontent,
                    'status' => 1
                ]);

                // success
            } else {
                //print_r($e);
                // error
            }
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    function suppliers()
    {
        $suppliers =  Supplier::all()->where("status", "=", "0")->first();
        if ($suppliers != null) {
            $Contents = $this->curl_get_file_contents($suppliers->xml_link);
            Storage::disk('local')->put("/suppliers/" . $suppliers->id . ".xml", $Contents);
            $suppliers->status = 1;
            $suppliers->save();
        }
    }

    function processSuppliers()
    {
        $suppliers =  Supplier::all()->where("status", "=", "1")->first();
        if ($suppliers != null) {
            $contents = Storage::disk('local')->get("/suppliers/" . $suppliers->id . ".xml");
            $xmlObject = simplexml_load_string($contents, 'SimpleXMLElement', LIBXML_NOCDATA);
            $json = json_encode($xmlObject);
            $phpDataArray = json_decode($json, true);
            foreach ($phpDataArray["urun"] as $prodctskey => $product) {
                try {
                    Supplier_detail::create([
                        'supplier_id' => $suppliers->id,
                        'product' => $product["isim"],
                        'sku' => ' ',
                        'name' => ' ',
                        'additionalimages' => $product["resim"],
                        'description' => $product["details"],
                        'price' => $product["price2"],
                        'shipping_day' => $product["shippingDay"],
                        'kdv' => ' ',
                        'quantity' => $product["stockAmount"],
                        'product_category' => $product["kategori"],
                        'variants' => $product["kategori_id"],
                        'barcode' => ' ',
                        'status' => $product["status"],
                    ]);
                } catch (Exception $e) {
                    echo "Exception on product ID " . $product["id"] . " <br>";
                }
            }
            $suppliers->status = 2;
            $suppliers->save();
        } else {
            echo "Nothing found!";
        }
    }
    
    public function updateexchangerate()
    {
        $url = 'https://api.exchangerate.host/latest?base=USD';

        $response = $this->curl_get_file_contents($url);
        $newsData = json_decode($response);

        $rates = $newsData->rates;
        foreach ($rates as $key => $value) {
            $val = number_format($value, 6) . "";
            $val_change = str_replace(',', '', $val);

            if ($key != "TRY") {

                Exchange_rate::create([
                    'curr_code' => $key,
                    'curr_value' => $val_change
                ]);
            }
        }
    }
    public function updateordersS()
    {
        if(date("h")>env('ORDERDOWNLOAD_CRON_AT'))
        {
            if(env('ORDERDOWNLOAD_CRON_AT')>=10)
            {
                $BegningOfToday= date("Y-m-d ".env('AND_ORDERDOWNLOAD_CRON_AT')."::00:00");
                $DateNow=date("Y-m-d ".env('AND_ORDERDOWNLOAD_CRON_AT')."::05:00");
            }
            else
            {
                $BegningOfToday= date("Y-m-d 0".env('AND_ORDERDOWNLOAD_CRON_AT')."::00:00");
                $DateNow=date("Y-m-d 0".env('AND_ORDERDOWNLOAD_CRON_AT')."::05:00");
            }
        }
        else
        {
            if(env('ORDERDOWNLOAD_CRON_AT')>=10)
            {
                $BegningOfToday= date("Y-m-d ".env('ORDERDOWNLOAD_CRON_AT')."::00:00");
                $DateNow=date("Y-m-d ".env('ORDERDOWNLOAD_CRON_AT')."::05:00");
            }
            else
            {
                $BegningOfToday= date("Y-m-d 0".env('ORDERDOWNLOAD_CRON_AT')."::00:00");
                $DateNow=date("Y-m-d 0".env('ORDERDOWNLOAD_CRON_AT')."::05:00");
            }
        }
        $table = 'stores';
        $list = DB::table($table)
        ->select('stores.id', 'stores.user_id')
        ->selectRaw('DATE_FORMAT(stores.orders_last_updated_at,"%Y-%m-%d") as orders_last_updated_at')
        ->join('users', $table . '.user_id', '=', 'users.id')
        ->join('user_amazon_integrations', $table . '.id', '=', 'user_amazon_integrations.store_id')
        ->where('users.is_active', 1)
        ->where('user_amazon_integrations.admin_integration_id','!=', '')
        ->where('stores.orders_last_updated_today','=', 0)
        ->where(function($query) use ($BegningOfToday){
            $query->where('stores.orders_last_updated_at','<', $BegningOfToday)
                ->orWhere('stores.orders_last_updated_at',null);
        })
        ->whereIn('users.type', [0, 1])
        //->groupBy('stores.user_id')
        ->orderBy('stores.id', 'asc')
        ->take(env('UPDATEINVENTORY_SELECT_STORES_IN_A_GO'))
        ->get();
        $updateOrderItems=array();
        $updateOrder=array();
        $totalOrder=0;
        $totalOrderItems=0;
        //dd(count($list));
        if(count($list)<=0)
        {
            Store::where('orders_last_updated_today', 1)->update(array('orders_last_updated_today'=>0));
        }
        foreach ($list as $item) {
            $DateNow= date('Y-m-d h:i:s');
            $storeid = $item->id;
            $userid = $item->user_id;
            $lastup = $item->orders_last_updated_at;
            if($lastup==null)
            {
                $date = new DateTime(); // For today/now, don't pass an arg.
                $date->modify("-30 day");
                $lastup=$date->format("Y-m-d");
                $lastup="";
            }
            echo "Running Store= " . $storeid . " For Order Sync<br>\n";
            $Orders =  OrderModel::getOrders($storeid, $lastup);
            $exchangeRate=array();
            //dd($Orders);
            foreach ($Orders["orders"] as $order) {
               // echo "<pre>";
               // print_r($order);
               // echo "</pre>";
                $orderid = $order["amazon_order_id"];
                $orderCurrency = isset($order["order_total"]['currency_code'])?($order["order_total"]['currency_code']==""?0:$order["order_total"]['currency_code']):0;
                $order_total=isset($order["order_total"]['amount'])?($order["order_total"]['amount']==""?0:$order["order_total"]['amount']):0;
                if($order_total>0)
                {
                    if(!isset($exchangeRate[$orderCurrency]))
                    {
                        $curr_value = Exchange_rate::where('curr_code', '=', $orderCurrency)
                                ->orderBy('created_at', 'desc')
                                ->first();
                        $exchangeRate[$orderCurrency]=$curr_value->curr_value;
                    }
                    $order_total_usd = $order_total * $exchangeRate[$orderCurrency];
                    $ordertotal_usd = round($order_total_usd, 2);
                }
                else
                {
                    continue;//Order is Canceled With No Data
                }
                
                $Order_items =  OrderModel::vieworderS($orderid, $storeid);
                $updateOrder[]=array(
                    'user_id' => $userid,
                    'store_id' => $storeid,
                    'amazon_order_id' => $order["amazon_order_id"],
                    'purchasedate' => new DateTime($order["purchase_date"]) ?? '0000-00-00 00:00:00',
                    'orderstatus' => $order["order_status"],
                    'ordertotal' => $order["order_total"]['amount'] ?? '0.00',
                    'currency_code' => $order["order_total"]['currency_code'] ?? '0.00',
                    'number_of_items_shipped' => $order["number_of_items_shipped"],
                    'deliverydate' => new DateTime($order["latest_delivery_date"]) ?? '0000-00-00 00:00:00',
                    'shipping_id' => $order["fulfillment_channel"],
                    'latest_ship_date' => new DateTime($order["latest_ship_date"]) ?? '0000-00-00 00:00:00',
                    'is_replacement_order' => $order["is_replacement_order"],
                    'number_of_items_unshipped' => $order["number_of_items_unshipped"],
                    'shipment_service_level_category' => $order["shipment_service_level_category"],
                    'order_type' => $order["order_type"],
                    'fulfillment_channel' => $order["fulfillment_channel"],
                    'marketplace_id' => $order["marketplace_id"],
                    'buyername' => $Order_items["buyername"] ?? '',
                    'buyeremail' => $Order_items["buyeremail"] ?? '',
                    'address1' => $Order_items["address1"] ?? '',
                    'address2' => $Order_items["address2"] ?? '',
                    'municipality' => $Order_items["municipality"] ?? '',
                    'postal' => $Order_items["postalcode"] ?? '',
                    'city' => $Order_items["city"] ?? '',
                    'country' => $Order_items["country"] ?? '',
                    'ordertotal_usd' => $ordertotal_usd ?? '0.00'
                    );
                
                foreach($Order_items['order_items'] as $itemKey=>$itemValue)
                {
                    //echo "<pre>";
                   // print_r($itemValue);
                   // echo "</pre>";
                    $price=($itemValue['item_price']->getAmount() == '') ? 0 : $itemValue['item_price']->getAmount();
                    $priceusd = $price * $exchangeRate[$orderCurrency];
                    $price_usd = round($priceusd, 2);
                    $totalOrderItems++;
                    
                    $updateOrderItems[]=array(
                    'user_id'=>$userid,
                    'store_id'=>$storeid,
                    'order_id'=>$orderid,
                    'item_id' => ($itemValue->getOrderItemId() == '') ? '' : $itemValue->getOrderItemId(),
                    'asin' => ($itemValue->getASIN() == '') ? '' : $itemValue->getASIN(),
                    'quantity' => ($itemValue['product_info']->getNumberOfItems() == '' || $itemValue['product_info']->getNumberOfItems() == null) ? $itemValue->getQuantityShipped() : $itemValue['product_info']->getNumberOfItems(),
                    'sku' => ($itemValue->getSellerSKU() == '') ? '' : $itemValue->getSellerSKU(),
                    'producttitle' => ($itemValue->getTitle() == '') ? '' : $itemValue->getTitle(),
                    'condition' => ($itemValue->getConditionId() == '') ? '' : $itemValue->getConditionId(),
                    'price_usd' => $price_usd ?? '0.00',
                    'price' => $price ?? '0.00',
                    'condition_id'=> ($itemValue->getConditionId() == '' || $itemValue->getConditionId() == null) ? '' : $itemValue->getConditionId(),
                    'quantity_ordered' => ($itemValue->getQuantityShipped() == '' || $itemValue->getQuantityShipped() == null) ? 0 : $itemValue->getQuantityShipped(),
                    'quantity_shipped' => ($itemValue->getQuantityOrdered() == '' || $itemValue->getQuantityOrdered() == null) ? 0 : $itemValue->getQuantityOrdered()
                    );
                    if($totalOrderItems>=env('UPDATEORDERITEMS_INSERT_OR_UPDATE_A_GO'))
                    {
                        Order_detail::upsert($updateOrderItems, ['user_id', 'store_id','order_id','item_id','sku'], ['asin','quantity','producttitle','condition','price_usd','price','quantity_ordered','quantity_shipped','condition_id']);
                        $updateOrderItems=array();
                        $totalOrderItems=0;
                    }
                }
                $totalOrder++;
                if($totalOrder>=env('UPDATEORDER_INSERT_OR_UPDATE_A_GO'))
                {
                    Order::upsert($updateOrder, ['user_id', 'store_id','amazon_order_id'], ['purchasedate','orderstatus','ordertotal','currency_code','number_of_items_shipped','deliverydate','shipping_id','latest_ship_date','is_replacement_order','number_of_items_unshipped','shipment_service_level_category','order_type','fulfillment_channel','marketplace_id','buyername','buyeremail','address1','address2','municipality','postal','city','country','ordertotal_usd']);
                    $updateOrder=array();
                    $totalOrder=0;
                }
            }
            Store::where('id', $storeid)->update(array('orders_last_updated_at' => $DateNow,'orders_last_updated_today'=>1));
        }
        if(!empty($updateOrder))
        {
            Order::upsert($updateOrder, ['user_id', 'store_id','amazon_order_id'], ['purchasedate','orderstatus','ordertotal','currency_code','number_of_items_shipped','deliverydate','shipping_id','latest_ship_date','is_replacement_order','number_of_items_unshipped','shipment_service_level_category','order_type','fulfillment_channel','marketplace_id','buyername','buyeremail','address1','address2','municipality','postal','city','country','ordertotal_usd']);
            $updateOrder=array();
            $totalOrder=0;
        }
        if(!empty($updateOrderItems))
        {
            Order_detail::upsert($updateOrderItems, ['user_id', 'store_id','order_id','item_id','sku'], ['asin','quantity','producttitle','condition','price_usd','price','quantity_ordered','quantity_shipped','condition_id']);
            $updateOrderItems=array();
            $totalOrderItems=0;
        }
    }
    public function updateorders()
    {
        $table = 'users';
        $list = DB::table($table)
            ->select('stores.id', 'stores.user_id', 'stores.store_name')
            ->selectRaw('DATE_FORMAT(stores.last_updated_at,"%Y-%m-%d") as last_updated_at')
            ->join('stores', $table . '.id', '=', 'stores.user_id')
            ->where($table . '.is_active', 1)
            //->whereIn($table . '.type', [0, 1])
            ->whereRaw('EXISTS (select 1 from user_amazon_integrations where user_amazon_integrations.store_id=stores.id and user_amazon_integrations.admin_integration_id>0)')
            ->orderBy('stores.last_updated_at', 'asc')
            ->take(1)
            ->get();

        foreach ($list as $store) {
            $storeid = $store->id;
            $userid = $store->user_id;


            echo "Running for " . $store->store_name . "<br>";

            $lastup = $store->last_updated_at;
            $Orders =  OrderModel::getOrders($storeid, $lastup);

            Store::where('id', '=', $storeid)
                ->update([
                    'last_updated_at' => now()
                ]);

            foreach ($Orders["orders"] as $order) {
                $duplicate = Order::where('amazon_order_id', '=', $order["amazon_order_id"])->first();
                if (is_null($duplicate)) {

                    $add_orders = Order::create([
                        'user_id' => $userid,
                        'store_id' => $storeid,
                        'amazon_order_id' => $order["amazon_order_id"],
                        'purchasedate' => new DateTime($order["purchase_date"]) ?? '0000-00-00 00:00:00',
                        'orderstatus' => $order["order_status"],
                        'ordertotal' => $order["order_total"]['amount'] ?? '0.00',
                        'currency_code' => $order["order_total"]['currency_code'] ?? '0.00',
                        'number_of_items_shipped' => $order["number_of_items_shipped"],
                        'deliverydate' => new DateTime($order["latest_delivery_date"]) ?? '0000-00-00 00:00:00',
                        'shipping_id' => $order["fulfillment_channel"],
                        'latest_ship_date' => new DateTime($order["latest_ship_date"]) ?? '0000-00-00 00:00:00',
                        'is_replacement_order' => $order["is_replacement_order"],
                        'number_of_items_unshipped' => $order["number_of_items_unshipped"]
                    ])->id;

                    $orderid = $order["amazon_order_id"];
                    $Order_details =  OrderModel::vieworder($orderid, $storeid);

                    if ($Order_details["qty"] > 1) {
                        $price = ($order["order_total"]['amount']) / $Order_details["qty"];
                        $order_total = $order["order_total"]['amount'];
                    } else {
                        $price = ($order["order_total"]['amount']) ?? '0.00';
                        $order_total = ($order["order_total"]['amount']) ?? '0.00';
                    }
                    if ($price != '0.00') {
                        $curr_value = Exchange_rate::where('curr_code', '=', $order["order_total"]['currency_code'])
                            ->orderBy('created_at', 'desc')
                            ->first();
                        $priceusd = $price / $curr_value->curr_value;
                        $price_usd = round($priceusd, 2);
                        $order_total_usd = $order_total / $curr_value->curr_value;
                        $ordertotal_usd = round($order_total_usd, 2);
                    } else {
                        $price_usd = '0.00';
                        $ordertotal_usd = '0.00';
                    }

                    Order::where('id', '=', $add_orders)
                        ->update([
                            'buyername' => $Order_details["buyername"] ?? '',
                            'buyeremail' => $Order_details["buyeremail"] ?? '',
                            'address1' => $Order_details["address1"] ?? '',
                            'address2' => $Order_details["address2"] ?? '',
                            'municipality' => $Order_details["municipality"] ?? '',
                            'postal' => $Order_details["postalcode"] ?? '',
                            'city' => $Order_details["city"] ?? '',
                            'country' => $Order_details["country"] ?? '',
                            'ordertotal_usd' => $ordertotal_usd ?? '0.00'
                        ]);
                    if ($Order_details["qty"] == null || $Order_details["qty"] == '') {
                        $quantity = 0;
                    } else {
                        $quantity = $Order_details["qty"];
                    }
                    Order_detail::create([
                        'order_id' => $add_orders,
                        'item_id' => $Order_details["OrderItemId"] ?? '',
                        'asin' => $Order_details["asin"] ?? '',
                        'quantity' => $quantity,
                        'sku' => $Order_details["SellerSKU"] ?? '',
                        'producttitle' => $Order_details["Title"] ?? '',
                        'condition' => $Order_details["ConditionId"] ?? '',
                        'price_usd' => $price_usd ?? '0.00',
                        'price' => $price ?? '0.00'
                    ]);
                } else {

                    $orderid = $order["amazon_order_id"];
                    $Order_details =  OrderModel::vieworder($orderid, $storeid);

                    Order::where('id', '=', $duplicate->id)
                        ->update([
                            'orderstatus' => $order["order_status"],
                            'number_of_items_shipped' => $order["number_of_items_shipped"],
                            'number_of_items_unshipped' => $order["number_of_items_unshipped"],
                            'deliverydate' => new DateTime($order["latest_delivery_date"]) ?? '0000-00-00 00:00:00',
                            'shipping_id' => $order["fulfillment_channel"],
                            'latest_ship_date' => new DateTime($order["latest_ship_date"]) ?? '0000-00-00 00:00:00',
                            'is_replacement_order' => $order["is_replacement_order"]
                        ]);
                }
            }
        }
    }

    public function updatecate()
    {

        //$storeid = '49';
        //$asin = 'B0886L3CFV';
        //$cate =  OrderModel::getCate($storeid, $asin);
        //print_r ($cate[0]["package_dimensions"]);
        //exit;

        $table = 'users';
        $list = DB::table($table)
            ->select('stores.id', 'stores.user_id', 'stores.store_name')
            ->selectRaw('DATE_FORMAT(stores.last_updated_at,"%Y-%m-%d") as last_updated_at')
            ->join('stores', $table . '.id', '=', 'stores.user_id')
            ->where($table . '.is_active', 1)
            //->whereIn($table . '.type', [0, 1])
            ->whereRaw('EXISTS (select 1 from user_amazon_integrations where user_amazon_integrations.store_id=stores.id and user_amazon_integrations.admin_integration_id>0)')
            ->orderBy('stores.last_updated_at', 'asc')
            ->take(5)
            ->get();

        foreach ($list as $store) {
            $asinList = DB::table('webscrap_products')
                ->join('user_search_requests', 'webscrap_products.request_id', '=', 'user_search_requests.id')
                ->where('user_search_requests.store_id', '=', $store->id)
                ->whereRaw('webscrap_products.package_height  is null')
                ->select('webscrap_products.asin', 'webscrap_products.id')
                ->take(50)
                ->get();

            foreach ($asinList as $asinArr) {
                //echo $store->id;
                echo "Running for " . $asinArr->asin . "<br>";
                $cate =  OrderModel::getCate($store->id, $asinArr->asin);

                if (isset($cate[0]["package_dimensions"])) {
                    $pack = $cate[0]["package_dimensions"];
                    print_r($pack);

                    $h = $pack["height"]["value"];
                    $l = $pack["length"]["value"];
                    $w = $pack["width"]["value"];
                    $wt = $pack["weight"]["value"];
                    $unit_d = $pack["height"]["units"];
                    $unit_w = $pack["weight"]["units"];
                    Webscrap_product::where('id', '=', $asinArr->id)
                        ->update([
                            'package_height' => $h,
                            'package_length' => $l,
                            'package_width' => $w,
                            'package_weight' => $wt,
                            'package_weight_units' => $unit_w,
                            'package_dimension_units' => $unit_d,
                        ]);
                }
            }
        }
    }
    public function updateexchangeratetry()
    {
        $data = $this->curl_get_file_contents('https://www.akbank.com/_vti_bin/AkbankServicesSecure/FrontEndServiceSecure.svc/GetExchangeData');
        $phpDataArray = json_decode($data, true);
        $stringGetExchangeDataResult = $phpDataArray["GetExchangeDataResult"];

        if ($stringGetExchangeDataResult != "") {

            $list = json_decode($stringGetExchangeDataResult)->Currencies;
            foreach ($list as $item) {
                //print_r($item);
                if ($item->Name == "USD") {
                    $sellRate = $item->Sell;
                    $sellRate = str_replace(',', '.', $sellRate);

                    $sellRate = $sellRate + ".12";

                    echo $sellRate;

                    Exchange_rate::create([
                        'curr_code' => "TRY",
                        'curr_value' => $sellRate
                    ]);
                }
            }
        }
    }

    public function updateinventories_qty()
    {

        $last_updated =  Batch_list::where('batch', '=', 'inventory_quantity')->first();

        $table = 'webscrap_product_prices_views';
        $list = DB::table($table)
            ->join('user_search_requests', 'webscrap_product_prices_views.request_id', '=', 'user_search_requests.id')
            ->select('webscrap_product_prices_views.asin', 'webscrap_product_prices_views.max_qty', 'user_search_requests.store_id')
            ->where('webscrap_product_prices_views.updated_at', '>', $last_updated->updated_at)
            ->orderBy('webscrap_product_prices_views.updated_at', 'asc')
            ->get();

        foreach ($list as $inventory) {

            $updatedqty = DB::table('inventories')
                ->where('asin', '=', $inventory->asin)
                ->where('store_id', '=', $inventory->store_id)
                ->where('stock', '!=', $inventory->max_qty)
                ->first();

            if ($updatedqty != '') {

                Inventory::where('id', '=', $updatedqty->id)
                    ->update(['stock' => $inventory->max_qty]);

                $fromhistory = DB::table('inventory_histories')
                    ->where('asin', '=', $inventory->asin)
                    ->where('store_id', '=', $inventory->store_id)
                    ->orderBy('created_at', 'desc')
                    ->first();

                Inventory_history::create([
                    'user_id' => $fromhistory->user_id,
                    'store_id' => $inventory->store_id,
                    'asin' => $inventory->asin,
                    'old_qty' => $fromhistory->new_qty,
                    'new_qty' => $inventory->max_qty,
                    'old_price' => $fromhistory->old_price,
                    'new_price' => $fromhistory->new_price,
                    'type' => 2,
                    'status' => 1
                ]);
            }
        }
    }

    public function updateinventories_price()
    {
        $last_updated =  Batch_list::where('batch', '=', 'inventory_price')->first();

        $store = DB::table('stores')
        ->join('countries', 'stores.country_id', '=', 'countries.id')
        ->select('stores.*', 'countries.currency_code')
        ->get();

        foreach ($store as $amzstore) {
            
            $calcTime = Carbon::now()->subHour(4);
            $table = 'inventories';
            $inventories = DB::table($table)
                ->where('store_id', '=', $amzstore->id)
                ->where('last_updated', '<', $calcTime)
                ->get();

            $store_curr = Exchange_rate::where('curr_code', '=', $amzstore->currency_code)->select('curr_value')->first();

            foreach ($inventories as $upinventory) {

                $inventory = DB::table('webscrap_product_prices_views')
                            ->join('webscrap_target_products_views', 'webscrap_product_prices_views.asin', '=', 'webscrap_target_products_views.asin')
                            ->select(
                                'webscrap_product_prices_views.max_qty',
                                'webscrap_product_prices_views.product_cost as product_cost',
                                'webscrap_product_prices_views.buy_box_seller_id as buy_box_seller_id',
                                'webscrap_product_prices_views.lowest_rate as lowest_rate',
                                'webscrap_product_prices_views.lowest_seller_id as lowest_seller_id',
                                'webscrap_target_products_views.product_cost as target_product_cost',
                                'webscrap_target_products_views.buy_box_seller_id as target_buy_box_seller_id',
                                'webscrap_target_products_views.lowest_rate as target_lowest_rate',
                                'webscrap_target_products_views.lowest_seller_id as target_lowest_seller_id'
                                )
                            ->where('webscrap_product_prices_views.country_id', '=', $amzstore->country_id)
                            ->where('webscrap_target_products_views.country_id', '=', $amzstore->country_id)
                            ->where('webscrap_product_prices_views.asin', '=', $upinventory->asin)
                            ->where('webscrap_target_products_views.asin', '=', $upinventory->asin)
                            ->first();
                
                if ($amzstore->last_sku != '') {
                    $last_sku = $amzstore->last_sku;
                } else {
                    $last_sku = 0;
                }
                    
                if ($inventory != '') {
                    
                    $last_sku = $last_sku + 1;
                    $sku = $amzstore->sku .''. $last_sku;

                    //view lowest

                    $lowest_rate = $inventory->lowest_rate;
                    $lowest_seller_id = $inventory->lowest_seller_id;

                    if ($lowest_rate == "" || $lowest_rate == "0" || $lowest_rate > $inventory->product_cost) {
                        $lowest_rate = $inventory->product_cost;
                        $lowest_seller_id = $inventory->buy_box_seller_id;
                    }

                    //target lowest

                    $target_lowest_rate = $inventory->target_lowest_rate;
                    $target_lowest_seller_id = $inventory->target_lowest_seller_id;

                    if ($target_lowest_rate == "" || $target_lowest_rate == "0" || $target_lowest_rate > $inventory->target_product_cost) {
                        $target_lowest_rate = $inventory->target_product_cost;
                        $target_lowest_seller_id = $inventory->target_buy_box_seller_id;
                    }

                    $pricing_detail = PricingCalc::getMinMaxProduct($lowest_rate);

                    $productprice = $lowest_rate;
                    $tax1 = $amzstore->enable_tax;
                    $tax1type = 'P'; // Always %
                    $tax2 = $amzstore->tax1_t;
                    $tax2type = $amzstore->tax1_o;
                    $tax3 = $amzstore->tax2_t;
                    $tax3type = $amzstore->tax2_o;
                    $tax4 = $amzstore->tax3_t;
                    $tax4type = $amzstore->tax3_o;
                    $shipping = $amzstore->shipping;
                    $importfee = $amzstore->import_fee_t;
                    $importfeeType = $amzstore->import_fee_o;
                    $formula = $pricing_detail->formula;
                    $amazon_commission = $amzstore->amazon_commission;

                    $min = $pricing_detail->min_profit;
                    $min_price = PricingCalc::calcSellingPrice($productprice, $min, $tax1, $tax1type, $tax2, $tax2type, $tax3, $tax3type, $tax4, $tax4type, $shipping, $importfee, $importfeeType, $formula, $amazon_commission);
                    $max = $pricing_detail->maximum;
                    $max_price = PricingCalc::calcSellingPrice($productprice, $max, $tax1, $tax1type, $tax2, $tax2type, $tax3, $tax3type, $tax4, $tax4type, $shipping, $importfee, $importfeeType, $formula, $amazon_commission);
                    $std = $pricing_detail->standard;
                    $std_price = PricingCalc::calcSellingPrice($productprice, $std, $tax1, $tax1type, $tax2, $tax2type, $tax3, $tax3type, $tax4, $tax4type, $shipping, $importfee, $importfeeType, $formula, $amazon_commission);
                    
                    /*echo '| productprice-'. $productprice;
                    echo '| tax1-'. $tax1;
                    echo '| tax1type-'. $tax1type;
                    echo '| tax2-'. $tax2;
                    echo '| tax2type-'. $tax2type;
                    echo '| tax3-'. $tax3;
                    echo '| tax3type-'. $tax3type;
                    echo '| tax4-'. $tax4;
                    echo '| tax4type-'. $tax4type;
                    echo '| importfee-'. $importfee;
                    echo '| importfeeType-'. $importfeeType;
                    echo '| formula-'. $formula;
                    echo '| amazon_commission-'. $amazon_commission;
                    echo '| '. $min .'-'. $min_price;
                    echo '| ';
                    echo $max .'-'. $max_price;
                    echo '| ';
                    echo $std .'-'. $std_price;
                    exit;*/
                    
                    $min_price_c = round($min_price * $store_curr->curr_value, 2);
                    $max_price_c = round($max_price * $store_curr->curr_value, 2);
                    $std_price_c = round($std_price * $store_curr->curr_value, 2);
                    
                    $lowest_rate_c = round($lowest_rate * $store_curr->curr_value, 2);
                    $product_cost_c = round($inventory->product_cost * $store_curr->curr_value, 2);
                    $target_lowest_rate_c = round($target_lowest_rate * $store_curr->curr_value, 2);
                    $target_product_cost_c = round($inventory->target_product_cost * $store_curr->curr_value, 2);

                    $min_price = round($min_price, 2);
                    $max_price = round($max_price, 2);
                    $std_price = round($std_price, 2);

                    Inventory::where('id', $upinventory->id)
                    ->update([         
                        'last_updated' => now()
                    ]);                        
                    
                    $flag=false;

                    if ($upinventory->min_price != $min_price_c) { $flag=true; }
                    if ($upinventory->max_price != $max_price_c) { $flag=true; }
                    if ($upinventory->price != $std_price_c) { $flag=true; }
                    if ($upinventory->min_price_usd != $min_price) { $flag=true; }
                    if ($upinventory->max_price_usd != $max_price) { $flag=true; }
                    if ($upinventory->price_usd != $std_price) { $flag=true; }
                    if ($upinventory->amazon_fee != $amzstore->amazon_commission) { $flag=true; }
                    if ($upinventory->product_cost_usd != $inventory->product_cost) { $flag=true; }
                    if ($upinventory->product_cost != $product_cost_c) { $flag=true; }
                    if ($upinventory->buy_box_seller_id != $inventory->buy_box_seller_id) { $flag=true; }
                    if ($upinventory->lowest_product_cost_usd != $lowest_rate) { $flag=true; }
                    if ($upinventory->lowest_product_cost != $lowest_rate_c) { $flag=true; }
                    if ($upinventory->lowest_product_seller_id != $lowest_seller_id) { $flag=true; }
                    if ($upinventory->target_product_cost_usd != $inventory->target_product_cost) { $flag=true; }
                    if ($upinventory->target_product_cost != $target_product_cost_c) { $flag=true; }
                    if ($upinventory->target_buy_box_seller_id != $inventory->target_buy_box_seller_id) { $flag=true; }
                    if ($upinventory->target_lowest_product_cost_usd != $target_lowest_rate) { $flag=true; }
                    if ($upinventory->target_lowest_product_cost != $target_lowest_rate_c) { $flag=true; }
                    if ($upinventory->target_lowest_product_seller_id != $target_lowest_seller_id) { $flag=true; }

                    
                    echo 'min_price<br>' . $upinventory->min_price .'-'. $min_price_c . '<br>';
                    echo 'max_price<br>' . $upinventory->max_price .'-'. $max_price_c . '<br>';
                    echo 'price<br>' . $upinventory->price .'-'. $std_price_c . '<br>';
                    echo 'min_price_usd<br>' . $upinventory->min_price_usd .'-'. $min_price . '<br>';
                    echo 'max_price_usd<br>' . $upinventory->max_price_usd .'-'. $max_price . '<br>';
                    echo 'price_usd<br>' . $upinventory->price_usd .'-'. $std_price . '<br>';
                    echo 'amazon_fee<br>' . $upinventory->amazon_fee .'-'. $amzstore->amazon_commission . '<br>';
                    echo 'product_cost_usd<br>' . $upinventory->product_cost_usd .'-'. $inventory->product_cost . '<br>';
                    echo 'product_cost<br>' . $upinventory->product_cost .'-'. $product_cost_c . '<br>';
                    echo 'buy_box_seller_id<br>' . $upinventory->buy_box_seller_id .'-'. $inventory->buy_box_seller_id . '<br>';
                    echo 'lowest_product_cost_usd<br>' . $upinventory->lowest_product_cost_usd .'-'. $lowest_rate . '<br>';
                    echo 'lowest_product_cost<br>' . $upinventory->lowest_product_cost .'-'. $lowest_rate_c . '<br>';
                    echo 'lowest_product_seller_id<br>' . $upinventory->lowest_product_seller_id .'-'. $lowest_seller_id . '<br>';
                    echo 'target_product_cost_usd<br>' . $upinventory->target_product_cost_usd .'-'. $inventory->target_product_cost . '<br>';
                    echo 'target_product_cost<br>' . $upinventory->target_product_cost .'-'. $target_product_cost_c . '<br>';
                    echo 'target_buy_box_seller_id<br>' . $upinventory->target_buy_box_seller_id .'-'. $inventory->target_buy_box_seller_id . '<br>';
                    echo 'target_lowest_product_cost_usd<br>' . $upinventory->target_lowest_product_cost_usd .'-'. $target_lowest_rate . '<br>';
                    echo 'target_lowest_product_cost<br>' . $upinventory->target_lowest_product_cost .'-'. $target_lowest_rate_c . '<br>';
                    echo '$upinventory->target_lowest_product_seller_id<br>' . $upinventory->target_lowest_product_seller_id .'-'. $target_lowest_seller_id . '<br>';

                    if ($flag==true) {
                        Inventory::where('id', $upinventory->id)
                        ->update([         
                            'min_price' => $min_price_c,
                            'max_price' => $max_price_c,
                            'price' => $std_price_c,
                            'min_price_usd' => $min_price,
                            'max_price_usd' => $max_price,
                            'price_usd' => $std_price,
                            'amazon_fee' => $amzstore->amazon_commission,
                            'product_cost_usd' => $inventory->product_cost,
                            'product_cost' => $product_cost_c,
                            'buy_box_seller_id' => $inventory->buy_box_seller_id,
                            'lowest_product_cost_usd' => $lowest_rate,
                            'lowest_product_cost' => $lowest_rate_c,
                            'lowest_product_seller_id' => $lowest_seller_id,
                            'target_product_cost_usd' => $inventory->target_product_cost,
                            'target_product_cost' => $target_product_cost_c,
                            'target_buy_box_seller_id' => $inventory->target_buy_box_seller_id,
                            'target_lowest_product_cost_usd' => $target_lowest_rate,
                            'target_lowest_product_cost' => $target_lowest_rate_c,
                            'target_lowest_product_seller_id' => $target_lowest_seller_id
                        ]);
    

                        $fromhistory = DB::table('inventory_histories')
                        ->where('asin', '=', $upinventory->asin)
                        ->where('store_id', '=', $upinventory->store_id)
                        ->orderBy('created_at', 'desc')
                        ->first();
                        
                        if ($fromhistory != '') {
                            Inventory_history::create([
                                'user_id' => $fromhistory->user_id,
                                'store_id' => $upinventory->store_id,
                                'asin' => $upinventory->asin,
                                'old_qty' => $fromhistory->old_qty,
                                'new_qty' => $fromhistory->new_qty,
                                'old_price' => $fromhistory->new_price, 
                                'new_price' => $min_price_c,
                                'type' => 1, 
                                'status' => 1
                            ]);   
                        }

                            
                    }

                }

            }

            
        }


    }


}
