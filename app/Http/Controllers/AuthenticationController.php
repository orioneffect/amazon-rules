<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use DB;
use App\User;
use Hash;
use Mail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Support\Facades\Password;
use Session;
use Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\Eloquent\Model;
use App\Models\Language;
use App\Models\User_config;
use App\Models\User_main_config;

class AuthenticationController extends BaseController
{
    function login(){
        if (Auth::check()) {
            // The user is logged in...
            if(Auth::user()->type == 3) {
                return redirect('admin/ticket');
            }

            if(Auth::user()->is_active == 0) {
                return redirect('subscription/addnew');
            } else {
                Session::put('consultant', 'no');
                return redirect('integration/mystore');
            }
        }
        else
        {
            $languageNa = DB::table('languages')->where('status','=',1)->get();
        return view('authentication.login', compact('languageNa'));
        }
        
    }

    function logout(Request $request){
        Auth::logout();
        Cache::flush();
        $request->session()->forget('storeid');
        return redirect('authentication/login');
    }    

    function validatelogin(Request $request) {
        Cache::flush();
        //$request->session()->forget('storeid');
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if(Auth::user()->email_verified_at == null) {
                return redirect('authentication/login')->with('error', 'Oops! You didn\'t confirm your email yet.');
            }
            $defaultlang = Language::find(Auth::user()->default_language_id)?Language::find(Auth::user()->default_language_id)->name:'en';
            Cache::put('lang',$defaultlang);

            if(Auth::user()->type == 3) {
                return redirect('admin/ticket');
            }

            if(Auth::user()->is_active == 0) {
                return redirect('subscription/addnew');
            } else {
                Session::put('consultant', 'no');
                return redirect('integration/mystore');
            }
        }
        return redirect('authentication/login')->with('error', 'Oops! You have entered invalid credentials');
    }

    function consvalidatelogin($email) {

        $decrypted = Crypt::decryptString($email);

/*        $request->session()->forget('storeid');
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);*/
        Cache::flush();
        Cache::put('id', Auth::user()->id);
        Session::put('consultant', 'yes');
        //Session::put('admin', 'yes');
        //echo Cache::get('id');
        //exit;
        $credentials = User::where('email', '=',$decrypted)->first();
        
        if (Auth::loginUsingId($credentials->id)) {
            if(Auth::user()->email_verified_at == null) {
                return redirect('authentication/login')->with('error', 'Oops! You didn\'t confirm your email yet.');
            }
            if(Auth::user()->is_active == 0) {
                return redirect('subscription/addnew');
            } else {
                return redirect('integration/mystore');
            }
        }
        return redirect('authentication/login')->with('error', 'Oops! You have entered invalid credentials');
    }
    function conslogout(){
        Auth::logout();
        if(Cache::has('id')){
            $id = Cache::get('id');

            if (Auth::loginUsingId($id)) {
                if(Auth::user()->email_verified_at == null) {
                    return redirect('authentication/login')->with('error', 'Oops! You didn\'t confirm your email yet.');
                }
                    Session::put('consultant', 'no');
                    return redirect('dashboard/consettings');
            }
        }
            return redirect('authentication/login')->with('error', 'Oops! You have entered invalid credentials');
    }

    function adminvalidatelogin($email) {

        $decrypted = Crypt::decryptString($email);

/*        $request->session()->forget('storeid');
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);*/
        Cache::flush();
        Cache::put('admin_id', Auth::user()->id);
        Session::put('admin', 'yes');
        //Session::put('admin', 'yes');
        //echo Cache::get('id');
        //exit;
        $credentials = User::where('email', '=',$decrypted)->first();
        
        if (Auth::loginUsingId($credentials->id)) {
            if(Auth::user()->email_verified_at == null) {
                return redirect('authentication/login')->with('error', 'Oops! You didn\'t confirm your email yet.');
            }
            if(Auth::user()->is_active == 0) {
                return redirect('subscription/addnew');
            } else {
                return redirect('integration/mystore');
            }
        }
        return redirect('authentication/login')->with('error', 'Oops! You have entered invalid credentials');
    }
    function adminlogout(){
        Auth::logout();
        if(Cache::has('admin_id')){
            $id = Cache::get('admin_id');

            if (Auth::loginUsingId($id)) {
                if(Auth::user()->email_verified_at == null) {
                    return redirect('authentication/login')->with('error', 'Oops! You didn\'t confirm your email yet.');
                }
                    Session::put('admin', 'no');
                    return redirect('admin/manageusers');
            }
        }
            return redirect('authentication/login')->with('error', 'Oops! You have entered invalid credentials');
    }    

    function login2(){
        return view('authentication.login2');
    }
    function register(){
        return view('authentication.register');
    }
    function store(Request $request) {

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|',
            'password' => 'confirmed|min:6'
        ]);


        $id = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ])->id;

        User_main_config::create([
            'user_id' => $id,
            'phone_number' => $request->phone
        ]);

        User_config::create([
            'user_id' => $id
        ]);


        $encryptValue=Crypt::encryptString($id);
        $details = [
            'to_name' => $request->name,
            'link' => config('app.url').'/public/authentication/confirmemail/'.$encryptValue
        ];

        \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));
        return view('authentication.register')->with('success','success');
    }
    function forgotpassword(){
        return view('authentication.forgotpassword');
    }


    function forgotpasswordemail(Request $request) {

        $request->validate([
            'email' => 'required|string|email'
        ]);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
                    ? back()->with(['status' => __($status)])
                    : back()->withErrors(['email' => __($status)]);

        //return view('authentication.forgotpassword');
    }
    function page404(){
        return view('authentication.page404');
    }
    function maintenance(){
        return view('authentication.maintenance');
    }
    function comingsoon(){
        return view('authentication.comingsoon');
    }
}
