<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use \App\Models\Supplier;
use DB;
use Auth;
use App\Models\Supplier_detail;
use App\Models\User_file_upload;
class OptionsController extends Controller
{
    function suppliers() {
        return view('options.suppliers');
    }

    function uploadfiletoserver(Request $req) {
        $req->validate([
            'file' => 'required|mimes:csv,txt,xlsx,xls,pdf,doc,docx,PNG,png,tiff,jpg,jpeg|max:5120'
        ]);
        $fileName = $req->file->getClientOriginalName();
        $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');

        User_file_upload::create([
        'document_name'=> $req->documentname,
        'store_name'=> $req->store,
        'file_path'=> $filePath,
        'notes'=> $req->notes,
        'user_id' => Auth::user()->id,
        ]);
        return redirect('options/fileupload')->with('success','success');
    }

    function fileupload() {
        $fileupload=DB::table('user_file_uploads')
            ->where("user_id",'=' , Auth::user()->id)
            ->orderBy('user_file_uploads.id', 'desc')->get();
        return view('options.fileupload', ['fileupload' => $fileupload]);
    }

    private function getList($table,$offset,$searchby,$searchval) {
        if($searchval!='') {
            $brandlist=DB::table($table)
            ->where($searchby, 'LIKE', '%' . $searchval .'%')
            ->orderBy($table.'.id', 'desc')->offset($offset)->limit(50)->get();
        } else {
            $brandlist=DB::table($table)
            ->orderBy($table.'.id', 'desc')->offset($offset)->limit(50)->get();
        }

        return $brandlist;
    }


    private function getList2($table,$supplierId,$offset,$searchby,$searchval) {
        if($searchval!='') {
            $brandlist=DB::table($table)
            ->where("supplier_id",'=' , $supplierId)
            ->where($searchby, 'LIKE', '%' . $searchval .'%')
            ->orderBy($table.'.id', 'desc')->offset($offset)->limit(50)->get();
        } else {
            $brandlist=DB::table($table)
            ->where("supplier_id",'=' , $supplierId)
            ->orderBy($table.'.id', 'desc')->offset($offset)->limit(50)->get();
        }

        return $brandlist;
    }

    private function getList3($table,$offset,$searchby,$searchval) {
        if($searchval!='') {
            $brandlist=DB::table($table)
            ->where("user_id",'=' , Auth::user()->id)
            ->where($searchby, 'LIKE', '%' . $searchval .'%')
            ->orderBy($table.'.id', 'desc')->offset($offset)->limit(50)->get();
        } else {
            $brandlist=DB::table($table)
            ->where("user_id",'=' , Auth::user()->id)
            ->orderBy($table.'.id', 'desc')->offset($offset)->limit(50)->get();
        }
        return $brandlist;
    }


    function suppliersdetails(Request $request) {
        $supplier = Supplier::findOrFail($request->id);
        return view('options.supplierdetails',compact('supplier'));
    }

    function listsearch2(Request $request) {
        $list = $this->getList2('supplier_details',$request->supplierId,$request->loaded,$request->searchby,$request->searchval);
        $strOutput = '';
        foreach ($list as $item) {
            $strOutput .= '<tr>';
                $strOutput .='
                <td>
                    <label class="switch">
                        <input type="checkbox" class="exclude_item" name="" value="'.$item->id.'">
                        <span class="slider round"></span>
                    </label>
                </td>
                <td><img src="'.$item->additionalimages.'" alt="Avatar" class="w80 rounded mr-2"></img></td>
                <td><h6 class="mb-0">'.$item->product.'</h6></td>
                <td><h6 class="mb-0">'.$item->product_category.'</h6></td>
                <td>'.$item->price.'</td>
                <td>'.$item->quantity.'</td>';

                if ($item->status=='1') {
                    $strOutput .='<td><h6 class="mb-0"><a href="#a"><span class="text-danger">Pending</span></a></h6></td>';
                } else {
                    $strOutput .='<td><h6 class="mb-0"><a href="#a"><span class="text-green">Active</span></a></h6></td>';
                }

                $strOutput .='
                <td><h6 class="mb-0 bg-light">'.$item->description.'</h6></td>
                <td><h6 class="mb-0">'.$item->created_at.'</h6></td>
                <td>
                    <button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="showConfirmMessage('.$item->id.')"><i class="fa fa-trash-o text-danger"></i></button>
                </td>
            </tr>';
        }
        return $strOutput;
    }


    function listsearch3(Request $request) {
        $list = $this->getList3('user_file_uploads',$request->loaded,$request->searchby,$request->searchval);
        $strOutput = '';
        foreach ($list as $item) {
            $strOutput .= '<tr>';
                $strOutput .='
                <td><h6 class="mb-0">'.$item->document_name.'</h6></td>
                <td><h6 class="mb-0">'.$item->store_name.'</h6></td>
                <td><h6 class="mb-0">'.$item->notes.'</h6></td>
                <td>'.$item->file_path.' <i class="fa fa-download"></i></td>';
                $strOutput .='
                <td><h6 class="mb-0">'.$item->created_at.'</h6></td>
                <td>
                    <button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="showConfirmMessage('.$item->id.')"><i class="fa fa-trash-o text-danger"></i></button>
                </td>
            </tr>';
        }
        return $strOutput;
    }

    function listsearch(Request $request) {
        $list = $this->getList('suppliers',$request->loaded,$request->searchby,$request->searchval);
        $strOutput = '';
        foreach ($list as $item) {
            $strOutput .= '<tr>';
            if($item->status=='2') {
                $strOutput .= '<td><h6 class="mb-0"><a href="./suppliersdetails/?id='.$item->id.'">'.$item->company_name.'<a/></h6></td>';
            } else {
                $strOutput .= '<td><h6 class="mb-0">'.$item->company_name.'</h6></td>';
            }
            $strOutput .='<td><h6 class="mb-0">'.$item->phone.'</h6></td>
            <td><h6 class="mb-0">'.$item->email.'</h6></td>
            <td><h6 class="mb-0">'.$item->contact_name.'</h6></td>
            <td><h6 class="mb-0">'.$item->user_name.'</h6></td>';
            if ($item->status=='0') {
                $strOutput .='<td><h6 class="mb-0"><a href="#a"><span class="text-warning">Pending</span></a></h6></td>';
            } elseif ($item->status=='1') {
                $strOutput .='<td><h6 class="mb-0"><a href="#a"><span class="text-grey">In progress</span></a></h6></td>';
            } elseif ($item->status=='3') {
                $strOutput .='<td><h6 class="mb-0"><a href="#a"><span class="text-red">Failed</span></a></h6></td>';
            } else {
                $strOutput .='<td><h6 class="mb-0"><a href="#a"><span class="text-green">Success</span></a></h6></td>';
            }

            $strOutput .='

            <td><h6 class="mb-0">'.$item->created_at.'</h6></td>
            <td>
                <button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="showConfirmMessage('.$item->id.')"><i class="fa fa-trash-o text-danger"></i></button>
            </td>
        </tr>';
        }
        return $strOutput;
    }

    function savesuppliers(Request $request) {

        Supplier::create([
            'company_name'=> $request->companyname,
            'phone'=> $request->phone,
            'email'=> $request->email,
            'contact_name'=> $request->contact,
            'user_name'=> $request->user,
            'xml_link'=> $request->xml,
            ]);

        return 1;
    }

    public function download(Request $request) {
        $file_path = public_path('../storage/app/public/'.$request->filename);
        return response()->download($file_path);
    }

    function profitloss() {
        return view('options.profitloss');
    }

}
