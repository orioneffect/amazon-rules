<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ExpDateValidate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $monarr=explode("-", $value);
        $ccyear=$monarr[0];
        $ccmonth=$monarr[1];
        $month = date('m');
        $year = date('Y');

        if ($ccyear<$year) {
            return false;        }
        else if ($year==$ccyear && $ccmonth<$month) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Subscription.invalidExpDate');
    }
}
