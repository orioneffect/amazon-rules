<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use DB;
use Session;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

            View::composer('*', function($view)
            {
                if(Auth::check()) {
                    $feedbk = Session::get('feedbk');

                    $store_id = Session::get('storeid');
                    if ($store_id=='') {
                        $storeid = Auth::user()->default_store_id;
                        Session::put('storeid', $storeid);
                    } else {
                        $storeid = $store_id;
                    }

                    $stores =  DB::table('stores')
                    ->join('countries','stores.country_id', '=', 'countries.id')
                    ->select('countries.flag_path','stores.store_name')
                    ->where('stores.id','=',$storeid)
                    ->first();


                    $storesNa = DB::table('stores')
                    ->join('countries', 'stores.country_id', '=', 'countries.id')
                    ->join('users', 'stores.user_id', '=', 'users.id')
                    ->leftjoin('user_amazon_integrations','stores.id', '=', 'user_amazon_integrations.store_id')
                    ->select('stores.*', 'countries.flag_path', 'countries.country_name','users.name','user_amazon_integrations.admin_integration_id')
                    ->where('stores.user_id',Auth::user()->id)
                    ->where('user_amazon_integrations.admin_integration_id','>',0)
                    ->orderBy('stores.created_at','desc')
                    ->get();

                    /*$discountCheck = DB::table('discounts')
                    ->join('users', 'discounts.id', '=', 'users.coupon_code')
                    ->select('discounts.logo_path')
                    ->where('users.id',Auth::user()->id)
                    ->where('discounts.is_logo','=',1)
                    ->first();*/


                    $studentCheck = DB::table('student_consultants')
                    ->join('users', 'student_consultants.user_id', '=', 'users.id')
                    ->where('student_consultants.email', Auth::user()->email)
                    ->select('users.logo', 'student_consultants.impersonate')
                    ->first();


                    $countryNa = DB::table('stores')
                    ->join('countries', 'stores.country_id', '=', 'countries.id')
                    ->join('users', 'stores.user_id', '=', 'users.id')
                    ->leftjoin('user_amazon_integrations','stores.id', '=', 'user_amazon_integrations.store_id')
                    ->where('stores.user_id',Auth::user()->id)
                    ->where('user_amazon_integrations.admin_integration_id','>',0)
                    ->orderBy('stores.created_at','desc')
                    ->select('stores.country_id', 'countries.flag_path', 'countries.country_name')
                    ->distinct()
                    ->get();

                    $notificatioNa = DB::table('notifications')
                    ->join('notificationtypes', 'notifications.type_id', '=', 'notificationtypes.type_id')
                    ->select('notifications.*', 'notificationtypes.*')
                    ->where('notifications.is_read', 0)
                    ->where('notifications.user_id',Auth::user()->id)
                    ->orderBy('notifications.id','desc')
                    ->limit(10)
                    ->get();

                    $notifnos = DB::table('notifications')
                    ->where('notifications.is_read', 0)
                    ->where('notifications.user_id',Auth::user()->id)
                    ->count();

                    $user = DB::table('users')
                    ->where('id', '=', Auth::user()->id)
                    ->first();

                    $validtill = \Carbon\Carbon::parse(Auth::user()->plan_todate);
                    $today = \Carbon\Carbon::now();
                    $plan_validity = $validtill->diffInDays($today);

                    $fbkq = DB::table('fbk_quest_masters')->where('status','=',0)->get();
                    $fbka = DB::table('fbk_ans_masters')->where('status','=',0)->get();
                    $storecheck = DB::table('stores')->where('user_id','=',Auth::user()->id)->first();
                    $currencyNa = DB::table('countries')->distinct()->get(['currency_code']);
                    $languageNa = DB::table('languages')->where('status','=',1)->get();
                    $countries = DB::table('countries')->get();
                    $stateselect = DB::table('countries')
                        ->join('states', 'countries.id', '=', 'states.country_id')
                        ->select('states.*')
                        ->where('countries.id', Auth::user()->country)
                        ->get();


                    $data =  array();
                    $data["feedbk"]=$feedbk;
                    $data["stores"]=$stores;
                    $data["storecheck"]=$storecheck;
                    $data["storesNav"]=$storesNa;
                    $data["countriesNav"]=$countryNa;
                    $data["country"]=$countries;
                    $data["state"]=$stateselect;
                    $data["currencyNav"]=$currencyNa;
                    $data["languageNav"]=$languageNa;
                    $data["notificationNav"]=$notificatioNa;
                    $data["nofnotifications"]=$notifnos;
                    $data["username"]=Auth::user()->name;
                    $data["email"]=Auth::user()->email;
                    $data["utype"]=Auth::user()->type;
                    $data["clogo"]=Auth::user()->logo;
                    $data["profileimg"]=Auth::user()->profileimg;
                    $data["coid"]=Auth::user()->id;
                    $data["lang"]=Auth::user()->default_language_id;
                    $data["curr"]=Auth::user()->currency;
                    $data["freeze"]=Auth::user()->plan_freeze;
                    $data["user"]=$user;
                    $data["fbkq"]=$fbkq;
                    $data["fbka"]=$fbka;
                    $data["plan_validity"]=$plan_validity;

                    $data["cons_id"]=0;
                    if (Cache::has('id')){
                        $con=Cache::get('id');
                            $data["cons_id"]=1;
                    }

                    /*if(isset($discountCheck)) {
                        $data["discountlogo"]=$discountCheck->logo_path;
                    }*/
                    if(isset($studentCheck)) {
                        $data["studentlogo"]=$studentCheck->logo;
                        $selected = "";
                        if ($studentCheck->impersonate == 1)
                        {
                            $selected = "checked";
                        }
                        $data["impersonate"]=$selected;
                    }

                    $view->with('maindata', $data);
                }
            });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
