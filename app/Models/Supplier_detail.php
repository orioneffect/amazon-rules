<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier_detail extends Model
{
    use HasFactory;
    protected $fillable = ['supplier_id','product','sku','category','additionalimages','description','price','shipping_day','kdv','quantity','product_category','variants','barcode','name'];
}
