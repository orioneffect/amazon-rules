<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'type_id', 'head', 'message', 'sent_time', 'sent_by', 'is_read', 'status', 'created_at', 'updated_at'];
}