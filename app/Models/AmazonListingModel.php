<?php
namespace App\Models;
use Exception;
class AmazonListingModel
{

    function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }


    public function getFeedReport($isJson=false) {

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\FeedsApi(AmazonApiconnector::getConfig());

        $result = $apiInstance->getFeedDocument("amzn1.tortuga.3.d0d1d5de-1ad8-4c45-8f37-8edbc1f5d738.T2LYTMM6N8YTVX");

        $payload=$result->getPayload();
        $compressionAlgorithm = $payload["compression_algorithm"];
        $key = $payload["encryption_details"]["key"];
        $iv = $payload["encryption_details"]["initialization_vector"];
        $reporturl = $payload["url"];

        echo $reporturl;

        $initializationVector = base64_decode($iv, true);
        $key = base64_decode($key, true);

        $fileContent=$this->curl_get_file_contents($reporturl);

        $decryptedFile = \App\Helper\ASECryptoStream::decrypt($fileContent, $key, $initializationVector);
        if(isset($compressionAlgorithm) && $compressionAlgorithm=='GZIP') {
            $decryptedFile=gzdecode($decryptedFile);
        }
        if($isJson==false) {
            $decryptedFile=str_replace("\n", '***', $decryptedFile);
            $decryptedFile=str_replace("\t", '^^^', $decryptedFile);
        }
        print_r($decryptedFile);

    }

    public function createListing() {

        $config = AmazonApiconnector::getConfig();

        try {
            $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\FeedsApi($config);
            $contentType = 'text/xml; charset=UTF-8'; // please pay attention here, the content_type will be used many time

            $body='
            {
                "contentType": '.$contentType.'
            }';

            $feedDocument = $apiInstance->createFeedDocument($body);

            $feedDocumentId = $feedDocument->getPayload()->getFeedDocumentId();

            echo $feedDocumentId."<br>";

            $url = $feedDocument->getPayload()->getUrl();
            $key = $feedDocument->getPayload()->getEncryptionDetails()->getKey();
            $key = base64_decode($key, true);
            $initializationVector = base64_decode($feedDocument->getPayload()->getEncryptionDetails()->getInitializationVector(), true);
            $encryptedFeedData = openssl_encrypt(utf8_encode(
                '<?xml version="1.0" encoding="iso-8859-1"?>
                <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
                  <Header>
                    <DocumentVersion>1.02</DocumentVersion>
                    <MerchantIdentifier>A2DEC8N72MV2QZ</MerchantIdentifier>
                  </Header>
                <MessageType>Inventory</MessageType>
                <Message>
                <MessageID>1</MessageID>
                <OperationType>Update</OperationType>
                <Product>
                    <SKU>ORION-78687</SKU>
                    <StandardProductID>
                        <Type>ASIN</Type>
                        <Value>B07X1RW3RL</Value>
                    </StandardProductID>
                    <Condition>
                        <ConditionType>New</ConditionType>
                    </Condition>
                </Product>
                </Message>
            </AmazonEnvelope>'
            ), 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $initializationVector);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_POSTFIELDS => $encryptedFeedData,
                CURLOPT_HTTPHEADER => [
                    'Accept: application/xml',
                    'Content-Type: ' . $contentType,
                ],
            ));

            $response = curl_exec($curl);

            print_r($response);

            $error = curl_error($curl);

            print_r($error);

            $httpcode = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);

            echo "httpcode>>".$httpcode;

            sleep(10);

            exit;


            if ($httpcode >= 200 && $httpcode <= 299) {

                $createFeedParams = [
                    //"feedType" => "POST_INVENTORY_AVAILABILITY_DATA",
                    //"feedType" => "POST_PRODUCT_PRICING_DATA",
                    "feedType" => "POST_PRODUCT_DATA",
                        "marketplaceIds" => ["A2VIGQ35RCS4UG"],
                        "inputFeedDocumentId" => $feedDocumentId,
                        "feedOptions" => ""
                    ];

                    print_r($createFeedParams);
                    $r = $apiInstance->createFeed(json_encode($createFeedParams));
                    print_r($r);
                // success
            //} else {

                //echo "Error";
                // error
            }



            exit;
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }
}
