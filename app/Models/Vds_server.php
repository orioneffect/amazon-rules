<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vds_server extends Model
{
    use HasFactory;
    protected $table="vds_servers";
    protected $fillable = ['userid','orionemail','buyermail','sellermail','servercode','ipaddressv4','ipaddressv6','registerdate','expiredate','os','vdsusername','vdspwd','vdslocalipv4','vdslocalipv6','vdslocalmac','created_at','updated_at'];
}
