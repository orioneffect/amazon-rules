<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory_history extends Model
{
    use HasFactory;
    protected $fillable = ['store_id', 'user_id', 'asin', 'old_qty', 'new_qty', 'old_price', 'new_price', 'type', 'changed_at', 'status'];
}
