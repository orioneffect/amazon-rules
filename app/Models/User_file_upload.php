<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_file_upload extends Model
{
    use HasFactory;
    protected $fillable = ['document_name', 'store_name', 'file_path','notes','user_id'];
}
