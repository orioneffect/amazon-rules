<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fba_plan_product extends Model
{
    use HasFactory;
    protected $fillable = ['plan_id','asin','sku','condition','quantity'];
}
