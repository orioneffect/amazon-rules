<?php

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Model;
use Session;
use Exception;

class OrderModel extends Model
{
    public static function vieworder($orderid,$storeid)
    {

        $config = AmazonApiconnector::getBatchConfig($storeid);

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\OrdersApi($config);

        //$getOrder = $apiInstance->getOrder($orderid)->getPayload();
        $buyerinfo = $apiInstance->getOrderBuyerInfo($orderid)->getPayload();
        $address = $apiInstance->getOrderAddress($orderid)->getPayload();
        $items = $apiInstance->getOrderItems($orderid)->getPayload();
        print_r($items);
        //$mainorder=$apiInstance->getorder($orderid)->getPayload();

        $getOrderItems = $items->getOrderItems()[0]; // Loop needed

        $shippAddress = $address->getShippingAddress();
        print_r($buyerinfo);
        print_r($address);
        print_r($shippAddress);

        $data =  array();

        if (!(is_null($shippAddress))) {

            //$data['order']  =  $getOrder;
            $data['buyeremail']  =  ($buyerinfo->getBuyerEmail() == '') ? '' : $buyerinfo->getBuyerEmail();

            $data['address1']  = $shippAddress->getAddressLine1();
            //($shippAddress->getAddressLine1()=='') ? '' : $shippAddress->getAddressLine1();
            $data['address2']  = ($shippAddress->getAddressLine2() == '') ? '' : $shippAddress->getAddressLine2();
            $data['municipality']  = ($shippAddress->getMunicipality() == '') ? '' : $shippAddress->getMunicipality();
            $data['postalcode']  = ($shippAddress->getPostalCode() == '') ? '' : $shippAddress->getPostalCode();
            $data['city']  = ($shippAddress->getCity() == '') ? '' : $shippAddress->getCity();
            $data['country']  = ($shippAddress->getCounty() == '') ? '' : $shippAddress->getCounty();
            $data['buyername']  = ($shippAddress->getName() == '') ? '' : $shippAddress->getName();
            $data['countrycode']  = ($shippAddress->getCountryCode() == '') ? '' : $shippAddress->getCountryCode();
        }
        $data['asin']  = ($getOrderItems->getASIN() == '') ? '' : $getOrderItems->getASIN();
        $data['SellerSKU']  = ($getOrderItems->getSellerSKU() == '') ? '' : $getOrderItems->getSellerSKU();
        $data['ConditionId']  = ($getOrderItems->getConditionId() == '') ? '' : $getOrderItems->getConditionId();
        $data['OrderItemId']  = ($getOrderItems->getOrderItemId() == '') ? '' : $getOrderItems->getOrderItemId();
        $data['Title']  = ($getOrderItems->getTitle() == '') ? '' : $getOrderItems->getTitle();
        $data['qty']  = ($getOrderItems->getQuantityShipped() == '') ? '' : $getOrderItems->getQuantityShipped();

        //$data['order_details']  = $mainorder;

        //print_r($data['order_details']);
        //exit;
        //$catelogApiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($config);
        //$marketplace_id = $config->getMarketplaceid();
        //$productResponse = $catelogApiInstance->getCatalogItem($marketplace_id,$getOrderItems->getASIN())->getPayload();
        //$data['productURL']  = $productResponse->getAttributeSets()[0]->getSmallImage()->getURL() ?? '';
        //$productResponse->getAttributeSets()[0]->getSmallImage()->getURL()=='') ? '' : $productResponse->getAttributeSets()[0]->getSmallImage()->getURL();
        //$data['productSize']  = ($productResponse->getAttributeSets()[0]->getSize()=='') ? '' : $productResponse->getAttributeSets()[0]->getSize();


        return $data;
    }
    public static function vieworderS($orderid,$storeid)
    {

        $config = AmazonApiconnector::getBatchConfig($storeid);

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\OrdersApi($config);

        //$getOrder = $apiInstance->getOrder($orderid)->getPayload();
        $buyerinfo = $apiInstance->getOrderBuyerInfo($orderid)->getPayload();
        $address = $apiInstance->getOrderAddress($orderid)->getPayload();
        $items = $apiInstance->getOrderItems($orderid)->getPayload();
        //print_r($items);
        //$mainorder=$apiInstance->getorder($orderid)->getPayload();

        $getOrderItems = $items->getOrderItems(); // Loop needed

        $shippAddress = $address->getShippingAddress();

        $data =  array();

        if (!is_null($shippAddress)) {

            //$data['order']  =  $getOrder;
            $data['buyeremail']  =  ($buyerinfo->getBuyerEmail() == '') ? '' : $buyerinfo->getBuyerEmail();

            $data['address1']  = $shippAddress->getAddressLine1();
            //($shippAddress->getAddressLine1()=='') ? '' : $shippAddress->getAddressLine1();
            $data['address2']  = ($shippAddress->getAddressLine2() == '') ? '' : $shippAddress->getAddressLine2();
            $data['municipality']  = ($shippAddress->getMunicipality() == '') ? '' : $shippAddress->getMunicipality();
            $data['postalcode']  = ($shippAddress->getPostalCode() == '') ? '' : $shippAddress->getPostalCode();
            $data['city']  = ($shippAddress->getCity() == '') ? '' : $shippAddress->getCity();
            $data['country']  = ($shippAddress->getCounty() == '') ? '' : $shippAddress->getCounty();
            $data['buyername']  = ($shippAddress->getName() == '') ? '' : $shippAddress->getName();
            $data['countrycode']  = ($shippAddress->getCountryCode() == '') ? '' : $shippAddress->getCountryCode();
        }
        $data['order_items']= $getOrderItems;
        //$data['order_details']  = $mainorder;

        //print_r($data['order_details']);
        //exit;
        //$catelogApiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($config);
        //$marketplace_id = $config->getMarketplaceid();
        //$productResponse = $catelogApiInstance->getCatalogItem($marketplace_id,$getOrderItems->getASIN())->getPayload();
        //$data['productURL']  = $productResponse->getAttributeSets()[0]->getSmallImage()->getURL() ?? '';
        //$productResponse->getAttributeSets()[0]->getSmallImage()->getURL()=='') ? '' : $productResponse->getAttributeSets()[0]->getSmallImage()->getURL();
        //$data['productSize']  = ($productResponse->getAttributeSets()[0]->getSize()=='') ? '' : $productResponse->getAttributeSets()[0]->getSize();


        return $data;
    }

    public static function getOrders($storeid, $lastup, $page = null)
    {
        /*if (Cache::has('orders')) {
            $orders=Cache::get('orders');
        } else {*/
        $nextToken = '';
        if ($page == null && Cache::has('nextToken')) {
            //$nextToken = Cache::pull('nextToken');
        }
        $config = AmazonApiconnector::getBatchConfig($storeid);

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\OrdersApi($config);
        $marketplace_id = $config->getMarketplaceid();

        if ($lastup=='') {
            $lastup = "2021-01-01";
        }

        $result = $apiInstance->getOrders(array($marketplace_id), null, null, $lastup, null, null, null, null, null, null, 10, null, $nextToken);
        $orders = $result->getPayload();
        $nextToken = $orders["next_token"] ?? '';

        Cache::put('nextToken', $nextToken, 3600);

        $data =  array();
        $data['next_token']  =  $nextToken;
        $data['orders']  =  $orders["orders"];

        //}
        //    print_r ($data);
        return $data;
    }

    public static function getCate($storeid, $asin)
    {
        if (Cache::has('apibatchconfig'.$storeid)) {
            $config=Cache::get('apibatchconfig'.$storeid);
            echo "Fetching from cache <br>";
        } else {
            $config = AmazonApiconnector::getBatchConfig($storeid);
            Cache::put('apibatchconfig'.$storeid, $config, 3600);
        }


        $cateapiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($config);
        $marketplace_id = $config->getMarketplaceid();

        $response=$cateapiInstance->getCatalogItem($marketplace_id,$asin);

        $cate = $response->getPayload()->getAttributeSets();

        return $cate;
    }

}
