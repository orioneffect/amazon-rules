<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fba_plan extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','store_id','shipment_id','to_country','seller_label','notes','from_name','from_address1','from_address2','from_city','from_country','from_state','from_pincode'];
}
