<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pricing_detail extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'store_id', 'price_1', 'price_2', 'min_profit','standard','maximum', 'one_seller', 'critic_stock','repricer_interval', 'formula','extraprofit'];
}
