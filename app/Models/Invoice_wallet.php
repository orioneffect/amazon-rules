<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice_wallet extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','wallet_amt','status','trans_id','paymt_status','paymt_type'];
}
