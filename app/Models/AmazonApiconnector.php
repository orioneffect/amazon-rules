<?php
namespace App\Models;
use Cache;
use Illuminate\Database\Eloquent\Model;
use Session;
use Exception;
class AmazonApiconnector
{

    public static function getBatchConfig($storeid) {

        //if (Cache::has('apiconfig')) {
        //    $config=Cache::get('apiconfig');
        //} else {
            $integration = Integration::where('id', '=', '1')->first();
            $userIntegration = User_amazon_integration::where('store_id', '=', $storeid)->first();
            $store = Store::where('id', '=', $storeid)->first();
            $country_id=$store->country_id;
            $country = Country::where('id', '=', $country_id)->first();

            $userIntegration = User_amazon_integration::where('store_id', '=', $storeid)->first();

            $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                $userIntegration->refresh_token,
                $integration->client_id,
                $integration->client_secret
            );
            $assumedRole = \ClouSale\AmazonSellingPartnerAPI\AssumeRole::assume(
                $country->region,
                $integration->access_key,
                $integration->secret_key,
                $integration->role_arn
            );

            $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
            $config->setHost($country->endpoint);
            $config->setAccessToken($accessToken);
            $config->setAccessKey($assumedRole->getAccessKeyId());
            $config->setSecretKey($assumedRole->getSecretAccessKey());
            $config->setRegion($country->region);
            $config->setSecurityToken($assumedRole->getSessionToken());
            $config->setMarketplaceid($country->marketplaceid);

            Cache::put('apibatchconfig', $config, 3600);
        //}

        //print_r($config);

        //exit;
        return $config;
    }


    public static function getConfig() {

        //if (Cache::has('apiconfig')) {
        //    $config=Cache::get('apiconfig');
        //} else {
            $integration = Integration::where('id', '=', '1')->first();
            Session::put('storeid',42);
            //dd(Session::get('storeid'));
            if(Session::get('storeid')==null)
            {
                Session::put('storeid',40);
            }
            
            $userIntegration = User_amazon_integration::where('store_id', '=', Session::get('storeid'))->first();
            $store = Store::where('id', '=', Session::get('storeid'))->first();
            $country_id=$store->country_id;
            $country = Country::where('id', '=', $country_id)->first();

            $userIntegration = User_amazon_integration::where('store_id', '=', Session::get('storeid'))->first();
            $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                $userIntegration->refresh_token,
                $integration->client_id,
                $integration->client_secret
            );
            $assumedRole = \ClouSale\AmazonSellingPartnerAPI\AssumeRole::assume(
                $country->region,
                $integration->access_key,
                $integration->secret_key,
                $integration->role_arn
            );

            $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
            $config->setHost($country->endpoint);
            $config->setAccessToken($accessToken);
            $config->setAccessKey($assumedRole->getAccessKeyId());
            $config->setSecretKey($assumedRole->getSecretAccessKey());
            $config->setRegion($country->region);
            $config->setSecurityToken($assumedRole->getSessionToken());
            $config->setMarketplaceid($country->marketplaceid);

            Cache::put('apiconfig', $config, 3600);
        //}

        //print_r($config);

        //exit;
        return $config;
    }

    public static function getUsaConfig() {

        if (Cache::has('apiconfig')) {
            $config=Cache::get('apiconfig');
        } else {
            $integration = Integration::where('id', '=', '2')->first();

            $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                $integration->admin_refresh_token,
                $integration->client_id,
                $integration->client_secret
            );
            $assumedRole = \ClouSale\AmazonSellingPartnerAPI\AssumeRole::assume(
                $integration->region,
                $integration->access_key,
                $integration->secret_key,
                $integration->role_arn
            );

            $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
            $config->setHost($integration->endpoint);
            $config->setAccessToken($accessToken);
            $config->setAccessKey($assumedRole->getAccessKeyId());
            $config->setSecretKey($assumedRole->getSecretAccessKey());
            $config->setRegion($integration->region);
            $config->setSecurityToken($assumedRole->getSessionToken());


            Cache::put('apiconfig', $config, 3600);
        }
        return $config;
    }
}
