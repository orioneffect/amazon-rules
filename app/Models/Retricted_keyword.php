<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Retricted_keyword extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','keywords'];
}
