<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail_account extends Model
{
    use HasFactory;
    protected $table="mail_accounts";
    protected $fillable = ['userid','buyermail','sellermail','stores','created_at','updated_at'];
}
