<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payoneer extends Model
{
    use HasFactory;
    protected $fillable = ['payoneer','user_id','default' ,'status'];
}
