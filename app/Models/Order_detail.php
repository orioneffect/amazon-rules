<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    use HasFactory;
    protected $fillable = ['order_id', 'item_id', 'sku', 'producttitle', 'asin', 'category', 'condition', 'quantity', 'price_usd', 'cost_usd', 'shipping_cost_usd', 'import_fee_usd', 'amz_comm_usd', 'profit_usd', 'price', 'cost', 'shipping_cost', 'import_fee', 'amz_comm', 'profit', 'profit_percent', 'productimage', 'status', 'created_at', 'updated_at'];
}