<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_feed extends Model
{
    use HasFactory;
    protected $fillable = [ 'store_id', 'feed_document_id', 'feed_id', 'feed_type', 'status', 'xml_content'];
}
