<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amazon_Product_Search_Api extends Model
{

    public function searchItems($searchtext,$nexttoken=null,$brandnames=null,$catenames=null) {

        if($nexttoken==""){
            $nexttoken=null;
        }

        if($brandnames==""){
            $brandnames=null;
        } else {
            $brandnames=implode(',',$brandnames);
        }

        if($catenames==""){
            $catenames=null;
        } else {
            $catenames=implode(',',$catenames);
        }

        $config=AmazonApiconnector::getUsaConfig();

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogNewApi($config);
        //$marketplace_id = $config->getMarketplaceid();
        $marketplace_id="ATVPDKIKX0DER";
        $result = $apiInstance->searchCatalogItems(array($searchtext),array($marketplace_id),array('identifiers,images,productTypes,salesRanks,summaries,variations'),$brandnames,$catenames,20,$nexttoken);
        return $result;
    }
}
