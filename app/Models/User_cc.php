<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_cc extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'cc', 'exp_mon','cvv','package_id','default_cc'];
}
