<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fbk_user_master extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'satisfied', 'answer_count'];
}
