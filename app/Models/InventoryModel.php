<?php
namespace App\Models;
use Cache;
use Illuminate\Database\Eloquent\Model;
use Session;
class InventoryModel extends Model
{
    public function getInventory($page=null) {



        /*

        if (Cache::has('integration')) {
            $integration=Cache::get('integration');
        } else {*/
            $integration = Integration::where('id', '=', '1')->first();
          //  Cache::put('integration', $integration, 3600);
        //}


        $userIntegration = User_amazon_integration::where('store_id', '=', Session::get('storeid'))->first();
        $store = Store::where('id', '=', Session::get('storeid'))->first();
        $country_id=$store->country_id;
        $country = Country::where('id', '=', $country_id)->first();


        //require_once '../vendor/autoload.php';
        /*if (Cache::has('apiconfig')) {
            $config=Cache::get('apiconfig');
        } else {*/
            $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                $userIntegration->refresh_token,
                $integration->client_id,
                $integration->client_secret
            );
            $assumedRole = \ClouSale\AmazonSellingPartnerAPI\AssumeRole::assume(
                $integration->region,
                $integration->access_key,
                $integration->secret_key,
                $integration->role_arn
            );

            $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
            $config->setHost($integration->endpoint);
            $config->setAccessToken($accessToken);
            $config->setAccessKey($assumedRole->getAccessKeyId());
            $config->setSecretKey($assumedRole->getSecretAccessKey());
            $config->setRegion($integration->region);
            $config->setSecurityToken($assumedRole->getSessionToken());

            Cache::put('apiconfig', $config, 3600);
        //}
        /*if (Cache::has('orders')) {
            $orders=Cache::get('orders');
        } else {*/


            $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\FbaInventoryApi($config);

            $marketplace_id = 'A33AVAJ2PDY3EV';
            $granularity_type = "Marketplace"; // string | The granularity type for the inventory aggregation level.
            $granularity_id = 'A33AVAJ2PDY3EV'; // string | The granularity ID for the inventory aggregation level.
            $marketplace_ids = array($marketplace_id); // string[] | The marketplace ID for the marketplace for which to return inventory summaries.
            $details = false; // bool | true to return inventory summaries with additional summarized inventory details and quantities. Otherwise, returns inventory summaries only (default value).
            $result = $apiInstance->getInventorySummaries($granularity_type, $granularity_id, $marketplace_ids);

            print_r($result);
            exit;
    }
}
