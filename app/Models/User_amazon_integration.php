<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_amazon_integration extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'store_id', 'admin_integration_id','refresh_token'];
}
