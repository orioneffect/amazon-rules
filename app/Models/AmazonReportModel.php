<?php
namespace App\Models;
use Cache;
use Illuminate\Database\Eloquent\Model;
use Session;
use Exception;
class AmazonReportModel extends Model
{
    function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else return FALSE;
    }


    public function viewDoc($docId,$isJson=false) {

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi(AmazonApiconnector::getConfig());

        $result = $apiInstance->getReportDocument($docId);
        $payload=$result->getPayload();
        $compressionAlgorithm = $payload["compression_algorithm"];
        $key = $payload["encryption_details"]["key"];
        $iv = $payload["encryption_details"]["initialization_vector"];
        $reporturl = $payload["url"];

        $initializationVector = base64_decode($iv, true);
        $key = base64_decode($key, true);

        $fileContent=$this->curl_get_file_contents($reporturl);

        $decryptedFile = \App\Helper\ASECryptoStream::decrypt($fileContent, $key, $initializationVector);
        if(isset($compressionAlgorithm) && $compressionAlgorithm=='GZIP') {
            $decryptedFile=gzdecode($decryptedFile);
        }
        if($isJson==false) {
            $decryptedFile=str_replace("\n", '***', $decryptedFile);
            $decryptedFile=str_replace("\t", '^^^', $decryptedFile);
        }
        return $decryptedFile;

    }


    public function viewBatchDoc($docId,$storeid,$isJson=false) {

        $config=AmazonApiconnector::getBatchConfig($storeid);

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi($config);

        $result = $apiInstance->getReportDocument($docId);
        $payload=$result->getPayload();
        $compressionAlgorithm = $payload["compression_algorithm"];
        $key = $payload["encryption_details"]["key"];
        $iv = $payload["encryption_details"]["initialization_vector"];
        $reporturl = $payload["url"];

        $initializationVector = base64_decode($iv, true);
        $key = base64_decode($key, true);

        $fileContent=$this->curl_get_file_contents($reporturl);

        $decryptedFile = \App\Helper\ASECryptoStream::decrypt($fileContent, $key, $initializationVector);
        if(isset($compressionAlgorithm) && $compressionAlgorithm=='GZIP') {
            $decryptedFile=gzdecode($decryptedFile);
        }
        if($isJson==false) {
            $decryptedFile=str_replace("\n", '***', $decryptedFile);
            $decryptedFile=str_replace("\t", '^^^', $decryptedFile);
        }
        return $decryptedFile;
    }


    public function viewReport($reportId) {

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi(AmazonApiconnector::getConfig());
        $result = $apiInstance->getReport($reportId);
        //dd($result["payload"]);
        echo "<pre>";
            print_r($result);
            echo "</pre>";
        try {
            $docId = $result["payload"]["report_document_id"];
        } catch (Exception $e) {
            $docId="";
        }
        return $docId;
    }


    public function viewBatchReport($reportId,$storeid) {

        $config=AmazonApiconnector::getBatchConfig($storeid);

        $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi($config);
        $result = $apiInstance->getReport($reportId);
        try {
            $docId = $result["payload"]["report_document_id"];
        } catch (Exception $e) {
            $docId="";
        }
        return $docId;
    }
    public function getReport($reportType,$fromDt='',$toDt='') {

        if($fromDt=='' || $toDt=='') {
            $fromDt=null;
            $toDt=null;
        }

        try {

            $config=AmazonApiconnector::getConfig();
            $marketplace_id = $config->getMarketplaceid();
            $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi($config);
            $requestBody=new \ClouSale\AmazonSellingPartnerAPI\Models\Reports\CreateReportSpecification([
                'report_type' => $reportType,
                'marketplace_ids' => [$marketplace_id],
                'data_start_time' => $fromDt,
                'data_end_time' => $toDt,
            ]);
            $result = $apiInstance->createReport($requestBody);
            $reportid = $result["payload"]["report_id"];
        } catch (ApiException $e) {
            $reportid="";
        }
        return $reportid;
    }
    public function getBatchReport($reportType,$storeid) {

        try {

            $config=AmazonApiconnector::getBatchConfig($storeid);
            $marketplace_id = $config->getMarketplaceid();
            $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi($config);
            $requestBody=new \ClouSale\AmazonSellingPartnerAPI\Models\Reports\CreateReportSpecification([
                'report_type' => $reportType,
                'marketplace_ids' => [$marketplace_id]
            ]);
            $result = $apiInstance->createReport($requestBody);
            $reportid = $result["payload"]["report_id"];
        } catch (ApiException $e) {
            $reportid="";
        }
        return $reportid;
    }


    public function deleteReport($reportId) {

        $config = $this->getConfig();

        try {
            $apiInstance = new \ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi($config);
            $result = $apiInstance->cancelReport($reportId);
            //print_r($result);
            //exit;
        } catch (Exception $e) {
            //print_r($e);
        }
    }
}
