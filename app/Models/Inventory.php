<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable = ['store_id', 'source', 'user_id', 'sku', 'asin', 'min_price', 'max_price', 'price', 'min_price_usd', 'max_price_usd', 'price_usd', 'stock', 'amazon_fee', 'product_cost_usd', 'status','lowest_product_cost_usd','lowest_product_cost','lowest_product_seller_id','target_product_cost_usd','target_product_cost','target_buy_box_seller_id','target_lowest_product_cost_usd','target_lowest_product_cost','target_lowest_product_seller_id','buy_box_seller_id','product_cost'];

}
