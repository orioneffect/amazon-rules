<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student_consultant extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'class_id', 'studname', 'email', 'company_name', 'notes', 'status', 'updated_at', 'created_at', 'end_at'];
}
