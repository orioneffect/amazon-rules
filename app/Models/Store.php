<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    protected $fillable = ['store_name', 'country_id','merchant','sku','stock','user_id','budget','price_update_flag','stock_update_flag','product_removal_flag','product_insert_flag','amazon_commission', 'currency_update_o', 'currency_update_t', 'sales_tax_c', 'import_fee_c', 'import_fee_t', 'import_fee_c', 'restricted_keywords', 'restricted_products', 'blacklist', 'whitelist', 'common_pools_for_brand', 'common_pools_for_product', 'handling_time_c', 'handling_time_t', 'order_confirmation', 'pre_order_confirmation_c', 'pre_order_confirmation_t', 'pre_order_confirmation'
    ,'country_same_asin','add_tax1_c','add_tax1_t','add_tax2_c','add_tax2_t','black_seller','norepricer_black_seller','norepricer_black_seller','cate_black','enable_seller_central_tax','shippingfee_c','shippingfee_t','shippingfee_o'];
}
