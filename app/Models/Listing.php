<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'store_id', 'listing_id', 'sku', 'asin', 'quantity', 'product_id', 'product_name', 'status', 'created_at', 'updated_at','price'];
}
