<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance_transfer extends Model
{
    use HasFactory;
    
    protected $table="balance_transfers";
    protected $fillable = ['userid','targetemail','targetuserid','currency','exccurvalue','amount','curdate','created_at','updated_at'];

}
