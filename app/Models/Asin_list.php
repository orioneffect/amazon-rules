<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asin_list extends Model
{
    use HasFactory;
    protected $fillable = ['category','user_id','store_id','brand_or_product','list_name','trade_mark','type','asin'];
}
