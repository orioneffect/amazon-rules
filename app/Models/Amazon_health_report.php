<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amazon_health_report extends Model
{
    use HasFactory;
    protected $fillable = ['report_content', 'user_id','amazon_report_id','store_id'];
}
