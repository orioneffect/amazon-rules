<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fbk_user_answer extends Model
{
    use HasFactory;
    protected $fillable = ['feedbk_id','qid','user_ans','user_ans_text', 'status','updated_at','created_at'];
}
