<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'classroom_name', 'teacher', 'features', 'notes', 'status', 'edu_st_date', 'edu_end_date', 'updated_at'];
}
