<?php

namespace App\Models;

use DB;
use Auth;
use Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricingCalc extends Model
{
    use HasFactory;

    public static function calcSellingPrice($productprice, $profit, $tax1, $tax1type, $tax2, $tax2type, $tax3, $tax3type, $tax4, $tax4type, $shipping, $importfee, $importfeeType, $formula, $amazon_commission, $sellertax = 0, $extraprofit = 0, $exchangerate = 0)
    {
        $data = Formula::find($formula);
        if (isset($data)) {
            if ($tax1 == "") {
                $tax1 = 0;
            }
            if ($tax2 == "") {
                $tax2 = 0;
            }
            if ($tax3 == "") {
                $tax3 = 0;
            }
            if ($tax4 == "") {
                $tax4 = 0;
            }
            if ($shipping == "") {
                $shipping = 0;
            }
            if ($importfee == "") {
                $importfee = 0;
            } else {
                if ($importfeeType == "P") {
                    $importfee = $productprice * $importfee / 100;
                }
            }

            $selleingPrice = 0;
            $price1 = PricingCalc::calcTax($tax1type, $tax1, $productprice, $profit, $data->id);
            $price2 = PricingCalc::calcTax($tax2type, $tax2, $productprice, $profit, $data->id);
            $price3 = PricingCalc::calcTax($tax3type, $tax3, $productprice, $profit, $data->id);
            $price4 = PricingCalc::calcTax($tax4type, $tax4, $productprice, $profit, $data->id);

            if ($data->id == 1) {
                $selleingPrice = $productprice + ($price1 + $price2 + $price3 + $price4) + ($shipping + $importfee);
            } elseif ($data->id == 2) {
                $prePrice2 = $productprice + $price1 + $price2 + $price3 + $price4;
                $selleingPrice = $prePrice2 + ($prePrice2 * $profit / 100) + ($shipping + $importfee);
            } elseif ($data->id == 3) {
                $prePrice2 = $productprice + $price1 + $price2 + $price3 + $price4 + $shipping + $importfee;
                $selleingPrice = $prePrice2 + ($prePrice2 * $profit / 100);
            } elseif ($data->id == 4) {
                $a = $productprice + ($productprice * $tax1 / 100);

                if ($tax2type == "P") {
                    $b = $a + ($a * $tax2 / 100);
                } else {
                    $b = $a + $tax2;
                }

                if ($tax3type == "P") {
                    $c = $b + ($b * $tax3 / 100);
                } else {
                    $c = $b + $tax3;
                }


                if ($tax3type == "P") {
                    $d = $c + ($c * $tax4 / 100);
                } else {
                    $d = $c + $tax4;
                }


                $e = $d + ($d * $profit / 100);

                $selleingPrice = $e + $importfee + $shipping;
            } elseif ($data->id == 5) {
                $a = $productprice + ($productprice * $tax1 / 100);

                if ($tax2type == "P") {
                    $b = $a + ($a * $tax2 / 100);
                } else {
                    $b = $a + $tax2;
                }

                if ($tax3type == "P") {
                    $c = $b + ($b * $tax3 / 100);
                } else {
                    $c = $b + $tax3;
                }


                if ($tax3type == "P") {
                    $d = $c + ($c * $tax4 / 100);
                } else {
                    $d = $c + $tax4;
                }

                $d = $d + $importfee + $shipping;
                $e = $d + ($d * $profit / 100);
                $selleingPrice = $e;
            } else {
                $selleingPrice = 0;
            }

            $selleingPriceTarget = ($selleingPrice * $exchangerate) + $extraprofit; // Adding extra profit.
            $withCommSelleingPrice = round(($selleingPriceTarget * 100) / (100 - $amazon_commission), 2);
            $finalvalue = ($sellertax * 100) / (100 - ($withCommSelleingPrice + $sellertax));
            return  $finalvalue;
        } else {
            return 0;
        }
    }

    private static function calcTax($taxType, $taxAmount, $productprice, $profit, $type)
    {
        switch ($type) {
            case 1:
                if ($taxType == "P") {
                    $price1 = ($productprice + ($productprice * $profit / 100)) * $taxAmount / 100;
                } else {
                    $price1 = $taxAmount;
                }
                break;
            case 2:
                if ($taxType == "P") {
                    $price1 = $productprice * $taxAmount / 100;
                } else {
                    $price1 = $taxAmount;
                }
                break;
            case 3:
                if ($taxType == "P") {
                    $price1 = $productprice * $taxAmount / 100;
                } else {
                    $price1 = $taxAmount;
                }
                break;
            case 4:
                if ($taxType == "P") {
                    $price1 = $productprice * $taxAmount / 100;
                } else {
                    $price1 = $taxAmount;
                }
                break;
            case 5:
                if ($taxType == "P") {
                    $price1 = $productprice * $taxAmount / 100;
                } else {
                    $price1 = $taxAmount;
                }
                break;
            default:
                $price1 = 0;
                break;
        }

        return $price1;
    }

    public static function getMinMax($price)
    {

        $data = DB::table('pricing_details')
            ->where('price_1', '>=', $price)
            //->where('price_2','<=',$price)
            ->where('user_id', '=', Auth::user()->id)
            ->where('store_id', '=', Session::get('storeid'))

            ->orderBy('price_1', 'asc')
            ->first();

        if (isset($data)) {

            echo "Min: " . ($price + ($price * $data->min_profit / 100)) . "<br>";
            echo "Max: " . ($price + ($price * $data->maximum / 100));
        } else {

            $data = DB::table('pricing_details')
                ->where('price_2', '<=', $price)
                //->where('price_2','<=',$price)
                ->where('user_id', '=', Auth::user()->id)
                ->where('store_id', '=', Session::get('storeid'))
                ->orderBy('price_2', 'desc')
                ->first();


            echo "Min: " . ($price + ($price * $data->min_profit)) . "<br>";
            echo "Max: " . ($price + ($price * $data->maximum));
        }
    }

    public static function getMinMaxProduct($price)
    {

        $data = DB::table('pricing_details')
            ->where('price_1', '<=', $price)
            //->where('price_2','<=',$price)
            ->where('user_id', '=', Auth::user()->id)
            ->where('store_id', '=', Session::get('storeid'))
            ->orderBy('price_1', 'asc')
            ->first();

        if (isset($data)) {
            return $data;
        } else {
            $data = DB::table('pricing_details')
                ->where('price_2', '>=', $price)
                //->where('price_2','<=',$price)
                ->where('user_id', '=', Auth::user()->id)
                ->where('store_id', '=', Session::get('storeid'))
                ->orderBy('price_2', 'desc')
                ->first();
            if (isset($data)) {
                return $data;
            } else {
                return null;
            }
        }
    }
}
