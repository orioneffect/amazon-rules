<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amazon_report extends Model
{
    use HasFactory;
    protected $fillable = ['report_type_id', 'from_date', 'to_date','user_id','amazon_report_id','store_id','admin_report','reportType','begning','total','filePath','fileProcessStatus'];
}
