<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notificationtype extends Model
{
    use HasFactory;
    protected $fillable = ['type_title', 'type_color', 'type_image', 'status', 'created_at', 'updated_at'];
}