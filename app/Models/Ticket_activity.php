<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket_activity extends Model
{
    use HasFactory;
    protected $fillable = ['message','message_by','user_id','url','ticket_id'];
    
}
