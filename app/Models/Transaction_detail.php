<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction_detail extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'plan_id', 'discount_amt', 'discount_code', 'price_u', 'price_t', 'final_price_u', 'final_price_t', 'status'];
}
