<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Class_consultant extends Model
{
    use HasFactory;
    protected $fillable = ['class_name', 'user_id', 'status', 'updated_at', 'created_at'];
}
