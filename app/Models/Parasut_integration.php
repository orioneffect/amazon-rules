<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parasut_integration extends Model
{
    use HasFactory;
    protected $table="parasut_integrations";
    protected $fillable = ['userid','client_id','client_secret','callback_urls','parusername','paruserpwd','invoicetype','pardescription','status'];
}
