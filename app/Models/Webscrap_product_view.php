<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Webscrap_product_view extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'asin', 'product_title', 'product_imgurl', 'product_cost', 'total_ratings', 'product_rating','source'];
}
