<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category_list extends Model
{
    use HasFactory;
    protected $fillable = ['category_name','user_id','store_id'];
}
