<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'store_id', 'amazon_order_id', 'ordertotal_usd', 'ordertotal', 'currency_code', 'buyername', 'buyeremail', 'address1', 'address2', 'municipality', 'postal', 'city', 'country', 'shipping_id', 'number_of_items_shipped', 'number_of_items_unshipped', 'purchasedate', 'deliverydate', 'orderstatus', 'created_at', 'updated_at'];
}
