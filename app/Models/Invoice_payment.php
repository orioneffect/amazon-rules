<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice_payment extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'payment_type', 'from_account_name', 'to_account_name', 'amount', 'description', 'status','cc_type', 'trans_id'];
}
