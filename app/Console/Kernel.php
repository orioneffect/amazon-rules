<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // CRON TO REQUEST INVENTORY REPORT FROM AMAZON
        $schedule->call('\App\Http\Controllers\BatchController@updateinventory')->twiceDaily(env('REUESTINVENTORYREPORT_CRON_AT'), env('AND_REUESTINVENTORYREPORT_CRON_AT'))->name('RequestInventoryFromAmazon')->withoutOverlapping();

        // CRON TO CHECK IF INVENTORY REPORT REQUESTED IS READY TO DOWNLOAD THEN DOWNLOAD IT 
        $schedule->call('\App\Http\Controllers\BatchController@updateinventorydoc')->cron('*/'.env('REUESTINVENTORYREPORT_GETDOC_CRON_EVERY').' * * * *')->name('RequestInventoryDocFromAmazon')->withoutOverlapping();

        // IMPORT THE DOWNLOADED INVENTORY FILE TO THE DATABASE 
        $schedule->call('\App\Http\Controllers\BatchController@processinventorydoc')->cron('*/'.env('IMPORTINVENTORYREPORT_TODATABASE_CRON_EVERY').' * * * *')->name('ImportInventoryToDatabase')->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
