<!DOCTYPE html>
<html>
<body>
<p>
Hello {{ $details['to_name'] }}
</p>
<p>
First, you must complete your registration by clicking on the below link:
</p>
<p>
<a href="{{$details['link']}}">Click Here to confirm</a>

</p>
<p>
See you there!
</p>
<br>
<p>
Best regards,
</p>
<p>
{{env('APP_NAME')}}
</p>
<p>

</body>
</html>
