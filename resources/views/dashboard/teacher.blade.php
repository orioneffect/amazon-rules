@extends('layout.master')
@section('parentPageTitle', __('dashboard.dashboard'))
@section('title', __('dashboard.teacher'))
@section('content')

@if (session()->has('message'))
    <div class="alert alert-success">
        {{ __('dashboard.teacher_added', ['row' => session()->get('message')]) }}
    </div>
@endif
@if (session()->has('message_updt'))
    <div class="alert alert-success">
        {{ __('dashboard.teacher_updt', ['row' => session()->get('message_updt')]) }}
    </div>
@endif
@if (session()->has('err'))
    <div class="alert alert-danger">
        {{ __('dashboard.duplicate_rows', ['row' => session()->get('err')]) }}
    </div>
@endif

<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item" onclick="$('#updatesection').hide();"><a class="nav-link active show"
                    data-toggle="tab" href="#list"><i class="fa fa-list"></i> {{ __('dashboard.teacher_list') }}</a>
            </li>
            <li class="nav-item" onclick="$('#updatesection').hide();"><a class="nav-link" data-toggle="tab"
                    href="#addnew"><i class="fa fa-plus-square"></i> {{ __('dashboard.add_teacher') }}</a></li>
            <li class="nav-item displaynone" id="updatesection"><a class="nav-link" data-toggle="tab"
                    href="#update"><i class="fa fa-pencil-square-o"></i> {{ __('dashboard.update_teacher') }}</a></li>
        </ul>
        <div class="tab-content mt-2">
            <div class="tab-pane show active" id="list">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="row clearfix">
                                <div class="col-lg-10">
                                    <div class="card">
                                        <div class="body">
                                            <div class="table-responsive">
                                                <input type="hidden" value="teachers" id="refname">
                                                <table class="table table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>{{ __('dashboard.teacher_name') }}</th>
                                                            <th>{{ __('dashboard.email') }}</th>
                                                            <th>{{ __('dashboard.actions') }}</th>

                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th>{{ __('dashboard.teacher_name') }}</th>
                                                            <th>{{ __('dashboard.email') }}</th>
                                                            <th>{{ __('dashboard.actions') }}</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody id="con_student_tbl">
                                                        @isset($teacher)
                                                            @foreach ($teacher as $list)
                                                                <tr>
                                                                    <td>{{ $list->teacher }}</td>
                                                                    <td>{{ $list->email }}</td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-sm btn-default"
                                                                            title="" data-toggle="tooltip"
                                                                            data-placement="top"
                                                                            data-original-title="Update"
                                                                            onclick=up_teacher({{ $list->id }})><i
                                                                                class="fa fa-pencil-square-o"></i></button>
                                                                        <!--<button type="button" class="btn btn-sm btn-default" title="" data-toggle="tooltip" data-placement="top" data-original-title="Ban"><i class="icon-trash" onclick=demolish({{ $list->id }})></i></button>-->
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endisset
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="addnew">
                <div class="col-12 displaynone" id="successmsg">
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle"></i> <span id="insertedcount"></span>
                    </div>
                    <div class="alert alert-warning displaynone" role="alert" id="duplicatecountdisplay">
                        <i class="fa fa-warning"></i> <span id="duplicatecount"></span>
                    </div>
                </div>
                <form id="frm" method="POST" action="{{ route('dashboard.add_teacher') }}">
                    @csrf
                    <div class="body mt-2">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{ __('dashboard.name_teacher') }} <span></span>*</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="teacher_name"
                                        name="teacher_name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{ __('dashboard.teacher_email') }} *</label>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="" id="email" name="email"
                                        required>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i>
                                    {{ __('dashboard.add_teacher') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="update">
                <div class="col-12 displaynone" id="successmsg">
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle"></i> <span id="insertedcount"></span>
                    </div>
                    <div class="alert alert-warning displaynone" role="alert" id="u_duplicatecountdisplay">
                        <i class="fa fa-warning"></i> <span id="u_duplicatecount"></span>
                    </div>
                </div>
                <form id="u_frm" method="POST" action="{{ route('dashboard.update_teacher') }}">
                    @csrf
                    <div class="body mt-2">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{ __('dashboard.name_teacher') }}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="u_teacher_name"
                                        name="u_teacher_name" required>
                                    <input type="hidden" id="u_id" name="u_id">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{ __('dashboard.teacher_email') }}</label>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="" id="u_email"
                                        name="u_email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <button type="submit" id="u_btn" class="btn btn-primary"><i
                                        class="fa fa-plus-square"></i> {{ __('dashboard.update_teacher') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">


    <style>
        td.details-control {
            background: url('../assets/images/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('../assets/images/details_close.png') no-repeat center center;
        }

        .bootstrap-tagsinput {
            background-color: rgb(204, 204, 204) !important;
        }

    </style>

@stop


@section('page-script')
    <script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>

    <script src="{{ asset('js/common/common.js?v=1.2') }}"></script>


    <script>
        $('#teacherlist').multiselect({
            includeSelectAllOption: true,
        });

        $('#u_teacherlist').multiselect({
            includeSelectAllOption: true,
        });

        $('#frm').parsley();

        $('#u_frm').parsley();

        function up_teacher(id) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/dashboard/get_teacher?id=" + id,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $('[href="#update"]').tab('show');
                    $('#updatesection').show();
                    $("#u_id").val(response.id);
                    $("#u_teacher_name").val(response.teacher);
                    $("#u_email").val(response.email);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }
    </script>
@stop
