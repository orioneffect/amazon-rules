@extends('layout.master')
@section('parentPageTitle', __('dashboard.dashboard'))
@section('title', __('dashboard.prflss'))


@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card planned_task">
            <div class="header">
                <h2>{{__('dashboard.reppl')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">

                <div class="col-lg-4">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.income')}}</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{__('dashboard.prsale')}}</td>
                                <td class="text-green">$480.079,59</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.prref')}}</td>
                                <td class="text-danger">(-$21.052,57)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.cogs')}}</td>
                                <td class="text-danger">(-$177.376,86)</td>
                              
                            </tr>
                            <tr>
                                <td>{{__('dashboard.cogr')}}</td>
                                <td>$7.036,73</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.invcre')}}</td>
                                <td>$8.901,80</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shpcr')}}</td>
                                <td>$7.259,31</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shcrer')}}</td>
                                <td class="text-danger">(-$132,15)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.gwcre')}}</td>
                                <td>$366,62</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.preb')}}</td>
                                <td class="text-danger">(-$3.646,58)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.prorere')}}</td>
                                <td>$45,12</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.refee')}}</td>
                                <td>$31,05</td>
                               
                            </tr>
                            <tr>
                                <td><strong>{{__('dashboard.toinc')}}</strong></td>
                                <td><strong>$301.512,06</strong></td>
                               
                            </tr>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>                    
    </div>


    <div class="col-lg-4">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.expenses')}}</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <td>{{__('dashboard.sellfee')}}</td>
                                <td class="text-danger">(-$71.214,83)</td>
                                
                            </tr>
                            <tr>
                                <td>{{__('dashboard.selferef')}}</td>
                                <td class="text-green">$3.286,87</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.readfe')}}</td>
                                <td class="text-danger">(-$644,64)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbfupuf')}}</td>
                                <td class="text-danger">(-$98.864,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbfupof')}}</td>
                                <td class="text-danger">(-$17.001,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbawbf')}}</td>
                                <td class="text-danger">(-$15.483,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbretpof')}}</td>
                                <td class="text-danger">(-$12.465,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbretwebafe')}}</td>
                                <td class="text-danger">(-$8.152,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbretpuf')}}</td>
                                <td class="text-danger">(-$11.864,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.gwrchar')}}</td>
                                <td class="text-danger">(-$5.854,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shicharef')}}</td>
                                <td>$87.03</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shihold')}}</td>
                                <td class="text-danger">(-$1.154,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shichar')}}</td>
                                <td class="text-danger">(-$8.864,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fba30')}}</td>
                                <td class="text-danger">(-$5.864,71)</td>
                              
                            </tr>
                            <tr>
                                <td>{{__('dashboard.remof')}}</td>
                                <td class="text-danger">(-$98,1)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.subfee')}}</td>
                                <td class="text-danger">(-$98.864,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.fbaincainfe')}}</td>
                                <td class="text-danger">(-$64,71)</td>
                                
                            </tr>
                            <tr>
                                <td>{{__('dashboard.inshife')}}</td>
                                <td class="text-danger">(-$4,71)</td>
                                
                            </tr>
                            <tr>
                                <td>{{__('dashboard.labfee')}}</td>
                                <td class="text-danger">(-$98,71)</td>
                                
                            </tr>
                            <tr>
                                <td>{{__('dashboard.adjust')}}</td>
                                <td class="text-danger">(-$864,71)</td>
                                
                            </tr>
                            <tr>
                                <td><strong>{{__('dashboard.toexpens')}}</strong></td>
                                <td><strong>$200.024,89</strong></td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>                    
    </div>






    <div class="col-lg-4">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.tax')}}</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <td>{{__('dashboard.saletax')}}</td>
                                <td class="text-green">$20.763,53</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.saletaxref')}}</td>
                                <td class="text-danger">(-$979,81)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shiptax')}}</td>
                                <td class="text-green">$98,78</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.shiptaxre')}}</td>
                                <td class="text-danger">(-$2,71)</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.giwrtax')}}</td>
                                <td class="text-green">$12,63</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.marfacre')}}</td>
                                <td class="text-danger">(-$20.809,28)</td>
                              
                            </tr>
                            <tr>
                                <td>{{__('dashboard.marfactaref')}}</td>
                                <td class="text-green">$982,52</td>
                               
                            </tr>
                            <tr>
                                <td>{{__('dashboard.retbatax')}}</td>
                                <td class="text-green">$1,69</td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>                    
    </div>

    

            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop