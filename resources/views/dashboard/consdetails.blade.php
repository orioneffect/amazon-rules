@extends('layout.master')
@section('parentPageTitle', __('dashboard.dashboard'))
@section('title', __('dashboard.constst'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2>{{__('dashboard.studst')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
            <div class="col row align-items-end">   <div>
                          
                        </div>
            </div>
            <div class="row align-items-end">

            <div class="col">
                <a href="#" class="badge badge-success">{{__('dashboard.numinvent')}} <strong>4.792</strong></a>                   
                </div>
<div class="col-auto pr-0">
<form action="/switch-daterange" id="SwitchDateRange" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="SC1P1ewPQgKuhLWK3WIgr9PSUsDw8gE4e1b4yw-xOFPCCECzaIW8HrZpw9YU5uWwnwPpXHsUvHDb0TFe6k2fs63oa3Qzb8q-MlgzdyEvtv7P92KQ_0LfKvhcPKgN4DVEBdOE3AzqV-_XEM0a_SD92g2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><select class="form-control" data-val="true" data-val-number="The field DefaultSelectedDateRange must be a number." data-val-required="The DefaultSelectedDateRange field is required." id="daterange-selection" name="DefaultSelectedDateRange"><option selected="selected" value="0">{{__('dashboard.0')}}</option>
<option value="1">{{__('dashboard.1')}}</option>
<option value="3">{{__('dashboard.3')}}</option>
<option value="7">{{__('dashboard.7')}}</option>
<option value="14">{{__('dashboard.14')}}</option>
<option value="30">{{__('dashboard.30')}}</option>
<option value="60">{{__('dashboard.60')}}</option>
<option value="90">{{__('dashboard.90')}}</option>
<option value="180">{{__('dashboard.180')}}</option>
<option value="365">{{__('dashboard.365')}}</option>
<option value="36500">{{__('dashboard.alltime')}}</option>
</select></form></div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                    <option value="2" data-name="mexico" > Mexico</option>
                    <option value="3" data-name="uae"> U.A.E.</option>

                <option value="-1" data-name="allcountries">All Countries</option>
            </select>
</form>    </div>
</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="-1" data-name="{{__('dashboard.allclass')}}" selected="&quot;selected&quot;"  data-select2-id="2"> {{__('dashboard.allclass')}}</option>
                    <option value="2" data-name="birebir" > BIREBIR</option>
                    <option value="3" data-name="55">55</option>
                    <option value="4" data-name="winner">Winner</option>
            </select>
</form>    </div>
</div>
           </div>

<hr>

          


    <div class="row clearfix">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-indigo text-white rounded-circle"><i class="fa fa-briefcase"></i></div>
                    <div class="ml-4">
                        <span>{{__('dashboard.prsale')}}</span>
                        <h4 class="mb-0 font-weight-medium">$87,805</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-azura text-white rounded-circle"><i class="fa fa-credit-card"></i></div>
                    <div class="ml-4">
                        <span>{{__('dashboard.toprofit')}}</span>
                        <h4 class="mb-0 font-weight-medium">$53,651</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-orange text-white rounded-circle"><i class="fa fa-users"></i></div>
                    <div class="ml-4">
                        <span>{{__('dashboard.torder')}}</span>
                        <h4 class="mb-0 font-weight-medium">655</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-pink text-white rounded-circle"><i class="fa fa-life-ring"></i></div>
                    <div class="ml-4">
                        <span>{{__('dashboard.toreturn')}}</span>
                        <h4 class="mb-0 font-weight-medium">$13,651</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>


<div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.bestsell')}}</h3>
                <ul class="header-dropdown dropdown">
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <ul class="nav nav-tabs3">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#revenue-new2">Revenue</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#cat-new2">Categories</a></li>
            </ul>
            <div class="tab-content mt-0">
                <div class="tab-pane active show" id="revenue-new2">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                    <th>{{__('dashboard.classroom')}}</th>
                                    <th>{{__('dashboard.username')}}</th>
                                    <th>{{__('dashboard.country')}}</th>        
                                    <th>{{__('dashboard.store')}}</th>
                                    <th>{{__('dashboard.order')}}</th>
                                    <th>{{__('dashboard.sale')}}</th>
                                    <th>{{__('dashboard.esprof')}}</th>
                                    <th>%</th>
                        </thead>


                            <tfoot>
                            <th>{{__('dashboard.classroom')}}</th>
                                    <th>{{__('dashboard.username')}}</th>
                                    <th>{{__('dashboard.country')}}</th>        
                                    <th>{{__('dashboard.store')}}</th>
                                    <th>{{__('dashboard.order')}}</th>
                                    <th>{{__('dashboard.sale')}}</th>
                                    <th>{{__('dashboard.esprof')}}</th>
                                    <th>%</th>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td><span>BIREBIR</span></td>
                                    <td><span>Erkan Laçin</span></td>
                                    <td class="w40"><img src="../assets/images/flag/us.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Lacin's Store</span></td>
                                    <td><span>10</span></td>
                                    <td><span>$1.250	</span></td>
                                    <td><span class="badge badge-success">$375</span></td>
                                    <td class="w100 text-info"><strong>%30</strong></td>
                                </tr>

                                <tr>
                                <tr>
                                    <td><span>BIREBIR</span></td>
                                    <td><span>Samet Demirci</span></td>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Samet's Store</span></td>
                                    <td><span>9</span></td>
                                    <td><span>$1.150	</span></td>
                                    <td><span class="badge badge-success">$375</span></td>
                                    <td class="w100 text-info"><strong>%25</strong></td>
                                </tr>

                                
                                <tr>
                                    <td><span>55</span></td>
                                    <td><span>Hakan Laçin</span></td>
                                    <td class="w40"><img src="../assets/images/flag/au.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Hakan's Store</span></td>
                                    <td><span>9</span></td>
                                    <td><span>$1.50	</span></td>
                                    <td><span class="badge badge-success">$275</span></td>
                                    <td class="w100 text-info"><strong>%20</strong></td>
                                </tr>
                                

                                <tr>
                                    <td><span>55</span></td>
                                    <td><span>John Patrick</span></td>
                                    <td class="w40"><img src="../assets/images/flag/nl.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Angelo</span></td>
                                    <td><span>7</span></td>
                                    <td><span>$950	</span></td>
                                    <td><span class="badge badge-success">$175</span></td>
                                    <td class="w100 text-info"><strong>%17</strong></td>
                                </tr>

                                <tr>
                                    <td><span>Winner</span></td>
                                    <td><span>Gokhan Tunç</span></td>
                                    <td class="w40"><img src="../assets/images/flag/gb.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Lacin's Store</span></td>
                                    <td><span>5</span></td>
                                    <td><span>$650	</span></td>
                                    <td><span class="badge badge-success">$155</span></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                </tr>

                                <tr>
                                    <td><span>BIREBIR</span></td>
                                    <td><span>Erhan Turkoglu</span></td>
                                    <td class="w40"><img src="../assets/images/flag/gb.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Erhan's Store</span></td>
                                    <td><span>3</span></td>
                                    <td><span>$250	</span></td>
                                    <td><span class="badge badge-success">$50</span></td>
                                    <td class="w100 text-info"><strong>%20</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="cat-new2">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <tbody>
                                <thead>

                            <th>{{__('dashboard.country')}}</th>
                            <th></th>
                            <th>{{__('dashboard.category')}}</th>
                            <th>{{__('dashboard.orquantity')}}</th>
                            <th>{{__('dashboard.sale')}}</th>
                            <th>{{__('dashboard.esprof')}}</th>
                            <th>%</th>

                                </thead>
                                <tfoot>
                            <th>{{__('dashboard.country')}}</th>
                            <th></th>
                            <th>{{__('dashboard.category')}}</th>
                            <th>{{__('dashboard.orquantity')}}</th>
                            <th>{{__('dashboard.sale')}}</th>
                            <th>{{__('dashboard.esprof')}}</th>
                            <th>%</th>
                                </tfoot>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/us.svg" class="w35 rounded-circle"></td>
                                    <td class="w60"><img src="../assets/images/coin/BTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>256</span></td>
                                    <td><span class="badge badge-success w100 text-info">$28.650</span></td>
                                    <td class="w100 text-info"><strong>$8.750</strong></td>
                                    <td class="w100 text-info"><strong>%27,5</strong></td>
                                </tr>

                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td> 
                                    <td class="w60"><img src="../assets/images/coin/ETH.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Electronics</p></td>
                                    <td><span>175</span></td>
                                    <td><span class="badge badge-success w100 text-info">$17.500</span></td>
                                    <td class="w100 text-info"><strong>$5.425</strong></td>
                                    <td class="w100 text-info"><strong>%32</strong></td>
                                </tr>

                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/us.svg " class="w35 rounded-circle"></td> 
                                    <td class="w60"><img src="../assets/images/coin/XRP.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Toys</p></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$3.425</strong></td>
                                    <td class="w100 text-info"><strong>%27</strong></td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td> 
                                    <td class="w60"><img src="../assets/images/coin/qtum.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Automotive</p></td>
                                    <td><span>95</span></td>
                                    <td><span class="badge badge-success w100 text-info">$11.500</span></td>
                                    <td class="w100 text-info"><strong>$2.425</strong></td>
                                    <td class="w100 text-info"><strong>%25</strong></td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/gb.svg " class="w35 rounded-circle"></td> 
                                    <td class="w60"><img src="../assets/images/coin/BTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Beauty</p></td>
                                    <td><span>92</span></td>
                                    <td><span class="badge badge-success w100 text-info">$11.500</span></td>
                                    <td class="w100 text-info"><strong>$2.325</strong></td>
                                    <td class="w100 text-info"><strong>%24,7</strong></td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/gb.svg " class="w35 rounded-circle"></td> 
                                    <td class="w60"><img src="../assets/images/coin/neo.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Fine Arts</p></td>
                                    <td><span>50</span></td>
                                    <td><span class="badge badge-success w100 text-info">$6.500</span></td>
                                    <td class="w100 text-info"><strong>$1.250</strong></td>
                                    <td class="w100 text-info"><strong>$15</strong></td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/nl.svg " class="w35 rounded-circle"></td> 
                                    <td class="w60"><img src="../assets/images/coin/LTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Books</p></td>
                                    <td><span>45</span></td>
                                    <td><span class="badge badge-success w100 text-info">$3.500</span></td>
                                    <td class="w100 text-info"><strong>$925</strong></td>
                                    <td class="w100 text-info"><strong>$22</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row clearfix">
    
        <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                <h2>{{__('dashboard.yourtc')}}</h2>
                </div>
            <small class="text-muted">{{__('dashboard.saleperf')}}</small>
                <div class="table-responsive">
                    <table class="table table-hover table-custom spacing5 mb-0">
                    <tbody>
                        <tr>
                            <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>United States</small>
                                <h5 class="mb-0">$5,434</h5>
                            </td>
                            <td>
                                <span class="chart">5,3,7,8,6,1,4,9</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/au.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Australia</small>
                                <h5 class="mb-0">$2,015</h5>
                            </td>
                            <td>
                                <span class="chart">4,2,2,5,6,9,8,1</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/ca.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Canada</small>
                                <h5 class="mb-0">$1,005</h5>
                            </td>
                            <td>
                                <span class="chart">7,5,3,9,5,1,4,6</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/gb.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>United Kingdom</small>
                                <h5 class="mb-0">$2,850</h5>
                            </td>
                            <td>
                                <span class="chart">3,5,6,4,9,5,5,2</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/nl.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Netherlands</small>
                                <h5 class="mb-0">$1,052</h5>
                            </td>
                            <td>
                                <span class="chart">8,2,1,5,6,3,4,9</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>        
    
    


    <div class="col-lg-4 col-md-12">
                        <ul class="nav nav-tabs3">
                            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#TODAY">{{__('dashboard.today')}}</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#THIS-WEEK">{{__('dashboard.week')}}</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#THIS-MONTH">{{__('dashboard.month')}}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="TODAY">
                                <div class="big-text">
                                    <h3 class="text-success"><strong>$38.770</strong></h3>
                                </div>
                                <span>{{__('dashboard.catrev')}}</span>
                                <hr>                                
                                <div class="form-group mb-4">
                                    <label class="d-block">Home&Kitchen <span class="float-right">11.500 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-azura" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="d-block">Beauty <span class="float-right">10.750 <i class="fa fa-long-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="d-block">Toys <span class="float-right">10.020 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 23%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="d-block">Automotive <span class="float-right">6.500 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="THIS-WEEK">
                            <div class="big-text">
                                    <h3 class="text-success"><strong>$1.040.000</strong></h3>
                                </div>
                                <span>{{__('dashboard.catrev')}}</span>
                                <hr>                                
                                <div class="form-group mb-4">
                                    <label class="d-block">Home&Kitchen <span class="float-right">300.000 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-azura" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="d-block">Beauty <span class="float-right">280.000 <i class="fa fa-long-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="d-block">Toys <span class="float-right">260.000 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 23%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="d-block">Automotive <span class="float-right">200.000<i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="THIS-MONTH">
                            <div class="big-text">
                                    <h3 class="text-success"><strong>$255.689</strong></h3>
                                </div>
                                <span>{{__('dashboard.catrev')}}</span>
                                <hr>                                
                                <div class="form-group mb-4">
                                    <label class="d-block">Home&Kitchen <span class="float-right">75.000 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-azura" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="d-block">Beauty <span class="float-right">70.000 <i class="fa fa-long-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="d-block">Toys <span class="float-right">65.000 <i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 23%;"></div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="d-block">Automotive <span class="float-right">45.689<i class="fa fa-dollar"></i></span></label>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar bg-indigo" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>







</div>





            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jvectormap/jquery-jvectormap-2.0.3.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/flotscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/jvectormap.bundle.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/index4.js') }}"></script>
@stop