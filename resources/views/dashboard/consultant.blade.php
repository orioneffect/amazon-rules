@extends('layout.master')
@section('parentPageTitle', __('dashboard.dashboard'))
@section('title', __('dashboard.consset'))
@section('content')

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ __('dashboard.inserted_rows', ['row' => session()->get('message')]) }}
        </div>
    @endif
    @if (session()->has('message_updt'))
        <div class="alert alert-success">
            {{ __('dashboard.banstudent', ['row' => session()->get('message_updt')]) }}
        </div>
    @endif
    @if (session()->has('err'))
        <div class="alert alert-danger">
            {{ __('dashboard.duplicate_rows', ['row' => session()->get('err')]) }}
        </div>
    @endif


    <div class="modal fade" id="updatelogo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('dashboard.addlogo') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="frm" method="POST" action="{{ route('dashboard.addlogo') }}" enctype="multipart/form-data"
                        validate>
                        @csrf
                        <div>
                            <div class="header">
                                <p>{{ __('dashboard.load_logo') }} <small>{{ __('dashboard.tryimg') }}</small></p>
                            </div>
                            <div class="body form-group">
                                <input type="file" name="file" class="dropify" data-max-file-size="5120K"
                                    data-allowed-file-extensions="PNG png tiff jpg jpeg">
                                @isset($maindata['coid'])
                                    <input type="hidden" name="id" value={{ $maindata['coid'] }}>
                                @endisset
                            </div>
                            <div>
                                <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i>
                                    {{ __('dashboard.upload') }}</button>
                                <button type="button" class="btn btn-secondary" onclick="$('#ccalert').hide();"
                                    data-dismiss="modal">{{ __('settings.close') }}</button>
                            </div>
                        </div>
                        <div class="alert alert-warning displaynone" id="ccalert"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card planned_task">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="body">
                                <div class="table-responsive">
                                    <button type="button" class="btn btn-sm btn-primary float-right"
                                        data-original-title="Add Logo" data-toggle="modal" data-target="#updatelogo"
                                        data-whatever="updatelogo"><i class="fa fa-picture-o"></i>
                                        {{ __('dashboard.updatelogo') }}</button>
                                    <table class="table table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>{{ __('dashboard.clname') }}</th>
                                                <th>{{ __('dashboard.stuname') }}</th>
                                                <th>{{ __('dashboard.stuemail') }}</th>
                                                <th>{{ __('dashboard.comname') }}</th>
                                                <th>{{ __('dashboard.status') }}</th>
                                                <th>{{ __('dashboard.regdate') }}</th>
                                                <th>{{ __('dashboard.endate') }}</th>
                                                <th>{{ __('dashboard.notes') }}</th>
                                                <th>{{ __('dashboard.actions') }}</th>

                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>{{ __('dashboard.clname') }}</th>
                                                <th>{{ __('dashboard.stuname') }}</th>
                                                <th>{{ __('dashboard.stuemail') }}</th>
                                                <th>{{ __('dashboard.comname') }}</th>
                                                <th>{{ __('dashboard.status') }}</th>
                                                <th>{{ __('dashboard.regdate') }}</th>
                                                <th>{{ __('dashboard.endate') }}</th>
                                                <th>{{ __('dashboard.notes') }}</th>
                                                <th>{{ __('dashboard.actions') }}</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="con_student_tbl">


                                            @isset($tocons['student'])
                                                @foreach ($tocons['student'] as $list)

                                                    <tr>
                                                        <td>{{ $list->classroom_name }}</td>
                                                        <td>{{ $list->studname }}</td>
                                                        <td>{{ $list->email }}</td>
                                                        <td>{{ $list->company_name }}</td>
                                                        <td id="status{{ $list->id }}">
                                                            @if ($list->status == '2')
                                                                <span
                                                                    class="badge badge-warning ml-0 mr-0">{{ __('dashboard.disable') }}</span>
                                                            @elseif ($list->status == '1')
                                                                <span
                                                                    class="badge badge-danger ml-0 mr-0">{{ __('dashboard.banned') }}</span>
                                                            @elseif ($list->status == '3')
                                                                <span
                                                                    class="badge badge-success ml-0 mr-0">{{ __('dashboard.active') }}</span>
                                                            @elseif ($list->status == '0')
                                                                <span
                                                                    class="badge badge-pending ml-0 mr-0">{{ __('dashboard.pending') }}</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ $list->created_at }}</td>
                                                        <td>{{ $list->end_at }}</td>
                                                        <td>{{ $list->notes }}</td>
                                                        <td>
                                                            @if ($list->impersonate == '0')
                                                                <button type="button" class="btn btn-sm btn-default" title=""
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    data-original-title="Impersonate" disabled><i
                                                                        class="icon-link"></i></button>
                                                            @elseif ($list->impersonate == '1')
                                                                <input type="hidden" name="email" value={{ $list->email }}>

                                                                @php
                                                                    $encryptValue = Crypt::encryptString($list->email);
                                                                @endphp

                                                                <a href="{{ route('authentication.consvalidatelogin', ['user' => $encryptValue]) }}"
                                                                    class="btn btn-sm btn-default"
                                                                    data-original-title="Impersonate">
                                                                    <span class="badge badge-success ml-n2 mr-n2"><i
                                                                            class="icon-link"></i></span></a>
                                                            @endif
                                                            <button type="button" class="btn btn-sm btn-default" title=""
                                                                data-toggle="tooltip" data-placement="top"
                                                                data-original-title="Ban"
                                                                onclick=ban_student({{ $list->id }})><i
                                                                    class="icon-trash"></i></button>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                            @endisset

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <hr>

                <form id="studenttxt" method="POST" action="{{ route('dashboard.add_upload') }}" validate
                    enctype="multipart/form-data" onsubmit="document.getElementById('btn').disabled=true;">
                    @csrf

                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2>{{ __('dashboard.load1') }} <small>{{ __('dashboard.try1') }}</small></h2>
                            </div>


                            <div class="body form-group">

                                <select id="classroom" name="classroom" class="form-control" required>
                                    <option value="">{{ __('dashboard.select_classroom_name') }}</option>
                                    @isset($tocons['classroom'])
                                        @foreach ($tocons['classroom'] as $list)
                                            <option value="{{ $list->id }}">{{ $list->classroom_name }}</option>
                                        @endforeach
                                    @endisset
                                </select>

                                <input type="file" name="file" class="dropify" data-max-file-size="5120K"
                                    data-allowed-file-extensions="txt">
                            </div>
                            <div>
                                <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i>
                                    {{ __('dashboard.upload') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}">

    <style>
        td.details-control {
            background: url('../assets/images/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('../assets/images/details_close.png') no-repeat center center;
        }

    </style>
@stop

@section('page-script')
    <script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

    <script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
    <script src="{{ asset('assets/bundles/chartist.bundle.js') }}"></script>
    <script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
    <script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/index2.js') }}"></script>

    <script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>

    <script>
        function ban_student(id) {
            var message = "{{ __('dashboard.banned') }}";
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/dashboard/banstudent?id=" + id,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    console.log(response);
                    swal("Updated!", {
                        icon: "success",
                    });
                    $("#status" + id).html('<span class="badge badge-danger ml-0 mr-0">' + message + '</span>');
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }
    </script>
@stop
