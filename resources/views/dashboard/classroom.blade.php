@extends('layout.master')
@section('parentPageTitle', __('dashboard.dashboard'))
@section('title', __('dashboard.classroom'))
@section('content')

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ __('dashboard.class_added', ['row' => session()->get('message')]) }}
        </div>
    @endif
    @if (session()->has('message_updt'))
        <div class="alert alert-success">
            {{ __('dashboard.class_updt', ['row' => session()->get('message_updt')]) }}
        </div>
    @endif

    @if (session()->has('err'))
        <div class="alert alert-danger">
            {{ __('dashboard.duplicate_rows', ['row' => session()->get('err')]) }}
        </div>
    @endif


    <div class="row clearfix">
        <div class="card">
            <ul class="nav nav-tabs">
                <li class="nav-item" onclick="$('#updatesection').hide();"><a class="nav-link active show"
                        data-toggle="tab" href="#list"><i class="fa fa-list"></i> {{ __('dashboard.class_list') }}</a>
                </li>
                <li class="nav-item" onclick="$('#updatesection').hide();"><a class="nav-link" data-toggle="tab"
                        href="#addnew"><i class="fa fa-plus-square"></i> {{ __('dashboard.add_class_list') }}</a></li>
                <li class="nav-item displaynone" id="updatesection"><a class="nav-link" data-toggle="tab"
                        href="#update"><i class="fa fa-pencil-square-o"></i> {{ __('dashboard.update_class_list') }}</a>
                </li>
            </ul>
            <div class="tab-content mt-2">


                <div class="tab-pane show active" id="list">

                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12">
                            <div class="card planned_task">

                                <div class="row clearfix">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="body">
                                                <div class="table-responsive">
                                                    <input type="hidden" value="classrooms" id="refname">
                                                    <table class="table table-striped table-hover dataTable js-exportable">
                                                        <thead>
                                                            <tr>
                                                                <th>{{ __('dashboard.id') }}</th>
                                                                <th>{{ __('dashboard.clname') }}</th>
                                                                <th>{{ __('dashboard.teacher_name') }}</th>
                                                                <th>{{ __('dashboard.features') }}</th>
                                                                <th>{{ __('dashboard.edu_st_dt') }}</th>
                                                                <th>{{ __('dashboard.endate') }}</th>
                                                                <th>{{ __('dashboard.notes') }}</th>
                                                                <th>{{ __('dashboard.actions') }}</th>

                                                            </tr>
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th>{{ __('dashboard.id') }}</th>
                                                                <th>{{ __('dashboard.clname') }}</th>
                                                                <th>{{ __('dashboard.teacher_name') }}</th>
                                                                <th>{{ __('dashboard.features') }}</th>
                                                                <th>{{ __('dashboard.edu_st_dt') }}</th>
                                                                <th>{{ __('dashboard.endate') }}</th>
                                                                <th>{{ __('dashboard.notes') }}</th>
                                                                <th>{{ __('dashboard.actions') }}</th>
                                                            </tr>
                                                        </tfoot>
                                                        <tbody id="con_student_tbl">


                                                            @isset($toclass['classroom'])
                                                                @foreach ($toclass['classroom'] as $list)

                                                                    <tr>
                                                                        <td>{{ $list->id }}</td>
                                                                        <td>{{ $list->classroom_name }}</td>
                                                                        <td>
                                                                            @php
                                                                                $count = 0;
                                                                                foreach (explode(',', $list->teacher) as $key) {
                                                                                    echo $count ? ',' : '';
                                                                                    $count = 1;
                                                                                    echo $toclass['teacher'][$key];
                                                                                }
                                                                            @endphp
                                                                        </td>
                                                                        <td>{{ $list->features }}</td>
                                                                        <td>{{ $list->edu_st_date }}</td>
                                                                        <td>{{ $list->edu_end_date }}</td>
                                                                        <td>{{ $list->notes }}</td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-sm btn-default"
                                                                                title="" data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                data-original-title="Update"
                                                                                onclick=up_class({{ $list->id }})><i
                                                                                    class="fa fa-pencil-square-o"></i></button>
                                                                            <!--<button type="button" class="btn btn-sm btn-default"
                                                                                        title="" data-toggle="tooltip"
                                                                                        data-placement="top"
                                                                                        data-original-title="Remove"
                                                                                        onclick=demolish({{ $list->id }})><i
                                                                                            class="icon-trash"></i></button>-->
                                                                        </td>
                                                                    </tr>

                                                                @endforeach
                                                            @endisset

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>

                </div>

                <div class="tab-pane" id="addnew">
                    <div class="col-12 displaynone" id="successmsg">
                        <div class="alert alert-success" role="alert">
                            <i class="fa fa-check-circle"></i> <span id="insertedcount"></span>
                        </div>
                        <div class="alert alert-warning displaynone" role="alert" id="duplicatecountdisplay">
                            <i class="fa fa-warning"></i> <span id="duplicatecount"></span>
                        </div>
                    </div>
                    <form id="frm" method="POST" action="{{ route('dashboard.add_class') }}"
                        enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="body mt-2">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.classroom_name') }} <span class="text text-danger">*</span></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="" id="classname"
                                            name="classname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.select_teacher') }} <span class="text text-danger">*</span></label>
                                    <div class="multiselect_div">
                                        <select id="teacherlist" name="teacherlist[]" class="multiselect multiselect-custom"
                                            multiple="multiple" data-parsley-required
                                            data-parsley-trigger-after-failure="change"
                                            data-parsley-errors-container="#error-multiselect">
                                            @isset($toclass['teacher'])
                                                @foreach ($toclass['teacher'] as $key => $value)
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <p id="error-multiselect"></p>
                                </div>
                            </div>
                            <div class="row clearfix mb-3">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.features_list') }} <span class="text text-danger">*</span></label>
                                    <div class="input-group demo-tagsinput-area">
                                        <input required type="text" class="form-control" data-role="tagsinput"
                                            id="features" name="features" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.notes') }} <span class="text text-danger">*</span></label>
                                    <div class="form-group">
                                        <textarea required rows="5" class="form-control" placeholder="" id="class_notes"
                                            name="class_notes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix mb-3">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.edu_dt') }} <span class="text text-danger">*</span></label>
                                    <div class="input-daterange input-group" data-provide="datepicker">
                                        <input type="text" class="input-sm form-control" readonly required id="edu_dt_start"
                                            name="edu_dt_start">
                                        <span class="input-group-addon range-to">to</span>
                                        <input type="text" class="input-sm form-control" readonly required
                                            name="edu_dt_end">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i>
                                        {{ __('dashboard.create_class') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="update">
                    <div class="col-12 displaynone" id="u_successmsg">
                        <div class="alert alert-success" role="alert">
                            <i class="fa fa-check-circle"></i> <span id="u_insertedcount"></span>
                        </div>
                        <div class="alert alert-warning displaynone" role="alert" id="u_duplicatecountdisplay">
                            <i class="fa fa-warning"></i> <span id="u_duplicatecount"></span>
                        </div>
                    </div>
                    <form id="u_frm" method="POST" action="{{ route('dashboard.update_class') }}"
                        enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="body mt-2">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.classroom_name') }}</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="" id="u_classname"
                                            name="u_classname" required>
                                        <input type="hidden" id="u_id" name="u_id">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.select_teacher') }}</label>
                                    <div class="multiselect_div">
                                        <select id="u_teacherlist" name="u_teacherlist[]"
                                            class="multiselect multiselect-custom" multiple="multiple" data-parsley-required
                                            data-parsley-trigger-after-failure="change"
                                            data-parsley-errors-container="#u_error-multiselect">
                                            @isset($toclass['teacher'])
                                                @foreach ($toclass['teacher'] as $key => $value)
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <p id="u_error-multiselect"></p>
                                </div>
                            </div>
                            <div class="row clearfix mb-3">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.features_list') }}</label>
                                    <div class="input-group demo-tagsinput-area">
                                        <input required type="text" class="form-control" data-role="tagsinput"
                                            id="u_features" name="u_features" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.notes') }}</label>
                                    <div class="form-group">
                                        <textarea required rows="5" class="form-control" placeholder="" id="u_class_notes"
                                            name="u_class_notes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix mb-3">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{ __('dashboard.edu_dt') }}</label>
                                    <div class="input-daterange input-group" data-provide="datepicker">
                                        <input type="text" class="input-sm form-control" readonly required
                                            id="u_edu_dt_start" name="u_edu_dt_start">
                                        <span class="input-group-addon range-to">to</span>
                                        <input type="text" class="input-sm form-control" readonly required id="u_edu_dt_end"
                                            name="u_edu_dt_end">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <button type="submit" id="u_btn" class="btn btn-primary"><i
                                            class="fa fa-plus-square"></i>
                                        {{ __('dashboard.update_class_list') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    @stop

    @section('page-styles')
        <link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
            href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
        <link rel="stylesheet"
            href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}">
        <link rel="stylesheet"
            href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/multi-select/css/multi-select.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">


        <style>
            td.details-control {
                background: url('../assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }

            tr.shown td.details-control {
                background: url('../assets/images/details_close.png') no-repeat center center;
            }

            .bootstrap-tagsinput {
                background-color: rgb(204, 204, 204) !important;
            }

        </style>

    @stop


    @section('page-script')
        <script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
        <!-- Bootstrap Tags Input Plugin Js -->
        <script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
        <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
        <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>
        <script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
        <script src="{{ asset('assets/bundles/chartist.bundle.js') }}"></script>
        <script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
        <script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
        <script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/multi-select/js/jquery.multi-select.js?v=1.1') }}"></script>
        <!-- Multi Select Plugin Js -->
        <script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
        <!-- Bootstrap Colorpicker Js -->
        <script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
        <!-- Input Mask Plugin Js -->
        <script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>
        <script src="{{ asset('js/common/common.js?v=1.2') }}"></script>

        <script>
            $('#teacherlist').multiselect({
                includeSelectAllOption: true,
            });

            $('#frm').parsley();

            $('#u_frm').parsley();

            function up_class(id) {
                var ENDPOINT = "{{ url('/') }}";
                $.ajax({
                        url: ENDPOINT + "/dashboard/get_class?id=" + id,
                        datatype: "json",
                        type: "get",
                    })
                    .done(function(response) {
                        $('[href="#update"]').tab('show');
                        $('#updatesection').show();
                        $("#u_id").val(response.id);
                        $("#u_classname").val(response.classroom_name);
                        $('#u_teacherlist').val(response.teacher.split(','));

                        $('#u_teacherlist').multiselect({
                            includeSelectAllOption: true,
                        });

                        $('#u_features').tagsinput('add', response.features);
                        $('#u_features').tagsinput('refresh');
                        $("#u_class_notes").val(response.notes);
                        $("#u_edu_dt_start").val(response.edu_st_date);
                        $("#u_edu_dt_end").val(response.edu_end_date);
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError) {
                        console.log('Server error occured');
                    });
            }
        </script>
    @stop
