@extends('layout.master')
@section('parentPageTitle', __('dashboard.dashboard'))
@section('title', __('dashboard.userdash'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2>{{__('dashboard.dashboard')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
            <div class="col row align-items-end">
            <iframe src="https://free.timeanddate.com/clock/i7zfsr3x/n256/tlca/fn5/fs10/fcf00/tct/pct/ftb/pa8/tt0/tw1/tm1/th1/ta1/tb4" frameborder="0" width="116" height="38" allowtransparency="true"></iframe>
                
                        <div>
                            <div class="card-value float-right text-muted"><i class="wi wi-fog"></i></div>
                            <h6 class="mb-1">12°C</h6>
                        </div>
            </div>
            <div class="row align-items-end">

            <div class="col">
                <a href="#" class="badge badge-success">{{__('dashboard.numinvent')}} <strong>4.792</strong></a>                   
                </div>
<div class="col-auto pr-0">
<form action="/switch-daterange" id="SwitchDateRange" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="SC1P1ewPQgKuhLWK3WIgr9PSUsDw8gE4e1b4yw-xOFPCCECzaIW8HrZpw9YU5uWwnwPpXHsUvHDb0TFe6k2fs63oa3Qzb8q-MlgzdyEvtv7P92KQ_0LfKvhcPKgN4DVEBdOE3AzqV-_XEM0a_SD92g2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><select class="form-control" data-val="true" data-val-number="The field DefaultSelectedDateRange must be a number." data-val-required="The DefaultSelectedDateRange field is required." id="daterange-selection" name="DefaultSelectedDateRange"><option selected="selected" value="0">{{__('dashboard.0')}}</option>
<option value="1">{{__('dashboard.1')}}</option>
<option value="3">{{__('dashboard.3')}}</option>
<option value="7">{{__('dashboard.7')}}</option>
<option value="14">{{__('dashboard.14')}}</option>
<option value="30">{{__('dashboard.30')}}</option>
<option value="60">{{__('dashboard.60')}}</option>
<option value="90">{{__('dashboard.90')}}</option>
<option value="180">{{__('dashboard.180')}}</option>
<option value="365">{{__('dashboard.365')}}</option>
<option value="36500">{{__('dashboard.alltime')}}</option>
</select></form></div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                    <option value="2" data-name="mexico" > Mexico</option>
                    <option value="3" data-name="uae"> U.A.E.</option>

                <option value="-1" data-name="allcountries">All Countries</option>
            </select>
</form>    </div>
</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
                    <option value="2" data-name="immanuel" > immanuel</option>
                    <option value="3" data-name="Sirius">Sirius</option>
                    <option value="-1" data-name="allstores">All Stores</option>
            </select>
</form>    </div>
</div>
           </div>

<hr>
           <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card text-white  bg-success ">
                    <div class="card-header">{{__('dashboard.prsale')}} </div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h4  text-primary ">CN$ 150 / $ 120</div>
                        <div class="d-flex">
                            <small class="text-muted">{{__('dashboard.prvmnt')}}</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card text-white  bg-primary ">
                    <div class="card-header">{{__('dashboard.profit')}}</div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h4  text-success ">CN$ 45 / $ 35</div>
                        <div class="d-flex">
                            <small class="text-muted">{{__('dashboard.prvmnt')}}</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card text-white bg-warning">
                    <div class="card-header">{{__('dashboard.order')}}<span class="badge badge-secondary">25</span></div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h5 text-secondary">CN$ 1500 / $ 1182</div>
                        <div class="d-flex">
                            <small class="text-muted">{{__('dashboard.prvmnt')}}</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card text-white bg-danger">
                    <div class="card-header">{{__('dashboard.returns')}}<span class="badge badge-secondary">3</span></div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h5 text-danger">CN$ 127 / $ 100</div>
                       
                        <div class="d-flex">
                            <small class="text-muted">{{__('dashboard.prvmnt')}}</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<hr>

<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.order')}}<small>{{__('dashboard.anyfind')}}</small></h2>
                <ul class="header-dropdown dropdown">
                    
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>{{__('dashboard.country')}}</th>
                                <th>{{__('dashboard.store')}}</th>
                                <th>{{__('dashboard.image')}}</th>
                                <th>{{__('dashboard.description')}}</th>
                                <th>{{__('dashboard.asin')}}</th>
                                <th>{{__('dashboard.orderid')}}</th>
                                <th>{{__('dashboard.status')}}</th>
                                <th>{{__('dashboard.price')}}</th>
                                <th>{{__('dashboard.cost')}}</th>
                                <th>{{__('dashboard.shipping')}}</th>
                                <th>{{__('dashboard.importfee')}}</th>
                                <th>{{__('dashboard.amzcom')}}</th>
                                <th>{{__('dashboard.profit')}}</th>
                                <th>{{__('dashboard.date')}}</th>
                                <th>{{__('dashboard.shippro')}}</th>
                                <th>{{__('dashboard.ordernow')}}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>{{__('dashboard.country')}}</th>
                                <th>{{__('dashboard.store')}}</th>
                                <th>{{__('dashboard.image')}}</th>
                                <th>{{__('dashboard.description')}}</th>
                                <th>{{__('dashboard.asin')}}</th>
                                <th>{{__('dashboard.orderid')}}</th>
                                <th>{{__('dashboard.status')}}</th>
                                <th>{{__('dashboard.price')}}</th>
                                <th>{{__('dashboard.cost')}}</th>
                                <th>{{__('dashboard.shipping')}}</th>
                                <th>{{__('dashboard.importfee')}}</th>
                                <th>{{__('dashboard.amzcom')}}</th>
                                <th>{{__('dashboard.profit')}}</th>
                                <th>{{__('dashboard.date')}}</th>
                                <th>{{__('dashboard.shippro')}}</th>
                                <th>{{__('dashboard.ordernow')}}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                            <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td>  
                            <td><span>Lacin's Store</span></td>
                               <td><div class="avatar-group d-none d-sm-flex">
                                <a href="https://www.amazon.com/dp/B00V9XLMV8" title="" class="avatar avatar-xs" target="_blank" data-toggle="tooltip" data-original-title="B00V9XLMV8">
                                    <img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" width="30" height="40"alt="" class="avatar-img rounded-circle lazy" style="">
                                </a>

                </div></td> 
                <td>Snow Trausers</td>
                <td>B00V9XLMV8</td>
                <td>702-3654893-9829842</td>
                <td><span class="badge badge-success">{{__('dashboard.confirmed')}}</span></td>
                <td><div class="bg-primary text-white">$: 500,00</div> <p><div class="bg-danger text-white"> CN$: 635,00</div></td>
                <td><div class="bg-warning text-white">$: 250,00</div> <p> <div class="bg-danger text-white">CN$: 317,50</div></td>
                <td><div class="bg-info text-white">$: 20,00</div> <p> <div class="bg-danger text-white">CN$: 25,40</div></td>
                <td><div class="bg-info text-white">$: 18,00</div> <p> <div class="bg-danger text-white">CN$: 22,86</div></td>
                <td><div class="bg-info text-white">$: 100,00</div> <p> <div class="bg-danger text-white">CN$: 127,00</div></td>
                <td><div class="bg-success text-white">$: 112,00</div> <p> <div class="bg-danger text-white">CN$: 142,24</div></td>
                <td>15/09/2021</td>
                <td><div class="form-group">
<select name="shipping" id="shipping" class="form-control show-tick" required>
<option value="1" Selected>Shipentegra</option>
<option value="2">Shiphack</option>
<option value="3">Loncago</option>
<option value="4">Global Shipping</option>
                </select>
                </div>
</td>
                <td><span class="badge badge-success">{{__('dashboard.ordernow')}}</span></td>
                            </tr>

                            <tr>
                            <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td>  
                            <td><span>Angelo</span></td> 
                                <td><div class="avatar-group d-none d-sm-flex">
                                <a href="https://www.amazon.com/dp/B01KZOTTSO" title="" class="avatar avatar-xs" target="_blank" data-toggle="tooltip" data-original-title="B00V9XLMV8">
                                    <img src="https://m.media-amazon.com/images/I/91elt7DAGsL._SY450_.jpg" width="30" height="40"alt="" class="avatar-img rounded-circle lazy" style="">
                                </a>

                </div></td>
                <td>Any Product</td>
                <td>B01KZOTTSO</td>                
                <td>702-7656536-6773056</td>
                <td><span class="badge badge-danger">{{__('dashboard.cancelled')}}</span></td>
                <td><div class="bg-primary text-white">$: 500,00</div> <p><div class="bg-danger text-white"> CN$: 635,00</div></td>
                <td><div class="bg-warning text-white">$: 250,00</div> <p> <div class="bg-danger text-white">CN$: 317,50</div></td>
                <td><div class="bg-info text-white">$: 20,00</div> <p> <div class="bg-danger text-white">CN$: 25,40</div></td>
                <td><div class="bg-info text-white">$: 18,00</div> <p> <div class="bg-danger text-white">CN$: 22,86</div></td>
                <td><div class="bg-info text-white">$: 100,00</div> <p> <div class="bg-danger text-white">CN$: 127,00</div></td>
                <td><div class="bg-success text-white">$: 112,00</div> <p> <div class="bg-danger text-white">CN$: 142,24</div></td>
               <td>15/09/2021</td>
                <td><div class="form-group">
<select name="shipping" id="shipping" class="form-control show-tick" required>
<option value="1" Selected>Shipentegra</option>
<option value="2">Shiphack</option>
<option value="3">Loncago</option>
<option value="4">Global Shipping</option>
                </select>
                </div></td>
                <td><span class="badge badge-danger">{{__('dashboard.cancelled')}}</span></td>
                            </tr>

                            <tr>
                            <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td>  
                            <td><span>Immanuel Store</span></td> 
                                <td><div class="avatar-group d-none d-sm-flex">
                                <a href="https://www.amazon.com/dp/B01597SD8Y" title="" class="avatar avatar-xs" target="_blank" data-toggle="tooltip" data-original-title="B00V9XLMV8">
                                    <img src="https://m.media-amazon.com/images/I/71V+Xp-+adL._AC_SY355_.jpg" width="30" height="40"alt="" class="avatar-img rounded-circle lazy" style="">
                                </a>

                </div></td>
                <td>Any Product</td>
                <td>B01597SD8Y</td>                
                <td>702-7656536-6773056</td>
                <td><span class="badge badge-warning">{{__('dashboard.pending')}}</span></td>
                <td><div class="bg-primary text-white">$: 500,00</div> <p><div class="bg-danger text-white"> CN$: 635,00</div></td>
                <td><div class="bg-warning text-white">$: 250,00</div> <p> <div class="bg-danger text-white">CN$: 317,50</div></td>
                <td><div class="bg-info text-white">$: 20,00</div> <p> <div class="bg-danger text-white">CN$: 25,40</div></td>
                <td><div class="bg-info text-white">$: 18,00</div> <p> <div class="bg-danger text-white">CN$: 22,86</div></td>
                <td><div class="bg-info text-white">$: 100,00</div> <p> <div class="bg-danger text-white">CN$: 127,00</div></td>
                <td><div class="bg-success text-white">$: 112,00</div> <p> <div class="bg-danger text-white">CN$: 142,24</div></td>
               <td>15/08/2021</td>
                <td><div class="form-group">
<select name="shipping" id="shipping" class="form-control show-tick" required>
<option value="1" Selected>Shipentegra</option>
<option value="2">Shiphack</option>
<option value="3">Loncago</option>
<option value="4">Global Shipping</option>
                </select>
                </div></td>
                <td><span class="badge badge-primary">{{__('dashboard.waiting')}}</span></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


 
    

    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.bestsell')}}</h3>
                <ul class="header-dropdown dropdown">
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <ul class="nav nav-tabs3">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#product-new2">{{__('dashboard.products')}}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#cat-new2">{{__('dashboard.cats')}}</a></li>
            </ul>
            <div class="tab-content mt-0">
                <div class="tab-pane active show" id="product-new2">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                    <th>{{__('dashboard.country')}}</th>        
                                    <th>{{__('dashboard.store')}}</th>
                                    <th>{{__('dashboard.image')}}</th>
                                    <th>{{__('dashboard.description')}}</th>
                                    <th>{{__('dashboard.asin')}}</th>
                                    <th>{{__('dashboard.category')}}</th>
                                    <th>{{__('dashboard.quantity')}}</th>
                                    <th>{{__('dashboard.estcost')}}</th>
                                    <th>{{__('dashboard.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dashboard.view')}}</th>
                        </thead>


                            <tfoot>
                            <th>{{__('dashboard.country')}}</th>        
                                    <th>{{__('dashboard.store')}}</th>
                                    <th>{{__('dashboard.image')}}</th>
                                    <th>{{__('dashboard.description')}}</th>
                                    <th>{{__('dashboard.asin')}}</th>
                                    <th>{{__('dashboard.category')}}</th>
                                    <th>{{__('dashboard.quantity')}}</th>
                                    <th>{{__('dashboard.estcost')}}</th>
                                    <th>{{__('dashboard.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dashboard.view')}}</th>
                            </tfoot>
                            <tbody>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td>  
                            <td><span>Lacin's Store</span></td>
                                <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>{{__('dashboard.view')}}</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Immanuel Store</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Neverland</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/nl.svg " class="w35 rounded-circle">
                            </td> <td><span>Yonja Street</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="cat-new2">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <tbody>
                                <thead>
<th>{{__('dashboard.country')}}</th>
<th>{{__('dashboard.store')}}</th>
<th></th>
<th>{{__('dashboard.category')}}</th>
<th>{{__('dashboard.products')}}</th>
<th>{{__('dashboard.orquentity')}}</th>
<th>{{__('dashboard.badget')}}</th>
<th>{{__('dashboard.profit')}}</th>
<th>%</th>

                                </thead>
                                <tfoot>
<th>{{__('dashboard.country')}}</th>
<th>{{__('dashboard.store')}}</th>
<th></th>
<th>{{__('dashboard.category')}}</th>
<th>{{__('dashboard.products')}}</th>
<th>{{__('dashboard.orquentity')}}</th>
<th>{{__('dashboard.badget')}}</th>
<th>{{__('dashboard.profit')}}</th>
<th>%</th>

                                </tfoot>
                                <tr><td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Yonja Street</span></td>
                                    <td class="w60"><img src="../assets/images/coin/BTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="../assets/images/coin/ETH.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Immanuel</span></td>
                                    <td class="w60"><img src="../assets/images/coin/XRP.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Lacin</span></td>
                                    <td class="w60"><img src="../assets/images/coin/qtum.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="../assets/images/coin/BTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Sirius</span></td>
                                    <td class="w60"><img src="../assets/images/coin/neo.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/nl.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Neverland</span></td>
                                    <td class="w60"><img src="../assets/images/coin/LTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.repricer')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div id="stackedbar-chart" class="ct-chart"></div>
            </div>
        </div>
    </div>


    <div class="col-lg-3 col-md-9 col-sm-9">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.yourtc')}} <small class="text-muted">{{__('dashboard.streven')}}</small></h2>
            </div>                        
            <div class="table-responsive">
                <table class="table table-hover table-custom spacing5 mb-0">
                    <tbody>
                        <tr>
                            <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>United States</small>
                                <h6 class="mb-0">$5,434</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/au.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Australia</small>
                                <h6 class="mb-0">$2,015</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/ca.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Canada</small>
                                <h6 class="mb-0">$1,005</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/gb.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>United Kingdom</small>
                                <h6 class="mb-0">$2,850</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/nl.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Netherlands</small>
                                <h6 class="mb-0">$1,052</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
            </div>

        <div class="col-lg-3 col-md-9 col-sm-9">
        <div class="card">
            <div class="header">
                <h2>{{__('dashboard.yourts')}} <small class="text-muted">{{__('dashboard.tsreven')}}</small></h2>
            </div>                        
            <div class="table-responsive">
                <table class="table table-hover table-custom spacing5 mb-0">
                    <tbody>
                        <tr>
                            <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Sirius</small>
                                <h6 class="mb-0">$5,434</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/au.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Immanuel</small>
                                <h6 class="mb-0">$2,015</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/ca.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Angelo</small>
                                <h6 class="mb-0">$3,005</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/gb.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Yonja Street</small>
                                <h6 class="mb-0">$2,850</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../assets/images/flag/nl.svg" class="w35 rounded-circle">
                            </td>
                            <td>
                                <small>Lasera's World</small>
                                <h6 class="mb-0">$1,052</h6>
                            </td>
                            <td>
                                <span class="chart"><canvas width="51" height="35" style="display: inline-block; width: 51px; height: 35px; vertical-align: top;"></canvas></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
         
    


        


</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

<link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}">

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/chartist.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/index2.js') }}"></script>
@stop