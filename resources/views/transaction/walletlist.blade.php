@extends('layout.master')
@section('parentPageTitle', __('menus.trans'))
@section('title', __('transaction.walletlist'))

@section('content')

    <div class="tab-pane" id="Maincredits">
        <div class="row clearfix">
            <div class="col-12">
                <div class="card text-white bg-orange">
                    <div class="card-header">{{ __('transaction.credits') }}</div>


                    <div class="card-body">
                        <h5 class="card-title">{{ __('transaction.ccpayment') }}</h5>
                        <hr />


                        <button type="button" class="btn btn-primary">{{ __('transaction.paynow') }}</button>
                        <hr />
                        <h5 class="card-title">{{ __('transaction.payoneerpay') }}</h5>




                        <p class="card-text">
                        <div>


                            <div>
                                <button class="btn btn-primary mr-3 mb-5" type="button" data-toggle="collapse"
                                    data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    {{ __('transaction.addnewpayoneer') }}
                                </button>

                                <button class="btn btn-primary  mb-5" type="button" data-toggle="collapse"
                                    data-target="#informpayoneer" aria-expanded="false" aria-controls="collapseExample">
                                    {{ __('transaction.informpayoneer') }}
                                </button>

                            </div>
                            <div class="collapse" id="informpayoneer" style="">


                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>{{ __('transaction.listpayoneer') }}</td>
                                                    <td>
                                                        <div>
                                                            <select name="accounttype" id="accounttype"
                                                                class="form-control show-tick">
                                                                <option value="X" selected>erkanlacin.pynr@gmail.com
                                                                </option>
                                                                <option value="Y">lacin.pynr@gmail.com</option>
                                                                <option value="Z">support.orion@gmail.com</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ __('transaction.addc') }}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="addbalance" id="addbalance"
                                                                class="form-control show-tick" required>
                                                                <option value="50" Selected>$ 50</option>
                                                                <option value="75">$ 75</option>
                                                                <option value="100">$ 100</option>
                                                                <option value="150">$ 150</option>
                                                                <option value="200">$ 200</option>
                                                                <option value="300">$ 300</option>
                                                                <option value="400">$ 400</option>
                                                                <option value="500">$ 500</option>
                                                                <option value="750">$ 750</option>
                                                                <option value="1000">$ 1000</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group" id="credit_type"></div>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>{{ __('transaction.descpayoneer') }}</td>
                                                    <td colspan="2"><input type="textarea" name="descpayoneer"
                                                            id="descpayoneer" value="" class="form-control"
                                                            placeholder="{{ __('transaction.descpayoneer') }} *"
                                                            required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <button type="button" class="btn btn-secondary"
                                                            onclick="$('#ccalert').hide();"
                                                            data-dismiss="modal">{{ __('transaction.close') }}</button>
                                                        <button type="button" class="btn btn-primary"
                                                            onclick="save_cclist();">{{ __('transaction.save') }}</button>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="collapse" id="collapseExample" style="">
                                <p><span>{{ __('transaction.payoneerpayment') }}</span></p>
                                <div class="form-group demo-masked-input">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">

                                            <span class="input-group-text"><span><img
                                                        src="../assets/images/payoneer_Logo.png" data-toggle="tooltip"
                                                        data-placement="top" title="Payment Logo"
                                                        class="w150 h35 rounded"></span></span>
                                        </div>
                                        <input type="text" class="form-control" name="buyeraccount2" id="buyeraccount2">
                                    </div>
                                </div>

                                <button type="button" class="btn btn-secondary" onclick="$('#ccalert').hide();"
                                    data-dismiss="modal">{{ __('transaction.close') }}</button>
                                <button type="button" class="btn btn-primary"
                                    onclick="save_cclist();">{{ __('transaction.save') }}</button>

                            </div>
                            <div class="collapse" id="Deneme" style="">

                                <div class="d-flex align-items-center">

                                    <div class="modal-body">
                                        <form>
                                            @csrf
                                            <div class="form-group demo-masked-input">
                                                <label for="ccardnumber"
                                                    class="col-form-label">{{ __('transaction.ccnum') }}</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-credit-card"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control credit-card" name="ccardnumber"
                                                        id="ccardnumber" oninput="$('#ccalert').hide();">
                                                </div>
                                            </div>
                                            <div class="form-group demo-masked-input">
                                                <label for="expiredate"
                                                    class="col-form-label">{{ __('transaction.epdate') }}</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-calendar"></i></span>
                                                    </div>
                                                    <input type="month" class="form-control month" name="expiredate"
                                                        id="expiredate" oninput="$('#ccalert').hide();">
                                                </div>
                                            </div>
                                            <div class="form-group demo-masked-input">
                                                <label for="CVVV"
                                                    class="col-form-label">{{ __('transaction.cvv') }}</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                class="fa fa-credit-card-alt"></i></span>
                                                    </div>
                                                    <input type="number" name="CVVV" class="form-control month" required
                                                        data-parsley-min="3" value="" id="CVVV"
                                                        oninput="$('#ccalert').hide();">
                                                </div>


                                                <div class="alert alert-warning displaynone" id="ccalert"></div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" onclick="$('#ccalert').hide();"
                                            data-dismiss="modal">{{ __('transaction.close') }}</button>
                                        <button type="button" class="btn btn-primary"
                                            onclick="save_cclist();">{{ __('transaction.save') }}</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    </p>


                    <br>
                    <hr>
                    <h4><span class="badge badge-success"><strong>{{ __('transaction.orcredit') }} $ <span
                                    id="tot_credit"></span></strong></span></h4>
                    <br>

                    <div class="row clearfix">
                        <div class="col-lg-6">
                            <table>
                                <thead>
                                    <th style="width: 50%;">{{ __('transaction.addusd') }}</th>
                                    <th style="width: 50%;">{{ __('transaction.crtype') }}</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <select name="addbalance" id="addbalance" class="form-control show-tick"
                                                    required>
                                                    <option value="20" Selected>$ 20</option>
                                                    <option value="25">$ 25</option>
                                                    <option value="35">$ 35</option>
                                                    <option value="40">$ 40</option>
                                                    <option value="50">$ 50</option>
                                                    <option value="75">$ 75</option>
                                                    <option value="100">$ 100</option>
                                                    <option value="150">$ 150</option>
                                                    <option value="200">$ 200</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group" id="credit_type">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><button type="submit" class="btn btn-primary"
                                                onclick="save_credits();">{{ __('transaction.addc') }}</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <br>
                    <hr>

                    <div class="col-lg-6">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th style="width: 15%;">{{ __('transaction.addedorusd') }}</th>
                                    <th style="width: 15%;">{{ __('transaction.paytyp') }}</th>
                                </thead>
                                <tbody id="credits-wrapper">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@stop

@section('page-styles')

    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/nouislider/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
    <style>
        .demo-card label {
            display: block;
            position: relative;
        }

        .demo-card .col-lg-4 {
            margin-bottom: 30px;
        }

    </style>

@stop

@section('page-script')

    <script src="{{ asset('assets/bundles/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/ui/dialogs.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <!-- Bootstrap Colorpicker Js -->
    <script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
    <!-- Input Mask Plugin Js -->
    <script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="{{ asset('assets/vendor/nouislider/nouislider.js') }}"></script>
    <!-- noUISlider Plugin Js -->
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/advanced-form-elements.js') }}"></script>
    <script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>

    <script>
        function save_credits() {
            var addbalance = $('#addbalance').val();
            var credits_feetype = $('#credits_feetype').val();
            var ENDPOINT = "{{ url('/') }}";
            var data = {
                addbalance: addbalance,
                credits_feetype: credits_feetype,
                _token: "{{ csrf_token() }}"
            }
            $.ajax({
                    url: ENDPOINT + "/transaction/add_credits",
                    datatype: "json",
                    data: data,
                    type: "post",
                })
                .done(function(response) {
                    $("#credits-wrapper").html('');
                    load_credits();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {});
        }

        function save_cclist() {
            var cardno = $('#ccardnumber').val();
            var expdt = $('#expiredate').val();
            var cvv = $('#CVVV').val();
            var re16digit = /^\d{16}$/;
            var re3digit = /^\d{3}$/;
            var wscardno = cardno.replace(/ /g, "");
            $('#ccalert').hide();
            if (cardno == '' || !re16digit.test(wscardno)) {
                $('#ccalert').html("{{ __('transaction.please_enter_valid_cc_number') }}");
                $('#ccalert').show();
            } else if (expdt == '') {
                $('#ccalert').html("{{ __('transaction.please_enter_valid_exp_date') }}");
                $('#ccalert').show();
            } else if (cvv == '' || !re3digit.test(cvv)) {
                $('#ccalert').html("{{ __('transaction.please_enter_valid_cvv') }}");
                $('#ccalert').show();
            } else {
                addcclist(cardno, expdt, cvv);
                $('#ccalert').hide();
                $('#changecreditcardshipping').modal('toggle');
            }
        }

        function addcclist(cardno, expdt, cvv) {
            var ENDPOINT = "{{ url('/') }}";
            var data = {
                cardno: cardno,
                expdt: expdt,
                cvv: cvv,
                _token: "{{ csrf_token() }}"
            }
            $.ajax({
                    url: ENDPOINT + "/transaction/add_cclist",
                    datatype: "json",
                    data: data,
                    type: "post",
                })
                .done(function(response) {
                    $("#cclist-wrapper").html('');
                    load_cclist();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {});
        }

        function load_cclist() {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/transaction/view_cclist",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#cclist-wrapper").html(response);
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function del_cc(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, Confirm',
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true,    
            }, function(isConfirm) {
                if (isConfirm) {
                    var ENDPOINT = "{{ url('/') }}";
                    $.ajax({
                            url: ENDPOINT + "/transaction/delete_cc?id=" + id,
                            datatype: "json",
                            type: "get",
                        }).done(function(response) {
                            console.log(response);
                            if (response == 0) {
                                swal("Sorry! cant delete default", {
                                    icon: "warning",
                                });
                            } else {
                                swal("Deleted!", {
                                    icon: "success",
                                });
                            }
                            $("#cclist-wrapper").html('');
                            load_cclist();
                        })
                        .fail(function(jqXHR, ajaxOptions, thrownError) {
                            console.log('Server error occured');
                        });
                }
            });
        }

        function set_default_cc(id) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/transaction/update_default_cc?id=" + id,
                    datatype: "json",
                    type: "get",

                })
                .done(function(response) {
                    $("#cclist-wrapper").html('');
                    load_cclist();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }
    </script>
    <script>
        $(function() {
            load_cclist();
            load_credits();
            load_credit_type();
        });
    </script>

@stop
