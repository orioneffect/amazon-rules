@extends('layout.master')
@section('parentPageTitle', __('feedbacks.feedbacks'))
@section('title', __('feedbacks.createnew'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2>{{__('feedbacks.createnew')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
            <div class="header">
                <h6 class="text-primary">{{__('feedbacks.messagetittle')}}</h6>
            </div>


            <div class="card">
            <button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal">
            {{__('feedbacks.confirm')}}
</button>
<button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#exampleModal">
            {{__('feedbacks.delivered')}}
</button>
<button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#3">
            {{__('feedbacks.feedback')}}
</button>
<button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#4">
            {{__('feedbacks.feedback')}} 2
</button>
<button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#5">
            {{__('feedbacks.shipped')}}
</button>
<button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#6">
            {{__('feedbacks.review')}}
</button>
<button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#7">
            {{__('feedbacks.allinone')}}
</button>
<br><p>
            <div class="mail-inbox">
                <div class="mobile-left">
                    <a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i class="fa fa-bars"></i></a>
                </div>
                <div class="body mail-left">
                    <div class="mail-compose m-b-20">
                        <a href="#" class="btn btn-danger btn-block btn-round">{{__('feedbacks.variables')}}</a>
                    </div>
                    <div class="mail-side">
                        <ul class="nav">
                            <li>{!! trans('feedbacks.amazonorderid') !!}</li><p>
                            <li>{!! trans('feedbacks.asin') !!}</li><p>
                            <li>{!! trans('feedbacks.BuyerEmail') !!}</li><p>
                            <li>{!! trans('feedbacks.DeliveredDate') !!}</li><p>
                            <li>{!! trans('feedbacks.FeedbackLink') !!}</li><p>
                            <li>{!! trans('feedbacks.FLinkWithProduct') !!}</li><p>
                            <li>{!! trans('feedbacks.ProductName') !!}</li><p>
                            <li>{!! trans('feedbacks.ProductUrl') !!}</li><p>
                            <li>{!! trans('feedbacks.ProductUrlWithName') !!}</li><p>
                            <li>{!! trans('feedbacks.PurchaseDate') !!}</li><p>
                            <li>{!! trans('feedbacks.SenderName') !!}</li><p>
                            <li>{!! trans('feedbacks.ShipDate') !!}</li><p>
                            <li>{!! trans('feedbacks.StoreName') !!}</li><p>
                                                        
                        </ul>
                    </div>
                </div>
                <div class="body mail-right check-all-parent">
                    <div class="mail-compose">
                        <form>
                         
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Subject">
                            </div>
                        </form>
                        <div class="summernote">
                        Hello Amazon Valued Customer,
                            <br/>
                            <br/>
                            <p>Great news! Your order has been shipped. It left our warehouse earlier today on its way to you! You can expect it to be on your doorstep within ((days of delivery)) days</p>
                            <p>
                                <ul>
                                    <li>Product name 1</li>
                                    <li>Product name 2</li>
                                    <li>Product name 3</li>
                                </ul>
                            
<p>You made a great customer choice shopping with us. At ((Your Brand)), we truly care about your customer experience and, we are 100% dedicated to your complete satisfaction. Your feedback will help us to improve our services.</p>
<p>Thank you for shopping at ((Your Brand)). If you have any questions or concerns please let us know. If you do not receive your item within ((days of delivery)) days, then please contact Amazon support here: Amazon Customer Support</p>

<p>Best Regards,
<p>((Your Brand))
<p>Customer Care Team

                        </div>
                        <div class="m-t-30 text-right">
                            <button type="button" class="btn btn-success btn-round">{{__('feedbacks.save')}}</button>
                            <button type="button" class="btn btn-secondary btn-round">{{__('feedbacks.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('feedbacks.modaltitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {!! trans('feedbacks.modaldesc') !!}
      <!-- {{__('feedbacks.modaldesc')}} -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('feedbacks.close')}}</button>
      </div>
    </div>
  </div>
</div>




<h2>Advanced Form Example With Validation</h2>
<ul class="header-dropdown dropdown">
<li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
<li class="dropdown">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
<ul class="dropdown-menu">
<li><a href="javascript:void(0);">Action</a></li>
<li><a href="javascript:void(0);">Another Action</a></li>
<li><a href="javascript:void(0);">Something else</a></li>
</ul>
</li>
</ul>
</div>
<div class="body wizard_validation">
<form id="wizard_with_validation" method="POST" role="application" class="wizard clearfix" novalidate="novalidate"><div class="steps clearfix"><ul role="tablist"><li role="tab" class="first done" aria-disabled="false" aria-selected="false"><a id="wizard_with_validation-t-0" href="#wizard_with_validation-h-0" aria-controls="wizard_with_validation-p-0"><span class="number">1.</span> Account Information</a></li><li role="tab" class="current error" aria-disabled="false" aria-selected="true"><a id="wizard_with_validation-t-1" href="#wizard_with_validation-h-1" aria-controls="wizard_with_validation-p-1"><span class="current-info audible">current step: </span><span class="number">2.</span> Profile Information</a></li><li role="tab" class="last disabled" aria-disabled="true"><a id="wizard_with_validation-t-2" href="#wizard_with_validation-h-2" aria-controls="wizard_with_validation-p-2"><span class="number">3.</span> Terms Conditions - Finish</a></li></ul></div><div class="content clearfix">
<h3 id="wizard_with_validation-h-0" tabindex="-1" class="title">Account Information</h3>
<fieldset id="wizard_with_validation-p-0" role="tabpanel" aria-labelledby="wizard_with_validation-h-0" class="body" aria-hidden="true" style="left: -960.438px; display: none;">
<div class="row clearfix">
<div class="col-lg-4 col-md-12">
<div class="form-group">
<input type="text" class="form-control" placeholder="Username *" name="username" required="" aria-required="true" aria-invalid="false">
<label id="username-error" class="error" for="username" style="display: none;"></label></div>
</div>
<div class="col-lg-4 col-md-12">
<div class="form-group">
<input type="password" class="form-control validate-equalTo-blur" placeholder="Password *" name="password" id="password" required="" aria-required="true" aria-invalid="false">
<label id="password-error" class="error" for="password" style="display: none;"></label></div>
</div>
<div class="col-lg-4 col-md-12">
<div class="form-group">
<input type="password" class="form-control" placeholder="Confirm Password *" name="confirm" required="" aria-required="true" aria-invalid="false">
<label id="confirm-error" class="error" for="confirm" style="display: none;"></label></div>
</div>
</div>
</fieldset>
<h3 id="wizard_with_validation-h-1" tabindex="-1" class="title current">Profile Information</h3>
<fieldset id="wizard_with_validation-p-1" role="tabpanel" aria-labelledby="wizard_with_validation-h-1" class="body current" aria-hidden="false" style="left: 0px;">
<div class="row clearfix">
<div class="col-lg-4 col-md-12">
<div class="form-group">
<input type="text" name="name" placeholder="First Name *" class="form-control" required="" aria-required="true">
<label id="name-error" class="error" for="name">This field is required.</label></div>
</div>
<div class="col-lg-4 col-md-12">
<div class="form-group">
<input type="text" name="surname" placeholder="Last Name *" class="form-control" required="" aria-required="true">
<label id="surname-error" class="error" for="surname">This field is required.</label></div>
</div>
<div class="col-lg-4 col-md-12">
<div class="form-group">
<input type="email" name="email" placeholder="Email *" class="form-control" required="" aria-required="true">
<label id="email-error" class="error" for="email">This field is required.</label></div>
</div>
<div class="col-md-12">
<div class="form-group">
<input min="18" type="number" name="age" placeholder="Age *" class="form-control" required="" aria-required="true">
<div class="help-info">The warning step will show up if age is less than 18</div>
<label id="age-error" class="error" for="age">This field is required.</label></div>
<div class="form-group">
<textarea name="address" cols="30" rows="3" placeholder="Address *" class="form-control no-resize" required="" aria-required="true"></textarea>
<label id="address-error" class="error" for="address">This field is required.</label></div>
</div>
 </div>
</fieldset>
<h3 id="wizard_with_validation-h-2" tabindex="-1" class="title">Terms Conditions - Finish</h3>
<fieldset id="wizard_with_validation-p-2" role="tabpanel" aria-labelledby="wizard_with_validation-h-2" class="body" aria-hidden="true" style="display: none;">
<div class="form-group">
<div class="fancy-checkbox">
<label><input type="checkbox" name="acceptTerms"><span>I agree with the Terms and Conditions.</span></label>
</div>
</div>
</fieldset>
</div><div class="actions clearfix"><ul role="menu" aria-label="Pagination"><li class="" aria-disabled="false"><a href="#previous" role="menuitem">Previous</a></li><li aria-hidden="false" aria-disabled="false"><a href="#next" role="menuitem">Next</a></li><li aria-hidden="true" style="display: none;"><a href="#finish" role="menuitem">Finish</a></li></ul></div></form>
</div>











            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/summernote/dist/summernote.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/summernote/dist/summernote.js') }}"></script>
<script>
    $('.toggle-email-nav').on('click', function() {
		$('.mail-left').toggleClass('open');
	});
</script>
@stop