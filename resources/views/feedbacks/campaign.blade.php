@extends('layout.master')
@section('parentPageTitle', __('feedbacks.feedbacks'))
@section('title', __('feedbacks.campaign'))


@section('content')

<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2></h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
            <div class="col row align-items-end">
            <div>
            </div>
        </div>
            <div class="row align-items-end">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            {{__('feedbacks.readesc')}}
</button>
            <div class="col">
            
                </div>
<div class="col-auto pr-0">

</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                    <option value="2" data-name="mexico" > Mexico</option>
                    <option value="3" data-name="uae"> U.A.E.</option>

                <option value="-1" data-name="allcountries">All Countries</option>
            </select>
</form>    </div>
</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
                    <option value="2" data-name="immanuel" > immanuel</option>
                    <option value="3" data-name="Sirius">Sirius</option>
                    <option value="-1" data-name="allstores">All Stores</option>
            </select>
</form> 
</div>
</div>  
<button id="btn-add" type="button" class="btn btn-primary"><span>{{__('feedbacks.createnew')}}</span></button>
</div>
</div>
           </div>

<hr>


<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2>{{__('feedbacks.campaign')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <h4></h4>



<ul class="nav nav-tabs3">
<li class="nav-item"><a class="nav-link show active" data-toggle="tab" href="#Home-new2">{{__('feedbacks.allcampaign')}}</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-new2">{{__('feedbacks.activecampaign')}}</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Contact-new2">{{__('feedbacks.passivecampaign')}}</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane show active" id="Home-new2">
<h6></h6>

<div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>{{__('feedbacks.title')}}</th>
                                <th>{{__('feedbacks.sentdate')}}</th>
                                <th>{{__('feedbacks.readingrate')}}</th>
                                <th>{{__('feedbacks.statistics')}}</th>
                                <th>{{__('feedbacks.credate')}}</th>
                                <th>{{__('feedbacks.actions')}}</th>
                            </tr>
                        </thead>
                            <tfoot>
                        <tr>
                                <th>{{__('feedbacks.title')}}</th>
                                <th>{{__('feedbacks.sentdate')}}</th>
                                <th>{{__('feedbacks.readingrate')}}</th>
                                <th>{{__('feedbacks.statistics')}}</th>
                                <th>{{__('feedbacks.credate')}}</th>
                                <th>{{__('feedbacks.actions')}}</th>
                        </tr>
                            </tfoot>
                        <tbody>
                    
                    <tr>                           
                <td>{{__('feedbacks.read')}}</td>
                <td>01.09.2021</td>
                <td>10/7,5</td>
                <td>Sent - 100<p> 
                <div id="progress-striped-active" class="progress progress-striped active">
<div class="progress-bar progress-bar-primary" data-transitiongoal="75" aria-valuenow="75" style="width: 75%;">Read 75%</div>
</div>
<p>
<div id="progress-striped-active" class="progress progress-striped active">
<div class="progress-bar progress-bar-warning" data-transitiongoal="67" aria-valuenow="67" style="width: 67%;">Delivered 85%</div>
</div>

                </td>
                <td><span class="badge badge-success">21.09.2021</span></td>
                <td>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>

                </td>
                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>

</div>


<div class="tab-pane" id="Profile-new2">
<h6></h6>

<div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>{{__('feedbacks.title')}}</th>
                                <th>{{__('feedbacks.sentdate')}}</th>
                                <th>{{__('feedbacks.readingrate')}}</th>
                                <th>{{__('feedbacks.statistics')}}</th>
                                <th>{{__('feedbacks.credate')}}</th>
                                <th>{{__('feedbacks.actions')}}</th>
                            </tr>
                        </thead>
                            <tfoot>
                        <tr>
                                <th>{{__('feedbacks.title')}}</th>
                                <th>{{__('feedbacks.sentdate')}}</th>
                                <th>{{__('feedbacks.readingrate')}}</th>
                                <th>{{__('feedbacks.statistics')}}</th>
                                <th>{{__('feedbacks.credate')}}</th>
                                <th>{{__('feedbacks.actions')}}</th>
                        </tr>
                            </tfoot>
                        <tbody>
                    
                    <tr>                           
                <td>{{__('feedbacks.delivered')}}</td>
                <td>01.09.2021</td>
                <td>10/7,5</td>
                <td>Sent - 100<p> 
                <div id="progress-striped-active" class="progress progress-striped active">
<div class="progress-bar progress-bar-primary" data-transitiongoal="75" aria-valuenow="75" style="width: 75%;">Read 75%</div>
</div>
<p>
<div id="progress-striped-active" class="progress progress-striped active">
<div class="progress-bar progress-bar-warning" data-transitiongoal="67" aria-valuenow="67" style="width: 67%;">Delivered 85%</div>
</div>

                </td>
                <td><span class="badge badge-success">21.09.2021</span></td>
                <td>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>

                </td>
                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>

</div>
<div class="tab-pane" id="Contact-new2">
<h6></h6>

<div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>{{__('feedbacks.title')}}</th>
                                <th>{{__('feedbacks.sentdate')}}</th>
                                <th>{{__('feedbacks.readingrate')}}</th>
                                <th>{{__('feedbacks.statistics')}}</th>
                                <th>{{__('feedbacks.credate')}}</th>
                                <th>{{__('feedbacks.actions')}}</th>
                            </tr>
                        </thead>
                            <tfoot>
                        <tr>
                                <th>{{__('feedbacks.title')}}</th>
                                <th>{{__('feedbacks.sentdate')}}</th>
                                <th>{{__('feedbacks.readingrate')}}</th>
                                <th>{{__('feedbacks.statistics')}}</th>
                                <th>{{__('feedbacks.credate')}}</th>
                                <th>{{__('feedbacks.actions')}}</th>
                        </tr>
                            </tfoot>
                        <tbody>
                    
                    <tr>                           
                <td>{{__('feedbacks.sent')}}</td>
                <td>01.09.2021</td>
                <td>10/7,5</td>
                <td>Sent - 100<p> 
                <div id="progress-striped-active" class="progress progress-striped active">
<div class="progress-bar progress-bar-primary" data-transitiongoal="75" aria-valuenow="75" style="width: 75%;">Read 75%</div>
</div>
<p>
<div id="progress-striped-active" class="progress progress-striped active">
<div class="progress-bar progress-bar-warning" data-transitiongoal="67" aria-valuenow="67" style="width: 67%;">Delivered 85%</div>
</div>

                </td>
                <td><span class="badge badge-success">21.09.2021</span></td>
                <td>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>

                </td>
                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>



</div>
</div>






<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('feedbacks.modaltitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {!! trans('feedbacks.modaldesc') !!}
      <!-- {{__('feedbacks.modaldesc')}} -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('feedbacks.close')}}</button>
      </div>
    </div>
  </div>
</div>






            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop