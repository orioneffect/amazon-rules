@extends('layout.authentication')
@section('title', 'Register')
@section('title', 'Notifications')

@section('content')
<div class="pattern">
    <span class="red"></span>
    <span class="indigo"></span>
    <span class="blue"></span>
    <span class="green"></span>
    <span class="orange"></span>
</div>
<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <a href="#a"><img src="../assets/images/logo/10.jpeg" alt="Logo" class="w350 d-inline-block align-top mr-2"></a>
        </div>
        <div class="card">
            <div class="body">
                <p class="lead">{{__('authentication.createaccount')}}</p>
                @isset($success)
                    <h6 class="text text-red">{{__('authentication.createdsuccessfully')}}</h6>
                @endisset

                <form class="form-auth-small m-t-20" method="POST" action="{{ route('authentication.register') }}"  data-parsley-validate novalidate>
                    @csrf
                    <div class="demo-masked-input">

                    <div class="col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{__('authentication.your_name')}}" ><br>
                            @error('name')
                                    <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
                            </div>
                            <input type="email" name="email" class="form-control" required placeholder="{{__('authentication.your_email')}}"  autocomplete="email" value="{{ old('email') }}" >
                            @error('email')
                            <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                            </div>
                            <input type="tel" name="phone" class="form-control  phone-number" placeholder="{{__('authentication.your_phone')}}" required autocomplete="phone"  value="{{ old('phone') }}" >
                            @error('phone')
                            <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control round" placeholder="{{__('authentication.password')}}" required autocomplete="password">
                            @error('password')
                            <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                            </div>
                            <input type="password" name="password_confirmation" class="form-control round" placeholder="{{__('authentication.confirm_password')}}" required autocomplete="password">
                            @error('confirmpassword')
                            <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-round btn-block">{{__('authentication.register')}}</button>


                    <div class="bottom">
                        <span>{{__('authentication.already_have_account')}} <a href="{{route('authentication.login')}}">{{__('authentication.login')}}</a></span>
                    </div>

                    </div>


                </form>
              </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/nouislider/nouislider.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
<style>
    .demo-card label{ display: block; position: relative;}
    .demo-card .col-lg-4{ margin-bottom: 30px;}
</style>
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script><!-- Bootstrap Colorpicker Js -->
<script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script><!-- Input Mask Plugin Js -->
<script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/vendor/nouislider/nouislider.js') }}"></script><!-- noUISlider Plugin Js -->

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/advanced-form-elements.js') }}"></script>
<script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>

@stop
