@extends('layout.authentication')
@section('title', 'Login')


@section('content')
<div class="pattern">
    <span class="red"></span>
    <span class="indigo"></span>
    <span class="blue"></span>
    <span class="green"></span>
    <span class="orange"></span>
</div>

<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <a href="#a"><img src="../assets/images/logo/10.jpeg" alt="Logo" class="w350 d-inline-block align-top mr-2"></a>
        </div>

        
        <div class="card">
            <div class="body">



                <ul class="nav navbar-nav flush-right">
                    <li class="dropdown language-menu">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="fa fa-language"></i> Language
                        </a>
                        <div class="dropdown-menu vivify swoopInTop" aria-labelledby="navbarDropdown">

                                @foreach ($languageNa as $language)
                                    <a class="dropdown-item pt-2 pb-2"  href="{{ route('changelang') }}?lang={{$language->name}}">
                                        <img src="../assets/images/flag/language/128/{{$language->country_flag}}" class="w20 mr-2 rounded-circle"> 
                                        <span class="">{{$language->language}} </span>
                                    </a>
                                @endforeach
                        </div>
                    </li>
                </ul>



                <p class="lead">{{__('authentication.logintoaccount')}}</p>
                @if(session()->has('error'))
                <div class="alert alert-danger">
                     {{ session()->get('error') }}
                 </div>
                @endif
                <form class="form-auth-small m-t-20" action="{{ route('authentication.validatelogin') }}" method="POST">
                    @csrf <!-- {{ csrf_field() }} -->
                    <div class="form-group">
                        <label for="signin-email" class="control-label sr-only">{{__('authentication.email')}}</label>
                        <input type="email" class="form-control round" name="email" id="signin-email" value="" placeholder="Email" required>
                        @error('email')
                        <strong>{{ $message }}</strong>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="signin-password" class="control-label sr-only">{{__('authentication.password')}}</label>
                        <input type="password" class="form-control round" name="password" id="signin-password" value="" placeholder="Password" required>
                        @error('password')
                        <strong>{{ $message }}</strong>
                        @enderror
                    </div>
                    <div class="form-group clearfix">
                        <label class="fancy-checkbox element-left">
                            <input type="checkbox">
                            <span>{{__('authentication.rememberme')}}</span>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-round btn-block">{{__('authentication.login')}}</button>

                    <div class="bottom">
                        <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="{{route('authentication.forgotpassword')}}">{{__('authentication.forgotpwd')}}</a></span>
                        <span>{{__('authentication.donthaveaccount')}} <a href="{{route('authentication.register')}}">{{__('authentication.register')}}</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>
@stop

@section('page-styles')

@stop

@section('page-script')

@stop
