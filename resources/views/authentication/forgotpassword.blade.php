@extends('layout.authentication')
@section('title', 'Forgot Password')


@section('content')
<div class="pattern">
    <span class="red"></span>
    <span class="indigo"></span>
    <span class="blue"></span>
    <span class="green"></span>
    <span class="orange"></span>
</div>
<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <a class="navbar-brand" href="javascript:void(0);"><img src="../assets/images/icon.svg" width="30" height="30" class="d-inline-block align-top mr-2" alt="">Oculux</a>
        </div>
        <div class="card forgot-pass">
            <div class="body">
                <p class="lead mb-3"><strong>{{__('authentication.oops')}}</strong>,<br> {{__('authentication.forgotsomething')}}</p>


                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @else
                <p>{{__('authentication.type_email_to_rec_pwd')}}</p>
                @endif

                <form class="form-auth-small" method="post" action="{{ route('authentication.forgotpasswordemail') }}">
                    @csrf <!-- {{ csrf_field() }} -->
                    <div class="form-group">
                        <input type="email" class="form-control round" name="email" placeholder="{{__('authentication.email')}}" required>
                        @error('email')
                        <strong>{{ $message }}</strong>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-round btn-primary btn-lg btn-block">{{__('authentication.reset_pwd')}}</button>
                    <div class="bottom">
                        <span class="helper-text">{{__('authentication.know_your_pwd')}} <a href="{{route('authentication.login')}}">{{__('authentication.login')}}</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>
<!-- END WRAPPER -->
@stop

@section('page-styles')

@stop

@section('page-script')
@stop
