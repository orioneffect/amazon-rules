@extends('layout.authentication')
@section('title', 'Reset Password')

@section('content')
<div class="pattern">
    <span class="red"></span>
    <span class="indigo"></span>
    <span class="blue"></span>
    <span class="green"></span>
    <span class="orange"></span>
</div>
<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <a class="navbar-brand" href="javascript:void(0);"><img src="../../assets/images/icon.svg" width="30" height="30" class="d-inline-block align-top mr-2" alt="">Oculux</a>
        </div>
        <div class="card">
            <div class="body">

                <p class="lead mb-3"><strong>{{ __('Reset Password') }}</strong></p>

                    <form class="form-auth-small" method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                            <input id="email" type="email" class="form-control round" name="email" value="{{ $email ?? old('email') }}" readonly autocomplete="email" >
                            @error('email')
                                    <strong>{{ $message }}</strong>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control round" name="password" placeholder="Password" required autocomplete="new-password" autofocus>
                            @error('password')
                                    <strong>{{ $message }}</strong>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control round" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password">
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
