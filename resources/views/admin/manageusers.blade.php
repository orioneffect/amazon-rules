@extends('layout.master')
@section('parentPageTitle', 'Admin')
@section('title', __('menus.manageusers'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-warning text-white"><i class="fa fa-user"></i> </div>
                <div class="content">
                    <span>{{__('admin.totusers')}}</span>
                    <h5 class="number mb-0">{{$data['user_count']}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-info text-white"><i class="fa fa-user-plus"></i> </div>
                <div class="content">
                    <span>{{__('admin.newusers')}}</span>
                    <h5 class="number mb-0"></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-success text-white"><i class="fa fa-user-circle-o"></i> </div>
                <div class="content">
                    <span>{{__('admin.activeusers')}}</span>
                    <h5 class="number mb-0">{{$data['userActive_count']}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-danger text-white"><i class="fa fa-building-o"></i> </div>
                <div class="content">
                    <span>{{__('admin.store')}}</span>
                    <h5 class="number mb-0">{{$data['store_count']}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card planned_task">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <input type="hidden" value="teachers" id="refname">
                                    <table class="table table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>{{__('admin.id')}}</th>
                                                <th>{{__('admin.name')}}</th>
                                                <th>{{__('admin.email')}}</th>
                                                <th>{{__('admin.country')}}</th>
                                                <th>{{__('admin.stores')}}</th>
                                                <th>{{__('admin.status')}}</th>
                                                <th>{{__('admin.logasuser')}}</th>
                                                <th>{{__('admin.type')}}</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>{{__('admin.id')}}</th>
                                                <th>{{__('admin.name')}}</th>
                                                <th>{{__('admin.email')}}</th>
                                                <th>{{__('admin.country')}}</th>
                                                <th>{{__('admin.stores')}}</th>
                                                <th>{{__('admin.status')}}</th>
                                                <th>{{__('admin.logasuser')}}</th>
                                                <th>{{__('admin.type')}}</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="con_student_tbl">
                                            @isset($data["user"])
                                            @foreach ($data["user"] as $list)
                                                <tr>
                                                    <td><a href="{{route('admin.userprofile')}}?id={{$list->id}}" target="_blank">{{$list->id}}</a></td>
                                                    <td>{{$list->name}}</td>
                                                    <td>{{$list->email}}</td>
                                                    <td>{{$list->country}}</td>
                                                    <td>{{$list->store_count}}</td>
                                                    <td>
                                                        @if ($list->is_active==1)
                                                            @if($list->type!=3)
                                                                <button type="button" class="btn btn-sm btn-success" title="" data-toggle="tooltip" data-placement="left" data-original-title="{{__('admin.clicktoban')}}">
                                                                    <i class="icon-check" onclick=change_status({{$list->id}},0)></i>
                                                                </button>
                                                            @endif
                                                        @else
                                                            @if($list->type!=3)
                                                                <button type="button" class="btn btn-sm btn-danger" title="" data-toggle="tooltip" data-placement="left" data-original-title="{{__('admin.clicktoactivate')}}">
                                                                    <i class="icon-close" onclick=change_status({{$list->id}},1)></i>
                                                                </button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @php
                                                        $encryptValue = Crypt::encryptString($list->email);
                                                        @endphp
                                                        @if ($list->is_active==1)
                                                            @if($list->type!=3)
                                                                <a href="{{ route('authentication.adminvalidatelogin', ['user' => $encryptValue]) }}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="right" data-original-title="{{__('admin.loguser')}}"><span class="badge badge-warning ml-n2 mr-n2"><i class="fa fa-arrow-circle-right"></i></span></a>
                                                            @endif
                                                        @else
                                                            @if($list->type!=3)
                                                                <a href="{{ route('authentication.adminvalidatelogin', ['user' => $encryptValue]) }}" class="btn btn-sm btn-default"  data-toggle="tooltip" data-placement="right" data-original-title="{{__('admin.loguser')}}"><span class="badge badge-danger ml-n2 mr-n2"><i class="fa fa-arrow-circle-right"></i></span></a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($list->is_active==1)
                                                        <select class="form-control mb-1" id="setstatus" name="setstatus" required onchange=change_type({{$list->id}},this.value)>
                                                            <option value="">--{{__('admin.setstatus')}}--</option>
                                                            @isset($data["type"])
                                                                @foreach ($data["type"] as $type)
                                                                    <option value="{{$type->typeid}}" {{ ($type->typeid == $list->type) ? 'selected' : ''  }}>{{$type->type}}</option>
                                                                @endforeach
                                                            @endisset
                                                        </select>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @endisset
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->

<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

<script>
    function change_status(id, val)
    {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/admin/change_userstatus?id="+id+"&val="+val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            window.location.reload();
          })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }    

    function change_type(id, val)
    {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/admin/change_usertype?id="+id+"&val="+val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            window.location.reload();
          })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }    

</script>


@stop
