@extends('layout.master')
@section('parentPageTitle', 'Admin')
@section('title', __('menus.feedback'))

@section('content')

@if (session()->has('message'))
    <div class="alert alert-success">
        {{ __('admin.questionsadded', ['row' => session()->get('message')]) }}
    </div>
@endif
@if (session()->has('message_updt'))
    <div class="alert alert-success">
        {{ __('admin.questionsupdate', ['row' => session()->get('message_updt')]) }}
    </div>
@endif
@if (session()->has('err'))
    <div class="alert alert-danger">
        {{ __('dashboard.duplicate_rows', ['row' => session()->get('err')]) }}
    </div>
@endif

<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item" onclick="$('#updatesection').hide();"><a class="nav-link active show"
                    data-toggle="tab" href="#list"><i class="fa fa-list"></i> {{ __('admin.listofquest') }}</a>
            </li>
            <li class="nav-item" onclick="$('#updatesection').hide();"><a class="nav-link" data-toggle="tab"
                    href="#addnew"><i class="fa fa-plus-square"></i> {{ __('admin.addquest') }}</a></li>
            <li class="nav-item displaynone" id="updatesection"><a class="nav-link" data-toggle="tab"
                    href="#update"><i class="fa fa-pencil-square-o"></i> {{ __('admin.updtquest') }}</a></li>
        </ul>
        <div class="mt-2">
        @if ($maindata['feedbk']==1)
        <button type="button" class="btn btn-outline-success" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="{{__('admin.clickhidefeedbk')}}">
            <i class="icon-check"><a class="text-light" href="{{ route('integration.feedbackstatus', ['feedbk' => '0']) }}"></i> {{ __('admin.hidefeedback') }}</a>
        </button>
        @else
        <button type="button" class="btn btn-outline-danger" title="" data-toggle="tooltip" data-placement="right" data-original-title="{{__('admin.clickshowfeedbk')}}">
            <i class="icon-close"><a href="{{ route('integration.feedbackstatus', ['feedbk' => '1']) }}" class="text-light"></i> {{ __('admin.showfeedback') }}</a>
        </button>
        @endif
        </div>
        
        <div class="tab-content mt-2">
            <div class="tab-pane show active" id="list">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="row clearfix">
                                <div class="col-lg-10">
                                    <div class="card">
                                        <div class="body">
                                            <div class="table-responsive">
                                                <input type="hidden" value="teachers" id="refname">
                                                <table class="table table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>{{ __('admin.questions') }}</th>
                                                            <th>{{ __('admin.type') }}</th>
                                                            <th>{{ __('admin.status') }}</th>
                                                            <th>{{ __('admin.edit') }}</th>

                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th>{{ __('admin.questions') }}</th>
                                                            <th>{{ __('admin.type') }}</th>
                                                            <th>{{ __('admin.status') }}</th>
                                                            <th>{{ __('admin.edit') }}</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody id="con_student_tbl">
                                                        @isset($data['feedbk'])
                                                            @foreach ($data['feedbk'] as $list)
                                                                <tr>
                                                                    <td>{{ $list->quest }}</td>
                                                                    <td>{{ $list->type }}</td>
                                                                    <td>
                                                                        @if ($list->status==0)
                                                                        <button type="button" class="btn btn-sm btn-success" title="" data-toggle="tooltip" data-placement="top" data-original-title="{{__('admin.clicktohide')}}">
                                                                            <i class="icon-check" onclick=change_status({{$list->id}},1)></i>
                                                                        </button>
                                                                        @else
                                                                        <button type="button" class="btn btn-sm btn-danger" title="" data-toggle="tooltip" data-placement="top" data-original-title="{{__('admin.clicktoshow')}}">
                                                                            <i class="icon-close" onclick=change_status({{$list->id}},0)></i>
                                                                        </button>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-sm btn-default"
                                                                            title="" data-toggle="tooltip"
                                                                            data-placement="top"
                                                                            data-original-title="Update"
                                                                            onclick=up_question({{ $list->id }})><i
                                                                                class="fa fa-pencil-square-o"></i></button>
                                                                        <button type="button" class="btn btn-sm btn-default" title="" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="icon-trash" onclick=del_question({{ $list->id }})></i></button>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endisset
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="addnew">
                <div class="col-12 displaynone" id="successmsg">
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle"></i> <span id="insertedcount"></span>
                    </div>
                    <div class="alert alert-warning displaynone" role="alert" id="duplicatecountdisplay">
                        <i class="fa fa-warning"></i> <span id="duplicatecount"></span>
                    </div>
                </div>
                <form id="frm" method="POST" action="{{ route('admin.add_question') }}">
                    @csrf
                    <div class="body mt-2">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{ __('admin.question') }} <span></span>*</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="quest"
                                        name="quest" required>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>Question Type *</label>
                                <select name="qtype" id="qtype" class="form-control show-tick" onchange=show_option(this.value) required>
                                    <option value="">--Select Type--</option>
                                    <option value="OPTION">OPTION</option>
                                    <option value="TEXT">TEXT</option>
                                </select>
                            </div>
                        </div>
                        <div id="options" class="row clearfix mt-3 displaynone">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>Options</label>
                                <input type="text" class="form-control text-warning" placeholder="" id="ans1" name="ans1" value="Terrible" required>
                                <input type="text" class="form-control text-warning" placeholder="" id="ans2" name="ans2" value="Poor" required>
                                <input type="text" class="form-control text-warning" placeholder="" id="ans3" name="ans3" value="Ok">
                                <input type="text" class="form-control text-warning" placeholder="" id="ans4" name="ans4" value="Good">
                                <input type="text" class="form-control text-warning" placeholder="" id="ans5" name="ans5" value="Great">
                                <input type="text" class="form-control text-warning" placeholder="" id="ans6" name="ans6">
                            </div>
                        </div>
                        <div class="row clearfix mt-3">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i>
                                    {{ __('admin.addquest') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="update">
                <div class="col-12 displaynone" id="successmsg">
                    <div class="alert alert-success" role="alert">
                        <i class="fa fa-check-circle"></i> <span id="insertedcount"></span>
                    </div>
                    <div class="alert alert-warning displaynone" role="alert" id="u_duplicatecountdisplay">
                        <i class="fa fa-warning"></i> <span id="u_duplicatecount"></span>
                    </div>
                </div>
                <form id="u_frm" method="POST" action="{{ route('admin.update_feedbkquest') }}">
                    @csrf
                    <div class="body mt-2">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{ __('admin.question') }}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="quest"
                                        name="quest" required>
                                    <input type="hidden" id="u_id" name="u_id">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <button type="submit" id="u_btn" class="btn btn-primary"><i
                                        class="fa fa-plus-square"></i> {{ __('admin.upquest') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">


    <style>
        td.details-control {
            background: url('../assets/images/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('../assets/images/details_close.png') no-repeat center center;
        }

        .bootstrap-tagsinput {
            background-color: rgb(204, 204, 204) !important;
        }

    </style>

@stop


@section('page-script')
    <script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>

    <script src="{{ asset('js/common/common.js?v=1.2') }}"></script>


    <script>

        $('#frm').parsley();

        $('#u_frm').parsley();

        function change_status(id, val)
        {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/admin/feedbk_status?id="+id+"&val="+val,
                datatype: "json",
                type: "get",
            })
            .done(function (response) {
                console.log(response);
                window.location.reload();
              })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
        }    
    
        function del_question(id)
        {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/admin/del_question?id="+id,
                datatype: "json",
                type: "get",
            })
            .done(function (response) {
                console.log(response);
                window.location.reload();
              })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
        }    
    
        function up_question(id) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/admin/get_question?id=" + id,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $('[href="#update"]').tab('show');
                    $('#updatesection').show();
                    $("#u_id").val(response.id);
                    $("#quest").val(response.quest);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }
        function show_option(val) {
            if (val=='OPTION') {
                $('#options').show();
            } else if (val=='TEXT') {
                $('#options').hide();
            }
        }

                
    </script>
@stop

