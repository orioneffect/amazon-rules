@extends('layout.master')
@section('parentPageTitle', 'Admin')
@section('title', __('menus.userfeedback'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-info text-white"><i class="fa fa-thumbs-o-up"></i> </div>
                <div class="content">
                    <span>{{__('admin.feedbkcount')}}</span>
                    <h5 class="number mb-0">{{$data['feedbk_count']}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-success text-white"><i class="fa fa-thumbs-up"></i> </div>
                <div class="content">
                    <span>{{__('admin.feedbksatisfied')}}</span>
                    <h5 class="number mb-0">{{$data['feedbk_satisfied']}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-danger text-white"><i class="fa fa-thumbs-down"></i> </div>
                <div class="content">
                    <span>{{__('admin.feedbkunsat')}}</span>
                    <h5 class="number mb-0">{{$data['feedbk_unsatisfied']}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-warning text-white"><i class="fa fa-list-ol"></i> </div>
                <div class="content">
                    <span>{{__('admin.totalquest')}}</span>
                    <h5 class="number mb-0">{{$data['feedbk_questcount']}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card planned_task">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <input type="hidden" value="teachers" id="refname">
                                    <table class="table table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>{{__('admin.id')}}</th>
                                                <th>{{__('admin.name')}}</th>
                                                <th>{{__('admin.type')}}</th>
                                                <th>{{__('admin.feedback')}}</th>
                                                <th>{{__('admin.qanswered')}}</th>
                                                <th>{{__('admin.datefeedback')}}</th>
                                                <th>{{__('admin.email')}}</th>
                                                <th>{{__('admin.country')}}</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>{{__('admin.id')}}</th>
                                                <th>{{__('admin.name')}}</th>
                                                <th>{{__('admin.type')}}</th>
                                                <th>{{__('admin.feedback')}}</th>
                                                <th>{{__('admin.qanswered')}}</th>
                                                <th>{{__('admin.datefeedback')}}</th>
                                                <th>{{__('admin.email')}}</th>
                                                <th>{{__('admin.country')}}</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="con_student_tbl">
                                            @isset($data["user"])
                                            @foreach ($data["user"] as $list)
                                                <tr>
                                                    
                                                    <td>
                                                        <a href="{{route('admin.viewfeedback')}}?id={{$list->id}}" target="_blank">
                                                            @if ($list->id < 10)
                                                                Feedbk0{{$list->id}}
                                                            @else
                                                                Feedbk{{$list->id}}
                                                            @endif
                                                            </a>
                                                    </td>
                                                    <td>{{$list->name}}</td>
                                                    <td>
                                                        <h6>
                                                        @if ($list->is_active==1)
                                                            @isset($data["type"])
                                                                @foreach ($data["type"] as $type)
                                                                    {{ ($type->typeid == $list->type) ? $type->type : ''  }}
                                                                @endforeach
                                                            @endisset
                                                        @endif
                                                        </h6>
                                                    </td>
                                                    <td>
                                                        @if ($list->satisfied == 1)
                                                        <span class="badge badge-success ml-0 mr-0">satisfied</span>
                                                        @else  
                                                         <span class="badge badge-warning ml-0 mr-0">unsatisfied</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$list->answer_count}} out of {{$data['feedbk_questcount']}}</td>
                                                    <td>{{$list->created_at}}</td>
                                                    <td>{{$list->email}}</td>
                                                    <td>{{$list->country}}</td>
                                                </tr>
                                            @endforeach
                                            @endisset
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->

<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

<script>
    function change_status(id, val)
    {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/admin/change_userstatus?id="+id+"&val="+val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            window.location.reload();
          })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }    

    function change_type(id, val)
    {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/admin/change_usertype?id="+id+"&val="+val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            window.location.reload();
          })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }    

</script>


@stop
