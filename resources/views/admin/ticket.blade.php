@extends('layout.master')
@section('parentPageTitle', 'Admin')
@section('title', 'Ticket List')


@section('content')
    <div class="row clearfix">
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="body top_counter">
                    <div class="icon bg-warning text-white"><i class="fa fa-ticket"></i> </div>
                    <div class="content">
                        <span>Total Tickets</span>
                        <h5 class="number mb-0">{{ $data['no_of_ticket'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="body top_counter">
                    <div class="icon bg-info text-white"><i class="fa fa-tags"></i> </div>
                    <div class="content">
                        <span>Responded</span>
                        <h5 class="number mb-0">{{ $data['no_of_responded'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="body top_counter">
                    <div class="icon bg-success text-white"><i class="fa fa-thumbs-o-up"></i> </div>
                    <div class="content">
                        <span>Resolve</span>
                        <h5 class="number mb-0">{{ $data['no_of_resolve'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="body top_counter">
                    <div class="icon bg-danger text-white"><i class="fa fa-thumbs-o-down"></i> </div>
                    <div class="content">
                        <span>Pending</span>
                        <h5 class="number mb-0">{{ $data['no_of_pending'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-12">
            <div class="card">
                <div class="body">
                    <form method="get" action="{{ route('admin.ticket') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="id" name="search_id">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <div class="input-group">
                                    <select class="form-control mb-1" id="setpriority" name="search_priority">
                                        <option value="">--Select Priority--</option>
                                        @isset($data['priorityNav'])
                                            @foreach ($data['priorityNav'] as $priority)
                                                <option value="{{ $priority->typeid }}">{{ $priority->visible }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="input-group">
                                    <div class="input-daterange input-group" data-provide="datepicker">
                                        <input type="text" class="input-sm form-control" readonly
                                            placeholder="--Select Date--" name="search_from">
                                        <span class="input-group-addon range-to">to</span>
                                        <input type="text" class="input-sm form-control" readonly
                                            placeholder="--Select Date--" name="search_to">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <div class="input-group">
                                    <select class="form-control mb-1" name="search_status">
                                        <option value="">--Select Status--</option>
                                        @isset($data['statusNav'])
                                            @foreach ($data['statusNav'] as $status)
                                                <option value="{{ $status->typeid }}">{{ $status->visible }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <button type="submit" id="btn" class="btn btn-sm btn-primary btn-block">Search</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-custom2 table-hover">
                    <thead>
                        <tr>
                            <th colspan="5">Ticket Detail</th>
                            <th colspan="2">Activity</th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Priority</th>
                            <th>User</th>
                            <th>Downloads</th>
                            <th>Date</th>
                            <th>Activity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($data['ticketlist'])
                            @foreach ($data['ticketlist'] as $list)
                                <tr>
                                    <td><a href="{{ route('admin.ticketdetails') }}?t_id={{ $list->id }}"
                                            target="_blank">OR{{ $list->id }}</a></td>
                                    <td><span>{{ $list->title }}</span></td>
                                    <td>
                                        @if ($list->priority == 'p1')
                                            <span class="badge badge-default ml-0 mr-0">
                                            @elseif ($list->priority == 'p2')
                                                <span class="badge badge-info ml-0 mr-0">
                                                @elseif ($list->priority == 'p3')
                                                    <span class="badge badge-danger ml-0 mr-0">
                                        @endif
                                        {{ $list->visible }}
                                        </span>
                                    </td>
                                    <td><span>{{ $list->name }}</span></td>
                                    <td>
                                        @if ($list->url != '')
                                            <h6 class="mb-0">
                                                <a href="{{ route('options.download') }}?filename={{ $list->url }}"><i
                                                        class="fa fa-download text-larger"></i></a>
                                            </h6>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td><span>{{ App\Models\CommonFunction::time_elapsed_string($list->created_at, false) }}</span>
                                    </td>
                                    <td>
                                        @isset($data['statusNav'])
                                            @foreach ($data['statusNav'] as $status)
                                                @if ($list->status == $status->typeid)
                                                    @if ($list->status == 'a2')
                                                        <span class="badge badge-warning ml-0 mr-0">
                                                        @elseif ($list->status == 'a4')
                                                            <span class="badge badge-danger ml-0 mr-0">
                                                            @elseif ($list->status == 'a3')
                                                                <span class="badge badge-success ml-0 mr-0">
                                                                @elseif ($list->status == 'a1')
                                                                    <span class="badge badge-pending ml-0 mr-0">
                                                    @endif
                                                    {{ $status->visible }}
                                                    </span>
                                                @endif
                                            @endforeach
                                        @endisset
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">

@stop

@section('page-script')
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- Bootstrap Tags Input Plugin Js -->

@stop
