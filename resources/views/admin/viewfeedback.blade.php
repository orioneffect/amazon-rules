@extends('layout.master')
@section('parentPageTitle', 'Pages')
@section('title', 'View Feedback')


@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card social">
            <div class="profile-header d-flex justify-content-between justify-content-center">
                <div class="d-flex">
                    <div class="mr-3">
                        @if ($user->profileimg!='')
                            <img src="../../storage/app/public/{{ $user->profileimg }}" class="rounded" alt="">
                        @else
                            <img src="../assets/images/user.png" class="rounded" alt="">
                        @endif
                    </div>
                    <div class="details">
                        <h5 class="mb-0">{{ $user->name }}</h5>
                        <span class="text-light">{{ $user->country }}</span>
                    </div>                                
                </div>
            </div>
        </div>                    
    </div>               

    <div class="col-xl-6 col-lg-4 col-md-5">
        <div class="card">
            <div class="body">
               
                @isset($fbk)
                @foreach ($fbk as $feedback)

                <small class="text-muted">{{ $feedback->quest }}</small>
                @if ($feedback->type == 1)
                    <p>{{ $feedback->answers }}</p>
                @else
                    <p>{{ $feedback->user_ans_text }}</p>
                @endif
                <hr>
                
                @endforeach
                @endisset

                <small class="text-muted">Satisfied by our service</small>
                @if ($feedback->satisfied == 1)
                    <p>satisfied</p>
                @else  
                    <p>unsatisfied</p>
                @endif
            </div>
        </div>
    </div>

</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop