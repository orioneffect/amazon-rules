@extends('layout.master')
@section('parentPageTitle', __('admin.userpay'))
@section('title', __('admin.admin'))

@section('content')

<div class="card">
    <div class="card-body">
        <form method="GET" action="{{ route('admin.userpayment') }}">
            @csrf
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-12">
                <div class="input-group">
                    <input type="text" id="search_mail" name="search_mail" class="form-control"
                        placeholder="{{ __('admin.usermail') }}">
                </div>
            </div>
            <div class="col-auto pr-0">
                <select class="form-control" id="duration" name="duration">
                    <option selected="selected" value="0">{{ __('dashboard.0') }}</option>
                    <option value="1">{{ __('dashboard.1') }}</option>
                    <option value="3">{{ __('dashboard.3') }}</option>
                    <option value="7">{{ __('dashboard.7') }}</option>
                    <option value="14">{{ __('dashboard.14') }}</option>
                    <option value="30">{{ __('dashboard.30') }}</option>
                    <option value="60">{{ __('dashboard.60') }}</option>
                    <option value="90">{{ __('dashboard.90') }}</option>
                    <option value="180">{{ __('dashboard.180') }}</option>
                    <option value="365">{{ __('dashboard.365') }}</option>
                    <option value="36500">{{ __('dashboard.alltime') }}</option>
                </select>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12">
                <button type="submit" class="btn btn-primary">{{ __('dropshipping.search') }}</button>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div id="history">
            <h5>{{ __('admin.appuserpay') }}</h5>
            <div class="table-responsive">
                <table class="table table-hover table-custom spacing8">
                    <thead>
                        <tr class="text-info">
                            <th>{{ __('admin.usermail') }}</th>
                            <th>{{ __('admin.payid') }}</th>
                            <th>{{ __('admin.paytype') }}</th>
                            <th>{{ __('admin.paydt') }}</th>
                            <th>{{ __('admin.amount') }}</th>
                            <th>{{ __('admin.from') }}</th>
                            <th>{{ __('admin.to') }}</th>
                            <th>{{ __('admin.paystatus') }}</th>
                        </tr>
                    </thead>
                    <tbody id="wallet_history_list">
                        @isset($data["paypend"])
                            @foreach ($data["paypend"] as $approve)
                            @php
                            if ($approve->payment_type == 0) { $payment_type = 'Payment Gateway'; }
                            else if ($approve->payment_type == 1) { $payment_type = 'Bill Payment'; }
                            else if ($approve->payment_type == 2) { $payment_type = 'Wallet'; }
                            else { $payment_type = 'Wallet Recharge'; }
                            @endphp
                                <tr class="text-light">
                                    <td>{{$approve->email}}</td>
                                    <td>{{$approve->id}}</td>
                                    <td>{{$payment_type}}</td>
                                    <td>{{$approve->created_at}}</td>
                                    <td><span class="text-info"><strong>$</strong></span>&nbsp;{{$approve->amount}}</td>
                                    <td>{{$approve->payoneer}}</td>
                                    <td>{{$approve->to_account_name}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-success text-light" title="" data-toggle="tooltip" data-placement="left" data-original-title="{{__('admin.appnow')}}">
                                            <i class="icon-check" onclick=approvepayment({{$approve->id}},1)></i>
                                        </button>&nbsp;
                                        <button type="button" class="btn btn-sm btn-danger text-light" title="" data-toggle="tooltip" data-placement="right" data-original-title="{{__('admin.closenow')}}">
                                            <i class="icon-close" onclick=approvepayment({{$approve->id}},0)></i>
                                        </button>
                                    </td>
                                </tr>    
                            @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>            
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div id="history">
            <h5>{{ __('admin.payoneerpay') }}</h5>
            <div class="table-responsive mb-4">
                <table class="table table-hover table-custom spacing8">
                    <thead>
                        <tr class="text-info">
                            <th>{{ __('admin.usermail') }}</th>
                            <th>{{ __('admin.payid') }}</th>
                            <th>{{ __('admin.paytype') }}</th>
                            <th>{{ __('admin.paydt') }}</th>
                            <th>{{ __('admin.amount') }}</th>
                            <th>{{ __('admin.from') }}</th>
                            <th>{{ __('admin.to') }}</th>
                            <th>{{ __('admin.apppay') }}</th>
                        </tr>
                    </thead>
                    @php
                    $total = 0;
                    @endphp
                    <tbody id="wallet_history_list">
                        @isset($data["payhist"])
                            @foreach ($data["payhist"] as $approve)
                            @php
                            $total = $total + $approve->amount;
                            if ($approve->payment_type == 0) { $payment_type = 'Payment Gateway'; }
                            else if ($approve->payment_type == 1) { $payment_type = 'Bill Payment'; }
                            else if ($approve->payment_type == 2) { $payment_type = 'Wallet'; }
                            else { $payment_type = 'Wallet Recharge'; }
                            @endphp
                                <tr class="text-secondary">
                                    <td>{{$approve->email}}</td>
                                    <td>{{$approve->id}}</td>
                                    <td>{{$payment_type}}</td>
                                    <td>{{$approve->created_at}}</td>
                                    <td><span class="text-info"><strong>$</strong></span>&nbsp;{{$approve->amount}}</td>
                                    <td>{{$approve->payoneer}}</td>
                                    <td>{{$approve->to_account_name}}</td>
                                    <td>{{$approve->updated_at}}</td>
                                </tr>    
                            @endforeach
                        @endisset
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8"><h4>Total Amount Paid : <span class="text-info"><strong>$</strong></span>&nbsp;{{$total}}</h4></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>            
    </div>
</div>

@stop


@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">

@stop

@section('page-script')

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script>

    function approvepayment(id, val) {
        if (val == 1) {
            var stitle = "Are you sure to Approve?";
            var stext = "Once Approved, you will not be able to revert!";
            var sicon = "success";
        } else 
        {
            stitle = "Are you sure to Reject?";
            stext = "Once rejected, you will not be able to revert!";
            sicon = "success";
        }

        swal({
            title: stitle,
            text: stext,
            icon: sicon,
            buttons: true,
            dangerMode: true,
        }, function(isConfirm) {
            if (isConfirm) {
                var ENDPOINT = "{{ url('/') }}";
                $.ajax({
                    url: ENDPOINT + "/admin/approvepayment?id=" + id + "&val=" + val,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    window.location.reload();            
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
            }
        });





    }

    $(document).ready(function() {
        $("#search_mail").val('{{$data['usermail']}}');
        $("#duration").val('{{$data['duration']}}');
    });
</script>

@stop
