@extends('layout.master')
@section('parentPageTitle', 'Pages')
@section('title', 'User Profile')


@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card social">
            <div class="profile-header d-flex justify-content-between justify-content-center">
                <div class="d-flex">
                    <div class="mr-3">
                        <img class="rounded" src="../assets/images/{{$data['user']->logo}}" alt="">
                    </div>
                    <div class="details">
                        <h5 class="mb-0">{{$data['user']->name}}</h5>
                        <span class="text-light">{{$data['user']->company_name}}</span>
                        <p class="mb-0"><span>{{__('admin.country')}}: <strong>{{$data['country']->country}}</strong></span> <span>{{__('admin.store')}}: <strong>{{$data['stores']->store_count}}</strong></span></p>
                    </div>                                
                </div>
                <!--
                <div>
                    <button class="btn btn-primary btn-sm">Follow</button>
                    <button class="btn btn-success btn-sm">Message</button>
                </div>-->
            </div>
        </div>                    
    </div>               

    <div class="col-xl-4 col-lg-4 col-md-5">
        <div class="card">
            <div class="header">
                <h2>Info</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <small class="text-muted">{{__('admin.address')}}</small>
                <p>
                    {{$data['user']->address1}}&nbsp;| 
                    {{$data['user']->address2}}&nbsp;| 
                    {{$data['user']->city}}&nbsp;| 
                    {{$data['state_name']->state}}&nbsp;| 
                    {{$data['country']->country}}&nbsp;|
                    {{$data['user']->pincode}}
                </p>
                <div>
                    <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1923731.7533500232!2d-120.39098936853455!3d37.63767091877441!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1522391841133" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                </div>
                <hr>
                <small class="text-muted">{{__('admin.emailaddress')}}</small>
                <p>{{$data['user']->email}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('admin.phone')}}</small>
                <p>{{$data['user']->phone}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('admin.mobile')}}</small>
                <p>{{$data['user']->mobile_numb}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('admin.fax')}}</small>
                <p>{{$data['user']->fax}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('admin.contactperson')}}</small>
                <p class="m-b-0">{{$data['user']->contact_person}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('admin.website')}}</small>
                <p class="m-b-0">{{$data['user']->website_url}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('admin.social')}}</small>
                <!--<p><i class="fa fa-twitter m-r-5"></i> twitter.com/example</p>
                <p><i class="fa fa-facebook  m-r-5"></i> facebook.com/example</p>
                <p><i class="fa fa-github m-r-5"></i> github.com/example</p>
                <p><i class="fa fa-instagram m-r-5"></i> instagram.com/example</p>-->
            </div>
        </div>
    </div>

    <div class="col-xl-8 col-lg-8 col-md-7">
        <div class="card">
            <div class="header">
                <h2>Basic Information</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">                                    
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.name')}}</small>
                            <p>{{$data['user']->name}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.fname')}}</small>
                            <p>{{$data['user']->first_name}}&nbsp;</p>
                            <hr>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.lname')}}</small>
                            <p>{{$data['user']->last_name}}&nbsp;</p>
                            <hr>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.faddress')}}</small>
                            <p>{{$data['user']->address1}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.saddress')}}</small>
                            <p>{{$data['user']->address2}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.city')}}</small>
                            <p>{{$data['user']->city}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.state')}}</small>
                            <p>{{$data['state_name']->state}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.country')}}</small>
                            <p>{{$data['country']->country}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.pincode')}}</small>
                            <p>{{$data['user']->pincode}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    

                    
                </div>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h2>Account Data</h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.created')}}</small>
                            <p>{{$data['user']->created_at}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.shipping')}}</small>
                            <p>{{$data['ship_name']->default_ship}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.currency')}}</small>
                            <p>{{$data['user']->currency}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.defaultstore')}}</small>
                            <p>{{$data['store_name']->default_store}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.defaultlan')}}</small>
                            <p>{{$data['lan_name']->default_lan}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('admin.defaultcountry')}}</small>
                            <p>{{$data['default_country_name']->default_country}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>

                </div>
            </div>
        </div>                  
    </div>
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop