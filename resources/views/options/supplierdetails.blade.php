@extends('layout.master')
@section('parentPageTitle', __('options.options'))
@section('title', __('menus.suppliers'))
{{__('menus.suppliers')}}

@section('content')
<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#list">{{$supplier->company_name}}</a></li>
        </ul>
        <div class="tab-content mt-0">
            <div class="tab-pane show active" id="list">

            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-5 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('options.search')}}">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title="">Search</a>
                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-success btn-block" title="">{{__('options.pushintoamazon')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-custom spacing8">
                        <thead>
                            <tr><th>{{__('options.select')}}</th>
                                <th>{{__('options.image')}}</th>
                                <th>{{__('options.product')}}</th>
                                <th>{{__('options.category')}}</th>
                                <th>{{__('options.price')}}</th>
                                <th>{{__('options.quantity')}}</th>
                                <th>{{__('options.status')}}</th>
                                <th>{{__('options.details')}}</th>
                                <th>{{__('options.created')}}</th>
                                <th>{{__('options.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody id="data-wrapper">
                        </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="8"  class="auto-load text-center displaynone">
                                <div>
                                    <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px" y="0px" height="60" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                        <path fill="#000"
                                            d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s"
                                                from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                        </path>
                                    </svg>
                                </div>
                            </th>
                        </tr>
                        <tr >
                            <td colspan="8" id="nextload">
                                <button type="button"  href="#a" class="btn btn-primary btn-lg btn-block mb-3" onclick="infinteLoadMore()">{{ __('masterlang.showmore') }}</button>
                            </td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var loaded=0;
    var supplierId="{{$supplier->id}}";
    var ENDPOINT = "{{ url('/') }}";

    function search() {
        loaded=0;
        $("#data-wrapper").html('');
        infinteLoadMore();
    }

    function infinteLoadMore() {
        var searchby="product";
        var searchval=$("#searchval").val();
        var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/options/listsearch2?supplierId="+supplierId+"&loaded="+loaded+"&searchby="+searchby+"&searchval="+searchval,
                datatype: "html",
                type: "get",
                beforeSend: function () {
                    $('.auto-load').show();
                }
            })
            .done(function (response) {
                loaded = loaded  + 50;
                //console.log(response);
                $('.auto-load').hide();
                $("#data-wrapper").append(response);
                if(response == '') {
                    $('#nextload').html("{{__('dropshipping.no_more_data')}}");
                    //$('#nextload').hide();
                }
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
    }
</script>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script>

$(document).ready(function() {
    infinteLoadMore();
});
</script>
@stop
