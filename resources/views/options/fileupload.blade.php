@extends('layout.master')
@section('parentPageTitle', __('options.options'))
@section('title', __('menus.fileupload'))

@section('content')
<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#list">{{__('options.filelist')}}</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addnew">{{__('options.uploadnew')}}</a></li>
        </ul>
        <div class="tab-content mt-0">
            <div class="tab-pane show active" id="list">

            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-5 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('options.uploadsearch')}}">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title="">Search</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-custom spacing8">
                        <thead>
                            <tr>
                                <th>{{__('options.documentname')}}</th>
                                <th>{{__('options.storename')}}</th>
                                <th>{{__('options.notes')}}</th>
                                <th>{{__('options.download')}}</th>
                                <th>{{__('options.created')}}</th>
                                <th>{{__('options.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody id="data-wrapper">
                            @foreach ($fileupload as $fileuploa)
                            <tr>
                                <td><h6 class="mb-0">{{$fileuploa->document_name}}</h6></td>
                                <td><h6 class="mb-0">{{$fileuploa->store_name}}</h6></td>
                                <td><h6 class="mb-0">{{$fileuploa->notes}}</h6></td>
                                <td><h6 class="mb-0"><a href="{{ route('options.download') }}?filename={{$fileuploa->file_path}}" ><i class="fa fa-download text-larger"></i></h6></td>
                                <td><h6 class="mb-0">{{$fileuploa->created_at}}</h6></td>
                                <td><h6 class="mb-0"></h6></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="addnew">
            <div class="col-12 displaynone" id="successmsg"  >
                <div class="alert alert-success" role="alert" >
                    <i class="fa fa-check-circle"></i> {{__('dropshipping.success')}}
                </div>
            </div>
            <form method="POST" id="frm" name="frm" validate enctype="multipart/form-data">
                @csrf
                <div class="body mt-2">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('options.documentname')}}</label>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="{{__('options.documentname')}}" name="documentname" required/>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('options.selectstore')}}</label>
                            <select name="store" id="store" class="form-control show-tick" required>
                                <option value="">{{__('options.selectstore')}} *</option>
                                <option value="{{__('options.generalfile')}}">{{__('options.generalfile')}}</option>
                                @isset($maindata["storesNav"])
                                    @foreach ($maindata["storesNav"] as $store)
                                        <option value="{{$store->store_name}}">{{$store->store_name}}</option>
                                    @endforeach
                                @endisset
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="body">
                                <input type="file" name="file" class="dropify" data-max-file-size="5120K" required data-allowed-file-extensions="csv txt xlsx xls pdf doc docx PNG png tiff jpg jpeg" >
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('options.notes')}}</label>
                            <div class="form-group">
                                <textarea class="form-control" type="text" placeholder="{{__('options.notes')}}" name="notes" required rows="5"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="submit" id="btn" class="btn btn-primary" onclick="">{{__('options.createnew')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
<script>
function createnew() {

    $("#successmsg").hide();
    var form = $('#frm');
    var data = new FormData(form); // <-- 'this' is your form element


    console.log($(data)


    $.ajax({
        /* the route pointing to the post function */
        url: ENDPOINT +'/options/fileupload',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: data,
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) {
            console.log(data)
        }
    });
    return false;

}

$(document).ready(function() {
    infinteLoadMore();
});
</script>
@stop
