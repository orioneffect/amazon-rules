@extends('layout.master')
@section('parentPageTitle', __('options.options'))
@section('title', __('menus.profitloss'))


@section('content')
<div class="row clearfix">

    <div class="col-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> {{__('integration.store_changed')}}
            </div>
        @endif

        @if (session('success1'))
            <div class="alert alert-success alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> {{__('integration.store_added')}}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-warning"></i> {{__('integration.error')}}
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-hover table-custom spacing0">
                <tbody>
                    <tr >
                        <td  class="bg bg-orange">{{__('(')}}</td>
                        <td class="bg bg-orange">{{__('options.productprice')}}</td>
                        <td class="bg bg-orange">{{__('+')}}</td>
                        <td class="bg bg-orange">{{__('options.amazoncommission20')}}</td>
                        <td class="bg bg-orange">{{__('+')}}</td>
                        <td class="bg bg-orange">{{__('options.importfee')}}</td>
                        <td class="bg bg-orange">{{__('+')}}</td>
                        <td class="bg bg-orange">{{__('options.salestax25')}}</td>
                        <td class="bg bg-orange">{{__('+')}}</td>
                        <td class="bg bg-orange">{{__('options.shippingprice')}}</td>
                        <td class="bg bg-orange">{{__(')')}}</td>
                        <td class="bg bg-orange">{{__('*')}}</td>
                        <td class="bg bg-orange">{{__('options.profit20')}}</td>
                        <td class="bg bg-orange">{{__('options.totalamount')}}</td>
                        <td>{{__('options.productprice')}}</td>
                        <td>{{__('options.amazoncommission20')}}</td>
                        <td>{{__('options.salestax25')}}</td>
                        <td>{{__('options.importfee')}}</td>
                        <td>{{__('options.shippingprice')}}</td>
                        <td>{{__('options.totalcost')}}</td>
                        <td>{{__('options.profitloss')}}</td>
                    </tr>
                    <tr>
                        <td class="bg bg-orange"><h6 class="mb-0">Sample</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">25</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">5</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">7</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">6.25</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">8</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">10.25</h6></td>
                        <td class="bg bg-orange"><h6 class="mb-0">61.5</h6></td>
                        <td><h6 class="mb-0">25</h6></td>
                        <td><h6 class="mb-0">12.3</h6></td>
                        <td><h6 class="mb-0">15.375</h6></td>
                        <td><h6 class="mb-0">7</h6></td>
                        <td><h6 class="mb-0">8</h6></td>
                        <td><h6 class="mb-0">67.675</h6></td>
                        <td><h6 class="mb-0">-10.04%</h6></td>
                    </tr>
                    <tr>
                        <td colspan="21">&nbsp;</td>
                    </tr>
                    <tr>

                        <td class="bg bg-pink">{{__('(')}}</td>
                        <td class="bg bg-pink">{{__('options.productprice')}}</td>
                        <td class="bg bg-pink">{{__('*')}}</td>
                        <td class="bg bg-pink">{{__('options.profit20')}}</td>
                        <td class="bg bg-pink">{{__(')+')}}</td>
                        <td class="bg bg-pink">{{__('options.shippingprice')}}</td>
                        <td class="bg bg-pink">{{__('+')}}</td>
                        <td class="bg bg-pink">{{__('options.importfee')}}</td>
                        <td class="bg bg-pink">{{__('+')}}</td>
                        <td class="bg bg-pink">{{__('options.salestax25')}}</td>
                        <td class="bg bg-pink">{{__('+')}}</td>
                        <td class="bg bg-pink">{{__('options.amazoncommission20')}}</td>
                        <td class="bg bg-pink">&nbsp;</td>
                        <td class="bg bg-pink">{{__('options.totalamount')}}</td>
                        <td>{{__('options.productprice')}}</td>
                        <td>{{__('options.amazoncommission20')}}</td>
                        <td>{{__('options.salestax25')}}</td>
                        <td>{{__('options.importfee')}}</td>
                        <td>{{__('options.shippingprice')}}</td>
                        <td>{{__('options.totalcost')}}</td>
                        <td>{{__('options.profitloss')}}</td>
                    </tr>
                    <tr>
                        <td class="bg bg-pink"><h6 class="mb-0">Sample</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">25</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">5</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">8</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">7</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">6.25</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">10.25</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-pink"><h6 class="mb-0">56.5</h6></td>
                        <td><h6 class="mb-0">25</h6></td>
                        <td><h6 class="mb-0">10.25</h6></td>
                        <td><h6 class="mb-0">6.25</h6></td>
                        <td><h6 class="mb-0">7</h6></td>
                        <td><h6 class="mb-0">8</h6></td>
                        <td><h6 class="mb-0">56.5</h6></td>
                        <td><h6 class="mb-0">0.00%</h6></td>
                    </tr>
                    <tr>
                        <td colspan="21">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="bg bg-purple">{{__('(')}}</td>
                        <td class="bg bg-purple">{{__('options.productprice')}}</td>
                        <td class="bg bg-purple">&nbsp;</td>
                        <td class="bg bg-purple">{{__('options.shippingprice')}}</td>
                        <td class="bg bg-purple">&nbsp;</td>
                        <td class="bg bg-purple">{{__('options.importfee')}}</td>
                        <td class="bg bg-purple">&nbsp;</td>
                        <td class="bg bg-purple">{{__('options.salestax25')}}</td>
                        <td class="bg bg-purple">&nbsp;</td>
                        <td class="bg bg-purple">{{__('options.profit20')}}</td>
                        <td class="bg bg-purple">{{__(')')}}</td>
                        <td class="bg bg-purple">{{__('options.amazoncommission20')}}</td>
                        <td class="bg bg-purple">&nbsp;</td>
                        <td class="bg bg-purple">{{__('options.totalamount')}}</td>
                        <td>{{__('options.productprice')}}</td>
                        <td>{{__('options.amazoncommission20')}}</td>
                        <td>{{__('options.salestax25')}}</td>
                        <td>{{__('options.importfee')}}</td>
                        <td>{{__('options.shippingprice')}}</td>
                        <td>{{__('options.totalcost')}}</td>
                        <td>{{__('options.profitloss')}}</td>
                    </tr>
                    <tr>
                        <td class="bg bg-purple"><h6 class="mb-0">Sample</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">25</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">8</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">7</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">6.25</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">9.25</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">11.1</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-purple"><h6 class="mb-0">66.6</h6></td>
                        <td><h6 class="mb-0">25</h6></td>
                        <td><h6 class="mb-0">11.1</h6></td>
                        <td><h6 class="mb-0">6.25</h6></td>
                        <td><h6 class="mb-0">7</h6></td>
                        <td><h6 class="mb-0">8</h6></td>
                        <td><h6 class="mb-0">57.35</h6></td>
                        <td><h6 class="mb-0">13.89%</h6></td>
                    </tr>
                    <tr>
                        <td colspan="21">&nbsp;</td>
                    </tr>
                    <tr>

                        <td class="bg bg-green">{{__('(')}}</td>
                        <td class="bg bg-green">{{__('options.productprice')}}</td>
                        <td class="bg bg-green">&nbsp;</td>
                        <td class="bg bg-green">{{__('options.shippingprice')}}</td>
                        <td class="bg bg-green">&nbsp;</td>
                        <td class="bg bg-green">{{__('options.importfee')}}</td>
                        <td class="bg bg-green">&nbsp;</td>
                        <td class="bg bg-green">{{__('options.salestax25')}}</td>
                        <td class="bg bg-green">&nbsp;</td>
                        <td class="bg bg-green">{{__('options.amazoncommission20')}}</td>
                        <td class="bg bg-green">{{__(')')}}</td>
                        <td class="bg bg-green">{{__('options.profit20')}}</td>
                        <td class="bg bg-green">&nbsp;</td>
                        <td class="bg bg-green">{{__('options.totalamount')}}</td>
                        <td>{{__('options.productprice')}}</td>
                        <td>{{__('options.amazoncommission20')}}</td>
                        <td>{{__('options.salestax25')}}</td>
                        <td>{{__('options.importfee')}}</td>
                        <td>{{__('options.shippingprice')}}</td>
                        <td>{{__('options.totalcost')}}</td>
                        <td>{{__('options.profitloss')}}</td>
                    </tr>
                    <tr>
                        <td class="bg bg-green"><h6 class="mb-0">Sample</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">25</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">8</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">7</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">6.25</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">9.25</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">11.1</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-green"><h6 class="mb-0">66.6</h6></td>
                        <td><h6 class="mb-0">25</h6></td>
                        <td><h6 class="mb-0">9.25</h6></td>
                        <td><h6 class="mb-0">6.25</h6></td>
                        <td><h6 class="mb-0">7</h6></td>
                        <td><h6 class="mb-0">8</h6></td>
                        <td><h6 class="mb-0">55.5</h6></td>
                        <td><h6 class="mb-0">16.67%</h6></td>
                    </tr>
                    <tr>
                        <td colspan="21">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="bg bg-info">{{__('(')}}</td>
                        <td class="bg bg-info">{{__('options.productprice')}}</td>
                        <td class="bg bg-info">&nbsp;</td>
                        <td class="bg bg-info">{{__('options.shippingprice')}}</td>
                        <td class="bg bg-info">&nbsp;</td>
                        <td class="bg bg-info">{{__('options.importfee')}}</td>
                        <td class="bg bg-info">&nbsp;</td>
                        <td class="bg bg-info">{{__('options.profit20')}}</td>
                        <td class="bg bg-info">&nbsp;</td>
                        <td class="bg bg-info">{{__('options.amazoncommission20')}}</td>
                        <td class="bg bg-info">{{__(')')}}</td>
                        <td class="bg bg-info">{{__('options.salestax25')}}</td>
                        <td class="bg bg-info">&nbsp;</td>
                        <td class="bg bg-info">{{__('options.toplamsatış')}}</td>
                        <td>{{__('options.productprice')}}</td>
                        <td>{{__('options.amazoncommission20')}}</td>
                        <td>{{__('options.salestax25')}}</td>
                        <td>{{__('options.importfee')}}</td>
                        <td>{{__('options.shippingprice')}}</td>
                        <td>{{__('options.totalcost')}}</td>
                        <td>{{__('options.profitloss')}}</td>
                    </tr>
                    <tr>
                        <td class="bg bg-info"><h6 class="mb-0">Örnek</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">25</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">8</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">7</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">8</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">9.25</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">6.25</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">&nbsp;</h6></td>
                        <td class="bg bg-info"><h6 class="mb-0">57.25</h6></td>
                        <td><h6 class="mb-0">25</h6></td>
                        <td><h6 class="mb-0">9.25</h6></td>
                        <td><h6 class="mb-0">6.25</h6></td>
                        <td><h6 class="mb-0">7</h6></td>
                        <td><h6 class="mb-0">8</h6></td>
                        <td><h6 class="mb-0">55.5</h6></td>
                        <td><h6 class="mb-0">3.06%</h6></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop

