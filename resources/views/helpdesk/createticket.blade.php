@extends('layout.master')
@section('parentPageTitle', __('menus.helpdesk'))
@section('title', __('menus.createticket'))

@section('content')
@if($errors->any())
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="body top_counter">
                {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
            </div>
        </div>
    </div>
</div>
@endif
<div class="row clearfix">
    <div class="col-12">
        <div class="card">
            <div class="col-lg-12 col-md-12">
                <form method="POST" id="ticket_frm" action="{{ route('helpdesk.add_ticket') }}" validate enctype="multipart/form-data">
                    @csrf
                    <div class="body mt-2">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{__('helpdesk.title')}}</label>
                                <div class="form-group">
                                    <textarea required rows="1" class="form-control" placeholder="" id="title" name="title"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{__('helpdesk.desc')}}</label>
                                <div class="form-group">
                                    <textarea required rows="3" class="form-control" placeholder="" id="description" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{__('helpdesk.priority')}}</label>
                                <div class="form-group" id="list_priority">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label>{{__('helpdesk.screenshots')}}</label>
                                <div class="form-group">
                                    <input type="file" name="file" class="dropify" data-max-file-size="5120K" data-allowed-file-extensions="pdf doc docx PNG png tiff jpg jpeg" >
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i> {{__('helpdesk.create_ticket')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">

@stop
@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>

<script>
    function listpriority() {
        var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/helpdesk/priority_list",
                datatype: "json",
                type: "get",
            })
            .done(function (response) {
              $("#list_priority").html(response);
              console.log(response)  ;
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
      }

    $(document).ready(function() {
        listpriority();
    });

</script>
  @stop
