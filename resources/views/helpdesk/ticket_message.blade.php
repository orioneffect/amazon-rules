@extends('layout.master')
@section('parentPageTitle', __('menus.helpdesk'))
@section('title', __('menus.ticket_activity'))

@section('content')
<div class="row clearfix">
    <div class="col-lg-4 col-md-12">
        <div class="card c_grid c_yellow">
            <div class="body text-center">
                <div class="circle">
                    <img class="rounded-circle" src="../assets/images/{{$data['ticketdetails']->logo}}" alt="">
                    
                </div>
                <h6 class="mt-3 mb-0">{{$data['ticketdetails']->name}}</h6>
                <i class="fa fa-envelope"></i> <span>{{$data['ticketdetails']->email}}</span> | 
                <i class="fa fa-phone-square"></i> <span>{{$data['ticketdetails']->phone}}</span>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h2>{{__('helpdesk.ticketdetails')}}</h2>
            </div>
            <div class="body">
                <span>{{$data['ticketdetails']->description}}</span>
            </div>                        
        </div>
        <div class="card">
            <div class="header">
                <h2>{{__('helpdesk.ticketinfo')}}</h2>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">{{__('helpdesk.title')}}: </small>
                    <p class="mb-0">{{$data['ticketdetails']->title}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">{{__('helpdesk.date')}}: </small>
                    <p  class="mb-0">{{$data['ticketdetails']->created_at}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">{{__('helpdesk.downloads')}}: </small>
                    @if($data['ticketdetails']->url!='')
                        <h6 class="mb-0"><a href="{{ route('options.download') }}?filename={{$data['ticketdetails']->url}}" ><i class="fa fa-download text-larger"></i></a></h6>
                    @else
                    <h6 class="mb-0">{{__('helpdesk.noneupload')}}</h6>
                    @endif
                </li>
                <li class="list-group-item">
                    <small class="text-muted">{{__('helpdesk.sts')}}: </small>
                    <div>
                        @if ($data['ticketdetails']->status == 'a2')
                            <span class="badge badge-warning ml-0 mr-0">
                        @elseif ($data['ticketdetails']->status == 'a4')
                            <span class="badge badge-danger ml-0 mr-0">
                        @elseif ($data['ticketdetails']->status == 'a3')
                            <span class="badge badge-success ml-0 mr-0">
                        @elseif ($data['ticketdetails']->status == 'a1')
                            <span class="badge badge-pending ml-0 mr-0">
                        @endif
                                {{$data['ticketdetails']->visible}}
                            </span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-8 col-md-12">
        <div class="card">
            <form method="post" action="{{ route('helpdesk.add_msg') }}" onsubmit="return assignvalue()" data-parsley-validate>
            @csrf
            <div class="body">
                <div class="summernote" id="message">
                </div>
                <input type="hidden" id="msg" name="msg" value="">
                
                <input type="hidden" id="ticket_id" name="ticket_id" value="{{$data['ticketdetails']->id}}">
                <input type="hidden" id="user_id" name="user_id" value="{{$data['ticketdetails']->user_id}}">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <select class="form-control mb-1" id="setstatus" name="setstatus" required>
                            <option value="">--{{__('helpdesk.setstatus')}}--</option>
                            @isset($data["statusNav"])
                                @foreach ($data["statusNav"] as $status)
                                    <option value="{{$status->typeid}}" {{ ($status->typeid == $data['ticketdetails']->status) ? 'selected' : ''  }}>{{$status->visible}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-lg-3 col-md-12">
                        <button class="btn btn-primary" name="Send" type="submit">{{__('helpdesk.sendmessage')}}</button>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <button type="button" class="btn btn-primary float-right" data-original-title="{{__('helpdesk.addlogo')}}" data-toggle="modal" data-target="#addscreenshot" data-whatever="addscreenshot"><i class="fa fa-picture-o"></i> {{__('admin.upsshot')}}</button>
                    </div>
                </div>
            </div>
            
        </form>
        </div>


        <div class="modal fade" id="addscreenshot" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">{{__('helpdesk.addsshot')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                <form id="url_frm" method="POST" action="{{ route('helpdesk.add_url') }}" enctype="multipart/form-data"  validate>
                    @csrf
                        <input type="file" name="file" class="dropify" data-max-file-size="5120K" data-allowed-file-extensions="PNG png tiff jpg jpeg" required>
                        <input type="hidden" id="ticket_id" name="ticket_id" value="{{$data['ticketdetails']->id}}">
                        <input type="hidden" id="user_id" name="user_id" value="{{$data['ticketdetails']->user_id}}">
        
                        <button type="submit" id="btn" class="btn btn-primary mt-2"><i class="fa fa-plus-square"></i> {{__('helpdesk.upload')}}</button>
                        <button type="button" class="btn btn-secondary mt-2" data-dismiss="modal">{{__('helpdesk.close')}}</button>
                </form>
                </div>
            </div>
            </div>
        </div>  




        <div class="card">
            <div class="header">
                <h2>{{__('helpdesk.ticketreplies')}}</h2>
                <ul class="header-dropdown dropdown">
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <ul class="timeline">
                    @isset($data["ticketactivity"])
                        @foreach ($data["ticketactivity"] as $list)
                            <li class="timeline-item">
                                <div class="timeline-info">
                                    <span>{{$list->created_at}}</span>
                                </div>
                                <div class="timeline-marker"></div>
                                <div class="timeline-content">
                                    <div>
                                        @if ($list->visible == 'Admin')
                                        <h6><span class="badge badge-warning ml-0 mr-0">{{__('helpdesk.admin')}}</span> {!! $list->message !!}</h6>
                                        @if ($list->url!='')
                                            <small class="text-muted">{{__('helpdesk.downloads')}}: </small>
                                            <h6 class="mb-0"><a href="{{ route('options.download') }}?filename={{$list->url}}" ><i class="fa fa-download text-larger"></i></a></h6>                
                                        @endif
                                    @else
                                        <h6 class="text-muted mt-0 mb-0">{{$list->message}}</h6>
                                        @if ($list->url!='')
                                            <small class="text-muted">{{__('helpdesk.downloads')}}: </small>
                                            <h6 class="mb-0"><a href="{{ route('options.download') }}?filename={{$list->url}}" ><i class="fa fa-download text-larger"></i></a></h6>                
                                        @endif
                                    @endif
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endisset
                </ul>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/summernote/dist/summernote.js?v=1.1') }}"></script>
<script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>

<script>
    function assignvalue() {
        $("#msg").val(document.querySelector(".note-editable").innerHTML);
        return true;
    }
</script>
@stop