@extends('layout.master')
@section('parentPageTitle', __('integration.integration'))
@section('title', __('integration.store_list'))


@section('content')
<div class="row clearfix">

    <div class="col-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> {{__('integration.store_changed')}}
            </div>
        @endif

        @if (session('success1'))
            <div class="alert alert-success alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> {{__('integration.store_added')}}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-warning"></i> {{__('integration.error')}}
            </div>
        @endif

        <div class="table-responsive">
            <table class="table table-hover table-custom spacing8">
                <thead>
                    <tr>
                        <th>{{__('integration.owner')}}</th>
                        <th>{{__('integration.store_name')}}</th>
                        <th>{{__('masterlang.country')}}</th>
                        <th>{{__('integration.stock')}}</th>
                        <th>{{__('integration.budget')}}</th>
                        <th>{{__('integration.current_store')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stores as $store)
                    <tr>
                        <td>
                            @if ($maindata["profileimg"]!='')
                                <img src="../../storage/app/public/{{$maindata["profileimg"]}}" alt="User Profile Picture" class="w30 rounded mr-2">
                            @else
                                <img src="../assets/images/xs/avatar1.jpg" alt="Avatar" class="w30 rounded mr-2">
                            @endif
                            <span>{{$store->name}}</span></td>
                        <td><h6 class="mb-0">{{$store->store_name}}</h6></td>

                        <td><img src="../assets/images/flag/store/128/{{$store->flag_path}}" alt="Avatar" class="w60 rounded mr-2"> <span>{{$store->country_name}}</span></td>
                        <td><h6 class="mb-0">{{$store->stock}}</h6></td>
                        <td><h6 class="mb-0">$ {{$store->budget}}</h6></td>

                        @if ($store->admin_integration_id=='')
                            <td><h6 class="mb-0"><a href="#a"><span class="text-danger">Inactive&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-default" title="" data-toggle="tooltip" data-placement="top" data-original-title="Delete Inactive Store" onclick="deletestore({{$store->id}})")><i class="icon-trash"></i></button>
                                </span></a></h6></td>
                        @else

                            @if ($store->id==session('storeid'))
                                <td><h6 class="mb-0"><a href="#a">Active</a></h6></td>
                            @else
                                <td><h6 class="mb-0"><a href="{{ route('integration.changestore', ['storeid' => $store->id]) }}">Go To this Store</a></h6></td>
                            @endif
                        @endif

                    </tr>
                    <!--https://orioneffect.com/integration/login?amazon_callback_uri=https%3A%2F%2Fsellercentral.amazon.com.tr%2Fapps%2Fauthorize%2Fconfirm%2Famzn1.sp.solution.0e4f2be9-6e41-4dee-9088-df3206811bad&amazon_state=MTYyNjM0MDQxMDIzMxnvv73qtJDvv73vv71277-9NFENce-_ve-_vXVi77-977-977-9bgFjEO-_ve-_vTHvv73vv71pB--_vXJLRTkIXUZX77-9PmQS77-9BUfvv70bK0Dvv70hPUh5Ge-_ve-_vS3vv71277-977-9&version=beta&selling_partner_id=ALAREFBAV160O-->

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script>

    function deletestore(id) {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
                url: ENDPOINT + "/integration/deletestore?id=" + id,
                datatype: "json",
                type: "get",
            })
            .done(function(response) {
                console.log(response)
                location.href=ENDPOINT + "/integration/mystore";
            })
            .fail(function(jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
    }

    
</script>
@stop
