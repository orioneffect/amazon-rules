@extends('layout.master')
@section('parentPageTitle', __('integration.integration'))
@section('title', __('integration.add_newstore'))


@section('content')
<style>
.wizard > .content > .body {position:relative !important;}

.img-btn > input{
  display:none !important
}
.img-btn > img{
  cursor:pointer !important;
  border:5px solid transparent !important;
}
.img-btn > input:checked + img{
  border-color:#e25985 !important;
  border-radius:0px !important;
}


</style>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="body wizard_validation">
                <form id="wizard_with_validation" name="addstorefrm" method="POST">
                    @csrf
                    <h3>{{__('integration.basic_details')}}</h3>
                    <fieldset>
                        <div class="row clearfix" >
                            @foreach($countries as $country)
                                <div class="col-6 col-md-4 col-xl-2">
                                    <label class="img-btn">
                                        <input type="radio" name="country" id="country{{$country->id}}" value="{{$country->id}}" required>
                                        <img src="../assets/images/flag/store/128/{{$country->flag_path}}" alt="Avatar" class="w150 mr-2 square">
                                        <div style="background-color:#007FFF" class="text text-white text-center p-md-1">{{$country->country_name}}</div>
                                    </label>
                                    <input type="hidden" name="country_url" id="country_url{{$country->id}}" value="{{$country->url_path}}" >
                                </div>
                           @endforeach
                        </div>

                        <div class="row clearfix" >
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{__('integration.store_name')}}</label>
                                    <input type="text" class="form-control"  id="store" name="store" placeholder="{{__('integration.store_name')}} *" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{__('integration.sku')}}</label>
                                    <input type="text" class="form-control"  id="sku" name="sku" placeholder="{{__('integration.sku')}} *" required>
                                </div>
                            </div>
                            <!--<div class="col-sm-12">
                                <div class="form-group">
                                    <label>{{__('integration.merchant')}}</label>
                                    <input type="text" class="form-control"  id="merchant" name="merchant" placeholder="{{__('integration.merchant')}} *" required>
                                </div>
                            </div>-->
                            <div class="col-sm-12">
                                <div class="form-group" >
                                    <label>{{__('integration.stock')}}</label>
                                    <input type="number" min="1" class="form-control" placeholder="{{__('integration.stock')}} *" required name="stock" id="stock">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group" >
                                    <label>{{__('integration.budget')}}</label>
                                    <input type="number" min="1" class="form-control" placeholder="{{__('integration.budget')}} *" required name="budget" id="budget">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h3>{{__('integration.add_on')}}</h3>
                    <fieldset>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    &nbsp; &nbsp; {{__('integration.price_update')}}
                                    <div class="float-left">
                                        <label class="switch">
                                            <input type="checkbox" name="priceupdate" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    &nbsp; &nbsp; {{__('integration.stock_update')}}
                                    <div class="float-left">
                                        <label class="switch">
                                            <input type="checkbox" name="stockupdate" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    &nbsp; &nbsp; {{__('integration.product_remove')}}
                                    <div class="float-left">
                                        <label class="switch">
                                            <input type="checkbox"  name="productremove" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    &nbsp; &nbsp; {{__('integration.product_insert')}}
                                    <div class="float-left">
                                        <label class="switch">
                                            <input type="checkbox" name="productinsert" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="storeid" name="storeid">
                    </fieldset>
                    <h3>{{__('integration.integration')}}</h3>
                    <fieldset>
                        <div class="row clearfix" >
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <a href="#a" onclick="linkAmazon()">Link Amazon Account</a>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix" >
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <a href="#a"  >Link Gmail Account</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-steps/jquery.steps.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-steps/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/ui/dialogs.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script>


        function linkAmazon() {

            if(document.addstorefrm.country.length>0) {
                for (i=0;i<document.addstorefrm.country.length;i++) {
                    if(document.addstorefrm.country[i].checked==true) {
                        country=document.addstorefrm.country[i].value;
                    }
                }
            }

            var storeid=$("#storeid").val();
            var countryURL=$("#country_url"+country).val();

            location.href=""+countryURL+"/apps/authorize/consent?application_id=amzn1.sp.solution.0e4f2be9-6e41-4dee-9088-df3206811bad&&version=beta&state="+storeid
        }

        var form = $('#wizard_with_validation').show();
        var country;

        form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            failed=true;

            if(document.addstorefrm.country.length>0) {
                failed=false;
                for (i=0;i<document.addstorefrm.country.length;i++) {
                    if(document.addstorefrm.country[i].checked==true) {
                        country=document.addstorefrm.country[i].value;
                        failed=true;
                    }
                }
            }

            if(failed==false) {
                swal("Failed!", "{{__('integration.select_cuntry')}}", "error");
                return false;
            }

            if (currentIndex==1 && form.valid()==true) {

                var priceupdate=0,productinsert=0,stockupdate=0,productremove=0;

                if (document.addstorefrm.priceupdate.checked) {
                    priceupdate=1;
                }
                if (document.addstorefrm.stockupdate.checked) {
                    stockupdate=1;
                }
                if (document.addstorefrm.productremove.checked) {
                    productremove=1;
                }
                if (document.addstorefrm.productinsert.checked) {
                    productinsert=1;
                }

                var checkstatus=false;

                $.ajax({
                    url: "{{url('integration/addstore')}}",
                    type: "POST",
                    data: {
                        country: country,
                        stock: $("#stock").val(),
                        store: $("#store").val(),
                        sku: $("#sku").val(),
                        merchant: $("#merchant").val(),
                        budget: $("#budget").val(),
                        priceupdate: priceupdate,
                        productinsert:productinsert,
                        stockupdate:stockupdate,
                        productremove: productremove,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        console.log(JSON.parse(result));
                        $("#storeid").val(JSON.parse(result).success);
                        checkstatus=true;
                    }
                });
                return true;
            } else {
                return form.valid();
            }
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            swal("Good job!", "Submitted!", "success");
        }
    });

</script>
@stop
