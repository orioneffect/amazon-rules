@extends('layout.master')
@section('parentPageTitle', 'Pages')
@section('title', 'User Profile')


@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card social">
            <div class="profile-header d-flex justify-content-between justify-content-center">
                <div class="d-flex">
                    <div class="mr-3">
                        @if ($data['user']->profileimg!='')
                            <img src="../../storage/app/public/{{ $data['user']->profileimg }}" class="rounded" alt="">
                        @else
                            <img src="../assets/images/user.png" class="rounded" alt="">
                        @endif
                    </div>
                    <div class="details">
                        <h5 class="mb-0">{{$data['user']->name}}</h5>
                        <span class="text-light">{{$data['user']->company_name}}</span>
                        <p class="mb-0"><span>{{__('pages.country')}}: <strong>{{$data['country']->country}}</strong></span> <span>{{__('pages.store')}}: <strong>{{$data['stores']->store_count}}</strong></span></p>
                    </div>                                
                </div>
            </div>
        </div>                    
    </div>               

    <div class="col-xl-4 col-lg-4 col-md-5">
        <div class="card">
            <div class="header">
                <h2>Info</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <form method="POST" id="profile_img" action="{{ route('pages.updateprofileimg') }}" validate enctype="multipart/form-data">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label>Update Profile Image</label>
                            <div class="form-group">
                                <input type="file" name="file" class="dropify" data-max-file-size="5120K" data-allowed-file-extensions="pdf doc docx PNG png tiff jpg jpeg" >
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i> Update Profile Image</button>
                        </div>
                    </div>
                </form>
                <hr/>
                <small class="text-muted">{{__('pages.emailaddress')}}</small>
                <p>{{$data['user']->email}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('pages.phone')}}</small>
                <p>{{$data['user']->phone}}&nbsp;</p>
                <hr>
                <small class="text-muted">{{__('pages.mobile')}}</small>
                <p>{{$data['user']->mobile_numb}}&nbsp;</p>
            </div>
        </div>
    </div>

    <div class="col-xl-8 col-lg-8 col-md-7">
        <div class="card">
            <div class="header">
                <h2>Basic Information</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">                                    
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.name')}}</small>
                            <p>{{$data['user']->name}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.fname')}}</small>
                            <p>{{$data['user']->first_name}}&nbsp;</p>
                            <hr>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.lname')}}</small>
                            <p>{{$data['user']->last_name}}&nbsp;</p>
                            <hr>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.faddress')}}</small>
                            <p>{{$data['user']->address1}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.saddress')}}</small>
                            <p>{{$data['user']->address2}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.city')}}</small>
                            <p>{{$data['user']->city}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.state')}}</small>
                            <p>{{$data['state_name']->state}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.country')}}</small>
                            <p>{{$data['country']->country}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.pincode')}}</small>
                            <p>{{$data['user']->pincode}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>

                    
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.contactperson')}}</small>
                            <p class="m-b-0">{{$data['user']->contact_person}}&nbsp;</p>            
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.website')}}</small>
                            <p class="m-b-0">{{$data['user']->website_url}}&nbsp;</p>            
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.fax')}}</small>
                            <p>{{$data['user']->fax}}&nbsp;</p>            
                            <hr>            
                        </div>
                    </div>
                    

                    
                </div>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h2>Account Data</h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.created')}}</small>
                            <p>{{$data['user']->created_at}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.shipping')}}</small>
                            <p>{{$data['ship_name']->default_ship}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.currency')}}</small>
                            <p>{{$data['user']->currency}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.defaultstore')}}</small>
                            <p>{{$data['store_name']->default_store}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.defaultlan')}}</small>
                            <p>{{$data['lan_name']->default_lan}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="form-group">
                            <small class="text-muted">{{__('pages.defaultcountry')}}</small>
                            <p>{{$data['default_country_name']->default_country}}&nbsp;</p>
                            <hr>            
                        </div>
                    </div>
                </div>
            </div>
        </div>                  
    </div>
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop