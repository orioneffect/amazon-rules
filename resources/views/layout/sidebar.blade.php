<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand">

        <div class="bg-white">
        <a href="#a"><img src="../assets/images/logo/10.svg" alt="Logo" class="w200"><span></span></a>
    </div>
    <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu fa fa-chevron-circle-left"></i></button>

    </div>
    <div class="sidebar-scroll">
        <div class="user-account">
            <div class="user_div">
                @if ($maindata["profileimg"]!='')
                <img src="../../storage/app/public/{{$maindata["profileimg"]}}" class="user-photo" alt="User Profile Picture">
                @else
                    <img src="../assets/images/user.png" class="rounded" alt="">
                @endif
            </div>
            @if ($maindata['storecheck']!='')

            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>@isset($maindata["username"]) {{ $maindata["username"] }}  @endisset</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">

                    <li><a href="{{route('pages.profile')}}"><i class="icon-user"></i>{{__('menus.myprofile')}}</a></li>
                    <li><a href="{{route('email.inbox')}}"><i class="icon-envelope-open"></i>{{__('menus.messages')}}</a></li>
                    <li><a href="{{route('settings.userdetails')}}"><i class="icon-settings"></i>{{__('menus.settings')}}</a></li>
                    <li class="divider"></li>
                    <li><a href="{{route('authentication.login')}}"><i class="icon-power"></i>{{__('menus.logout')}}</a></li>

                </ul>
            </div>
            @endif

        </div>
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <!--<li class="header">Main</li>

        @isset($maindata['freeze'])
        @if ($maindata['freeze']==1 && $maindata['plan_validity']>0)


                <li class="{{ Request::segment(1) === 'integration' ? 'active open' : null }}">
                    <a href="#integration" class="has-arrow"><i class="fa fa-bank"></i><span>{{__('menus.integration')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'addstore' ? 'active' : null }}"><a href="{{route('integration.addstore')}}">{{__('menus.addnewstore')}}</a></li>
                        <li class="{{ Request::segment(2) === 'mystore' ? 'active' : null }}"><a href="{{route('integration.mystore')}}">{{__('menus.mystores')}}</a></li>
                    </ul>
                </li>-->

            @if ($maindata['storecheck']!='')

                <li class="header">{{__('menus.amazon')}}</li>

                <li class="{{ Request::segment(1) === 'dashboard' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-amazon"></i><span>{{__('menus.dashboard')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'dashboard' ? 'active' : null }}"><a href="#">{{__('menus.dashboard')}}</a></li>
                        <li class="{{ Request::segment(2) === 'generaldashboard' ? 'active' : null }}"><a href="{{route('dashboard.generaldashboard')}}">{{__('menus.generaldashboard')}}</a></li>
                        <li class="{{ Request::segment(2) === 'profitloss' ? 'active' : null }}"><a href="{{route('dashboard.profitloss')}}">{{__('menus.profitloss')}}</a></li>
                        @isset($maindata['utype'])
                            @if ($maindata['utype']==1)
                                <li class="{{ Request::segment(2) === 'teacher' ? 'active' : null }}"><a href="{{route('dashboard.teacher')}}">{{__('menus.teacher')}}</a></li>
                                <li class="{{ Request::segment(2) === 'classroom' ? 'active' : null }}"><a href="{{route('dashboard.classroom')}}">{{__('menus.classroom')}}</a></li>
                                <li class="{{ Request::segment(2) === 'consettings' ? 'active' : null }}"><a href="{{route('dashboard.consettings')}}">{{__('menus.consettings')}}</a></li>
                                <li class="{{ Request::segment(2) === 'consdetails' ? 'active' : null }}"><a href="{{route('dashboard.consdetails')}}">{{__('menus.consdetails')}}</a></li>
                            @endif
                        @endisset

                    </ul>
                </li>

                <li class="{{ Request::segment(1) === 'dropshipping' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-amazon"></i><span>{{__('menus.dropshipping')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'productsearch' ? 'active' : null }}"><a href="{{route('dropshipping.productsearch')}}">{{__('menus.productsearch')}}</a></li>
                        <li class="{{ Request::segment(2) === 'addnewproduct' ? 'active' : null }}"><a href="{{route('dropshipping.addnewproduct')}}">{{__('menus.add_new_product')}}</a></li>
                        <li class="{{ Request::segment(2) === 'bestselling' ? 'active' : null }}"><a href="{{route('dropshipping.bestselling')}}">{{__('menus.bestselling')}}</a></li>
                        <li class="{{ Request::segment(2) === 'bulkupload' ? 'active' : null }}"><a href="{{route('dropshipping.bulkupload')}}">{{__('menus.bulkupload')}}</a></li>
                        <li class="{{ Request::segment(2) === 'restrictedkeywords' ? 'active' : null }}"><a href="{{route('dropshipping.restrictedkeywords')}}">{{__('menus.restricted_keywords')}}</a></li>
                        <li class="{{ Request::segment(2) === 'blacklistcategory' ? 'active' : null }}"><a href="{{route('dropshipping.blacklistcategory')}}">{{__('menus.black_list_cate')}}</a></li>
                        <li class="{{ Request::segment(2) === 'blacklist' ? 'active' : null }}"><a href="{{route('dropshipping.blacklist')}}">{{__('menus.black_list')}}</a></li>
                        <li class="{{ Request::segment(2) === 'whitelist' ? 'active' : null }}"><a href="{{route('dropshipping.whitelist')}}">{{__('menus.white_list')}}</a></li>
                        <li class="{{ Request::segment(2) === 'restrictedproducts' ? 'active' : null }}"><a href="{{route('dropshipping.restrictedproducts')}}">{{__('menus.restricted_products')}}</a></li>
                        <li class="{{ Request::segment(2) === 'commonbrandpool' ? 'active' : null }}"><a href="{{route('dropshipping.commonbrandpool')}}">{{__('menus.common_brand_pool')}}</a></li>
                        <li class="{{ Request::segment(2) === 'commonproductpool' ? 'active' : null }}"><a href="{{route('dropshipping.commonproductpool')}}">{{__('menus.common_product_pool')}}</a></li>
                        <li class="{{ Request::segment(2) === 'duplicatelistings' ? 'active' : null }}"><a href="{{route('dropshipping.duplicatelistings')}}">{{__('menus.duplicatelistings')}}</a></li>
                        <li class="{{ Request::segment(2) === 'suppressed' ? 'active' : null }}"><a href="{{route('dropshipping.suppressed')}}">{{__('menus.suppressed')}}</a></li>
                        <li class="{{ Request::segment(2) === 'buyboxreport' ? 'active' : null }}"><a href="{{route('dropshipping.buyboxreport')}}">{{__('menus.buyboxreport')}}</a></li>
                        <li class="{{ Request::segment(2) === 'salesreports' ? 'active' : null }}"><a href="{{route('dropshipping.salesreports')}}">{{__('menus.salesreports')}}</a></li>
                        <li class="{{ Request::segment(2) === 'searchhistory' ? 'active' : null }}"><a href="{{route('dropshipping.searchhistory')}}">{{__('menus.searchhistory')}}</a></li>
                        <li class="{{ Request::segment(2) === 'pendingapproval' ? 'active' : null }}"><a href="{{route('dropshipping.pendingapproval')}}">{{__('menus.pending_approval')}}</a></li>
                        <li class="{{ Request::segment(2) === 'approvedproducts' ? 'active' : null }}"><a href="{{route('dropshipping.approvedproducts')}}">{{__('menus.approvedproducts')}}</a></li>
                    </ul>
                </li>
                <li class="{{ Request::segment(1) === 'amazon' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-amazon"></i><span>Amazon Forms</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'orders' ? 'active' : null }}"><a href="{{route('amazon.orders')}}">{{__('menus.orders')}}</a></li>
                        <li class="{{ Request::segment(2) === 'inventory' ? 'active' : null }}"><a href="{{route('amazon.inventory')}}">{{__('menus.inventory')}}</a></li>
                        <li class="{{ Request::segment(2) === 'inventoryhistory' ? 'active' : null }}"><a href="{{route('amazon.inventoryhistory')}}">{{__('menus.inventoryhistory')}}</a></li>
                        <li class="{{ Request::segment(2) === 'reports' ? 'active' : null }}"><a href="{{route('amazon.reports')}}">{{__('menus.reports')}}</a></li>
                        <li class="{{ Request::segment(2) === 'healthreport' ? 'active' : null }}"><a href="{{route('amazon.healthreport')}}">{{__('menus.healthreport')}}</a></li>
                    </ul>
                </li>
                <li class="{{ Request::segment(1) === 'fba' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-amazon"></i><span>{{__('menus.fba')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'fbalist' || Request::segment(2) === 'fbalist' ? 'active' : null }}"><a href="{{route('fba.fbalist')}}">{{__('menus.fbalist')}}</a></li>
                        <li class="{{ Request::segment(2) === 'fbashipment' || Request::segment(2) === 'fbashipment' ? 'active' : null }}"><a href="{{route('fba.fbashipment')}}">{{__('menus.fbashipment')}}</a></li>
                        <li class="{{ Request::segment(2) === 'addnewplan' || Request::segment(2) === 'addnewplan' ? 'active' : null }}"><a href="{{route('fba.addnewplan')}}">{{__('menus.newplan')}}</a></li>
                        <li class="{{ Request::segment(2) === 'giveaway' || Request::segment(2) === 'giveaway' ? 'active' : null }}"><a href="{{route('fba.giveaway')}}">{{__('menus.giveaway')}}</a></li>

                    </ul>
                </li>
                <li class="{{ Request::segment(1) === 'options' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-amazon"></i><span>{{__('menus.options')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'suppliers' || Request::segment(2) === 'suppliersdetails' ? 'active' : null }}"><a href="{{route('options.suppliers')}}">{{__('menus.suppliers')}}</a></li>
                        <li class="{{ Request::segment(2) === 'fileupload' || Request::segment(2) === 'fileupload' ? 'active' : null }}"><a href="{{route('options.fileupload')}}">{{__('menus.fileupload')}}</a></li>
                        <li class="{{ Request::segment(2) === 'profitloss' || Request::segment(2) === 'profitloss' ? 'active' : null }}"><a href="{{route('options.profitloss')}}">{{__('menus.profitloss')}}</a></li>
                    </ul>
                </li>
            @endif

        @endif
        @endisset
        
            @if ($maindata['storecheck']!='')
    
            <!--
                <li class="{{ Request::segment(1) === 'transaction' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-cog"></i><span>{{__('menus.trans')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'walletlist' || Request::segment(2) === 'walletlist' ? 'active' : null }}">
                                <a href="{{route('transaction.walletlist')}}">{{__('menus.walletlist')}}</a>
                        </li>
                    </ul>
                </li>-->

                <li class="{{ Request::segment(1) === 'settings' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-cog"></i><span>{{__('menus.settings')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'shipping' || Request::segment(2) === 'shipping' ? 'active' : null }}"><a href="{{route('settings.shipping')}}">{{__('menus.shipping')}}</a></li>
                        <li class="{{ Request::segment(2) === 'generalsettings' || Request::segment(2) === 'generalsettings' ? 'active' : null }}">
                                <a href="{{route('settings.generalsettings')}}">{{__('menus.generalsettings')}}</a>
                        </li>
                    </ul>
                </li>


                <li class="{{ Request::segment(1) === 'helpdesk' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-cog"></i><span>{{__('menus.helpdesk')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'createticket' || Request::segment(2) === 'createticket' ? 'active' : null }}"><a href="{{route('helpdesk.createticket')}}">{{__('menus.createticket')}}</a></li>
                        <li class="{{ Request::segment(2) === 'ticketlist' || Request::segment(2) === 'ticketlist' ? 'active' : null }}"><a href="{{route('helpdesk.ticketlist')}}">{{__('menus.ticketlist')}}</a></li>
                    </ul>
                </li>
                @endif

        @isset($maindata['utype'])
        @if ($maindata['utype']==3)
        
                <li class="{{ Request::segment(1) === 'admin' ? 'active open' : null }}">
                    <a href="#amazon" class="has-arrow"><i class="fa fa-cog"></i><span>{{__('menus.admin_helpdesk')}}</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'ticket' || Request::segment(2) === 'ticket' ? 'active' : null }}"><a href="{{route('admin.ticket')}}">{{__('menus.ticketlist')}}</a></li>
                        <li class="{{ Request::segment(2) === 'manageusers' || Request::segment(2) === 'manageusers' ? 'active' : null }}"><a href="{{route('admin.manageusers')}}">{{__('menus.manageusers')}}</a></li>
                        <li class="{{ Request::segment(2) === 'userpayment' || Request::segment(2) === 'userpayment' ? 'active' : null }}"><a href="{{route('admin.userpayment')}}">{{__('menus.userpayment')}}</a></li>
                        <li class="{{ Request::segment(2) === 'userfeedback' || Request::segment(2) === 'userfeedback' ? 'active' : null }}"><a href="{{route('admin.userfeedback')}}">{{__('menus.userfeedback')}}</a></li>
                        <li class="{{ Request::segment(2) === 'feedback' || Request::segment(2) === 'feedback' ? 'active' : null }}"><a href="{{route('admin.feedback')}}">{{__('menus.feedback')}}</a></li>
                    </ul>
                </li>

        @endif
        @endisset
                        
            </ul>
        </nav>
    </div>
</div>
