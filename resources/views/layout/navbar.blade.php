<nav class="navbar top-navbar">

    <div class="container-fluid">
        <div class="navbar-left">
            <div class="navbar-btn">
                <a href="index.html"><img src="../assets/images/icon.svg" alt="Oculux Logo" class="img-fluid logo"></a>
                <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
            </div>

            <ul class="nav navbar-nav">
                <div style="position: absolute;top:0;left:30%">
                    @isset($maindata['utype'])
                        @if ($maindata['utype']==1)
                            @isset($maindata['clogo'])
                                <img src="../../storage/app/public/{{$maindata['clogo']}}" alt="Logo" class="w150 ml-5 mt-0 img-fluid"/>
                            @endisset
                            @isset($maindata['admin_id'])
                                @if ($maindata['admin_id']==1)
                                <div class="mt-2">
                                <a href="#a"
                                        onclick="location.href='{{ route('authentication.adminlogout') }}'"
                                        >
                                        <span class="badge badge-success text text-warning"><h6>
                                            You are now in {{$maindata['email']}} &nbsp; &nbsp;
                                            Logout <i class="icon-power text text-danger"></i>
                                        </h6>
                                        </span>
                                        </a></div>
                               @endif
                            @endisset
                        @elseif ($maindata['utype']==0)
                            @isset($maindata['cons_id'])
                                @if ($maindata['cons_id']==1)
                                <div class="mt-2">
                                <a href="#a"
                                        onclick="location.href='{{ route('authentication.conslogout') }}'"
                                        >
                                        <span class="badge badge-success text text-warning"><h6>
                                            You have logged in as user &nbsp; &nbsp;
                                            Logout <i class="icon-power text text-danger"></i>
                                        </h6>
                                        </span>
                                        </a></div>
                                @else
                                    <div>
                                    @isset($maindata['studentlogo'])
                                        <img src="../../storage/app/public/{{$maindata['studentlogo']}}" alt="Logo" class="w150 ml-5 mt-0 img-fluid"/>
                                    @endisset
                                    @isset($maindata['impersonate'])
                                        <label class="switch">
                                            <input type="checkbox" class="exclude_item" id="act" value="" {{$maindata['impersonate']}} onchange="activate_consultant(this.checked)">
                                            <span class="slider round"  title="{{__('menus.consultant_control_msg')}}"></span>
                                        </label>
                                        &nbsp;{{__('menus.consultant_control_msg')}}
                                    @endisset
                                    </div>
                               @endif
                            @endisset
                            @isset($maindata['admin_id'])
                                @if ($maindata['admin_id']==1)
                                <div class="mt-2">
                                <a href="#a"
                                        onclick="location.href='{{ route('authentication.adminlogout') }}'"
                                        >
                                        <span class="badge badge-success text text-warning"><h6>
                                            You are now in {{$maindata['email']}} &nbsp; &nbsp;
                                            Logout <i class="icon-power text text-danger"></i>
                                        </h6>
                                        </span>
                                        </a></div>
                               @endif
                            @endisset

                        @endif
                    @endisset
                </div>
                <!--
                <li><a href="javascript:void(0);" class="megamenu_toggle icon-menu" title="Mega Menu">Mega</a></li>
                <li class="p_social"><a href="{{route('extra.social')}}" class="social icon-menu" title="News">Social</a></li>
                <li class="p_news"><a href="{{route('extra.news')}}" class="news icon-menu" title="News">News</a></li>-->
            </ul>
        </div>


        <div class="navbar-right">
            <div id="navbar-menu">
                <ul class="nav navbar-nav">
                    <!--<li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-envelope"></i>
                            <span class="notification-dot bg-green">4</span>
                        </a>
                        <ul class="dropdown-menu right_chat email vivify swoopInRight"  style="left:-100px !important">
                            <li class="header green">You have 4 New eMail</li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="avtar-pic w35 bg-red"><span>FC</span></div>
                                        <div class="media-body">
                                            <span class="name">James Wert <small class="float-right text-muted">Just now</small></span>
                                            <span class="message">Update GitHub</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <div class="avtar-pic w35 bg-indigo"><span>FC</span></div>
                                        <div class="media-body">
                                            <span class="name">Folisise Chosielie <small class="float-right text-muted">12min ago</small></span>
                                            <span class="message">New Messages</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="../assets/images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Louis Henry <small class="float-right text-muted">38min ago</small></span>
                                            <span class="message">Design bug fix</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="media mb-0">
                                        <img class="media-object " src="../assets/images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Debra Stewart <small class="float-right text-muted">2hr ago</small></span>
                                            <span class="message">Fix Bug</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>-->



                    <li>
                        @if ($maindata['feedbk']==1)
                        <a href="" data-toggle="modal" data-target="#nav_feedback" data-whatever="nav_feedback"><i class="lnr lnr-menu fa fa-thumbs-o-up"></i></a>
                        @endif
                    </li>





                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="icon-bell"></i>
                            @isset($maindata["nofnotifications"])
                                <span class="notification-dot bg-azura">{{$maindata["nofnotifications"]}}</span>
                            @endisset
                        </a>
                        <ul class="dropdown-menu feeds_widget vivify swoopInTop" style="left:-100px !important">
                            @isset($maindata["nofnotifications"])
                                <li class="header blue">You have {{$maindata["nofnotifications"]}} New Notifications</li>
                            @endisset
                            <li>
                            @isset($maindata["notificationNav"])
                                @foreach ($maindata["notificationNav"] as $notification)
                                <a href="javascript:void(0);">
                                    <div class="feeds-left {{$notification->type_color}}"><i class="fa {{$notification->type_image}}"></i></div>
                                    <div class="feeds-body">
                                        <h4 class="title {{$notification->type_title}}">{{$notification->head}} <small class="float-right text-muted">{{App\Models\CommonFunction::time_elapsed_string($notification->sent_time,false)}}</small></h4>
                                        <small>{{$notification->message}}</small>
                                    </div>
                                </a>
                                @endforeach
                            @endisset

                            </li>
                        </ul>
                    </li>

                    <li class="dropdown language-menu">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="fa fa-language"></i>
                        </a>
                        <div class="dropdown-menu vivify swoopInTop" aria-labelledby="navbarDropdown">

                            @isset($maindata["languageNav"])
                                @foreach ($maindata["languageNav"] as $language)
                                    <a class="dropdown-item pt-2 pb-2"  href="{{ route('changelang') }}?lang={{$language->name}}">
                                        <img src="../assets/images/flag/language/128/{{$language->country_flag}}" class="w20 mr-2 rounded-circle">
                                        <span class="">{{$language->language}} </span>
                                    </a>
                                @endforeach
                            @endisset
                        </div>
                    </li>

                    <li class="dropdown language-menu">
                        <a href="#a" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <div id="flagstore">
                                @isset($maindata["stores"])
                                <img src="../assets/images/flag/store/128/{{$maindata["stores"]->flag_path}}" alt="Avatar" class="w30 mr-1 rounded-circle"> <span>{{$maindata["stores"]->store_name}}</span>
                                @endisset
                            </div>
                        </a>
                        <div class="dropdown-menu vivify swoopInRight" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item pt-1 pb-1" href="{{ route('integration.mystore')}}"> <span class=""><i class="fa fa-list"></i> {{__('menus.showall')}}</span></a>
                            <a class="dropdown-item pt-1 pb-1" href="{{ route('integration.addstore')}}"> <span class=""><i class="fa fa-plus"></i> {{__('menus.addnew')}}</span></a>
                            <div class="dropdown-divider"></div>
                            @isset($maindata["storesNav"])
                                @foreach ($maindata["storesNav"] as $store)
                                    <a class="dropdown-item pt-1 pb-1" href="{{ route('integration.changestore', ['storeid' => $store->id]) }}"><img src="../assets/images/flag/store/128/{{$store->flag_path}}" alt="Avatar" class="w30 mr-1 rounded-circle"> <span class="">{{$store->store_name}} </span></a>
                                    <div class="dropdown-divider"></div>
                                @endforeach
                            @endisset
                        </div>
                    </li>
                    <!--<li><a href="javascript:void(0);" class="search_toggle icon-menu" title="Search Result"><i class="icon-magnifier"></i></a></li>
                    <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="Right Menu"><i class="icon-bubbles"></i><span class="notification-dot bg-pink"></span></a></li>-->

                    <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="Right Menu">&nbsp;</a></li>

                    <li><a href="{{route('authentication.logout')}}" class="icon-menu"><i class="icon-power"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>




</nav>

<div class="modal fade" id="nav_feedback" tabindex="-1" role="dialog" aria-labelledby="nav_feedbackTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="nav_feedbackTitle">Feedback</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="userfebk" name="userfebk">
            @csrf
        <div class="modal-body">

            @isset($maindata["fbkq"])
                @foreach ($maindata["fbkq"] as $fbkq)
                <input type="hidden" name="qid{{$loop->iteration}}" value={{$fbkq->id}}>
                @if ($fbkq->type == 'OPTION')
                    <input type="hidden" name="qtype{{$loop->iteration}}" value={{$fbkq->type}}>
                    <label><span class="text-warning">{{$fbkq->quest}}</span></label>
                    <br />

                    <div class="row clearfix">
                            @isset($maindata["fbka"])
                                @foreach ($maindata["fbka"] as $fbka)
                                    @if ($fbkq->id == $fbka->qid)
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <label class="fancy-radio">
                                            <input type="radio" name="user_ans{{$fbkq->id}}" value={{$fbka->id}}>
                                            <span><i></i>{{$fbka->answers}}</span>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            @endisset
                        </div>
                    @else
                        <div class="form-group">
                            <label><span class="text-warning">{{$fbkq->quest}}</span></label>
                            <textarea class="form-control" name="user_ans_text" rows="5" cols="30"></textarea>
                        </div>
                    @endif
                <hr/>
                @endforeach


                <input type="hidden" name="totalquest" value="{{count($maindata["fbkq"])}}">
            @endisset
            <div class="form-group">
                <label><span class="text-warning">Satisfied by our service</span></label>

                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <label class="fancy-radio">
                            <input type="radio" name="satisfied" value=1>
                            <span><i></i>Yes</span>
                        </label>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <label class="fancy-radio">
                            <input type="radio" name="satisfied" value=0>
                            <span><i></i>No</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="alert alert-success displaynone" role="alert"  id="navsucc">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <i class="lnr lnr-menu fa fa-thumbs-o-up"></i> Feedback Successfully Saved
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-warning displaynone" data-dismiss="modal" id="navclose">&times; Close</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="navcancel">Cancel</button>
          <button type="submit" class="btn btn-primary" id="navfsubmit">Submit Feedback</button>
        </div>
        </form>
      </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    function activate_consultant(status)
    {
        if (status) {
            flag=1;
        } else {
            flag=0;
        }
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/common/actconsultant?flag="+flag,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {

        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }
    $("#userfebk").submit(function (e) {
        var ENDPOINT = "{{ url('/') }}";
        e.preventDefault();
        var $form = $(this);
        if (!$form.valid) {
            console.log("form invalid")
        }
        var data=$form.serialize();
        console.log(data);
        $.ajax({
            url: ENDPOINT +"/api/savefeed",
            type: "POST",
            data: data,
            dataType: "JSON",
            success: function (data) {
                data=JSON.parse(data);
                console.log(data);
                if(data.status=="success") {

                    $("#navcancel").hide();
                    $("#navfsubmit").hide();
                    $("#navclose").show();
                    $("#navsucc").show();
                }
            }
        });
    });
    function loadstore() {
        $("#flagstore").html('');
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/api/loadstore",
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            $("#flagstore").html(response);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

</script>
