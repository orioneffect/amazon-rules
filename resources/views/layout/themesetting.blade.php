<div class="themesetting">
    <a href="javascript:void(0);" class="theme_btn"><i class="icon-magic-wand"></i></a>
    <div class="card theme_color">
        <div class="header">
            <h2>Theme Color</h2>
        </div>
        <ul class="choose-skin list-unstyled mb-0">
            <li data-theme="green" @if ($maindata['user']->theme_color == '1') class="active" @endif><div class="green" onclick="updatethemetbl('theme_color', 1)"></div></li>
            <li data-theme="orange" @if ($maindata['user']->theme_color == '2') class="active" @endif><div class="orange" onclick="updatethemetbl('theme_color', 2)"></div></li>
            <li data-theme="blush" @if ($maindata['user']->theme_color == '3') class="active" @endif><div class="blush" onclick="updatethemetbl('theme_color', 3)"></div></li>
            <li data-theme="cyan" @if ($maindata['user']->theme_color == '4') class="active" @endif><div class="cyan" onclick="updatethemetbl('theme_color', 4)"></div></li>
            <li data-theme="indigo" @if ($maindata['user']->theme_color == '5') class="active" @endif><div class="indigo" onclick="updatethemetbl('theme_color', 5)"></div></li>
            <li data-theme="red" @if ($maindata['user']->theme_color == '6') class="active" @endif><div class="red" onclick="updatethemetbl('theme_color', 6)"></div></li>
        </ul>
    </div>
    <div class="card font_setting">
        <div class="header">
            <h2>Font Settings</h2>
        </div>
        <div>
            <div class="fancy-radio mb-2">
                <label><input name="font" value="font-krub" type="radio" @if ($maindata['user']->theme_font == '1') checked @endif onclick="updatethemetbl('theme_font', 1)"><span><i></i>Krub Google font</span></label>
            </div>
            <div class="fancy-radio mb-2">
                <label><input name="font" value="font-montserrat" type="radio" @if ($maindata['user']->theme_font == '2') checked @endif onclick="updatethemetbl('theme_font', 2)"><span><i></i>Montserrat Google font</span></label>
            </div>
            <div class="fancy-radio">
                <label><input name="font" value="font-roboto" type="radio" @if ($maindata['user']->theme_font == '3') checked @endif onclick="updatethemetbl('theme_font', 3)"><span><i></i>Robot Google font</span></label>
            </div>
        </div>
    </div>
    <div class="card setting_switch">
        <div class="header">
            <h2>Settings</h2>
        </div>
        <ul class="list-group">
            <li class="list-group-item">
                Light Version
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="lv-btn" @if ($maindata['user']->light_version == '1') checked @endif onclick="this.checked?updatethemetbl('light_version', 1):updatethemetbl('light_version', 0)">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                RTL Version
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="rtl-btn" @if ($maindata['user']->rtl == '1') checked @endif onclick="this.checked?updatethemetbl('rtl', 1):updatethemetbl('rtl', 0)">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                Horizontal Menu
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="hmenu-btn" @if ($maindata['user']->h_menu == '1') checked @endif onclick="this.checked?updatethemetbl('h_menu', 1):updatethemetbl('h_menu', 0)">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                Mini Sidebar
                <div class="float-right">
                    <label class="switch">
                        <input type="checkbox" class="mini-sidebar-btn" @if ($maindata['user']->mini_sidebar == '1') checked @endif  onclick="this.checked?updatethemetbl('mini_sidebar', 1):updatethemetbl('mini_sidebar', 0)">
                        <span class="slider round"></span>
                    </label>
                </div>
            </li>
        </ul>
    </div>
    <!--
    <div class="card">
        <div class="form-group">
            <label class="d-block">Traffic this Month <span class="float-right">77%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="d-block">Server Load <span class="float-right">50%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
            </div>
        </div>
    </div>
    -->
</div>
<script>
function updatethemetbl(optionid,optionid_val) {
    
    var ENDPOINT = "{{ url('/') }}";
      $.ajax({
            url: ENDPOINT + "/api/updateuser?optionid="+optionid+"&optionid_val="+optionid_val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
          //swal("Updated!", "Data successfully Updated!", "success");
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
  }
</script>