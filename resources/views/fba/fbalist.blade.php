@extends('layout.master')
@section('parentPageTitle', __('fba.fba'))
@section('title', __('menus.fbalist'))


@section('content')
<div class="row clearfix">
    <!-- New Setup -->
    <div class="col-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-3 col-sm-12">
                        <div class="input-group">
                            <input type="text" id="searchval" class="form-control" placeholder="{{__('fba.title')}}">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12">
                        <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title="">Search</a>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <a href="#a" data-toggle="modal" data-target="#addnewmodel" class="btn btn-sm btn-success btn-block" title="">{{__('fba.addnew_product')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="table-responsive mb-4">
            <table class="table">
                <thead>
                    <tr>
                        <th style="width: 20px;">#</th>
                        <th>{{__('fba.title')}} /
                            {{__('fba.asin')}} / {{__('fba.sku')}}
                        </th>
                        <th >{{__('fba.quantity')}}</th>
                        <th >{{__('fba.price')}}</th>
                        <th >{{__('fba.repricer_method')}}</th>
                        <th >{{__('fba.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="d-flex align-items-center">
                                <img src="../assets/images/products/1.jpeg" alt="Avatar" class="w80 rounded mr-2"></img>
                            </div>
                        </td>
                        <td width="10%" wrap >
                            <p>FLYUN 6Pairs 10MM Eyeball Black Screw Stud Earrings...</p>
                            <p>{{__('fba.brand')}}: Flyun</p>
                            <p>B085ZVYQP6</p>
                            <p>AMZR-ARF-1234699623</
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>{{__('fba.inbound')}}</td>
                                    <td>12</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.reserved')}}</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.sellable')}}</td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.unsellable')}}</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.total')}}</td>
                                    <td>16</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>{{__('fba.listprice')}}</td>
                                    <td>$200</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.cost')}}</td>
                                    <td>$250</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.profit')}}</td>
                                    <td>$50</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.buybox')}}</td>
                                    <td>$200</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.lowestprice')}}</td>
                                    <td>$190</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <select class="form-control show-tick" required name="trademark" id="trademark">
                                <option value="">--{{__('fba.repricer_method')}}--</option>
                                <option value="1">Aggressive Sales Generator</option>
                                <option value="2">Balanced Sales Accumulator</option>
                                <option value="3">Cautious Sales Enhancer</option>
                                <option value="4">Dynamic Profit Builder</option>
                                <option value="5">Hybrid Profit Harvester</option>
                                <option value="6">Passive Profit Maximizer</option>
                            </select>

                        </td>
                        <td>
                            <button type="button" class="btn btn-sm btn-default" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-sm btn-default" title="Delete" data-toggle="tooltip" data-placement="top"><i class="icon-trash"></i></button>
                        </td>
                    </tr>


                    <tr>
                        <td>

                            <div class="d-flex align-items-center">
                                <img src="../assets/images/products/2.jpeg" alt="Avatar" class="w80 rounded mr-2"></img>
                            </div>
                        </td>
                        <td width="10%" wrap >
                            <p>SubZero 12501 GripTrax Traction Tool                            </p>
                            <p>{{__('fba.brand')}}: Subzero                            </p>
                            <p>B001FXIOCC
                            </p>
                            <p>AMZR-NEW-1182240295</p>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>{{__('fba.inbound')}}</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.reserved')}}</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.sellable')}}</td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.unsellable')}}</td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.total')}}</td>
                                    <td>14</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>{{__('fba.listprice')}}</td>
                                    <td>$60.64</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.cost')}}</td>
                                    <td>$70</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.profit')}}</td>
                                    <td>$10</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.buybox')}}</td>
                                    <td>$61</td>
                                </tr>
                                <tr>
                                    <td>{{__('fba.lowestprice')}}</td>
                                    <td>$59</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <select class="form-control show-tick" required name="trademark" id="trademark">
                                <option value="">--{{__('fba.repricer_method')}}--</option>
                                <option value="1">Aggressive Sales Generator</option>
                                <option value="2">Balanced Sales Accumulator</option>
                                <option value="3" selected>Cautious Sales Enhancer</option>
                                <option value="4">Dynamic Profit Builder</option>
                                <option value="5">Hybrid Profit Harvester</option>
                                <option value="6">Passive Profit Maximizer</option>
                            </select>

                        </td>
                        <td>
                            <button type="button" class="btn btn-sm btn-default" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-sm btn-default" title="Delete" data-toggle="tooltip" data-placement="top"><i class="icon-trash"></i></button>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>


    <div class="modal fade Form-Wizard-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="addnewmodel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addnewmodeltitle">{{__('fba.addnew_product')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pricing_page">
                    <div id="wizard_horizontal">
                        <h2>{{__('fba.choose_category')}}</h2>
                        <section>
                            <div class="row clearfix" >
                                <div class="col-sm-12">
                                    <div class="row clearfix" >
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('fba.category')}}</label>
                                                <input type="text" class="form-control"  id="store" name="store" placeholder="{{__('fba.category')}} *" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 mt-4">
                                            <a href="#a" class="btn btn-success mb-0"><i class="fa fa-search"></i> {{__('fba.search')}} </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row clearfix" >
                                        <div class="col-sm-12">
                                        <p>
                                            Category List
                                        </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row clearfix" >
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Automotive	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Baby Products	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Beauty	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Books	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Camera and Photo	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Cell Phones & Accessories	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Clothing & Accessories	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Computers	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Electronics	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Grocery & Gourmet Food	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Home & Garden	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Industrial & Scientific (Food Service and Janitorial, Sanitation, & Safety)	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Industrial & Scientific (Lab & Scientific Supplies)	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Industrial & Scientific (Other Industrial Supplies)	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Industrial & Scientific (Power Transmission)	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Jewelry	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Luggage & Travel Accessories	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Magazine Subscriptions	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Movies & TV	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Musical Instruments	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Office Products	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Pet Supplies	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Shoes, Handbags & Sunglasses	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Software	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Sports	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Tires & Wheels	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Tools & Home Improvement	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Toys & Games	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Video Games	'> </div> </div>
                                        <div class='col-sm-12'> <div class='input-group'> <div class='input-group-prepend'> <div class='input-group-text'> <input type='radio' aria-label='Radio button for following text input'> </div> </div> <input type='text' class='form-control' aria-label='Text input with radio button' value='	Watches	'> </div> </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h2>{{__('fba.offer')}}</h2>
                        <section>
                            <div class="row clearfix" >
                                <div class="col-sm-12">
                                    <div class="row clearfix" >
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('fba.product_id')}}</label>
                                                <input type="text" class="form-control"  id="store" name="store" placeholder="{{__('fba.product_id')}} *" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('fba.barcode_type')}}</label>
                                                <select class="form-control show-tick" required name="trademark" id="trademark">
                                                    <option value="Yes">--{{__('fba.barcode_type')}}--</option>
                                                    <option value="Yes">GTIN</option>
                                                    <option value="No">EAN</option>
                                                    <option value="No">EKK</option>
                                                    <option value="No">SALTY</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('fba.product_name')}}</label>
                                                <input type="text" class="form-control"  id="product_name" name="product_name" placeholder="{{__('fba.product_name')}} *" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('fba.brand')}}</label>
                                                <input type="text" class="form-control"  id="brand_name" name="brand_name" placeholder="{{__('fba.brand_exp')}} *" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('fba.producer')}}</label>
                                                <input type="text" class="form-control"  id="brand_name" name="brand_name" placeholder="{{__('fba.producer_exp')}} *" title="{{__('fba.producer_exp')}} *" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>{{__('fba.part_number')}}</label>
                                                <input type="text" class="form-control"  id="part_number" name="part_number" placeholder="{{__('fba.part_number_exp')}} *" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h2>{{__('fba.images')}}</h2>
                        <section>
                            <div class="row clearfix" >
                                <div class="col-sm-12">
                                    <div class="row clearfix" >
                                        <div class="col-sm-12">
                                            <div class="body">
                                                <input type="file" name="file" class="dropify" data-max-file-size="2048K" required data-allowed-file-extensions="PNG png tiff jpg jpeg" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-steps/jquery.steps.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-steps/jquery.steps.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/form-wizard.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>

@stop
