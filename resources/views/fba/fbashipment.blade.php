@extends('layout.master')
@section('parentPageTitle', 'FBA')
@section('title', 'FBA Shipments')


@section('content')
<div class="row clearfix">
    <div class="col-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="mb-1">{{__('fba.stores')}}</div>
                        <div class="multiselect_div">
                            <select name="store[]" id="store" class="multiselect multiselect-custom" multiple="multiple">
                                @isset($data["stores_nav"])
                                @foreach($data['stores_nav'] as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                                @endisset        
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12">
                        <div class="mb-1">{{__('fba.shipmentid')}}</div>
                        <div class="input-group">
                            <input type="text" name="shipmentid" id="shipmentid" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="mb-1">{{__('fba.shippingstatus')}}</div>
                        <div class="multiselect_div">
                            <select name="shipping_status[]" id="shipping_status" class="multiselect multiselect-custom" multiple="multiple">
                                @isset($data["shipping_status"])
                                @foreach($data['shipping_status'] as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                                @endisset        
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12">
                        <a href="javascript:load_fbashipment();" class="btn btn-lm btn-primary btn-block" title=""><i class="fa  fa-search-plus" > </i> Search</a>
                    </div>

                    <div class="col-lg-2 col-md-12 col-sm-12">
                        <a href="{{route('fba.addnewplan')}}" class="btn btn-lm btn-success btn-block" title=""><i class="fa  fa-plus-circle" > </i> Create New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-hover table-custom spacing4">
                <thead>
                    <tr>
                        <th>Plan ID</th>
                        <th>Store Name</th>
                        <th>Shipment ID</th>
                        <th>Status</th>
                        <th class="w100">SKUs</th>
                        <th class="w100">Quantity</th>
                        <th>Label</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody id="fbashipment">
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/multi-select/css/multi-select.css') }}">


@stop

@section('page-script')
<script src="{{ asset('assets/vendor/multi-select/js/jquery.multi-select.js?v=1.1') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

<script>
    $('#shipping_status').multiselect({
        includeSelectAllOption: true,
    });
    $('#store').multiselect({
        includeSelectAllOption: true,
    });


    function load_store() {
        country_id = $("#country_info").val();
        load_store_list(country_id);
    }

    function load_store_list(country) {
        var ENDPOINT = "{{ url('/') }}";
        $("#store_main").html('');
        $.ajax({
            url: ENDPOINT + "/fba/loadstore_list?country_id="+country,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
          response.forEach(function (val, key) {
                var selected='';
                $("#store_main").append('<option value="' + val.id + '"  '+selected+'  >' + val.store_name + '</option>');
            });
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
          console.log('Server error occured');
        });
      }

    
    function load_fbashipment() {
        var store = $('#store').val();
        var shipmentid = $('#shipmentid').val();
        var shipping_status = $('#shipping_status').val();
        $("#fbashipment").html('');
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/load_fbashipment?store="+store+"&shipmentid="+shipmentid+"&shipping_status="+shipping_status,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            $("#fbashipment").html(response);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    $(document).ready(function() {
        load_store();
        load_fbashipment();
    });

    </script>    
@stop
