@extends('layout.master')
@section('parentPageTitle', 'FBA')
@section('title', 'New Shipment Plan')

@section('content')
<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link show" id="tab_createplan" data-toggle="tab" href="#createplan"></i><h5>1. Create New Plan
                </h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link displaynone" id="tab_selectproduct" data-toggle="tab" href="#selectproduct"></i><h5>2. Select Products</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link displaynone" id="tab_estimate" data-toggle="tab" href="#estimate"></i><h5>3. Estimate</h5></a>
            </li>
            <li class="nav-item">
                <a class="nav-link displaynone" id="tab_confirmshipment" data-toggle="tab" href="#confirmshipment"></i><h5>4. Confirm Shipment</h5></a>
            </li>
        </ul>
        <div class="row clearfix" >
            <div class="col-sm-6">
                @isset($maindata['stores'])
                <img src="../assets/images/flag/store/128/{{$maindata["stores"]->flag_path}}" alt="Avatar" class="w30 mr-1 rounded-circle"> <span>{{$maindata["stores"]->store_name}}</span>
                @endisset
                <div id="flagstore">
                </div>
            </div>
        </div>


        <div class="tab-content mt-2">
            <div class="tab-pane show active" id="createplan">
                <div class="alert alert-success displaynone" role="alert"  id="addplansucc">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="lnr lnr-menu fa fa-thumbs-o-up"></i> Plan Successfully Saved
                </div>

                <form id="formplan" name="formplan">
                    @csrf
                    <div class="row clearfix" >
                        <div class="col-sm-6">
                            <div class="row clearfix" >
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>{{__('fba.country')}} *</label>
                                        <select name="to_country" id="to_country" class="form-control show-tick" required>
                                            <option value="">{{__('masterlang.select_country')}}</option>
                                            @foreach($data['countries'] as $country)
                                                <option value="{{$country->id}}">{{$country->country_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>{{__('fba.labelpref')}} *</label>
                                        <select name="seller_label" id="seller_label" class="form-control show-tick" required>
                                            <option value="" selected>{{__('fba.labelpref')}}</option>
                                            <option value={{__('fba.sellerlabel')}}>{{__('fba.sellerlabel')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>{{__('fba.notes')}}</label>
                                        <textarea name="notes" id="notes" rows="4" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label><span class="text-warning">{{__('fba.fromaddress')}}</span></label>

                                    <div class="row clearfix" >
                                        <div class="col-sm-12">
                                            <label>{{__('masterlang.name')}} *</label>
                                            <input type="text" name="from_name" id="from_name" value="" class="form-control border border-bottom" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="row clearfix mt-2" >
                                        <div class="col-sm-6">
                                            <label>{{__('masterlang.address')}} 1*</label>
                                            <input type="text" name="from_address1" id="from_address1" value="" class="form-control" placeholder="" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>{{__('masterlang.address')}} 2</label>
                                            <input type="text" name="from_address2" id="from_address2" value="" class="form-control" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="row clearfix mt-2" >
                                        <div class="col-sm-6">
                                            <label>{{__('masterlang.city')}}</label>
                                            <input type="text" name="from_city" id="from_city" value="" class="form-control" placeholder="" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Country *</label>
                                            <select name="from_country" id="from_country" class="form-control show-tick" required>
                                                <option value="">{{__('masterlang.select_country')}}</option>
                                                @foreach($data['countries'] as $country)
                                                    <option value="{{$country->id}}">{{$country->country_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row clearfix mt-2" >
                                        <div class="col-sm-6">
                                            <label>{{__('masterlang.pincode')}}</label>
                                            <input type="text" name="from_pincode"  value=""  class="form-control" placeholder="">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>State *</label>
                                            <select name="from_state" id="from_state" class="form-control show-tick" required>
                                                <option value="">{{__('masterlang.select_state')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row clearfix" >
                                        <div class="col-sm-12 mt-3">
                                            <button type="submit" class="btn btn-primary" id="plansubmit">Submit Plan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="selectproduct">
                <div class="row clearfix" >
                    <div class="col-sm-6" >
                        <div class="row clearfix  border-secondary rounded-lg">
                            <div class="col-sm-12" >
                                <div class="row clearfix" >
                                    <div class="col-sm-2" id="plan_id_display">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="searchname" id="searchname" value=""  class="form-control" placeholder="{{__('fba.searchproducts')}}">
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-primary" onclick="load_listings(2)"><i class="fa fa-search"></i></button>
                                    </div>

                                    <div class="col-sm-3 align-right">
                                        <button type="button" class="btn btn-success" onclick=addproduct()><i class="fa fa-plus-circle"></i> {{__('fba.additems')}}</button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row clearfix"  style="height: 600px;overflow: scroll" >
                                    <div class="col-sm-12">
                                        <div class="table-responsive-sm mb-4">
                                            <input type="hidden" id="planid" value="">
                                            <table class="table table-bordered table-hover text-wrap" >
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>ASIN/Sku</th>
                                                        <th class="text-wrap">Name</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="prod_listings">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row clearfix" >
                            <div class="col-sm-12">
                                <div id="selectedproducts" class="table-responsive mb-4">
                                    <div class="table-responsive mb-4">
                                        <h6>Selected Products</h6>
                                        <table class="table table-bordered table-hover table-sm">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>ASIN/Sku</th>
                                                    <th>Condition</th>
                                                    <th>Qty</th>
                                                    <th>Del</th>
                                                </tr>
                                            </thead>
                                            <tbody id="prod_selected">
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-success float-right" onclick=toestimate()><i class="fa fa-arrow-right"></i> {{__('fba.next')}}</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="estimate">
                <p>
                    <pre  class="text text-warning">

                        Request::
                        {
                            "InboundShipmentPlanRequestItems": [
                                {
                                    "ASIN": "B08CJL9P6Y",
                                    "Condition": "RefurbishedWithWarranty",
                                    "Quantity": 1,
                                    "SellerSKU": "AMZN-SKU-11111",
                                    "QuantityInCase": 1
                                }
                            ],
                            "LabelPrepPreference": "SELLER_LABEL",
                            "ShipFromAddress": {
                                "AddressLine1": "130 W Water",
                                "City": "do ",
                                "CountryCode": "US",
                                "Name": "ERIK",
                                "PostalCode": "08754",
                                "StateOrProvinceCode": "NJ",
                                "AddressLine2": "#4785",
                                "DistrictOrCounty": "Toms River"
                            },
                            "ShipToCountryCode": "US",
                            "ShipToCountrySubdivisionCode": ""
                        }

                    </pre>
                    <pre class="text text-success">

                        Response::

                        {
                            "payload": {
                                "InboundShipmentPlans": [
                                    {
                                        "ShipmentId": "FBA16C85X33V",
                                        "DestinationFulfillmentCenterId": "ABE8",
                                        "ShipToAddress": {
                                            "Name": "Amazon.com Services, Inc.",
                                            "AddressLine1": "401 Independence Road",
                                            "City": "Florence",
                                            "StateOrProvinceCode": "NJ",
                                            "CountryCode": "US",
                                            "PostalCode": "08518-2200"
                                        },
                                        "LabelPrepType": "SELLER_LABEL",
                                        "Items": [
                                            {
                                                "SellerSKU": "AMZN-SKU-11111",
                                                "FulfillmentNetworkSKU": "X002ZERUML",
                                                "Quantity": 1
                                            }
                                        ],
                                        "EstimatedBoxContentsFee": {
                                            "TotalUnits": 1,
                                            "FeePerUnit": {
                                                "CurrencyCode": "USD",
                                                "Value": 0.15
                                            },
                                            "TotalFee": {
                                                "CurrencyCode": "USD",
                                                "Value": 0.15
                                            }
                                        }
                                    }
                                ]
                            }
                        }


                    </pre>
                </p>
            </div>
            <div class="tab-pane" id="confirmshipment">
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>


<script>
    $(document).ready(function () {
        $('#from_country').on('change', function () {
            var idCountry = this.value;
            getStates(idCountry);
        });
    });

    function getStates(country) {

        $("#from_state").html('');
        $.ajax({
            url: "{{url('api/fetch-states')}}",
            type: "POST",
            data: {
                country_id: country,
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function (result) {
                $('#from_state').html('<option value="">{{__('masterlang.select_state')}} *</option>');
                $.each(result.states, function (key, value) {
                    var selected='';
                    $("#from_state").append('<option value="' + value.id + '"  '+selected+'  >' + value.state_name + '</option>');
                });
            }
        });
    }
    $("#formplan").submit(function (e) {
        var ENDPOINT = "{{ url('/') }}";
        e.preventDefault();
        var $form = $(this);
        if (!$form.valid) {
            console.log("form invalid")
        }
        var data=$form.serialize();
        console.log(data);
        $.ajax({
            url: ENDPOINT +"/fba/saveplan",
            type: "POST",
            data: data,
            dataType: "JSON",
            success: function (data) {
                data=JSON.parse(data);
                console.log(data);
                $('#planid').val(data);
                $("#plan_id_display").html('');
                $("#plan_id_display").append('<span class="text-info"><h7>Plan Id: '+data+'</h7></span>');
                //$("#addplansucc").show();
                $('#createplan').hide();
                $('#tab_createplan').hide();

                load_listings(1);
                $('#selectproduct').show();
                $('#tab_selectproduct').show();
            }
        });
    });

    function load_listings(type) {
        var name = $('#searchname').val();
        var id = $('#planid').val();
        $("#prod_listings").html('');
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/load_listings?name="+name+"&id="+id,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
          response.forEach(function (val, key) {
            $("#prod_listings").append('<tr><td><label class="switch"><input type="checkbox" name="chkbox" value='+val.id+'><span class="slider round"></span></label></td><td><small>'+val.asin+'<br>'+val.sku+'<br>Quantity : '+val.quantity+'</small></td><td><p class="text text-blue text-small">'+val.product_name+'</p></td></tr>');
          });
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function load_selectprod() {
        $("#prod_selected").html('');
        var id = $('#planid').val();
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/load_selectprod?id="+id,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            response.forEach(function (val, key) {
                $("#prod_selected").append('<tr><td><p>'+val.asin+'<br>'+val.sku+'</p></td><td>'+val.condition+'</td><td> <input type=hidden id="prod_id" value='+val.id+'><input type="number" id="quantity" value='+val.quantity+' class="w80" oninput="changeqty('+val.id+',this.value)" min="0" max="9999"> </td><td><button type="button" onclick="removeprod('+val.id+')" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" ><i class="fa fa-trash-o text-danger"></i></button></td></tr>');
            });
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function changeqty(id, quantity) {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/changeqty?id="+id+"&quantity="+quantity,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            $("#prod_selected").html('');
            load_selectprod();
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function removeprod(id) {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/removeprod?id="+id,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            load_listings(1);
            $("#prod_selected").html('');
            load_selectprod();
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }


    function addproduct() {
        var planid = $('#planid').val();
        var items = document.getElementsByName("chkbox");
        var selectedItems = "";
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == "checkbox" && items[i].checked == true) selectedItems += items[i].value + ",";
        }

        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/addproduct?id="+selectedItems+"&planid="+planid,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            $("#prod_listings").html('');
            $("#prod_selected").html('');
            load_selectprod();
            load_listings(1);
            console.log(response);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function toestimate() {
        var alertchange = '';
        var id = $('#planid').val();
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/fba/toestimate?id="+id,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            if (response!='') {
                $("#prod_selected").html('');
                response.forEach(function (val, key) {
                    if (val.quantity<1) {
                        alertchange = 'border border-danger';
                    }
                    $("#prod_selected").append('<tr><td><p>'+val.asin+'<br>'+val.sku+'</p></td><td>'+val.condition+'</td><td> <input type=hidden id="prod_id" value='+val.id+'><input type="number" id="quantity" value='+val.quantity+' class="w80'+alertchange+'" oninput="changeqty('+val.id+',this.value)" min="0" max="9999"> </td><td><button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" ><i class="fa fa-trash-o text-danger"></i></button></td></tr>');
                  });

            } else {
                $('#createplan').hide();
                $('#tab_createplan').hide();
                $('#selectproduct').hide();
                $('#tab_selectproduct').hide();
                $('#estimate').show();
                $('#tab_estimate').show();
            }
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }
</script>
@stop
