@extends('layout.master')
@section('parentPageTitle', __('menus.fba'))
@section('title', __('menus.giveaway'))

@section('content')

<div class="alert alert-success" role="alert">
  <h3 class="alert-heading">{{__('fba.giveawaytitle')}}</h3>
  <p><h6>{{__('fba.giveawaydesc')}}</h6></p>
</div>
<hr>


<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
        <div class="card-body">
                            <form>
                                <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>Select Country</label>
                                            <select class="form-control">
                                                <option value="">-- Select Country --</option>
                                                <option value="TR">Turkey</option>
                                                <option value="US">USA</option>
                                                <option value="CA">Canada</option>
                                                <option value="DE">Germany</option>
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>Select Store</label>
                                            <select class="form-control">
                                                <option value="">-- Select Store --</option>
                                                <option value="1">Immanuel Store</option>
                                                <option value="2">Lacin's Store</option>
                                                <option value="3">Angelo</option>
                                                <option value="4">Yonja Street</option>
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label>Select ASIN</label>
                                            <select class="form-control">
                                                <option value="">-- Select ASIN --</option>
                                                <option value="1">BCV6254987</option>
                                                <option value="2">BV24603150</option>
                                                <option value="3">BVC5601328</option>
                                                <option value="4">BVC5236401</option>
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="col-sm-7 col-md-7 col-lg-7">
                                        <div class="form-group">
                                            <label>Product Name</label>
                                            <input class="form-control" value="" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label>Product Price</label>
                                            <input class="form-control" value="" type="text">
                                        </div>
                                    </div>
                                   
                                </div>
                                <br>
                                <hr>
                                <br><br>

                                <h5>Giveaway Rules</h5>
<table>
  <thead>
    <tr>
    <th style="width: 14%">Quantity</th>
    <th style="width: 14%">Start Date</th>
    <th style="width: 17%">End Date</th>
    <th style="width: 12%">Total Price</th>
    <th style="width: 12%">Service Fee</th>
    <th style="width: 12%">Total Cost</th>
</tr>
</thead>
</table>
<div id="form-placeholder"></div>
<button id="btn-add" type="button" class="btn btn-primary">{{__('settings.add')}}</button>
<button id="btn-add" type="button" class="btn btn-success">{{__('settings.save')}}</button>
<hr/>

                                <div class="row">
                                    <div class="col-sm-12 text-right m-t-20">
                                        <button type="button" class="btn btn-primary">SAVE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
 



                    
</div>
</div>
</div>
</div>





@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@stop
@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/bundles/bootstrap.min.js') }}"></script>

<script>

function appendUserRow(id, user) {
            var html = "<div id=\"opt-row." + id + "\" class=\"form-group row\">\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"quantity" + id + "\" name=\"quantity" + id + "\" placeholder=\"quantity\" value=\"" + user.quantity + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"startdate" + id + "\" name=\"startdate" + id + "\" placeholder=\"startdate\" value=\"" + user.startdate + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"endate" + id + "\" name=\"endate" + id + "\" placeholder=\"endate\" value=\"" + user.endate + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"totalprice" + id + "\" name=\"totalprice" + id + "\" placeholder=\"totalprice\" value=\"" + user.totalprice + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"servicefee" + id + "\" name=\"servicefee" + id + "\" placeholder=\"servicefee\" value=\"" + user.servicefee + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"totalcost" + id + "\" name=\"totalcost" + id + "\" placeholder=\"totalcost\" value=\"" + user.totalcost + "\">\n" +
                "            </div>\n" +                                              
                "             <button type=\"button\" onclick=\"payment(" + id + ")\" class=\"btn btn-sm btn-default js-sweetalert\" title=\"Add Payment\" data-type=\"confirm\"><i class=\"fa fa-usd text-danger\"></i></button><button type=\"button\" onclick=\"delRow(" + id + ")\" class=\"btn btn-sm btn-default js-sweetalert\" title=\"Delete\" data-type=\"confirm\"><i class=\"fa fa-trash-o text-danger\"></i></button>\n" +
                "        </div>";
            $("#form-placeholder").append(html);
        }


function delRow(id) {
            var element = document.getElementById("opt-row." + id);
            element.parentNode.removeChild(element);
        }

$(document).ready(function () {
            var count = 0;
$("#btn-add").click(function () {
                appendUserRow(count++, {
                  quantity: "",
                  startdate: "",
                  endate: "",
                  totalprice: "",
                  servicefee: "",
                  totalcost: ""
                })
            });
});
  </script>


  @stop