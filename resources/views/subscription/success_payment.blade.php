@extends('layout.master')
@section('parentPageTitle', __('subscription.subscription'))
@section('title', __('subscription.paysucc'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <h4>Success Payment</h4>
            <div class="body">

                @if ($data['usertype'] == 0)
                    
                    <h4>"Welcome" {{$data['user']->name}}</h4><br>
                    <h5 class="text-warning">Received Payment Successfully<span class="text-light"></span></h5>
                    <h5 class="text-info">{{__('subscription.selplan')}}&nbsp;-&nbsp;<span class="text-light">{{$data['transuser']->package_name}}</span></h5>
                    <h5 class="text-info">{{__('subscription.refid')}}&nbsp;-&nbsp;<span class="text-light">{{$data['transuser']->id}}</span></h5>
                    <div class="mt-5">
                        <a href="{{ route('integration.addstore') }}">Add Store</a>
                    </div>
                    
                @else
                    
                    <h4>Welcome to upgraded plan "{{$data['user']->name}}"</h4><br>
                    <h5 class="text-warning">Received Payment Successfully<span class="text-light"></span></h5>
                    <h5 class="text-info">{{__('subscription.selplan')}}<span class="text-light">&nbsp;&nbsp;{{$data['transuser']->package_name}}</span></h5>
                    <h5 class="text-info">{{__('subscription.refid')}}<span class="text-light">&nbsp;&nbsp;{{$data['transuser']->id}}</span></h5>
                    <div class="mt-5">
                        <a href="{{ route('integration.mystore') }}">Go To Store</a>
                    </div>
                    
                @endif

            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop
