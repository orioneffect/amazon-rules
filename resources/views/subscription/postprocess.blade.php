@extends('layout.master')
@section('parentPageTitle', __('subscription.payment'))
@section('title', __('subscription.confirm_bankdetails_pay'))
@section('content')
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <h4>{{__('subscription.bank')}}</h4>
            <div class="body">
                <form method="POST" id="form_trans" name="frm" id="frm" validate onsubmit="return validate()" >
                    @csrf
                <!--<input type="hidden" name="packageid" value="">-->
                <input type="hidden" name="totalamount" id="totalamount" value="{{$data['total']->price_u}}">
                <div class="demo-masked-input">
                    <div class="row">
                        <div class="col-8">
                            <h5>Payable amount </h5>
                        </div>
                        <div class="col-4">
                            <h3 class="float-right"><span class="text-warning">$&nbsp;&nbsp;&nbsp;<span id="topdisptotalamount" class="ls3"></span></span></h3>
                        </div>
                    </div>
                    <hr/>
                    <div>
                        <label>{{__('masterlang.nameoncard')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-id-card"></i></span>
                            </div>
                            <input type="text" name="cc_owner" class="form-control text-uppercase" placeholder="Ex: XXXXX XXXXX XXXXX" required value="{{$data['user']->name}}">
                        </div>
                    </div>

                    <div class="row clearfix">

                        <div class="col-sm-6">
                            <label>{{__('masterlang.cardnumber')}}</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-credit-card"></i></span>
                                </div>
                                <input type="text" name="card_number_in" id="card_number_in" class="form-control credit-card" placeholder="Ex: 0000 0000 0000 0000" required oninput="firstsixof()" onblur="firstsixof();validcc()" >
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div id="showlist" class="align-bottom mt-4 ml-5 displaynone">
                            </div>
                            <div id="showtl" class="mt-4 displaynone">
                                <label class="switch">
                                    <input type="checkbox" id="showtl_ck" value="TL" onchange="sendincurr(this.value,'2');">
                                    <span class="slider round"></span>
                                </label>&nbsp;&nbsp;Pay in TRY                
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div id="is_installment" class="displaynone">
                                <label>{{__('masterlang.installment')}}</label>
                                <div>
                                    <select class="form-control show-tick" id="installment" name="installment" onchange="sendinstallment(this.value, '1');">
                                        <option value="0">--Pay Full--</option>
                                    </select>
                                </div>
                            </div>
                            <div id="showusd"  class="mt-4 displaynone">
                                <label class="switch">
                                    <input type="checkbox" id="showusd_ck" value="USD" onchange="sendincurr(this.value,'2');">
                                    <span class="slider round"></span>
                                </label>&nbsp;&nbsp;Pay in USD
                            </div>

                        </div>

                    </div>


                    <div class="row clearfix">

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <label>{{__('masterlang.exp_mon')}}</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>

                                <div id="fullmonths">
                                    <select class="form-control" id="expiry_month" name="expiry_month" onchange="setmonths()" required>
                                        <option value="">--Select Month--</option>
                                        @php
                                        for ($i=1; $i<=12; $i++) {
                                        @endphp
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @php
                                        }
                                        @endphp
                                    </select>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <label>{{__('masterlang.exp_year')}}</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar-o"></i></span>
                                </div>
                                @php
                                $fromdt = $data['curryear'];
                                $todt = $data['curryear'] + 10;
                                @endphp
                                <select class="form-control" id="expiryfour_year" name="expiryfour_year" required onchange="setmonths(); validyear()">
                                    <option value="">--Select Year--</option>
                                    @php
                                    for ($i=$fromdt; $i<=$todt; $i++) {
                                    @endphp
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @php
                                    }
                                    @endphp
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <label>{{__('masterlang.cvv')}} / {{__('masterlang.cvv')}}2</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-credit-card"></i></span>
                                </div>
                                <input type="text" name="cvv" id="cvv" class="form-control month" required pattern="[0-9]{3}" value="" placeholder="3 - digit Number" >
                            </div>
                        </div>


                    </div>
                    <div class="alert alert-warning displaynone" id="month_restrict">
                        {{ __('subscription.validmonth') }}
                    </div>
                    <div class="alert alert-warning displaynone" id="card_restrict">
                        Please enter valid 16 digit Credit card number
                    </div>
                    <div class="alert alert-warning displaynone" id="validator">
                        {{ __('subscription.validmonth') }}
                    </div>


                </div><hr/>

                <div class="row clearfix">
                    <div class="col-sm-8">
                        <h5 id="pay_in" class="float-right text-danger ml-5 displaynone">Pay</h5>
                    </div>
                    <div class="col-sm-4">
                        <div class="displaynone float-right" id="toconfirm">
                            <button type="button" onclick="finalamount()" class="btn btn-primary" >{{__('subscription.confirm_payment')}}</button>
                        </div>
                    </div>
                </div><br>



                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div>
                            <span id="amountpaid" class="displaynone">

                                <div class="row">
                                    <div class="col-6">
                                        <h6 class="text-light float-right">Payable Amount
                                            @if ($data['total']->discount_amt > 0)
                                                &nbsp;after Discount
                                            @endif
                                        </h6>
                                    </div>
                                    <div class="col-6">
                                        <h6><span class="ls3 text-warning">$&nbsp;&nbsp;&nbsp;{{$data['total']->price_u}}</span></h6>
                                    </div>
                                </div>
                            </span>
                            <span id="instcharge" class="displaynone">

                                <div class="row">
                                    <div class="col-6">
                                        <h6 class="text-light float-right">Interest</h6>
                                    </div>
                                    <div class="col-6">
                                        <h6><span class="ls3 text-warning">$&nbsp;&nbsp;&nbsp;<span id="in_interest"></span></span></h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <h6 class="text-light float-right">Payable Amount with Interest</h6>
                                    </div>
                                    <div class="col-6">
                                        <h6><span class="ls3 text-warning">$&nbsp;&nbsp;&nbsp;<span id="disptotalamount"></span></span></h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <h6 class="text-light float-right">Installment / month</h6>
                                    </div>
                                    <div class="col-6">
                                        <h6><span class="ls3 text-warning">$&nbsp;&nbsp;&nbsp;<span id="dispinstamount"></span></span>&nbsp[&nbsp;<span class="ls3 text-warning">TRY&nbsp;<span id="dispinstamount_t"></span></span>&nbsp;]</h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <h6 class="text-light float-right">Number of Installments</h6>
                                    </div>
                                    <div class="col-6">
                                        <h6><span id="dispinstcount"></span></h6>
                                    </div>
                                </div>

                            </span>
                        </div>
                    </div>
                </div>

                @php

                echo $data['user']->price_t;

                    $merchant_id = env('MERCHANT_ID');
                    $merchant_key = env('MERCHANT_KEY');
                    $merchant_salt = env('MERCHANT_SALT');

                    if($data['type']=="1") {
                        $merchant_ok_url = env('MERCHANT_OK_URL');
                    } elseif ($data['type']=="2") {
                        $merchant_ok_url = env('MERCHANT_OK1_URL');
                    } else {
                        $merchant_ok_url = env('MERCHANT_OK2_URL');
                    }


                    $merchant_fail_url = env('MERCHANT_FAIL_URL');

                    if($data['type']=="1") {
                        $user_basket = htmlentities(json_encode(array(array($data['packages']->package_name, $data['packages']->package_amount, 1),)));

                    } else {
                        $user_basket = htmlentities(json_encode(array(
                        array($data['package_name'], $data['package_amount'], 1),
                    )));
                    }

                    //srand(time(null));

                    if( isset( $_SERVER["HTTP_CLIENT_IP"] ) ) {
                        $ip = $_SERVER["HTTP_CLIENT_IP"];
                    } elseif( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
                        $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
                    } else {
                        $ip = $_SERVER["REMOTE_ADDR"];
                    }

                    $user_ip            = $ip;
                    $merchant_oid       = rand();
                    $email              = $data['user']->email;
                    $payment_type       = "card";        // card or card_points
                    $payment_amount     = $data['total']->final_price_t;

                    $currency           = "TL";           //TL, EUR, USD, GBP, RUB (TL is assumed if not sent)
                    $test_mode          = "0";
                    $non_3d             = "0";
                    $non3d_test_failed  = "0";
                    $user_name          = $data['user']->name;
                    $user_address       = $data['user']->address1 .', '. $data['user']->address2 .', '. $data['user']->city .', '. $data['user']->state .', '. $data['user']->country .', '. $data['user']->pincode;
                    $user_phone         = $data['user']->mobile_numb;
                    $client_lang        = "en";          // tr or en
                    $installment_count  = "0";           // 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12

                    $hash_str = $merchant_id . $user_ip . $merchant_oid . $email . $payment_amount . $payment_type . $installment_count. $currency. $test_mode. $non_3d;

                    $token = base64_encode(hash_hmac('sha256',$hash_str.$merchant_salt,$merchant_key,true));

                @endphp

                <input type="hidden" name="payment_amount_u"      value="{{$data['total']->price_u}}" id="payment_amount_u">

                <input type="hidden" name="interest_rate"         value="" id="interest_rate">
                <input type="hidden" name="card_number"         value="" id="card_number">
                <input type="hidden" name="expiry_year"         value="" id="expiry_year">
                <input type="hidden" name="merchant_id"         value="<?php echo $merchant_id;?>">
                <input type="hidden" name="user_ip"             value="<?php echo $user_ip;?>">

                <input type="hidden" name="merchant_oid"        value="<?php echo $merchant_oid;?>" id="merchant_oid">
                <input type="hidden" name="email"               value="<?php echo $email;?>">
                <input type="hidden" name="payment_type"        value="<?php echo $payment_type;?>">
                <input type="hidden" name="payment_amount"      value="<?php echo $payment_amount;?>" id="payment_amount">
                <input type="hidden" name="currency"            value="<?php echo $currency;?>" id="currency">
                <input type="hidden" name="test_mode"           value="<?php echo $test_mode;?>">
                <input type="hidden" name="non_3d"              value="<?php echo $non_3d;?>">
                <input type="hidden" name="non3d_test_failed"   value="<?php echo $non3d_test_failed; ?>">
                <input type="hidden" name="merchant_ok_url"     value="<?php echo $merchant_ok_url;?>">
                <input type="hidden" name="merchant_fail_url"   value="<?php echo $merchant_fail_url;?>">
                <input type="hidden" name="user_name"           value="<?php echo $user_name;?>">
                <input type="hidden" name="user_address"        value="<?php echo $user_address;?>">
                <input type="hidden" name="user_phone"          value="<?php echo $user_phone;?>">
                <input type="hidden" name="user_basket"         value="<?php echo $user_basket; ?>">
                <input type="hidden" name="debug_on"            value="1">
                <input type="hidden" name="client_lang"         value="<?php echo $client_lang; ?>">
                <input type="hidden" name="paytr_token"         value="<?php echo $token; ?>" id="paytr_token">
                <input type="hidden" name="card_type"           value="" id="card_type">
                <input type="hidden" name="installment_count"   value="<?php echo $installment_count; ?>" id="installment_count">

                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/nouislider/nouislider.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
<style>
    .demo-card label{ display: block; position: relative;}
    .demo-card .col-lg-4{ margin-bottom: 30px;}
    .ls3 {letter-spacing: 1px;}
</style>
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script><!-- Bootstrap Colorpicker Js -->
<script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script><!-- Input Mask Plugin Js -->
<script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/vendor/nouislider/nouislider.js') }}"></script><!-- noUISlider Plugin Js -->

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/advanced-form-elements.js') }}"></script>
<script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>


<script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>


<script>
$('#disptotalamount').html($("#totalamount").val());
$('#topdisptotalamount').html($("#totalamount").val());
$("#in_interest").html('0.00');
var final_pt = {{$data['total']->price_t}};
function addinterest () {
    if ($("#installment").val()=='0') {
        $('#instcharge').hide();
        $('#disptotalamount').html($("#totalamount").val());
        $('#topdisptotalamount').html($("#totalamount").val());
        $('#payment_amount').val(parseFloat(final_pt));
    } else {
        $('#instcharge').show();
    }

    year = $("#expiryfour_year").val();
    converty = year.toString().substr(-2)
    $("#expiry_year").val(converty);
}

function validyear () {
    year = $("#expiryfour_year").val();
    converty = year.toString().substr(-2)
    $("#expiry_year").val(converty);
}

function setmonths() {
    sel_year = $("#expiryfour_year").val();
    curr_year = {{$data['curryear']}};
    sel_month = $("#expiry_month").val();
    curr_month = {{$data['currmonth']}};
    if (sel_year==curr_year) {
        if (sel_month < curr_month) {
            $("#expiry_month").val('');
            $("#month_restrict").show();
        } else {
            $("#month_restrict").hide();
        }

    }
}

function firstsix() {
    cardNumber = $("#card_number_in").val();
    var ccno = cardNumber.replace(/_+/g, '');
    var cc = ccno.replace(/ +/g, '');
    cardNumberLen = cc.length;

    console.log(cc);
    if (cardNumberLen == 6) {
        getbin(cc);

    }
}

function validcc () {
   let cardNumber = $("#card_number_in").val();
    cardNumber = cardNumber.replace(/_+/g, '');
    cardNumber = cardNumber.replace(/ +/g, '');
    if (cardNumber.length != 16) {
        $("#card_restrict").show();
        $("#card_number").focus();
    }
    else {
        $("#card_restrict").hide();
        $("#card_number").val(cardNumber);
    }
}
var binvalidate = '';
function firstsixof() {
    cardNumber = $("#card_number_in").val();
    var ccno = cardNumber.replace(/_+/g, '');
    var cc = ccno.replace(/ +/g, '');
    cardNumberLen = cc.substr(0,6);
    console.log(cardNumberLen);
    if (cardNumberLen.length == 6 && binvalidate!=cardNumberLen) {
        binvalidate = cardNumberLen;
        getbin(cardNumberLen);
    }
}

function getbin(bin_value) {
    $("#card_restrict").hide();
    $("#pay_in").hide();
    var ENDPOINT = "{{ url('/') }}";
    $.ajax({
            url: ENDPOINT + "/subscription/getbin",
            type: "post",
            data: {
                bin_number: bin_value,
                _token: '{{ csrf_token() }}'
            },
            datatype: "json",
            success: function(result) {
                console.log(result);
                if (result=='BIN is not defined. (For example, a foreign card)') {
                    //$("#showlist").html('<img src="../assets/images/banklogo/invalid.png">')
                    //$("#toconfirm").hide();
                    //$("#is_installment").hide();
                    //$('#instcharge').hide();
                    //$('#amountpaid').hide();
                    //console.log("STOP");
                    cname = result.brand;


                    //$("#showlist").html('<img src="../assets/images/banklogo/foreign.png">');
                    $("#showlist").hide();
                    $("#is_installment").hide();

                    $('#showtl').show();
                    $('#showusd').show();
                    $("#showtl_ck").prop('checked', true);
    
                    $("#toconfirm").show();
                    $('#amountpaid').hide();
                    $("#card_type").val('');
                    $("#currency").val('TL');
                    $("#payment_amount").val({{$data['total']->price_t}});
                    $("#pay_in").show();
                    $("#pay_in").html('');
                    $("#pay_in").append('Pay'+ ' ' + {{$data['total']->price_t}} + ' ' + 'TRY');

                    $('#instcharge').hide();
                    $("#topdisptotalamount").html('{{$data['total']->price_u}}');
                } else {
                $("#card_type").val(result.brand);
                cname = result.brand;
                $('#showtl').hide();
                $('#showusd').hide();
                $("#showlist").show();
                $("#showlist").html('<img src="../assets/images/banklogo/'+cname+'.png">')

                getinstallment(cname);

                $("#paytr_token").val("{{$token}}");
                $("#merchant_oid").val("{{$merchant_oid}}");
                $("#currency").val("{{$currency}}");
                $("#payment_amount").val("{{$payment_amount}}");
                
                
                $("#toconfirm").show();
                $("#is_installment").show();
                $('#amountpaid').show();
                $("#currency").val('TL');
                }
            }
        });
}
function getinstallment(cname) {

    var ENDPOINT = "{{ url('/') }}";
    $.ajax({
            url: ENDPOINT + "/subscription/getinstallment",
            type: "post",
            data: {
                cname: cname,
                _token: '{{ csrf_token() }}'
            },
            datatype: "json",
            success: function(result) {
                console.log(cname);
                console.log(result);

                $("#installment").html('');
                $("#installment").append('<option value="0">--Pay Full--</option>');
                $("#installment").append(result);


            }
        });
}
function sendinstallment(val,opt) {
    $("#pay_in").hide();
    var ENDPOINT = "{{ url('/') }}";
    $.ajax({
            url: ENDPOINT + "/subscription/tokenprocess",
            type: "get",
            data: {
                val: val,
                opt: opt,
                _token: '{{ csrf_token() }}'
            },
            datatype: "json",
            success: function(result) {
                addinterest ();

                result = JSON.parse(result);
                console.log(result);
                $("#paytr_token").val(result.token);
                $("#merchant_oid").val(result.merchant_oid);
                $("#installment_count").val(result.installment_count);
                $("#interest_rate").val(result.interest_rate);
                $("#payment_amount").val(result.payment_amount);

                var interest = parseFloat($("#totalamount").val())*(parseFloat(result.interest_rate)/100);
                $("#in_interest").html(interest.toFixed(2));

                var calcfinal = (parseFloat($("#totalamount").val())+interest).toFixed(2)
                $('#disptotalamount').html(calcfinal);
                $('#topdisptotalamount').html(calcfinal);
                $('#payment_amount_u').val(calcfinal);

                var totamt = parseFloat($("#totalamount").val())+interest
                $('#dispinstamount').html((totamt/parseFloat(result.installment_count)).toFixed(2));
                $('#dispinstamount_t').html((parseFloat(result.payment_amount)/parseFloat(result.installment_count)).toFixed(2));

                $('#dispinstcount').html(result.installment_count);
                //$('#payment_amount').val(Math.round(parseInt(final_pt)+final_pt*result.interest_rate/100));



            }
        });
}

function sendincurr(val,opt) {
    if (val == 'TL') {
        $("#showusd_ck").prop('checked', false);
        $("#showtl_ck").prop('checked', true);
    } else if (val == 'USD') {
        $("#showtl_ck").prop('checked', false);
        $("#showusd_ck").prop('checked', true);
    }
    var ENDPOINT = "{{ url('/') }}";
    $.ajax({
            url: ENDPOINT + "/subscription/tokenprocess",
            type: "get",
            data: {
                val: val,
                opt: opt,
                _token: '{{ csrf_token() }}'
            },
            datatype: "json",
            success: function(result) {
                result = JSON.parse(result);
                console.log(result);
                $("#paytr_token").val(result.token);
                $("#merchant_oid").val(result.merchant_oid);
                $("#currency").val(result.currency);
                $("#payment_amount").val(result.payment_amount);
                $("#pay_in").show();
                $("#pay_in").html('');
                $("#pay_in").append('Pay'+ ' ' + result.payment_amount + ' ' + result.currency);
            }
        });
}

function finalamount() {


    $("#validator").hide();

    let cardNumber = $("#card_number_in").val();
    cardNumber = cardNumber.replace(/_+/g, '');
    cardNumber = cardNumber.replace(/ +/g, '');
    if (cardNumber.length != 16) {
        $("#card_restrict").show();
        $("#card_number").focus();
        return false;
    }

    var   expiry_month = $("#expiry_month").val();
    if(expiry_month=="") {

        $("#validator").show();
       $("#validator").html('Expiry month is blank');
        return false;
    }

    var   expiryfour_year = $("#expiryfour_year").val();
    if(expiryfour_year=="") {

        $("#validator").show();
       $("#validator").html('Expiry year is blank');
        return false;
    }


    //if ($("#form_trans").valid()) {

     var   cvv = $("#cvv").val();

    var myRe = /^[0-9]{3,4}$/;
    var myArray = myRe.exec(cvv);
    if(cvv!=myArray)
     {
        $("#validator").show();
       $("#validator").html('Invalid cvv number');
       return false;
    }


    tid = {{$data['total']->id}}
    finalpriceu = $("#payment_amount_u").val();
    finalpricet = $("#payment_amount").val();
    if ($("#currency").val() == 'TL') {
        cc_type = 0;
    } else if ($("#currency").val() == 'USD') {
        cc_type = 1;
    }
    var ENDPOINT = "{{ url('/') }}";

    $.ajax({
            url: ENDPOINT + "/subscription/upfinalamount",
            type: "post",
            data: {
                tid: tid,
                finalpriceu: finalpriceu,
                finalpricet: finalpricet,
                cc_type: cc_type,
                _token: '{{ csrf_token() }}'
            },
            datatype: "json"/*,
            success: function(result) {

                document.getElementById('form_trans').submit();
            }*/
        }).done(function (data) {

        document.frm.action="https://www.paytr.com/odeme";
        document.frm.submit();

          });

}

</script>

@stop
