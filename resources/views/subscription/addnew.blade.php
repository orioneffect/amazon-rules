@extends('layout.master')
@section('parentPageTitle', __('subscription.subscription'))
@section('title', __('subscription.subscription_list'))


@section('content')
<div class="row clearfix">
    @foreach($packages as $package)

    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card">
            <ul class="pricing body">
                <!--<li class="plan-img"><img class="img-fluid rounded-circle" src="../assets/images/plan-1.svg" alt="" /></li>-->
                <li><h3>{{$package->package_name}}</span></h3>
                <li class="price">
                    <h3><span>$</span> {{$package->package_amount}} <small>/ month</small></h3>
                </li>
                <li class="text"><i class="fa fa-check-circle text-success"></i>&nbsp;&nbsp; {{__('subscription.choose_plan')}}</li>
                <li class="text"><i class="fa fa-check-circle text-success"></i>&nbsp;&nbsp; {{__('subscription.no_of_store')}} : @if ($package->unlimited_stores==1) {{__('subscription.unlimited_stores')}} @else {{$package->no_of_store}} @endif</li>
                <li class="text"><i class="fa fa-check-circle text-success"></i>&nbsp;&nbsp; {{__('subscription.max_asin_count')}} : @if ($package->unlimited_asin_limit==1) {{__('subscription.unlimited_asin_limit')}} @else {{$package->max_asin_count}} @endif</li>
                <li class="text">@if ($package->stock_control==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.stock_control')}}</li>
                <li class="text">@if ($package->price_control	==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.price_control')}}</li>
                <li class="text">@if ($package->repricer==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.repricer')}}</li>
                <li class="text">@if ($package->advanced_filters==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.advanced_filters')}}</li>
                <li class="text">@if ($package->trademark_control==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.trademark_control')}}</li>
                <li class="text">@if ($package->dropshipping_flag==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.dropshipping_flag')}}</li>
                <li class="text">@if ($package->fba_flag==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.fba_flag')}}</li>
                <li class="text">@if ($package->ebay_etsy_flag==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.ebay_etsy_flag')}}</li>
                <li class="text">@if ($package->supplier_wholesale_america_flag==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.supplier_wholesale_america_flag')}}</li>
                <li class="text">@if ($package->supplier_wholesale_turkey_flag==1) <i class="fa fa-check-circle text-success">  @else <i class="fa fa-ban text-danger"> @endif </i>&nbsp;&nbsp; {{__('subscription.supplier_wholesale_turkey_flag')}}</li>

                <li class="plan-btn"><a href="{{ route('subscription.process', ['package' => $package->id]) }}" class="btn btn-round btn-outline-secondary">{{__('subscription.choose_plan')}}</a></li>
            </ul>
        </div>
    </div>
    @endforeach
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop
