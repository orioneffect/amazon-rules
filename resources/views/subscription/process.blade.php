@extends('layout.master')
@section('parentPageTitle', __('subscription.subscription'))
@section('title', __('subscription.confirm_subscription_personal'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="body top_counter">
                <div class="icon bg-info text-white"><i class="fa fa-shopping-cart"></i> </div>
                <div class="content">
                    <span>{{$data['packages']->package_name}}</span>
                    <h5 class="number mb-0">{{$data['packages']->package_amount}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@if($errors->any())
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="body top_counter">
                {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
            </div>
        </div>
    </div>
</div>
@endif
@if (Session::has('error'))
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="body top_counter">
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="body">
                <h4>{{__('subscription.personal')}}</h4>
                <form method="POST" action="{{ route('subscription.confirm') }}" validate>
                    @csrf
                <input type="hidden" name="packageid" value="{{$data['packages']->id}}">
                <div class="demo-masked-input">
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <label>{{__('masterlang.name')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" name="name" value="{{$data['user']->name}}" class="form-control" placeholder="{{__('masterlang.full_name')}} *" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label>{{__('masterlang.first_name')}}</label><div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" name="firstname" value="{{$data['user']->first_name}}" class="form-control" placeholder="{{__('masterlang.first_name')}} *" required>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <label>{{__('masterlang.last_name')}}</label><div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" name="lastname"  value="{{$data['user']->last_name}}" class="form-control" placeholder="{{__('masterlang.last_name')}} *" required>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <label>{{__('masterlang.email')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
                            </div>
                            <input type="text" name="email"  value="{{$data['user']->email}}" class="form-control email" placeholder="{{__('masterlang.email')}} *" required>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <label>{{__('masterlang.phone')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                            </div>
                            <input type="text" name="phone"  value="{{$data['user']->phone}}" class="form-control phone-number" placeholder="{{__('masterlang.phone')}} *" required>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <label>{{__('masterlang.address')}} 1</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-book"></i></span>
                            </div>
                            <input type="text" name="address1"  value="{{$data['user']->address1}}" class="form-control" placeholder="{{__('masterlang.address')}} 1*" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label>{{__('masterlang.address')}} 2</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-book"></i></span>
                            </div>
                            <input type="text" name="address2" value="{{$data['user']->address2}}" class="form-control" placeholder="{{__('masterlang.address')}} 2">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>{{__('masterlang.country')}}</label>
                            <select name="country" id="country" class="form-control show-tick" required>
                                <option value="">{{__('masterlang.select_country')}} *</option>
                                @foreach($data['countries'] as $country)
                                    <option value="{{$country->id}}" @if ($data['user']->country==$country->id)
                                        {{'selected'}}
                                    @endif>{{$country->country_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>{{__('masterlang.state')}}</label>
                            <select name="state" id="state" class="form-control show-tick" required>
                                <option value="">{{__('masterlang.select_state')}} *</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <label>{{__('masterlang.pincode')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-book"></i></span>
                            </div>
                            <input type="text" name="pincode"  value="{{$data['user']->pincode}}"  class="form-control" placeholder="{{__('masterlang.pincode')}}">
                        </div>
                    </div>
                </div>
<!--                <h4>{{__('subscription.bank')}}</h4>
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <label>{{__('masterlang.credit_card')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-credit-card"></i></span>
                            </div>
                            <input type="text" name="cc" class="form-control credit-card" placeholder="Ex: 0000 0000 0000 0000" required value="{{ old('cc') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>{{__('masterlang.exp_mon_year')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                            <input type="month" name="exp_mon" class="form-control month" required value="{{ old('exp_mon') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>{{__('masterlang.cvv')}} / {{__('masterlang.cvv')}}2</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-credit-card"></i></span>
                            </div>
                            <input type="number" name="cvv" class="form-control month" required data-parsley-min="3" value="{{ old('cvv') }}">
                        </div>
                    </div>
                </div>
            -->
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>{{__('masterlang.couponcode')}}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-code"></i></span>
                            </div>
                            <input type="text" name="discount" id="discount" class="form-control month" value="{{ old('discount') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="mt-4">
                            <button type="button" id="btnapply" class="btn btn-info">{{__('masterlang.apply')}}</button>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-sm-12 displaynone alert alert-success" id="successmsg">
                        <p class="text-success">
                        </p>
                    </div>
                    <div class="col-sm-12 displaynone alert alert-danger" id="failedmsg">
                        <p class="text-danger">
                        </p>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-sm-3">
                        <div class="alert alert-info" id="balancediv">
                            {{__('subscription.totalamount')}} : $ {{$data['packages']->package_amount}}
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-sm-3">
                        <div class="alert alert-info" >
                            {{__('subscription.discount')}} : $ <span id="discountdiv">0</span>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-sm-3">
                        <div class="alert alert-info" id="">
                            {{__('subscription.balance')}} : $ <span id="finalamountdiv">{{$data['packages']->package_amount}}</span>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="{{$data['packages']->package_amount}}" id="packagemounthidden" >
                <input type="hidden" name="discountamt" id="discountamt" value="0">
                <input type="hidden" name="tobepaid" id="tobepaid" value="{{$data['packages']->package_amount}}">
                <input type="hidden" value="" id="couponid" name="couponid" >

                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="mt-4">
                            <button type="submit" class="btn btn-primary">{{__('subscription.gotopaymentpage')}}</button>
                            <button type="button" class="btn btn-outline-secondary">{{__('masterlang.cancel')}}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/nouislider/nouislider.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
<style>
    .demo-card label{ display: block; position: relative;}
    .demo-card .col-lg-4{ margin-bottom: 30px;}
</style>
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script><!-- Bootstrap Colorpicker Js -->
<script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script><!-- Input Mask Plugin Js -->
<script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/vendor/nouislider/nouislider.js') }}"></script><!-- noUISlider Plugin Js -->

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/advanced-form-elements.js') }}"></script>
<script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $('#country').on('change', function () {
            var idCountry = this.value;
            getStates(idCountry);
        });
    });

    function getStates(country) {
        var userstate='{{$data['user']->state}}';
        $("#state").html('');
        $.ajax({
            url: "{{url('api/fetch-states')}}",
            type: "POST",
            data: {
                country_id: country,
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function (result) {
                $('#state').html('<option value="">{{__('masterlang.select_state')}} *</option>');
                $.each(result.states, function (key, value) {
                    var selected='';
                    if (userstate==value.id) {
                        selected='selected';
                    }
                    $("#state").append('<option value="' + value.id + '"  '+selected+'  >' + value.state_name + '</option>');
                });
            }
        });
    }

    var successmsg="{{__('subscription.coupon_success')}}";
    var failedmsg="{{__('subscription.coupon_failed')}}";


    $('#btnapply').on("click",function() {

        if($('#discount').val()=="") {
            return false;
        }
        $("#successmsg").hide();
        $("#failedmsg").hide();
        //var userstate='{{$data['user']->state}}';
        //$("#state").html('');
        $.ajax({
            url: "{{url('subscription/applydiscount')}}",
            type: "POST",
            data: {
                discount: $('#discount').val(),
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function (result) {
                var packagemounthidden=$("#packagemounthidden").val();
                var discount=0;
                var balance=packagemounthidden;
                if(result.status=='success') {
                    if(result.type==1) {
                        discount=parseInt(packagemounthidden) * parseInt(result.value)/100;
                        $("#discountamt").val(discount);
                        balance= parseInt(balance)-discount;
                    } else {
                        discount=parseInt(result.value);
                        $("#discountamt").val(discount);
                        balance= parseInt(balance)-discount;
                    }
                    $("#discountdiv").html(discount);
                    $("#finalamountdiv").html(balance);
                    $("#tobepaid").val(balance);
                    console.log(balance);
                    $("#successmsg").show();
                    $("#successmsg").html(successmsg.replace(":1:","$"+discount));
                    $("#couponid").val(result.id);
                } else {
                    $("#failedmsg").show();
                    $("#failedmsg").html(failedmsg.replace(":1:",result.message));
                }
            }
        });
    });

    @if ($data['user']->country!="")
        getStates({{$data['user']->country}});
    @endif

</script>

@stop
