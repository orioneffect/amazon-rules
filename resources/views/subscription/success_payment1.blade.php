@extends('layout.master')
@section('parentPageTitle', __('subscription.subscription'))
@section('title', __('subscription.paysucc'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <h4>Success Payment</h4>
            <div class="body">
                <h5 class="text-warning">Received Payment Successfully<span class="text-light"></span></h5>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop
