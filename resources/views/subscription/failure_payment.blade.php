@extends('layout.master')
@section('parentPageTitle', __('subscription.subscription'))
@section('title', __('subscription.confirm_subscription_bank'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <h4>Your transaction has been failed. Please <a href="../subscription/process/{{$data['package']}}">click here to  Retry</a>
            </h4>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop
