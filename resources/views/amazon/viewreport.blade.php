@extends('layout.master')
@section('parentPageTitle', __('amazon.amazon'))
@section('title', __('amazon.reports'))

@section('content')
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <ul class="nav nav-tabs3 table-nav">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Expenses">{{$data['report_name']}}</a></li>
            </ul>
            <div class="tab-content mt-0">
                <div class="tab-pane show active" id="Expenses">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                                @foreach(explode('***', $data['response']) as $info)
                                    @if ($loop->first)
                                        <thead>
                                            <tr>
                                                @foreach(explode('^^^', $info) as $column)
                                                <th>{{$column}}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                    @else
                                        <tr>
                                            @foreach(explode('^^^', $info) as $column)
                                            <th>{{$column}}</th>
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop
