@extends('layout.master')
@section('parentPageTitle', __('amazon.amazon'))
@section('title', __('amazon.health'))


@section('content')


@if (session('in_progress'))
    <div class="alert alert-warning alert-dismissible" role="alert" >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="fa fa-check-circle"></i> {{__('amazon.in_progress')}}
    </div>
@else

    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <i class="fa fa-check-circle"></i> {{__('amazon.new_report_added')}}
        </div>
    @else
        @if($lastgeenrated_on==null)
        <div class="alert alert-info" role="alert">
            <a href="{{ route('amazon.addhealthreport') }}" class="alert-link">{{__('amazon.generate_new')}}</a>.
        </div>
        @else
        <div class="alert alert-info" role="alert">
            {{__('amazon.last_generated_on')}} {{$lastgeenrated_on}}. <a href="#a" class="alert-link">{{__('amazon.generate_new')}}</a>.
        </div>

        <div class="row clearfix">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card text-white @if ($metrix->accountStatuses[0]->status == 'NORMAL') bg-secondary @elseif ($metrix->accountStatuses[0]->status == 'GOOD') bg-green @else bg-danger  @endif">
                    <div class="card-header">{{__('amazon.account_health_status')}} </div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h1 @if ($metrix->accountStatuses[0]->status == 'NORMAL') text-secondary @elseif ($metrix->accountStatuses[0]->status == 'GOOD') text-success @else text-danger  @endif">{{$metrix->accountStatuses[0]->status}}</div><!--
                        <div class="d-flex">
                            <small class="text-muted">Previous Month</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card text-white @if ($metrix->performanceMetrics[0]->accountHealthRating->ahrStatus == 'NORMAL') bg-secondary @elseif ($metrix->performanceMetrics[0]->accountHealthRating->ahrStatus == 'GOOD' || $metrix->performanceMetrics[0]->accountHealthRating->ahrStatus == 'GREAT') bg-green @else bg-danger  @endif">
                    <div class="card-header">{{__('amazon.accountHealthRating')}} </div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h1 @if ($metrix->performanceMetrics[0]->accountHealthRating->ahrStatus == 'NORMAL') text-secondary @elseif ($metrix->performanceMetrics[0]->accountHealthRating->ahrStatus == 'GOOD' || $metrix->performanceMetrics[0]->accountHealthRating->ahrStatus == 'GREAT') text-success @else text-danger  @endif">{{$metrix->performanceMetrics[0]->accountHealthRating->ahrStatus}}</div><!--
                        <div class="d-flex">
                            <small class="text-muted">Previous Month</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card text-white bg-danger">
                    <div class="card-header">{{__('amazon.warningstatus')}} </div>
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h1">{{__('amazon.none')}}</div><!--
                        <div class="d-flex">
                            <small class="text-muted">Previous Month</small>
                            <div class="ml-auto"><i class="fa fa-caret-up text-success"></i>4.00%</div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>{{__('amazon.performance_metrix')}} </h2>
                    </div>
                    <table class="table table-hover table-custom spacing5 m-t--5 mb-0">
                        <thead>
                            <tr>
                                <th>{{__('amazon.metrix')}}</th>
                                <th>{{__('amazon.status')}}</th>
                                <th>{{__('amazon.count')}}</th>
                                <th>{{__('amazon.rate')}} <br>in %</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.lateShipment')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->lateShipmentRate->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->lateShipmentRate->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->lateShipmentRate->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->lateShipmentRate->status == 'NORMAL' || $metrix->performanceMetrics[0]->lateShipmentRate->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->lateShipmentRate->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->lateShipmentRate->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->lateShipmentRate->lateShipmentCount}} Out Of {{$metrix->performanceMetrics[0]->lateShipmentRate->orderCount}} </td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->lateShipmentRate->rate<=$metrix->performanceMetrics[0]->lateShipmentRate->targetValue)<span class="text-success">{{$metrix->performanceMetrics[0]->lateShipmentRate->rate*100}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{$metrix->performanceMetrics[0]->lateShipmentRate->rate*100}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.invoiceDefectRate')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->invoiceDefectRate->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->invoiceDefectRate->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->invoiceDefectRate->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->invoiceDefectRate->status == 'NORMAL' || $metrix->performanceMetrics[0]->invoiceDefectRate->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->invoiceDefectRate->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->invoiceDefectRate->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->invoiceDefectRate->invoiceDefect->count}} Out Of {{$metrix->performanceMetrics[0]->invoiceDefectRate->orderCount}}</td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->invoiceDefectRate->rate<=$metrix->performanceMetrics[0]->invoiceDefectRate->targetValue)<span class="text-success">{{$metrix->performanceMetrics[0]->invoiceDefectRate->rate*100}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{$metrix->performanceMetrics[0]->invoiceDefectRate->rate*100}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>


                            <tr>
                                <td class="font-weight-bold">{{__('amazon.orderDefectRate_afn')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->orderDefectRate->afn->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->orderDefectRate->afn->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->orderDefectRate->afn->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->orderDefectRate->afn->status == 'NORMAL' || $metrix->performanceMetrics[0]->orderDefectRate->afn->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->orderDefectRate->afn->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->orderDefectRate->afn->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->orderDefectRate->afn->orderWithDefects->count}} Out Of {{$metrix->performanceMetrics[0]->orderDefectRate->afn->orderCount}}</td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->orderDefectRate->afn->rate<=$metrix->performanceMetrics[0]->orderDefectRate->afn->targetValue)<span class="text-success">{{$metrix->performanceMetrics[0]->orderDefectRate->afn->rate*100}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{$metrix->performanceMetrics[0]->orderDefectRate->afn->rate*100}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>


                            <tr>
                                <td class="font-weight-bold">{{__('amazon.orderDefectRate_mfn')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->orderDefectRate->mfn->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->orderDefectRate->mfn->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->orderDefectRate->mfn->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->orderDefectRate->mfn->status == 'NORMAL' || $metrix->performanceMetrics[0]->orderDefectRate->mfn->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->orderDefectRate->mfn->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->orderDefectRate->mfn->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->orderDefectRate->mfn->orderWithDefects->count}} Out Of {{$metrix->performanceMetrics[0]->orderDefectRate->mfn->orderCount}}</td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->orderDefectRate->mfn->rate<=$metrix->performanceMetrics[0]->orderDefectRate->mfn->targetValue)<span class="text-success">{{$metrix->performanceMetrics[0]->orderDefectRate->mfn->rate*100}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{$metrix->performanceMetrics[0]->orderDefectRate->mfn->rate*100}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.onTimeDeliveryRate')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->onTimeDeliveryRate->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->onTimeDeliveryRate->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->onTimeDeliveryRate->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->onTimeDeliveryRate->status == 'NORMAL' || $metrix->performanceMetrics[0]->onTimeDeliveryRate->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->onTimeDeliveryRate->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->onTimeDeliveryRate->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->onTimeDeliveryRate->onTimeDeliveryCount}} Out Of {{$metrix->performanceMetrics[0]->onTimeDeliveryRate->shipmentCountWithValidTracking}} </td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->onTimeDeliveryRate->rate>=$metrix->performanceMetrics[0]->onTimeDeliveryRate->targetValue)<span class="text-success">{{$metrix->performanceMetrics[0]->onTimeDeliveryRate->rate*100}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{$metrix->performanceMetrics[0]->onTimeDeliveryRate->rate*100}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>

                            <tr>
                                <td class="font-weight-bold">{{__('amazon.validTrackingRate')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->validTrackingRate->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->validTrackingRate->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->validTrackingRate->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->validTrackingRate->status == 'NORMAL' || $metrix->performanceMetrics[0]->validTrackingRate->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->validTrackingRate->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->validTrackingRate->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->validTrackingRate->validTrackingCount}} Out Of {{$metrix->performanceMetrics[0]->validTrackingRate->shipmentCount}} </td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->validTrackingRate->rate>=$metrix->performanceMetrics[0]->validTrackingRate->targetValue)<span class="text-success">{{round($metrix->performanceMetrics[0]->validTrackingRate->rate*100)}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{round($metrix->performanceMetrics[0]->validTrackingRate->rate*100)}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>


                            <tr>
                                <td class="font-weight-bold">{{__('amazon.preFulfillmentCancellationRate')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.target')}} : @if ($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->targetCondition == 'LESS_THAN') < @elseif ($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->targetCondition == 'GREATER_THAN') > @else = @endif {{round($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->status == 'NORMAL' || $metrix->performanceMetrics[0]->preFulfillmentCancellationRate->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->preFulfillmentCancellationRate->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->preFulfillmentCancellationRate->cancellationCount}} Out Of {{$metrix->performanceMetrics[0]->preFulfillmentCancellationRate->orderCount}} </td>
                                <td class="text-right">@if($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->rate<=$metrix->performanceMetrics[0]->preFulfillmentCancellationRate->targetValue)<span class="text-success">{{round($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->rate*100)}}% <i class="fa fa-caret-up"> @else <span class="text-danger">{{round($metrix->performanceMetrics[0]->preFulfillmentCancellationRate->rate*100)}}% <i class="fa fa-caret-down"></i> </span>@endif</td>
                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2>{{__('amazon.policy_violation')}}</h2>
                    </div>
                    <table class="table table-hover table-custom spacing5 m-t--5 mb-0">
                        <thead>
                            <tr>
                                <th>{{__('amazon.metrix')}}</th>
                                <th>{{__('amazon.status')}}</th>
                                <th>{{__('amazon.no_of_violation')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.listingPolicyViolations')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->listingPolicyViolations->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->listingPolicyViolations->status == 'NORMAL' || $metrix->performanceMetrics[0]->listingPolicyViolations->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->listingPolicyViolations->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->listingPolicyViolations->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->listingPolicyViolations->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.productAuthenticityCustomerComplaints')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status == 'NORMAL' || $metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.productAuthenticityCustomerComplaints')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status == 'NORMAL' || $metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->productAuthenticityCustomerComplaints->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.productConditionCustomerComplaints')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->productConditionCustomerComplaints->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->productConditionCustomerComplaints->status == 'NORMAL' || $metrix->performanceMetrics[0]->productConditionCustomerComplaints->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->productConditionCustomerComplaints->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->productConditionCustomerComplaints->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->productConditionCustomerComplaints->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.productSafetyCustomerComplaints')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->productSafetyCustomerComplaints->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->productSafetyCustomerComplaints->status == 'NORMAL' || $metrix->performanceMetrics[0]->productSafetyCustomerComplaints->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->productSafetyCustomerComplaints->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->productSafetyCustomerComplaints->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->productSafetyCustomerComplaints->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.receivedIntellectualPropertyComplaints')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->receivedIntellectualPropertyComplaints->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->receivedIntellectualPropertyComplaints->status == 'NORMAL' || $metrix->performanceMetrics[0]->receivedIntellectualPropertyComplaints->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->receivedIntellectualPropertyComplaints->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->receivedIntellectualPropertyComplaints->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->receivedIntellectualPropertyComplaints->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.restrictedProductPolicyViolations')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->restrictedProductPolicyViolations->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->restrictedProductPolicyViolations->status == 'NORMAL' || $metrix->performanceMetrics[0]->restrictedProductPolicyViolations->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->restrictedProductPolicyViolations->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->restrictedProductPolicyViolations->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->restrictedProductPolicyViolations->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.suspectedIntellectualPropertyViolations')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->suspectedIntellectualPropertyViolations->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->suspectedIntellectualPropertyViolations->status == 'NORMAL' || $metrix->performanceMetrics[0]->suspectedIntellectualPropertyViolations->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->suspectedIntellectualPropertyViolations->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->suspectedIntellectualPropertyViolations->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->suspectedIntellectualPropertyViolations->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.foodAndProductSafetyIssues')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->foodAndProductSafetyIssues->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->foodAndProductSafetyIssues->status == 'NORMAL' || $metrix->performanceMetrics[0]->foodAndProductSafetyIssues->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->foodAndProductSafetyIssues->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->foodAndProductSafetyIssues->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->foodAndProductSafetyIssues->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.customerProductReviewsPolicyViolations')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->customerProductReviewsPolicyViolations->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->customerProductReviewsPolicyViolations->status == 'NORMAL' || $metrix->performanceMetrics[0]->customerProductReviewsPolicyViolations->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->customerProductReviewsPolicyViolations->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->customerProductReviewsPolicyViolations->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->customerProductReviewsPolicyViolations->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.otherPolicyViolations')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->otherPolicyViolations->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->otherPolicyViolations->status == 'NORMAL' || $metrix->performanceMetrics[0]->otherPolicyViolations->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->otherPolicyViolations->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->otherPolicyViolations->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->otherPolicyViolations->defectsCount}} </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">{{__('amazon.documentRequests')}}
                                    <br>
                                    <a href="#a" class="text text-sm-left">{{__('amazon.maximum_allowed')}} : {{round($metrix->performanceMetrics[0]->documentRequests->targetValue*100)}}%</a>
                                </td>
                                <td><span class="badge @if ($metrix->performanceMetrics[0]->documentRequests->status == 'NORMAL' || $metrix->performanceMetrics[0]->documentRequests->status == 'NONE') badge-secondary @elseif ($metrix->performanceMetrics[0]->documentRequests->status == 'GOOD') badge-success @else badge-danger  @endif">{{$metrix->performanceMetrics[0]->documentRequests->status}}</span></td>
                                <td>{{$metrix->performanceMetrics[0]->documentRequests->defectsCount}} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
    @endif
@endif

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/flotscripts.bundle.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/index10.js') }}"></script>
@stop
