@extends('layout.master')
@section('parentPageTitle', __('amazon.amazon'))
@section('title', __('amazon.reports'))
@section('content')
<div class="row clearfix">
    <div class="col-lg-12">

        @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> {{__('amazon.new_report_added')}}
            </div>
        @endif

        @if (session('in_progress'))
            <div class="alert alert-warning alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check-circle"></i> {{__('amazon.in_progress')}}
            </div>
        @endif

        @if (session('deleted'))
            <div class="alert alert-danger alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-warning"></i> {{__('amazon.deleted')}}
            </div>
        @endif

        <div class="card">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Users">{{__('amazon.reports')}}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addUser">{{__('amazon.addreport')}}</a></li>
            </ul>
            <div class="tab-content mt-0">
                <div class="tab-pane show active" id="Users">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                <tr>
                                    <th>{{__('##')}}</th>
                                    <th class="w60">{{__('amazon.type')}}</th>
                                    <th>{{__('amazon.status')}}</th>
                                    <th>{{__('amazon.from')}}</th>
                                    <th>{{__('amazon.to')}}</th>
                                    <th>{{__('amazon.created')}}</th>
                                    <th class="w100">{{__('amazon.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($data["reports"] as $report)
                                <tr>
                                    <td class="width45">
                                        <div class="avtar-pic w35 bg-pink" data-toggle="tooltip" data-placement="top" title="Avatar Name"><span>&nbsp;</span></div>
                                    </td>
                                    <td>
                                        <h6 class="mb-0">{{$report->report_name}}</h6>
                                        @if ($report->status>0)
                                        <span><a href="{{ route('amazon.viewdoc') }}?doc_id={{$report->amazon_doc_id}}&report_name={{$report->report_name}}">View</a></span>
                                        @endif

                                    </td>
                                    @if ($report->status==0)
                                    <td><span class="badge badge-warning">In progress</span>
                                    <br>
                                    <span><a href="{{ route('amazon.viewreport', ['reportid' => $report->id]) }}">{{__('amazon.retry')}}</a></span>


                                    </td>
                                    @else
                                    <td><span class="badge badge-success">Generated</span></td>
                                    @endif

                                    <td>{{$report->from_date}}</td>
                                    <td>{{$report->to_date}}</td>
                                    <td>{{$report->created_at}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-default js-sweetalert" title="Delete" data-type="confirm" onclick="showConfirmMessage({{$report->id}})"><i class="fa fa-trash-o text-danger"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="addUser">
                    <form method="POST" action="{{ route('amazon.addnewreport') }}" validate onsubmit="document.getElementById('btn').setAttribute('disabled','disabled');">
                        @csrf
                        <div class="body mt-2">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <select class="form-control show-tick" required name="reporttype">
                                            <option value="">Select Report Type *</option>
                                            @foreach ($data["report_types"] as $report)
                                                <option value="{{$report->amazon_name}}">{{$report->report_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{__('amazon.from')}}</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control" placeholder="From Date" name="fromdt">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <label>{{__('amazon.to')}}</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control" placeholder="To Date" name="todt">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <button type="submit" id="btn" class="btn btn-primary">Create Report</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

<script>


function showConfirmMessage(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
        location.href="{{ route('amazon.deletereport') }}?id="+id;
    });
}
</script>
@stop
