<div id="megamenu" class="megamenu particles_jsa">
    <a href="javascript:void(0);" class="megamenu_toggle btn btn-danger"><i class="icon-close"></i></a>
    <div class="container">

        <div class="row clearfix">
            <div class="col-lg-4 col-md-12 col-sm-12">

                <div class="card text-white bg-green">
                    <div class="card-header" id="orderid"></div>
                    <input type="hidden" id="order_id">
                    <div class="card-body">
                        <div class="pb-4 m-0 text-center h1 text-success" id="orderstatus"></div>
                        <div class="d-flex">
                            <small class="text-muted">{{__('amazon.deliverydate')}}</small>
                            <div class="ml-auto" id="deliverydate"></div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="body">
                        <div class="media">
                            <img class="rounded mr-3" src="../assets/images/xs/avatar7.jpg" alt="">
                            <div class="media-body">
                                <h5 class="m-0" id="buyername"></h5>
                                <p class="text-muted mb-0 displaynone" id="buyeremail"></p>
                            </div>
                        </div>
                        <small class="text-muted">Address 1:- </small>
                        <p id="address1"></p>
                        <small class="text-muted">Address 2:- </small>
                        <p id="address2"></p>
                        <small class="text-muted">Municipality:- </small>
                        <p id="municipality"></p>
                        <small class="text-muted">City:- </small>
                        <p id="city"></p>
                        <small class="text-muted">Country:- </small>
                        <p class="mb-0" >
                            <span id="country"></span> - <span id="postal"></span>
                        </p>

                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">

                        <div class="card text-white bg-info">
                            <div class="card-header">{{__('amazon.price')}}</div>
                            <div class="card-body">
                                <div class="pb-4 m-0 text-center h1 text-info" id="orderprice"></div>
                                <div class="d-flex">
                                    <small class="text-muted">{{__('amazon.purchasedate')}}</small>
                                    <div class="ml-auto" id="purcahsedate"></div>
                                </div>
                            </div>
                        </div>
        
                        <div>
                            <div class="body">
                                <div class="media">
                                    <img class="rounded mr-3" id="productImage" src="../assets/images/xs/avatar7.jpg" alt="">
                                    <div class="media-body">
                                        <h5 class="m-0" id="producttitle"></h5>
                                        <p class="text-muted mb-0" id="itemid"></p>
                                    </div>
                                </div>
                                <small class="text-muted">SKU:- </small>
                                <p id="sku"></p>
                                <small class="text-muted">ASIN:- </small>
                                <p id="asin"></p>
                                <small class="text-muted">Condition:- </small>
                                <p id="condition"></p>
                                <small class="text-muted">Quantity:- </small>
                                <p id="qty"></p>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">

                        <div class="card text-white bg-dark">
                            <div class="card-header">Notes</div>
                            <div class="card-body p-1">
                                <div class="form-group">
                                        <textarea required rows="3" class="form-control" placeholder="" name="notes" id="notes" oninput="$('#notes_valid').hide();"></textarea>
                                </div>
                                <div class="float-left text-danger displaynone" id="notes_valid">Please Fill ... Notes</div>
                                <div class="float-right" ><button class="btn btn-primary" onclick="add_loadmsg(1)">Save</button></div>
                            </div>
                        </div>


                        <div>
                            <div class="card-body">
                                <div id="msg">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="particles-js"></div>
</div>
<script>
    function add_loadmsg(valid_notes) {
        var orderid = $('#order_id').val();
        var notes = $('#notes').val();
        if (notes == '' && valid_notes==1) {
            $('#notes_valid').show();
            $('#notes').focus();
        }
        $("#msg").html('');
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/amazon/add_loadmsg?orderid="+orderid+"&notes="+notes,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            $("#msg").html(response);
            
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

</script>