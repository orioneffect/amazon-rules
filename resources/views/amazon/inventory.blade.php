@extends('layout.master')
@section('parentPageTitle', __('amazon.amazon'))
@section('title', __('amazon.inventory'))


@section('content')
<div class="card">
<div class="row clearfix ClickTabs">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body AllInventory">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-indigo text-white rounded-circle"><i class="fa fa-briefcase"></i></div>
                    <a href="javascript:load_filters(true,0);" title="" class="text-white"><div class="ml-4 cursor-pointer">
                        <span>{{__('amazon.TotalInventory')}}</span>
                        <h4 class="mb-0 font-weight-medium">{{ $data['TotalCount'] }}</h4>
                    </div></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body ActiveInventory">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-azura text-white rounded-circle"><i class="fa fa-credit-card"></i></div>
                    <a href="javascript:load_filters(true,1);" title="" class="text-white"><div class="ml-4 cursor-pointer">
                        <span>{{__('amazon.ActiveInventory')}}</span>
                        <h4 class="mb-0 font-weight-medium">{{ $data['ActiveCount'] }}</h4>
                    </div></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body InactiveInventory">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-orange text-white rounded-circle"><i class="fa fa-users"></i></div>
                    <a href="javascript:load_filters(true,2);" title="" class="text-white"><div class="ml-4 cursor-pointer" >
                        <span>{{__('amazon.InactiveInventory')}}</span>
                        <h4 class="mb-0 font-weight-medium">{{ $data['InactiveCount'] }}</h4>
                    </div></a>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-pink text-white rounded-circle"><i class="fa fa-life-ring"></i></div>
                    <div class="ml-4">
                        <span>Bounce rate</span>
                        <h4 class="mb-0 font-weight-medium">$13,651</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
</div>
    <div class="body top_counter">
    <div class="row clearfix pb-3">
    <div class="icon bg-warning text-white"><i class="fa fa-filter"></i> </div>

<div class="col-lg-2">
        <div class="mb-1">Filter On</div>
        <select class="form-control" id="filterOn" name="filterOn">
            <option value="">--{{__('amazon.none')}}--</option>
            @isset($data['filter'])
                        @foreach($data['filter'] as $key => $value)
                            @if($value!='img' &&  $value!='lbh')
                            @if($value=='lbh')
                            <option value="{{ $value }}">{{__('amazon.'.$value)}}</option>
                            @else
                            <option value="{{ $value }}">{{ $value }}</option>
                            @endif
                            @endif
                        @endforeach
                        @endisset
        </select>
</div>
<div class="col-lg-2">
        <div class="mb-1">Condition</div>
        <select class="form-control" id="filterCondition" name="filterCondition">
            <option value="">--{{__('amazon.none')}}--</option>
            <option value="=">{{__('amazon.equalto')}}</option>
            <option value="!=">{{__('amazon.notequalto')}}</option>
            <option value=">">{{__('amazon.greaterthan')}}</option>
            <option value=">=">{{__('amazon.greaterthanequalto')}}</option>
            <option value="<">{{__('amazon.lessthan')}}</option>
            <option value="<=">{{__('amazon.lessthanequalto')}}</option>
        </select>
</div>
<div class="col-lg-2">
<div class="form-group">
                                    <label>Value</label>
                                    <input type="text" class="form-control" id="filterValue" name="filterValue" placeholder="Value" required="">
                                </div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12 mt-4">
                <a href="javascript:load_filters();" class="btn btn-lm btn-primary btn-block" title=""><i class="fa  fa-search-plus" > </i>{{__('amazon.search')}}</a>
                
            </div>
</div>
        <div class="row clearfix">
            


            
            <div class="col-lg-9 col-md-12 col-sm-12">
                <div class="{{ $data['cons'] == 'yes' ? 'displaynone' : 'mt-5' }} ">
                    <label class="switch">
                        <input type="checkbox" {{ $data['h_asin'] == '1' ? 'checked' : '' }} name="h_asin" id="h_asin" value="1" onchange="save_config('asin')">
                        <span class="slider round"></span>
                    </label>
                    {{__('amazon.hideasinc')}} &nbsp&nbsp&nbsp
                    <label class="switch">
                        <input type="checkbox"  {{ $data['h_product_name'] == '1' ? 'checked' : '' }} name="h_prodname" id="h_prodname" value="1" onchange="save_config('product_name')">
                        <span class="slider round"></span>
                    </label>
                    {{__('amazon.hidepnc')}}
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="row">
                    <div class="mb-1 col">{{__('amazon.colselect')}}</div>
                    <div class="mb-1 col"><a href="javascript:save_filters();" class="" title="">{{__('amazon.saveset')}}</a></div>
                </div>
                
                <div class="multiselect_div">
                    <select name="filter_status[]" id="filter_status" class="multiselect multiselect-custom" multiple="multiple" onchange="load_filters()">
                        @isset($data['filter'])
                        @foreach($data['filter'] as $key => $value)
                            @if($value=='lbh')
                            <option value="{{ $value }}">{{__('amazon.'.$value)}}</option>
                            @else
                            <option value="{{ $value }}">{{ $value }}</option>
                            @endif
                        @endforeach
                        @endisset
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card planned_task">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <input type="hidden" value="teachers" id="refname">
                                    <div id="filter">
                                        





                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/multi-select/css/multi-select.css') }}">
<style>
    .cursor-pointer{cursor:pointer;}
    .demo-card label{ display: block; position: relative;}
    .demo-card .col-lg-4{ margin-bottom: 30px;}
</style>


@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->

<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>


<script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script><!-- Input Mask Plugin Js -->
<script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>

<script src="{{ asset('assets/vendor/multi-select/js/jquery.multi-select.js?v=1.1') }}"></script><!-- Multi Select Plugin Js -->
<script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>






<script>
    var listing_status=0;
    $('#searchlist').multiselect({
        includeSelectAllOption: true,
    });

    function change_status(id, val)
    {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/admin/change_userstatus?id="+id+"&val="+val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            window.location.reload();
          })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function change_type(id, val)
    {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/admin/change_usertype?id="+id+"&val="+val,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
            window.location.reload();
          })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }


    function load_filters(Click=false,Status=0) {
        var filter_status = $('#filter_status').val();
        //var listing_status = $('#status').val();
        var filterOn = $('#filterOn').val();
        var filterCondition = $('#filterCondition').val();
        var filterValue = $('#filterValue').val();
        if(Click)
        {
            listing_status=Status;
            $(".ClickTabs").find(".body").removeClass("badge badge-info");
            if(listing_status==0)
            {
                $(".AllInventory").addClass("badge badge-info");
            }
            else if(listing_status==1)
            {
                $(".ActiveInventory").addClass("badge badge-info");
            }
            else if(listing_status==2)
            {
                $(".InactiveInventory").addClass("badge badge-info");
            }
        }
        $("#filter").html('');
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/amazon/load_filters?filter_status="+filter_status+ "&listing_status=" +listing_status+"&filterOn="+filterOn+"&filterCondition="+filterCondition+"&filterValue="+filterValue,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            $("#filter").html(response);
           // console.log(response);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function save_filters() {
        var filter_status = $('#filter_status').val();
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/amazon/savefilters?filter_status="+filter_status,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    function save_config(col) {
        if (col=='asin') {
            if ($('#h_asin').prop('checked')) {
                status = '1';
            } else {
                status = '0';
            }
        } else if (col=='product_name') {
            if ($('#h_prodname').prop('checked')) {
                status = '1';
            } else {
                status = '0';
            }
        }

        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/amazon/saveconfig?col="+col+ "&status=" +status,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            console.log(response);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }



    function loadcols() {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
            url: ENDPOINT + "/amazon/loadcols",
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
            $("#filter_status").val(response.split(','));
            $('#filter_status').multiselect({
                includeSelectAllOption: true,
            });

            //console.log(response);
            load_filters();

        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
    }

    $(document).ready(function() {
        loadcols();
    });
    
</script>


@stop
