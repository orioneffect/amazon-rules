@extends('layout.master')
@section('parentPageTitle', __('amazon.amazon'))
@section('title', __('amazon.inventoryh'))


@section('content')


    <div class="card mb-0"><div class="body">
        <form method="GET" action="{{ route('amazon.inventoryhistory') }}">
        <div class="row clearfix">
        <div class="col-auto pr-0">
            <div class="input-group">
                <input type="text" id="asin" name="asin" class="form-control" placeholder="{{__('amazon.asin')}}">
            </div>
        </div>
        <div class="col-auto pr-0">
            <select class="form-control" id="duration" name="duration">
            <option selected="selected" value="0">{{__('dashboard.0')}}</option>
            <option value="1">{{__('dashboard.1')}}</option>
            <option value="3">{{__('dashboard.3')}}</option>
            <option value="7">{{__('dashboard.7')}}</option>
            <option value="14">{{__('dashboard.14')}}</option>
            <option value="30">{{__('dashboard.30')}}</option>
            <option value="60">{{__('dashboard.60')}}</option>
            <option value="90">{{__('dashboard.90')}}</option>
            <option value="180">{{__('dashboard.180')}}</option>
            <option value="365">{{__('dashboard.365')}}</option>
            <option value="36500">{{__('dashboard.alltime')}}</option>
            </select>
        </div>
        <div class="col-auto pr-0">
            <div><button type="submit" id="btn" class="btn btn-primary">{{__('amazon.search')}}</button></div>
        </div>
    </div>
    </form>
</div></div>

<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                <ul class="header-dropdown dropdown">

                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('amazon.action')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('amazon.anothaction')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('amazon.something')}}</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>{{__('amazon.asin')}}</th>
                                <th>{{__('amazon.nqty')}}</th>
                                <th>{{__('amazon.nprice')}}</th>
                                <th>{{__('amazon.changed')}}</th>
                                <th>{{__('amazon.changedat')}}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>{{__('amazon.asin')}}</th>
                                <th>{{__('amazon.nqty')}}</th>
                                <th>{{__('amazon.nprice')}}</th>
                                <th>{{__('amazon.changed')}}</th>
                                <th>{{__('amazon.changedat')}}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @isset($data['invent_history'])
                                @foreach ($data['invent_history'] as $invent_history)
                                    <tr>
                                        <td><div class="text-white pl-1 pr-1">{{$invent_history->asin}}</div></td>
                                        <td><div class="text-white pl-1 pr-1 {{ $invent_history->type == '2' ? 'border border-warning' : '' }}">{{$invent_history->new_qty}}</div></td>
                                        <td><div class="text-white pl-1 pr-1 {{ $invent_history->type == '1' ? 'border border-warning' : '' }}">{{$invent_history->new_price}}</div></td>
                                        <td><div class="text-white pl-1 pr-1">
                                            @if ($invent_history->status == '1')
                                            {{ $invent_history->type == '1' ? 'Price updated from '.$invent_history->old_price.'  to  '.$invent_history->new_price : 'Quantity updated from '.$invent_history->old_qty.'  to  '.$invent_history->new_qty }}
                                            @elseif ($invent_history->status == '0')
                                            Added
                                            @else
                                            Deleted
                                            @endif
                                        </div></td>
                                        <td><div class="text-white pl-1 pr-1">{{App\Models\CommonFunction::time_elapsed_string($invent_history->changed_at, false)}}</div></td>
                                    </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

<link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}">

@stop

@section('page-script')
<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/chartist.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('assets/js/index2.js') }}"></script>

<script>

    $(document).ready(function() {
        $("#duration").val({{$data['duration']}});
        $("#asin").val('{{$data['asin']}}');
    });


</script>
@stop
