@extends('layout.master')
@section('parentPageTitle', __('amazon.amazon'))
@section('title', __('amazon.orders'))

@section('content')

    @include('amazon.orderdetail')


    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card planned_task">
                <div class="header">

                    <ul class="header-dropdown dropdown">
                        <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    </ul>
                </div>
                <div class="body">

                    <div class="col row align-items-end">


                        <div>

                        </div>
                    </div>

                    <div class="row align-items-end">
                        <div class="col-lg-4 col-md-3 col-sm-12">
                            <div class="input-group">
                                <input type="text" id="search_orderid" name="search_orderid" class="form-control"
                                    placeholder="{{ __('amazon.orderid') }}">
                            </div>
                        </div>


                        <div class="col-auto pr-0">
                            <select class="form-control" id="duration" name="duration">
                                <option selected="selected" value="0">{{ __('dashboard.0') }}</option>
                                <option value="1">{{ __('dashboard.1') }}</option>
                                <option value="3">{{ __('dashboard.3') }}</option>
                                <option value="7">{{ __('dashboard.7') }}</option>
                                <option value="14">{{ __('dashboard.14') }}</option>
                                <option value="30">{{ __('dashboard.30') }}</option>
                                <option value="60">{{ __('dashboard.60') }}</option>
                                <option value="90">{{ __('dashboard.90') }}</option>
                                <option value="180">{{ __('dashboard.180') }}</option>
                                <option value="365">{{ __('dashboard.365') }}</option>
                                <option value="36500">{{ __('dashboard.alltime') }}</option>
                            </select>
                        </div>



                        <div class="col-lg-2 col-md-12 col-sm-12">
                            <a href="javascript:load_orderlist();" class="btn btn-sm btn-primary btn-block" title=""><i
                                    class="fa fa-search"></i> {{ __('dropshipping.search') }}</a>
                        </div>

                    </div>




                </div>

                <hr>








                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-custom spacing8">
                                        <thead>
                                            <tr>
                                                <th>
                                                    {{ __('amazon.productinfo') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.orders') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.status') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.salesL') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.cost') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.profit') }}
                                                </th>
                                                
                                                <th>
                                                    {{ __('amazon.proper') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.purdate') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.orstatus') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.lastupdate') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.ornow') }}
                                                </th>
                                            </tr>
                                        </thead>

                                        <tfoot>
                                            <tr>
                                            <th>
                                                    {{ __('amazon.productinfo') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.orders') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.status') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.salesL') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.cost') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.profit') }}
                                                </th>
                                                
                                                <th>
                                                    {{ __('amazon.proper') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.purdate') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.orstatus') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.lastupdate') }}
                                                </th>
                                                <th>
                                                    {{ __('amazon.ornow') }}
                                                </th>
                                            </tr>
                                        </tfoot>


                                        <tbody id="data-wrapper">
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th colspan="5" class="auto-load text-center displaynone">
                                                    <div>
                                                        <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            height="60" viewBox="0 0 100 100"
                                                            enable-background="new 0 0 0 0" xml:space="preserve">
                                                            <path fill="#000"
                                                                d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                                                <animateTransform attributeName="transform"
                                                                    attributeType="XML" type="rotate" dur="1s"
                                                                    from="0 50 50" to="360 50 50"
                                                                    repeatCount="indefinite" />
                                                            </path>
                                                        </svg>
                                                    </div>
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                                <script>
                                    function openorder(orderid, status, shippeddate, amount) {
                                        //$("#orderid").html(orderid);
                                        $(".page-loader-wrapper").show();

                                        var orderidtext = "{{ __('amazon.orderid') }}";


                                        var ENDPOINT = "{{ url('/') }}";

                                        $.ajax({
                                                //                    url: ENDPOINT + "/amazon/vieworder/"+orderid,
                                                url: ENDPOINT + "/amazon/new_vieworder?orderid=" + orderid,
                                                datatype: "json",
                                                type: "get",
                                                beforeSend: function() {
                                                    //$('.auto-load').show();
                                                }
                                            })
                                            .done(function(obj) {
                                                //console.log(response)
                                                //var obj = JSON.parse(response);
                                                console.log(obj);


                                                $("#orderid").html(orderidtext + ' ' + orderid);
                                                $("#order_id").val(orderid);
                                                $("#deliverydate").html(obj.deliverydate);
                                                $("#orderstatus").html(status);
                                                $("#orderprice").html(amount);
                                                $("#purcahsedate").html(shippeddate);


                                                $("#buyername").html(obj.buyername);
                                                $("#buyeremail").html(obj.buyeremail);
                                                $("#address1").html(obj.address1);
                                                $("#address2").html(obj.address2);
                                                $("#municipality").html(obj.municipality);
                                                $("#city").html(obj.city);
                                                $("#postal").html(obj.postal);
                                                //                    $("#postal").html(obj.postalcode);
                                                $("#country").html(obj.country);


                                                $("#itemid").html("Item Order ID " + obj.item_id);
                                                //                    $("#itemid").html("Item Order ID " + obj.OrderItemId);
                                                $("#sku").html(obj.sku);
                                                //                    $("#sku").html(obj.SellerSKU);
                                                $("#producttitle").html(obj.producttitle);
                                                //                    $("#producttitle").html(obj.Title);
                                                $("#asin").html(obj.asin);
                                                $("#condition").html(obj.condition);
                                                //                    $("#condition").html(obj.ConditionId);
                                                $("#qty").html(obj.quantity);
                                                //                    $("#qty").html(obj.qty);
                                                $("#productImage").attr('src', obj.productimage);
                                                //                    $("#productImage").attr('src',obj.productURL);
                                                $(".page-loader-wrapper").hide();
                                                $("#megamenu").toggleClass("open");
                                                add_loadmsg(2);

                                            })
                                            .fail(function(jqXHR, ajaxOptions, thrownError) {
                                                console.log('Server error occured');
                                            });
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            @stop


            @section('page-styles')
            @stop

            @section('page-script')
                <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
                <script>
                    function load_orderlist() {
                        var orderid = $('#search_orderid').val();
                        var duration = $('#duration').val();
                        $("#data-wrapper").html('');
                        var ENDPOINT = "{{ url('/') }}";
                        $.ajax({
                                url: ENDPOINT + "/amazon/new_ordersPage?orderid=" + orderid + "&duration=" + duration,
                                datatype: "json",
                                type: "get",
                            })
                            .done(function(response) {
                                $("#data-wrapper").html(response);
                            })
                            .fail(function(jqXHR, ajaxOptions, thrownError) {
                                console.log('Server error occured');
                            });
                    }
                    $(document).ready(function() {
                        load_orderlist()
                    });
                </script>
            @stop
