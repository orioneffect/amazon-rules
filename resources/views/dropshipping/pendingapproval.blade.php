@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.pending_approval'))


@section('content')
    <div class="row clearfix">
        <div class="card">
            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-12">
                                <div class="input-group">
                                    <div class="col-lg-9 col-md-9 col-sm-8">
                                        <input type="text" id="searchval" class="form-control"
                                            placeholder="{{ __('dropshipping.asin') }}, {{ __('dropshipping.productname') }}">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i
                                                class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6">
                                <a href="#advanced_search" id="advancedsearchclose" role="button" aria-expanded="false"
                                    aria-controls="advanced_search" class="dropdown-toggle" data-toggle="collapse"
                                    title="">{{ __('dropshipping.advancedsearch') }}</a>
                            </div>
                            <!--<div class="col-lg-2 col-md-2 col-sm-6">
                                    <a href="javascript:search();" class="form-control btn btn-sm btn-info btn-block dropdown-toggle" title=""  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ __('dropshipping.selecteditems') }}<span class="badge badge-light">4</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);">{{ __('dropshipping.selecteditems') }}<span class="badge badge-light">4</span></a></li>
                                        <li><a href="javascript:void(0);">{{ __('dropshipping.allsearchresults') }}<span class="badge badge-light">10</span></a></li>
                                    </ul>
                                </div>-->
                            <div class="col-lg-2 col-md-2 col-sm-6">
                                <a href="javascript:search();"
                                    class="form-control btn btn-sm btn-primary btn-block dropdown-toggle" title=""
                                    data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">{{ __('dropshipping.chooseaction') }}</a>
                                <ul class="dropdown-menu">
                                    <li><a
                                            href="javascript:product_approve_reject(1,0);">{{ __('dropshipping.approve') }}</a>
                                    </li>
                                    <li><a
                                            href="javascript:product_approve_reject(2,0);">{{ __('dropshipping.remove') }}</a>
                                    </li>
                                    <li><a href="javascript:addtorestricated();">{{ __('dropshipping.add_to_restrictedproducts') }}Add
                                            to Restricted Products</a></li>
                                </ul>
                            </div>
                            <!--<div class="col-lg-2 col-md-2 col-sm-6">
                                    <div class="input-group">
                                        <select class="form-control" id="searchstore">
                                            <option value="">--{{ __('dropshipping.allstore') }}--</option>
                                            @isset($maindata['storesNav'])
                                                        @foreach ($maindata['storesNav'] as $store)
                                                            <option value="{{ $store->id }}">{{ $store->store_name }}</option>
                                                        @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>-->
                            <div class="col-lg-2 col-md-2 col-sm-6">
                                <div class="input-group">
                                    <select class="form-control" id="searchid_succ" onchange="listsearchid()">
                                        <option value="">--Search Id--</option>
                                        @isset($data['searchid_succ'])
                                            @foreach ($data['searchid_succ'] as $searchid_succ)
                                                <option value="{{ $searchid_succ->id }}"
                                                    {{ $data['searchid'] == $searchid_succ->id ? 'selected' : '' }}>
                                                    ({{ $searchid_succ->id }}) {{ $searchid_succ->created_at }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            @isset($data['country'])



            <div class="col-12 collapse align-items-center" id="advanced_search">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 border-right">
                                <input type="hidden" id="search_id" name="search_id" value="{{ $data['searchid'] }}"">
                                    <div><label class="  switch"><input
                                    onclick="linkoptions(this.checked,'products_without_prime_sellers_at_destination_store')"
                                    type="checkbox" id="products_with_prime_sellers_at_destination_store" value="1"><span
                                    class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_prime_sellers_at_destination_store', ['country' => $data['country']->dest_country]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'products_with_prime_sellers_at_destination_store')"
                                        type="checkbox" id="products_without_prime_sellers_at_destination_store"
                                        value="1"><span class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_without_prime_sellers_at_destination_store', ['country' => $data['country']->dest_country]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'products_without_sales_rank_in_destination_marketplace')"
                                        type="checkbox" id="products_with_sales_rank_in_destination_marketplace"
                                        value="1"><span class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_sales_rank_in_destination_marketplace', ['country' => $data['country']->dest_country]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'products_with_sales_rank_in_destination_marketplace')"
                                        type="checkbox" id="products_without_sales_rank_in_destination_marketplace"
                                        value="1"><span class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_without_sales_rank_in_destination_marketplace', ['country' => $data['country']->dest_country]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'filter_products_that_i_can_be_the_lowest')"
                                        type="checkbox" id="filter_products_that_i_cant_be_the_lowest" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.filter_products_that_i_cant_be_the_lowest') }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'filter_products_that_i_cant_be_the_lowest')"
                                        type="checkbox" id="filter_products_that_i_can_be_the_lowest" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.filter_products_that_i_can_be_the_lowest') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="filter_rejectedbyamazon_products_as_much_as_possible" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.filter_rejectedbyamazon_products_as_much_as_possible') }}</span>
                            </div>

                            <div class="mt-5"><label class="switch"><input
                                        onclick="linkoptions(this.checked,'products_without_sales_rank_in_us_marketplace')"
                                        type="checkbox" id="products_with_sales_rank_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_sales_rank_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'products_with_sales_rank_in_us_marketplace')"
                                        type="checkbox" id="products_without_sales_rank_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_without_sales_rank_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'only_non_sold_by_amazon_product_in_us_marketplace')"
                                        type="checkbox" id="only_sold_by_amazon_products_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_sold_by_amazon_products_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'only_sold_by_amazon_products_in_us_marketplace')"
                                        type="checkbox" id="only_non_sold_by_amazon_product_in_us_marketplace"
                                        value="1"><span class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_non_sold_by_amazon_product_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="shipping_price_higher_than_product_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.shipping_price_higher_than_product_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'non_fba_amazon_products_in_us_marketplace')"
                                        type="checkbox" id="fba_amazon_products_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.fba_amazon_products_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoptions(this.checked,'fba_amazon_products_in_us_marketplace')"
                                        type="checkbox" id="non_fba_amazon_products_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.non_fba_amazon_products_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_unavailable_products_in_us_marketplace" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_unavailable_products_in_us_marketplace', ['country' => $data['source_country']->country_name]) }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="products_with_the_same_brand_and_seller_name_on_amazoncom" value=""><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_the_same_brand_and_seller_name_on_amazoncom', ['host' => $data['source_site']]) }}</span>
                            </div>

                            <div class="mt-5"><label class="switch"><input
                                        onclick="linkoption2(this.checked,'products_with_not_trademark_registration_only')"
                                        type="checkbox" id="products_with_trademark_registration_only" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_trademark_registration_only') }}</span>
                            </div>
                            <div id="withtrade" class="displaynone">
                                <div class="ml-5"><i class="fa fa-chevron-circle-right"></i><label
                                        class="switch"><input type="checkbox"
                                            id="check_from_the_us_trademark_office_uspto" value="1"><span
                                            class="slider round"></span></label><span
                                        class="ml-2">{{ __('dropshipping.check_from_the_us_trademark_office_uspto') }}</span>
                                </div>
                                <div class="ml-5"><i class="fa fa-chevron-circle-right"></i><label
                                        class="switch"><input type="checkbox"
                                            id="check_from_the_canadian_trademark_office_cipo" value="1"><span
                                            class="slider round"></span></label><span
                                        class="ml-2">{{ __('dropshipping.check_from_the_canadian_trademark_office_cipo') }}</span>
                                </div>
                            </div>
                            <div><label class="switch"><input
                                        onclick="linkoption2(this.checked,'products_with_trademark_registration_only')"
                                        type="checkbox" id="products_with_not_trademark_registration_only" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_not_trademark_registration_only') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox" id="discounted_products_only"
                                        value="1"><span class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.discounted_products_only') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="products_with_unclear_shipping_information_only" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_unclear_shipping_information_only') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="products_with_unclear_import_fee_only" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.products_with_unclear_import_fee_only') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_products_whose_price_appears_in_the_cart" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_products_whose_price_appears_in_the_cart') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_products_with_uncertain_prices" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_products_with_uncertain_prices') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_products_whoes_asin_is_different_than_it_looks" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_products_whoes_asin_is_different_than_it_looks') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_products_whoes_specify_asin_based_profit_margin" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_products_whoes_specify_asin_based_profit_margin') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_products_compatible_with_sellerrunning_house" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_products_compatible_with_sellerrunning_house') }}</span>
                            </div>
                            <div><label class="switch"><input type="checkbox"
                                        id="only_products_not_compatible_with_sellerrunning_house" value="1"><span
                                        class="slider round"></span></label><span
                                    class="ml-2">{{ __('dropshipping.only_products_not_compatible_with_sellerrunning_house') }}</span>
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="row mt-2">
                                <div class="col-6">{{ __('dropshipping.status') }}</div>
                                <div class="col-6">
                                    <div class="input-group"><select class="form-control show-tick" required
                                            name="adv_status" id="adv_status">
                                            <option value="ALL">{{ __('dropshipping.pendingapproval') }}</option>
                                        </select></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.total_cost') }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="total_cost_min"
                                        class="form-control" placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-usd"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="total_cost_max"
                                        class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-usd"></i></span></div>
                                </div>
                            </div>
                            <hr />
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.categories') }}</div>
                                <div class="col-6">
                                    <div class="input-group">
                                        <select class="form-control show-tick" required name="categories" id="categories">
                                            <option value="">{{ __('dropshipping.selectfromlist') }}</option>
                                            @isset($data['category'])
                                                @foreach ($data['category'] as $list)
                                                    <option value="{{ $list->rank1brand }}">{{ $list->rank1brand }}
                                                    </option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.sub_category') }}</div>
                                <div class="col-6"><input type="text" id="sub_category" class="form-control"
                                        placeholder="{{ __('dropshipping.sub_category') }}"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.brand') }}</div>
                                <div class="col-6"><input type="text" id="brand"
                                        class="form-control text-success" placeholder="{{ __('dropshipping.brand') }}">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">
                                    {{ __('dropshipping.amazon_com_price', ['host' => $data['source_site']]) }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text"
                                        id="amazon_com_price_min" class="form-control"
                                        placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text"
                                        id="amazon_com_price_max" class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.shipping_price') }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="shipping_price_min"
                                        class="form-control" placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="shipping_price_max"
                                        class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">
                                    {{ __('dropshipping.amazon_com_quantity', ['host' => $data['source_site']]) }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text"
                                        id="amazon_com_quantity_min" class="form-control"
                                        placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text"
                                        id="amazon_com_quantity_max" class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.sales_rank') }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="sales_rank_com_min"
                                        class="form-control" placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="sales_rank_com_max"
                                        class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.review') }}</div>
                                <!--<div class="col-2"><div class="input-group"><select class="form-control show-tick" required name="review" id="review"><option value="">{{ __('dropshipping.select') }}</option></select></div></div>-->
                                <div class="col-6">
                                    <div class="input-group"><select class="form-control show-tick" required
                                            name="review_star" id="review_star">
                                            <option value="">{{ __('dropshipping.selectrating') }}</option>
                                            <option value="1" class="checked"><span>&starf;</option>
                                            <option value="2" class="checked"><span>&starf;&starf;</option>
                                            <option value="3" class="checked"><span>&starf;&starf;&starf;</option>
                                            <option value="4" class="checked"><span>&starf;&starf;&starf;&starf;
                                            </option>
                                            <option value="5" class="checked">
                                                <span>&starf;&starf;&starf;&starf;&starf;
                                            </option>
                                        </select></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.review_count') }}</div>
                                <!--<div class="col-2"><div class="input-group"><select class="form-control show-tick" required name="review_count" id="review_count"><option value="">{{ __('dropshipping.select') }}</option></select></div></div>-->
                                <div class="col-6"><input type="text" id="review_count_no" name="review_count_no"
                                        class="form-control" placeholder="{{ __('dropshipping.review_count') }}">
                                </div>
                            </div>
                            <hr />
                            <div class="row mt-2">
                                <div class="col-6">{{ __('dropshipping.sr_house_cost') }}</div>
                                <div class="col-3  input-group input-group-merge"><input type="text" id="sr_house_cost_min"
                                        class="form-control" placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-usd"></i></span></div>
                                </div>
                                <div class="col-3  input-group input-group-merge"><input type="text" id="sr_house_cost_max"
                                        class="form-control  form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-usd"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.sr_house_lbs') }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="sr_house_lbs_min"
                                        class="form-control" placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="sr_house_lbs_max"
                                        class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <hr />
                            <div class="row mt-2">
                                <div class="col-6">{{ __('dropshipping.sales_rank') }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text"
                                        id="sales_rank_destination_min" class="form-control"
                                        placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text"
                                        id="sales_rank_destination_max" class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">{{ __('dropshipping.offer_count') }}</div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="offer_count_min"
                                        class="form-control" placeholder="{{ __('dropshipping.min') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                                <div class="col-3 input-group input-group-merge"><input type="text" id="offer_count_max"
                                        class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-tag"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 text-success">{{ __('dropshipping.price_difference_with_lowest') }}
                                </div>
                                <div class="col-6 input-group input-group-merge"><input type="text"
                                        id="price_difference_with_lowest_max" class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-percent"></i></span></div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    {{ __('dropshipping.price_difference_between_me_and_the_average') }}</div>
                                <div class="col-6 input-group input-group-merge"><input type="text"
                                        id="price_difference_between_me_and_the_average_max"
                                        class="form-control form-control-appended"
                                        placeholder="{{ __('dropshipping.max') }}">
                                    <div class="input-group-append"><span class="input-group-text"><i
                                                class="fa fa-percent"></i></span></div>
                                </div>
                            </div>


                            <div class="row mt-2">
                                <div class="col-lg-2 col-md-6 col-sm-6">
                                    <a href="#advanced_search" role="button" aria-expanded="false"
                                        aria-controls="advanced_search" class="btn btn-link" data-toggle="collapse"
                                        title=""> {{ __('dropshipping.cancel') }}</a>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <a href="javascript:addToSearch();"
                                        class="form-control btn btn-sm btn-primary btn-block"
                                        title="">{{ __('dropshipping.advancedsearch') }}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endisset

        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-hover table-custom spacing8">
                    <thead>
                        <tr>
                            <th>
                                <!--{{ __('dropshipping.select_all') }}-->
                                <br>
                                <label class="switch">
                                    <input type="checkbox" id="selectall" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </th>
                            <th>{{ __('dropshipping.sourcetarget') }} {{ __('dropshipping.store') }}</th>
                            <th></th>
                            <th class="w10">{{ __('dropshipping.productname') }}</th>
                            <th>{{ __('dropshipping.asin') }}</th>
                            <th>{{ __('dropshipping.dimension') }}<br>{{ __('dropshipping.weight') }}</th>
                            <th>{{ __('dropshipping.minprice') }}</th>
                            <th>{{ __('dropshipping.price') }}</th>
                            <th>{{ __('dropshipping.maxprice') }}</th>
                            <th><i class="fa fa-calendar"></i> {{ __('dropshipping.created') }}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="data-wrapper">
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="7" class="auto-load text-center displaynone">
                                <div>
                                    <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="60"
                                        viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                        <path fill="#000"
                                            d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate"
                                                dur="1s" from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                        </path>
                                    </svg>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="7" id="stopload" display="none">
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    </div>


    <script>
        function addtorestricated() {
            swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm == true) {

                    var selectedItems = "";
                    var items = document.getElementsByName("chkbox");
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].type == "checkbox" && items[i].checked == true) selectedItems += items[i]
                            .value + ",";
                    }
                    var ENDPOINT = "{{ url('/') }}";
                    $.ajax({
                            url: ENDPOINT + "/dropshipping/addtorestricated?id=" + selectedItems,
                            datatype: "json",
                            type: "get",
                            beforeSend: function() {
                                $('.auto-load').show();
                            }
                        })
                        .done(function(response) {
                            console.log(response);
                            swal("Updated to restricated!", "", "success");
                            search();
                        })
                        .fail(function(jqXHR, ajaxOptions, thrownError) {
                            console.log('Server error occured');
                        });
                } else {
                    swal.close()
                }
            });
        }

        function product_approve_reject(status, id) {

            swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm == true) {

                    var items = document.getElementsByName("chkbox");
                    var selectedItems = "";
                    if (id > 0) {
                        selectedItems = id;
                    } else {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].type == "checkbox" && items[i].checked == true) selectedItems += items[i]
                                .value + ",";
                        }
                    }

                    var ENDPOINT = "{{ url('/') }}";
                    $.ajax({
                            url: ENDPOINT + "/dropshipping/updateStatus?id=" + selectedItems + "&status=" +
                                status,
                            datatype: "json",
                            type: "get",
                            beforeSend: function() {
                                $('.auto-load').show();
                            }
                        })
                        .done(function(response) {
                            console.log(response);
                            swal("Updated!", "", "success");
                            search();
                        })
                        .fail(function(jqXHR, ajaxOptions, thrownError) {
                            console.log('Server error occured');
                        });
                } else {
                    swal.close()
                }
            });
        }
        var loaded = 0;
        var ENDPOINT = "{{ url('/') }}";

        function listsearchid() {
            var id = $("#searchid_succ").val();
            var ENDPOINT = "{{ url('/') }}";
            location.href = ENDPOINT + "/dropshipping/pendingapproval?id=" + id;

        }

        function search() {
            loaded = 0;
            $("#data-wrapper").html('');
            $('#nextload').show();
            $('#stopload').hide();
            infinteLoadMore();
        }

        function infinteLoadMore() {
            var pooltbl = "user_products";
            var searchval = $("#searchval").val();
            var exclude = 0;

            if ($("#exclude").is(':checked')) {
                exclude = 1;
            }

            var searchid = "{{ $data['searchid'] }}";
            var searchstore = $("#searchstore").val();
            var searchid_succ = $("#searchid_succ").val();

            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/dropshipping/listpendingapp?searchval=" + searchval + "&searchid=" + searchid,
                    datatype: "html",
                    type: "get",
                    beforeSend: function() {
                        $('.auto-load').show();
                    }
                })
                .done(function(response) {
                    $('.auto-load').hide();
                    $("#data-wrapper").append(response);
                    if(response == '') {
                        $('#stopload').show();
                        $('#stopload').html("{{__('dropshipping.no_more_data')}}");
                        $('#nextload').hide();
                    }

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function addToSearch() {
            console.log("inside");
            $("#advancedsearchclose").click();
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + '/dropshipping/advancedsearch',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    products_with_prime_sellers_at_destination_store: $(
                        "#products_with_prime_sellers_at_destination_store").prop('checked'),
                    products_without_prime_sellers_at_destination_store: $(
                        "#products_without_prime_sellers_at_destination_store").prop('checked'),
                    products_with_sales_rank_in_destination_marketplace: $(
                        "#products_with_sales_rank_in_destination_marketplace").prop('checked'),
                    products_without_sales_rank_in_destination_marketplace: $(
                        "#products_without_sales_rank_in_destination_marketplace").prop('checked'),
                    filter_products_that_i_cant_be_the_lowest: $("#filter_products_that_i_cant_be_the_lowest")
                        .prop('checked'),
                    filter_products_that_i_can_be_the_lowest: $("#filter_products_that_i_can_be_the_lowest")
                        .prop('checked'),
                    filter_rejectedbyamazon_products_as_much_as_possible: $(
                        "#filter_rejectedbyamazon_products_as_much_as_possible").prop('checked'),
                    products_with_sales_rank_in_us_marketplace: $("#products_with_sales_rank_in_us_marketplace")
                        .prop('checked'),
                    products_without_sales_rank_in_us_marketplace: $(
                        "#products_without_sales_rank_in_us_marketplace").prop('checked'),
                    only_sold_by_amazon_products_in_us_marketplace: $(
                        "#only_sold_by_amazon_products_in_us_marketplace").prop('checked'),
                    only_non_sold_by_amazon_product_in_us_marketplace: $(
                        "#only_non_sold_by_amazon_product_in_us_marketplace").prop('checked'),
                    shipping_price_higher_than_product_in_us_marketplace: $(
                        "#shipping_price_higher_than_product_in_us_marketplace").prop('checked'),
                    fba_amazon_products_in_us_marketplace: $("#fba_amazon_products_in_us_marketplace").prop(
                        'checked'),
                    non_fba_amazon_products_in_us_marketplace: $("#non_fba_amazon_products_in_us_marketplace")
                        .prop('checked'),
                    only_unavailable_products_in_us_marketplace: $(
                        "#only_unavailable_products_in_us_marketplace").prop('checked'),
                    products_with_the_same_brand_and_seller_name_on_amazoncom: $(
                        "#products_with_the_same_brand_and_seller_name_on_amazoncom").prop('checked'),
                    products_with_trademark_registration_only: $("#products_with_trademark_registration_only")
                        .prop('checked'),
                    withtrade: $("#withtrade").prop('checked'),
                    check_from_the_us_trademark_office_uspto: $("#check_from_the_us_trademark_office_uspto")
                        .prop('checked'),
                    check_from_the_canadian_trademark_office_cipo: $(
                        "#check_from_the_canadian_trademark_office_cipo").prop('checked'),
                    products_with_not_trademark_registration_only: $(
                        "#products_with_not_trademark_registration_only").prop('checked'),
                    discounted_products_only: $("#discounted_products_only").prop('checked'),
                    products_with_unclear_shipping_information_only: $(
                        "#products_with_unclear_shipping_information_only").prop('checked'),
                    products_with_unclear_import_fee_only: $("#products_with_unclear_import_fee_only").prop(
                        'checked'),
                    only_products_whose_price_appears_in_the_cart: $(
                        "#only_products_whose_price_appears_in_the_cart").prop('checked'),
                    only_products_with_uncertain_prices: $("#only_products_with_uncertain_prices").prop(
                        'checked'),
                    only_products_whoes_asin_is_different_than_it_looks: $(
                        "#only_products_whoes_asin_is_different_than_it_looks").prop('checked'),
                    only_products_whoes_specify_asin_based_profit_margin: $(
                        "#only_products_whoes_specify_asin_based_profit_margin").prop('checked'),
                    only_products_compatible_with_sellerrunning_house: $(
                        "#only_products_compatible_with_sellerrunning_house").prop('checked'),
                    only_products_not_compatible_with_sellerrunning_house: $(
                        "#only_products_not_compatible_with_sellerrunning_house").prop('checked'),
                    adv_status: $("#adv_status").val(),
                    total_cost_min: $("#total_cost_min").val(),
                    total_cost_max: $("#total_cost_max").val(),
                    categories: $("#categories").val(),
                    sub_category: $("#sub_category").val(),
                    brand: $("#brand").val(),
                    amazon_com_price_min: $("#amazon_com_price_min").val(),
                    amazon_com_price_max: $("#amazon_com_price_max").val(),
                    shipping_price_min: $("#shipping_price_min").val(),
                    shipping_price_max: $("#shipping_price_max").val(),
                    amazon_com_quantity_min: $("#amazon_com_quantity_min").val(),
                    amazon_com_quantity_max: $("#amazon_com_quantity_max").val(),
                    sales_rank_com_min: $("#sales_rank_com_min").val(),
                    sales_rank_com_max: $("#sales_rank_com_max").val(),
                    review: $("#review").val(),
                    review_star: $("#review_star").val(),
                    review_count: $("#review_count").val(),
                    review_count_no: $("#review_count_no").val(),
                    sr_house_cost_min: $("#sr_house_cost_min").val(),
                    sr_house_cost_max: $("#sr_house_cost_max").val(),
                    sr_house_lbs_min: $("#sr_house_lbs_min").val(),
                    sr_house_lbs_max: $("#sr_house_lbs_max").val(),
                    sales_rank_destination_min: $("#sales_rank_destination_min").val(),
                    sales_rank_destination_max: $("#sales_rank_destination_max").val(),
                    offer_count_min: $("#offer_count_min").val(),
                    offer_count_max: $("#offer_count_max").val(),
                    price_difference_with_lowest_max: $("#price_difference_with_lowest_max").val(),
                    price_difference_between_me_and_the_average_max: $(
                        "#price_difference_between_me_and_the_average_max").val(),
                    search_id: $("#search_id").val()
                },
                //dataType: 'JSON',
                success: function(data) {
                    if (data !== 'undefined') {
                        $("#data-wrapper").html("{{ __('dropshipping.no_more_data') }}");
                    }
                    $("#data-wrapper").html(data);
                    $('.auto-load').hide();


                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $("#data-wrapper").html("{{ __('dropshipping.no_more_data') }}");
                    console.log("Status: " + textStatus);
                    console.log("Error: " + errorThrown);
                }

            });
        }

        function open_adv_search() {
            $('#advanced_search').show();
        }

        function adv_search() {
            $('#advanced_search').hide();
        }

        function linkoptions(isChecked, nextOptionId) {
            if (isChecked == true) {
                $('#' + nextOptionId).prop("checked", false);
            }
        }

        function linkoption2(isChecked, nextOptionId) {
            if (isChecked == true) {
                $('#' + nextOptionId).prop("checked", false);
            }
            if (nextOptionId == "products_with_not_trademark_registration_only") {
                $('#withtrade').show();
            }
            if (nextOptionId == "products_with_trademark_registration_only") {
                $('#check_from_the_us_trademark_office_uspto').prop("checked", false);
                $('#check_from_the_canadian_trademark_office_cipo').prop("checked", false);
                $('#withtrade').hide();
            }
            if ($('#products_with_trademark_registration_only').prop('checked') == false) {
                $('#check_from_the_us_trademark_office_uspto').prop("checked", false);
                $('#check_from_the_canadian_trademark_office_cipo').prop("checked", false);
                $('#withtrade').hide();
            }
        }
    </script>
@stop

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
    <style>
        .stars-outer {
            position: relative;
            display: inline-block;
        }

        .stars-inner {
            position: absolute;
            top: 0;
            left: 0;
            white-space: nowrap;
            overflow: hidden;
            width: 0;
        }

        .stars-outer::before {
            content: "\2605 \2605 \2605 \2605 \2605";
            font-size: 15px;
            font-weight: 900;
            color: #ccc;
        }

        .stars-inner::before {
            content: "\2605 \2605 \2605 \2605 \2605";
            font-size: 15px;
            font-weight: 900;
            color: #f8ce0b;
        }

        .checked {
            color: orange;
        }

    </style>
@stop

@section('page-script')
    <script src="{{ asset('js/dropshipping/dropshipping.js?v=1.2') }}"></script>
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('#btn').on("click", function() {
            $("#successmsg").hide();
            var brand = $("#brand").val();
            var brand = brand.replace(/(\r\n|\n|\r)/gm, "|"); //remove those line breaks

            $.ajax({
                /* the route pointing to the post function */
                url: ENDPOINT + '/dropshipping/addnew',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {
                    _token: "{{ csrf_token() }}",
                    brand: brand,
                    country: $("#country").val(),
                    trademark: $("#trademark").val(),
                    category: 1,
                    brandproduct: 1,
                    type: ''
                },
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function(data) {
                    if (data > 0) {
                        $("#successmsg").show();
                        $("#brand").val('');
                        search();
                    } else {
                        alert('Error');
                    }
                }
            });
        });

        $(document).ready(function() {
            infinteLoadMore();
        });
    </script>
@stop
