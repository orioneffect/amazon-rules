@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.restricted_keywords'))

@section('content')
<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#list"><i class="fa fa-list"></i> {{__('dropshipping.restricted_keywords')}}</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addnew"><i class="fa fa-plus-square"></i> {{__('dropshipping.add_restricted_keywords')}}</a></li>
        </ul>
        <div class="tab-content mt-0">
            <div class="tab-pane show active" id="list">

            <div class="col-12">
                <div class="alert alert-info alert-dismissible" role="alert" >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-info-circle"></i> {{__('dropshipping.restricted_keywords_description')}}
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-5 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('dropshipping.keywords')}}">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i class="fa fa-search"></i>  {{__('dropshipping.search')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-custom spacing8">
                        <thead>
                            <tr>
                                <th>{{__('dropshipping.keywords')}}</th>
                                <th><i class="fa fa-calendar"></i> {{__('dropshipping.created')}}</th>
                                <th>{{__('dropshipping.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody id="data-wrapper">
                        </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3"  class="auto-load text-center displaynone">
                                <div>
                                    <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px" y="0px" height="60" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                        <path fill="#000"
                                            d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s"
                                                from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                        </path>
                                    </svg>
                                </div>
                            </th>
                        </tr>
                        <tr >
                            <td colspan="3" id="nextload">
                                <button type="button"  href="#a" class="btn btn-primary btn-lg btn-block mb-3" onclick="infinteLoadMore()">{{ __('masterlang.showmore') }}</button>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="3" id="stopload" display="none">
                            </td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="addnew">
            <div class="col-12 displaynone" id="successmsg"  >
                <div class="alert alert-success" role="alert" >
                    <i class="fa fa-check-circle"></i> <span id="insertedcount"></span> 
                </div>
                <div class="alert alert-warning displaynone" role="alert" id="duplicatecountdisplay">
                    <i class="fa fa-warning"></i> <span id="duplicatecount"></span>
                </div>
            </div>
            <form id="frm">
                @csrf
                <div class="body mt-2">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('dropshipping.keywords')}}</label>
                            <div class="input-group demo-tagsinput-area">
                                <input required type="text" class="form-control" data-role="tagsinput" id="keywords" value="" placeholder="{{__('dropshipping.enter_keywords')}}">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i> {{__('dropshipping.create_restricted_keywords')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<style>
    .bootstrap-tagsinput {
        background-color:rgb(204, 204, 204) !important;
    }
</style>
@stop
@include('dropshipping.dropshippingscripts')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('js/dropshipping/dropshipping.js?v=1.1') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script>
    var loaded=0;
    var ENDPOINT = "{{ url('/') }}";

    function search() {
        loaded=0;
        $("#data-wrapper").html('');
        $('#nextload').show();
        $('#stopload').hide();
        infinteLoadMore();
    }

    function delkey(id) {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Confirm',
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true,
        }, function(isConfirm) {
            if (isConfirm) {
                var URL= "deletekeyword?id="+id
                deleteitem(URL)        
            }
        });


    }

    function infinteLoadMore() {
        var searchby="keywords";
        var searchval=$("#searchval").val();
        var ENDPOINT = "{{ url('/') }}";

        var URL=ENDPOINT + "/dropshipping/listsearch5?loaded="+loaded+"&searchby="+searchby+"&searchval="+searchval

        loadmore(URL,"{{__('dropshipping.no_more_data')}}")
    }
    

    $('#frm').submit(function (e) {

        e.preventDefault();
        var $form = $(this);
      
        // check if the input is valid using a 'valid' property
        if (!$form.valid) {
            console.log("form invalid")
        }  
        $("#successmsg").hide();
        $("#duplicatecountdisplay").hide();
        var keywords=$("#keywords").val();
        //var brand = brand.replace(/(\r\n|\n|\r)/gm, "|");//remove those line breaks

        $.ajax({
            /* the route pointing to the post function */
            url: ENDPOINT +'/dropshipping/restrictedkeywords',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: "{{ csrf_token() }}", keywords:keywords},
            dataType: 'JSON',
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
                data=JSON.parse(data); 
                if(data.status=="success") {
                    showresults(data);
                    $("#keywords").tagsinput('removeAll');
                    search();
                } else {
                    alert('Error');
                }
            }
        });
    });

    $(document).ready(function() {
        infinteLoadMore();
    });
</script>
@stop

