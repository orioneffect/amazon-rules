@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.productsearch'))


@section('content')
<div class="themesetting p-0" >
    <div class="card">
        <div class="accordion  mb-0" id="accordion">
            <div>
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne-demo" aria-expanded="true" aria-controls="collapseOne">
                        {{__('dropshipping.filterbybrandname')}}
                        </button>
                    </h5>
                </div>
                <div id="collapseOne-demo" class="collapse show  mb-0" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="body mb-0" id="filterbybrandname" >
                    </div>
                </div>
            </div>
            <div>
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo-demo" aria-expanded="false" aria-controls="collapseTwo">
                            {{__('dropshipping.filterbycategory')}}
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo-demo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="body" id="filterbycategory">
                    </div>
                </div>
            </div>
            <div>
                <a href="javascript:search();" class="btn btn-sm btn-success btn-block" title="">Apply</a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-Users-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h4" id="mySmallModalLabel">Confirm your selection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col-6 border-right pb-4 pt-4">
                        <label class="mb-0">Results</label>
                        <h4 class="font-30 font-weight-bold text-col-blue" id="totalresults">0</h4>
                    </div>
                    <div class="col-6 pb-4 pt-4">
                        <label class="mb-0">Pull Count</label>
                        <textarea id="numberofasin" style="font-size: 30px;width:100px;height:50px">

                        </textarea>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <label class="d-block">Keywords used</label>
                        <div class="">
                            <div class="text text-danger" id="keywords"></div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                            <div class="text text-info"><i class="fa fa-info-circle"></i> {{__('dropshipping.asin_will_be_filtered')}}</div>
                    </div>
                </div>
                <div>
                    <button type="button" class="btn btn-round btn-success">{{__('dropshipping.i_confirm_to_push')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="card">
        <div class="tab-content mt-0">
            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-5 col-sm-8">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('dropshipping.productsearch')}}">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-4">

                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i class="fa fa-search"></i> {{__('dropshipping.search')}}</a>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <a href="javascript:pushintoinventory();"  class="btn btn-sm btn-success btn-block" title=""><i class="fa fa-plus-circle"></i>  Push into inventory</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div id="data-record-count">

            </div>
            <div id="data-wrapper">

            </div>

            <div class="row">
                <div class="col-12 text text-center auto-load displaynone">
                        <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            x="0px" y="0px" height="60" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                            <path fill="#000"
                                d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s"
                                    from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                            </path>
                        </svg>
                </div>
                <div class="col-12 displaynone" id="nextload">
                    <button type="button"  href="#a" class="btn btn-primary btn-lg btn-block mb-3" onclick="infinteLoadMore()">{{ __('masterlang.showmore') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var nexttoken="";
    var ENDPOINT = "{{ url('/') }}";

    var output=`{"numberOfResults":2233912,"pagination":{"nextToken":"9HkIVcuuPmX_bm51o3-igBfN45pxW4Ru7ElIM6GCECYCuXJKzT26f0OdkF58sytB3jXk5bfp2ohwt_JvE7qiRlzsqvMR1-B5pD52ZbLDvkAzwZwgw1f_Uxc0rJXCGzfGUtFqnY9jL76rTiOXptKFCR1l1vj7KQoFIa7pWiOPHyaYWP7sBE9Fg7cGN2wE0an5ePw96h6ZL7m6olRxFOcqTWNanEVRjipq"},"refinements":{"brands":[{"numberOfResults":74329,"brandName":"Generic"},{"numberOfResults":22255,"brandName":"SHOUJIKE"},{"numberOfResults":17371,"brandName":"my-handy-design"},{"numberOfResults":12798,"brandName":"niBBuns"},{"numberOfResults":12612,"brandName":"Unknown"},{"numberOfResults":12195,"brandName":"Mobile phone housing"},{"numberOfResults":12157,"brandName":"Tek Styz"},{"numberOfResults":11791,"brandName":"GULTMEE"},{"numberOfResults":10806,"brandName":"TikTok CustomSHOp"},{"numberOfResults":10447,"brandName":"Cody Winn Elicia"}],"classifications":[{"numberOfResults":1887826,"displayName":"Cell Phones & Accessories","classificationId":"2335753011"},{"numberOfResults":1538739,"displayName":"Electronics","classificationId":"493964"},{"numberOfResults":99224,"displayName":"Clothing, Shoes & Jewelry","classificationId":"7141124011"},{"numberOfResults":77141,"displayName":"Sports & Outdoors","classificationId":"3375301"},{"numberOfResults":67399,"displayName":"Automotive","classificationId":"15690151"},{"numberOfResults":66912,"displayName":"Office Products","classificationId":"1084128"},{"numberOfResults":21234,"displayName":"Everything Else","classificationId":"10304191"},{"numberOfResults":17354,"displayName":"Home & Kitchen","classificationId":"1063498"},{"numberOfResults":11967,"displayName":"Industrial & Scientific","classificationId":"16310161"},{"numberOfResults":10329,"displayName":"Tools & Home Improvement","classificationId":"468240"}]},"items":[{"asin":"B08LNSY7GM","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/51dzXeAehvL.jpg","height":448,"width":500}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"CELLULAR_PHONE_CASE"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":1228},{"title":"Cell Phone Bumpers","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/17875442011","value":2},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":1230},{"title":"Cell Phone Bumpers","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/17875442011","value":2}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"LUPA Legacy","itemName":"LUPA iPhone 11 Wallet Cases","manufacturer":"LUPA"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B07WNNGXR6","B07WRJ9GTN","B0916HB3G7","B07WWTSDWS","B07Y8YMZ1K","B07Y8XT13K","B07Y8YCZZR","B07WNN5H1R","B083FPLD1C","B07WNNY15S"],"variationType":"PARENT"}]},{"asin":"B08CNGX32Y","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41KWETOYMzL.jpg","height":500,"width":496}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"KEYBOARDS"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"KLNANA","itemName":"Bluetooth Keyboard and Mouse Combo,Wireless Keyboard and Mouse for iPad pro\/iPad Air\/iPad\/iPad Mini, iPhone (iPadOS 13 \/ iOS 13 and Above),","manufacturer":"KLAMG"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08CNFZHVV","B08G4VG81M","B08G4VJGGW","B08G4XMXW7","B08CNF8M9X","B08CMW4MSB","B08G4ZMYQ1"],"variationType":"PARENT"}]},{"asin":"B095YFWCWD","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/51PrGWkjlyS.jpg","height":500,"width":500}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"CELLULAR_PHONE_CASE"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":104319},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":48661},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":104236},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":48617}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"13peas","itemName":"13peas for iPhone 12 Pro Max Case\uff0cLuxury Apple Case with Glossy Glitter Vacuum Plating Glass\uff0cElectroplating Gold Side Hybrid Protection Fashion Marble Cover for Women Girls Men","manufacturer":"13peas"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B095YDRMN8","B095YF18Q8","B095YBXHC8","B095YDGMHQ","B095YG9C2P","B095YF8199","B095YGK53K","B095YFZ7B2","B095YGF2SP","B095YGFC5L","B095YD5T4V","B095YG5QVX"],"variationType":"PARENT"}]},{"asin":"B08X6C21N2","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41jhFK6fCGS.jpg","height":500,"width":500}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"CELLULAR_PHONE_CASE"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":215904},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":101177},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":212733},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":100043}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"13peas","itemName":"13peas for iPhone 12 Case\uff0cSilicone Cases for Women Girly Men Clear Heart Pattern Apple Flexible Protective Cover Shockproof Bumper","manufacturer":"13peas"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08X6RF3Y1","B095YBL962","B08X6JV8H2","B08X6XJGLR","B095Y8KBSS","B08X6Z948C","B095Y7SLGX","B08X75Q9VW","B08X77HYFN","B0983NNQF8","B095Y8B217"],"variationType":"PARENT"}]},{"asin":"B08HKGQP47","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41rx4EwGyKL.jpg","height":500,"width":259}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"WIRELESS_ACCESSORY"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":1445929},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":707869},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":1171254},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":591790}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"Friday The 13th","itemName":"Friday the 13th Axe Poster Case","manufacturer":"Friday the 13th"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08NJX8X5R","B08NJWHJJV","B08NJVBNJ3","B08HKFZLNF","B08HKF1LT5","B08HKG3PSY","B08HKF1NC8","B08HKH2GCK","B08HKGQP44","B08HHG1DVF","B08HHFZTNZ"],"variationType":"PARENT"}]},{"asin":"B08HKDTJ3Y","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41KVS4I+d+L.jpg","height":500,"width":259}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"WIRELESS_ACCESSORY"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":303291},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":140187},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":292116},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":136233}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"Friday The 13th","itemName":"Friday the 13th Jason Cabin Case","manufacturer":"Friday the 13th"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08NDYSVK7","B08NDZ3DR2","B08NDY3F28","B08HKCML53","B08HHFLP3G","B08HHFM9CC","B08HKDT862","B08HKCLVCK","B08HHFNJ4H","B08HKBR72F","B08HKC9PQ7"],"variationType":"PARENT"}]},{"asin":"B093KM6NZ6","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41bnnRMyi9S.jpg","height":500,"width":500}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"CELLULAR_PHONE_CASE"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":215766},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":101114},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":212615},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":99992}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"dobo13","itemName":"dobo13 Case Compatible with iPhone 12","manufacturer":"dobo13"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B093KPQYVG","B093K8MZBF","B093KXP5Z5"],"variationType":"PARENT"}]},{"asin":"B08HHG1DWG","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41tLnvAR2CL.jpg","height":500,"width":259}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"WIRELESS_ACCESSORY"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":543002},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":249040},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":493901},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":231176}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"Friday The 13th","itemName":"Friday the 13th Retro Game Case","manufacturer":"Friday the 13th"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08N6HGJRC","B08N6G42BK","B08N6JCDWR","B08HKFPXFQ","B08HKH754T","B08HKFZLNY","B08HHFZTPT","B08HKFZLNW","B08HHG13J4","B08HKG3PTK","B08HKDW5GW"],"variationType":"PARENT"}]},{"asin":"B08X6L4SNF","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41jGvEUiSpS.jpg","height":500,"width":500}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"CELLULAR_PHONE_CASE"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":57279},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":25897},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":57123},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":25824}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"13peas","itemName":"13peas for iPhone 12 Pro Max Case\uff0cSilicone Cases for Women Girly Men Clear Heart Pattern Apple Flexible Protective Cover Shockproof Bumper","manufacturer":"13peas"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08X6J9SK5","B095XYR7TM","B08X7H1835","B08X6R7LPZ","B095XYHW32","B08X6TXNDY","B095XZQH57","B08X6YZCQB","B08X6M342H","B0983KYQ5P","B095Y17KZS"],"variationType":"PARENT"}]},{"asin":"B08X6GXWQ9","identifiers":[{"marketplaceId":"ATVPDKIKX0DER","identifiers":[]}],"images":[{"marketplaceId":"ATVPDKIKX0DER","images":[{"variant":"MAIN","link":"https:\/\/m.media-amazon.com\/images\/I\/41jhFK6fCGS.jpg","height":500,"width":500}]}],"productTypes":[{"marketplaceId":"ATVPDKIKX0DER","productType":"CELLULAR_PHONE_CASE"}],"ranks":[{"marketplaceId":"ATVPDKIKX0DER","ranks":[{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":125235},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":58993},{"title":"Cell Phones & Accessories","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless","value":125053},{"title":"Cell Phone Basic Cases","link":"http:\/\/www.amazon.com\/gp\/bestsellers\/wireless\/3081461011","value":58909}]}],"summaries":[{"marketplaceId":"ATVPDKIKX0DER","brandName":"13peas","itemName":"13peas for iPhone 12 Mini Case\uff0cSilicone Cases for Women Girly Men Clear Heart Pattern Apple Flexible Protective Cover Shockproof Bumper","manufacturer":"13peas"}],"variations":[{"marketplaceId":"ATVPDKIKX0DER","asins":["B08X73XZJN","B095Y8FX2P","B08X6RHSFD","B08X6Q8V1F","B095Y6YYC7","B08X65B41X","B095Y8Z8GW","B08X6T67N7","B08X71JWRJ","B0983N2DCZ","B095Y88XRQ"],"variationType":"PARENT"}]}]}`;


    function search() {
        $("#data-wrapper").html('');
        nexttoken="";
        infinteLoadMore();
    }

    function infinteLoadMore() {
        var searchval=$("#searchval").val();
        if(searchval=="") {
            $("#searchval").focus();
            return false;
        }

        var brandnames = [].filter.call(document.getElementsByName('brandnames[]'), (c) => c.checked).map(c => c.value);
        var catenames = [].filter.call(document.getElementsByName('catenames[]'), (c) => c.checked).map(c => c.value);

        console.log(brandnames);
        console.log(catenames);

        var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/dropshipping/productsearchApi",
                datatype: "html",
                data: {searchval:searchval,nexttoken:nexttoken,brandnames:brandnames,catenames:catenames,_token: "{{ csrf_token() }}"},
                type: "post",
                beforeSend: function () {
                    //$(".themesetting").toggleClass("open");
                    $('.auto-load').show();
                }
            })
            .done(function (response) {
                //response=JSON.parse(output);
                console.log(response)
                nexttoken = response.pagination.nextToken || '';
                if(nexttoken=='') {
                    $('#nextload').hide();
                } else {
                    $('#nextload').show();
                }

                var rownum=0;
                var str="";
                var brandfilter="";
                var catefilter="";

                for(var item of response.refinements.brands) {
                    brandfilter = brandfilter + `<div class="row clearfix mb-2">
                                <div class="col-12">
                                    <label class="fancy-checkbox margin-0">
                                        <input type="checkbox" name="brandnames[]" class="checkbox-tick" value="`+item.brandName+`" >
                                        <span><small>`+item.brandName+`</small></span>

                                    </label>
                                    <span class="float-right badge  badge-warning" id="filterbybrandnamecount">`+item.numberOfResults+`</span>
                                </div>
                        </div>`;
                }

                $("#filterbybrandname").html(brandfilter);
                for(var item of response.refinements.classifications) {
                    catefilter = catefilter + `<div class="row clearfix mb-2">
                            <div class="col-12">
                                <label class="fancy-checkbox margin-0">
                                    <input type="checkbox" name="catenames[]" class="checkbox-tick" value='`+item.classificationId+`'>
                                    <span><small>`+item.displayName+`</small></span>
                                </label>
                                <span class="float-right badge  badge-warning" id="filterbybrandnamecount">`+item.numberOfResults+`</span>
                            </div>
                        </div>`;

                }

                $("#filterbycategory").html(catefilter);
                $("#totalresults").html(response.numberOfResults);

                $("#data-record-count").html(`<div class="alert alert-info">Number of results :` + response.numberOfResults + `</div>`)
                str=`<div class="row clearfix">`;
                for(var item of response.items) {
                    manufacturer = item.summaries[0].manufacturer || '';
                    brandName = item.summaries[0].brandName || '';
                    var ranks = new Map();

                    for(var rank of item.ranks[0].ranks) {
                        if(ranks.has(rank.title)==false){
                            ranks.set(rank.title,rank.value);
                        }
                    }

                    if(item.ranks[0].ranks.length>0) {
                        rankstr=`<div class="col-sm-8 text" ><h6>Category</h6></div>
                                    <div class="col-sm-4 text" ><h6>Rank</h6></div>`;
                        ranks.forEach((v,k) => {
                        //for(var rank of item.ranks[0].ranks) {
                            rankstr=rankstr+`<div class="col-sm-12 text" >
                                <div class="row clearfix">
                                    <div class="col-sm-8 text" >
                                        `+k+`
                                        </div>
                                    <div class="col-sm-4 text" >
                                        `+v+`
                                        </div>
                                    </div>
                                </div>`;
                        });
                    } else {
                        rankstr="";
                    }
                    str=str+`
                        <div class="col-lg-3 col-md-6 col-sm-12 p-4 " id="`+rownum+`"><a href="#a">
                            <div class="row clearfix ">
                                <div class="col-sm-12 mb-2">
                                    <img src="`+item.images[0].images[0].link+`" alt="Avatar" class="rounded mr-2" style="width:280px;">
                                </div>
                                <div class="col-sm-12 text text-success">
                                    <h6>`+item.summaries[0].itemName+ `</h6>
                                </div>
                                <div class="col-sm-12 text" >
                                    Brand: `+ brandName + `
                                </div>
                                <div class="col-sm-12 text" >
                                    Manufacturer: `+manufacturer + `
                                </div>
                                <div class="col-sm-12">
                                    ASIN: `+item.asin + `
                                </div>
                                <div class="col-sm-12"><hr></div>
                                `+rankstr+`
                            </div></a>
                        </div>
                    `;
                    //console.log(item.asin);
                }
                //str=str+`</div>`;
                $("#data-wrapper").append(str);

                $('.auto-load').hide();
                $("#data-wrapper").append(response);

                $(".themesetting").toggleClass("open");
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
    }

    function pushintoinventory() {

        $("#keywords").html($("#searchval").val());
        $(".bd-Users-modal-sm").modal("show");
    }

</script>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop
