@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.add_new_product'))


@section('content')
<div class="row clearfix">
    <div class="card">
        <div class="col-12 displaynone" id="successmsg"  >
            <div class="alert alert-success" role="alert" >
                <i class="fa fa-check-circle"></i> {{__('dropshipping.success')}}
            </div>
        </div>

        <form method="POST" id="frm" action="#a" validate onsubmit="document.getElementById('btn').setAttribute('disabled','disabled');">
            @csrf
            <div class="body mt-2">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <h4>{{__('dropshipping.addproduct_bulk')}}</h4>
                        <ul>
                            <li>
                                {{__('dropshipping.addproduct_note1')}}
                            </li>
                            <li>
                                {{__('dropshipping.addproduct_note2')}}
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <label><b>{{__('dropshipping.asin')}}</b></label>
                        <div class="form-group">
                            <textarea rows="10"  class="form-control" placeholder="{{__('dropshipping.enter_asin')}}" id="asin" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <label><b>{{__('dropshipping.sku')}}</b></label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="sku" id="sku" placeholder="{{__('dropshipping.sku_note')}}">
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <button type="button" id="btn" class="btn btn-primary">{{__('dropshipping.create_products')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script>
var ENDPOINT = "{{ url('/') }}";

$('#btn').on("click",function() {
    $("#successmsg").hide();
    var asin=$("#asin").val();
    var asin = asin.replace(/(\r\n|\n|\r)/gm, "|");//remove those line breaks

    $.ajax({
        /* the route pointing to the post function */
        url: ENDPOINT +'/dropshipping/addnewproduct',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: "{{ csrf_token() }}", asin:asin, sku:$("#sku").val()},
        dataType: 'JSON',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) {
            if(data>0) {
                $("#successmsg").show();
                $("#asin").val('');
            } else {
                alert('Error');
            }
        }
    });
});

</script>
@stop
