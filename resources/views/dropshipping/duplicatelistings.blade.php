@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.duplicatelist'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2>{{__('dropshipping.duplicatelist')}}</h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">

            <div class="col row align-items-end">   <div>
                          
                          </div>
              </div>
            <div class="col"><h3>{{__('dropshipping.duplicatelist')}}: 
                  <a href="#" class="badge badge-danger"> <strong>4.792</strong></a></h3>                   
                  </div>  
                  <div class="row align-items-end">
  
              
                  <div class="col-lg-4 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('dropshipping.duplicatesearch')}}">
                                </div>
                            </div>
    <div class="col-auto">
      <div class="m-selection-list">
  <form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">
  
                      <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                      <option value="2" data-name="mexico" > Mexico</option>
                      <option value="3" data-name="uae"> U.A.E.</option>
  
                  <option value="-1" data-name="allcountries">All Countries</option>
              </select>
  </form>    </div>
 
  </div>
  
  <div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
                    <option value="2" data-name="immanuel" > immanuel</option>
                    <option value="3" data-name="Sirius">Sirius</option>
                    <option value="-1" data-name="allstores">All Stores</option>
            </select>
</form>    </div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i class="fa fa-search"></i> {{__('dropshipping.search')}}</a>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-danger btn-block" title=""><i class="fa fa-delete"></i> {{__('dropshipping.dupremove')}}</a>
                            </div>                                       
                        </div>
           
<hr>

<div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-custom spacing8">
                        <thead>
                            <tr>
                            <th>{{__('dropshipping.select_all')}}
                                <br>
                                <label class="switch">
                                    <input type="checkbox" id="selectall" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </th>
                                <th>{{__('dropshipping.asin')}}</th>
                                <th>{{__('dropshipping.productname')}}</th>
                                <th>{{__('dropshipping.duptrade')}}</th>
                                <th>{{__('dropshipping.dupsku')}}</th>
                                <th>{{__('dropshipping.source')}}</th>
                                <th>{{__('dropshipping.stock')}}</th>
                                <th>{{__('dropshipping.updated')}}</th>
                                <th><i class="fa fa-calendar"></i> {{__('dropshipping.created')}}</th>
                                <th>{{__('dropshipping.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody id="data-wrapper">
                        </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5"  class="auto-load text-center displaynone">
                                <div>
                                    <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px" y="0px" height="60" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                        <path fill="#000"
                                            d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s"
                                                from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                        </path>
                                    </svg>
                                </div>
                            </th>
                        </tr>
                        <tr >
                            <td colspan="5" id="nextload">
                                <button type="button"  href="#a" class="btn btn-primary btn-lg btn-block mb-3" onclick="infinteLoadMore()">{{ __('masterlang.showmore') }}</button>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="5" id="stopload" display="none">
                            </td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            </div>
        </div>








            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop