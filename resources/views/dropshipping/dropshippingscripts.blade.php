<script>
    function showresults(data) {

        $("#successmsg").show();
        successmsg="{{__('dropshipping.success')}}".replace(":count",data.records);
        $("#insertedcount").html(successmsg);

        if(data.duplicate>0) {                
            $("#duplicatecountdisplay").show();
            successmsg="{{__('dropshipping.duplicate')}}".replace(":count",data.duplicate);
            $("#duplicatecount").html(successmsg);
        } else {
            $("#duplicatecountdisplay").hide();
        }
    }
</script>