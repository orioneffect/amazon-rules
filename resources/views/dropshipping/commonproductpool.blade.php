@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.common_product_pool'))


@section('content')
<div class="row clearfix">
    <div class="card">
        <div class="col-12">
            <div class="alert alert-info alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-info-circle"></i> {{__('dropshipping.common_pool_product_description')}}
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="input-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('dropshipping.productname')}}">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
                                            <label class="switch">
                                                <input type="checkbox" id="exclude" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                            {{__('dropshipping.exclude_already_added')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12">
                            <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i class="fa fa-search"></i> {{__('dropshipping.search')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="alert alert-warning alert-dismissible" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-warning-circle"></i> {{__('dropshipping.common_pool_warning')}}
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-4 col-md-3 col-sm-12">
                            <div class="input-group">
                                <select class="form-control show-tick" required name="store" id="store">
                                    <option value="ALL">--{{__('dropshipping.allstore')}}--</option>
                                    @isset($maindata["storesNav"])
                                        @foreach ($maindata["storesNav"] as $store)
                                            <option value="{{$store->id}}">{{$store->store_name}}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12">
                            <a href="javascript:printChecked();" class="btn btn-sm btn-success btn-block" title=""><i class="fa fa-plus-circle"></i> {{__('dropshipping.add_to_brand_black_list')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-hover table-custom spacing8">
                    <thead>
                        <tr>
                            <th>{{__('dropshipping.select_all')}}
                                <br>
                                <label class="switch">
                                    <input type="checkbox" id="selectall" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </th>
                            <th>{{__('dropshipping.asin')}}</th>
                            <th>{{__('dropshipping.brandname')}}</th>
                            <th>{{__('dropshipping.trademark')}}</th>
                            <th>{{__('dropshipping.type')}}</th>
                            <th><i class="fa fa-calendar"></i> {{__('dropshipping.created')}}</th>
                        </tr>
                    </thead>
                    <tbody id="data-wrapper">
                    </tbody>
                <tfoot>
                    <tr>
                        <th colspan="6"  class="auto-load text-center displaynone">
                            <div>
                                <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    x="0px" y="0px" height="60" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                    <path fill="#000"
                                        d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s"
                                            from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                    </path>
                                </svg>
                            </div>
                        </th>
                    </tr>
                    <tr >
                        <td colspan="6" id="nextload">
                            <button type="button"  href="#a" class="btn btn-primary btn-lg btn-block mb-3" onclick="infinteLoadMore()">{{ __('masterlang.showmore') }}</button>
                        </td>
                    </tr>
                    <tr >
                        <td colspan="6" id="stopload" display="none">
                        </td>
                    </tr>
                </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function printChecked() {
        var store=$("#store").val();
        var items = document.getElementsByName("chkbox");
        var selectedItems = "";
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == "checkbox" && items[i].checked == true) selectedItems += items[i].value + ",";
        }
        var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/dropshipping/pooltoasin?id="+selectedItems+"&store="+store,
                datatype: "json",
                type: "get",
                beforeSend: function () {
                    $('.auto-load').show();
                }
            })
            .done(function (response) {

                console.log(response);
                search();

            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });

    }

    var loaded=0;
    var ENDPOINT = "{{ url('/') }}";

    function search() {
        loaded=0;
        $("#data-wrapper").html('');
        $('#nextload').show();
        $('#stopload').hide();
        infinteLoadMore();
    }

    function infinteLoadMore() {
        var searchby="asin";
        var pooltbl="common_pool_asins";
        var searchval=$("#searchval").val();
        var exclude=0;

        if($("#exclude").is(':checked')) {
            exclude=1;
        }

        var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                url: ENDPOINT + "/dropshipping/listsearchpool?pooltbl="+pooltbl+"&category=0&loaded="+loaded+"&searchby="+searchby+"&searchval="+searchval+"&exclude="+exclude,
                datatype: "html",
                type: "get",
                beforeSend: function () {
                    $('.auto-load').show();
                }
            })
            .done(function (response) {
                loaded = loaded  + 50;
                //console.log(response);
                $('.auto-load').hide();
                $("#data-wrapper").append(response);
                if(response == '') {
                    $('#stopload').show();
                    $('#stopload').html("{{__('dropshipping.no_more_data')}}");
                    $('#nextload').hide();
                }
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
    }
</script>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('js/dropshipping/dropshipping.js?v=1.2') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script>

$('#btn').on("click",function() {
    $("#successmsg").hide();
    var brand=$("#brand").val();
    var brand = brand.replace(/(\r\n|\n|\r)/gm, "|");//remove those line breaks

    $.ajax({
        /* the route pointing to the post function */
        url: ENDPOINT +'/dropshipping/addnew',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: "{{ csrf_token() }}", brand:brand, country:$("#country").val(), trademark:$("#trademark").val(),category:1,brandproduct:1,type:''},
        dataType: 'JSON',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) {
            if(data>0) {
                $("#successmsg").show();
                $("#brand").val('');
                search();
            } else {
                alert('Error');
            }
        }
    });
});

$(document).ready(function() {
    infinteLoadMore();
});
</script>
@stop
