@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.productsapproved'))

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card planned_task">
                <div class="header">
                    <ul class="header-dropdown dropdown">
                        <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    </ul>
                </div>
                <div class="body">
                    <form method="GET" action="{{ route('dropshipping.approvedproducts') }}">
                        @csrf
            
                    <div class="row align-items-end">
                        <div class="col-lg-4 col-md-3 col-sm-12">
                            <div class="input-group">
                                <input type="text" id="asin" name="asin" class="form-control"
                                    placeholder="{{ __('dropshipping.asin') }}">
                            </div>
                        </div>
                        <div class="col-auto pr-0">
                            <select class="form-control" id="duration" name="duration">
                                <option selected="selected" value="0">{{ __('dashboard.0') }}</option>
                                <option value="1">{{ __('dashboard.1') }}</option>
                                <option value="3">{{ __('dashboard.3') }}</option>
                                <option value="7">{{ __('dashboard.7') }}</option>
                                <option value="14">{{ __('dashboard.14') }}</option>
                                <option value="30">{{ __('dashboard.30') }}</option>
                                <option value="60">{{ __('dashboard.60') }}</option>
                                <option value="90">{{ __('dashboard.90') }}</option>
                                <option value="180">{{ __('dashboard.180') }}</option>
                                <option value="365">{{ __('dashboard.365') }}</option>
                                <option value="36500">{{ __('dashboard.alltime') }}</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-primary">{{ __('dropshipping.search') }}</button>
                        </div>
                    </div>
                    </form>

                </div>
                <hr>
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <input type="hidden" value="teachers" id="refname">
                                    <table class="table table-striped table-hover dataTable js-exportable ">
                                        <thead>
                                            <tr>
                                                <th>{{__('dropshipping.image')}}</th>
                                                <th>{{__('dropshipping.source')}}</th>
                                                <th>{{__('dropshipping.sku_caps')}}</th>
                                                <th>{{__('dropshipping.asin')}}</th>
                                                <th>{{__('dropshipping.productname')}}</th>
                                                <th>{{__('dropshipping.brand')}}</th>
                                                <th>{{__('dropshipping.category')}}</th>
                                                <th>{{__('dropshipping.minprice')}}</th>
                                                <th>{{__('dropshipping.price')}}</th>
                                                <th>{{__('dropshipping.maxprice')}}</th>
                                                <th>{{__('dropshipping.source_price')}}</th>
                                                <th>{{__('dropshipping.source_low')}}</th>
                                                <th>{{__('dropshipping.target_price')}}</th>
                                                <th>{{__('dropshipping.target_low')}}</th>                                                
                                                <th>{{__('dropshipping.amzfee')}}</th>
                                                <th>{{__('dropshipping.stock')}}</th>
                                                <th>{{__('dropshipping.status')}}</th>
                                                <th>{{__('dropshipping.datecreated')}}</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>{{__('dropshipping.image')}}</th>
                                                <th>{{__('dropshipping.source')}}</th>
                                                <th>{{__('dropshipping.sku_caps')}}</th>
                                                <th>{{__('dropshipping.asin')}}</th>
                                                <th>{{__('dropshipping.productname')}}</th>
                                                <th>{{__('dropshipping.brand')}}</th>
                                                <th>{{__('dropshipping.category')}}</th>
                                                <th>{{__('dropshipping.minprice')}}</th>
                                                <th>{{__('dropshipping.price')}}</th>
                                                <th>{{__('dropshipping.maxprice')}}</th>
                                                <th>{{__('dropshipping.source_price')}}</th>
                                                <th>{{__('dropshipping.source_low')}}</th>
                                                <th>{{__('dropshipping.target_price')}}</th>
                                                <th>{{__('dropshipping.target_low')}}</th>
                                                <th>{{__('dropshipping.amzfee')}}</th>
                                                <th>{{__('dropshipping.stock')}}</th>
                                                <th>{{__('dropshipping.status')}}</th>
                                                <th>{{__('dropshipping.datecreated')}}</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="con_student_tbl">
                                            @isset($data["products"])
                                            @foreach ($data["products"] as $products)
                                            @php
                                                if ($products->status == 0) {$status = 'In Queue';}
                                                else if ($products->status == 1) {$status = 'In Progress';}
                                                else if ($products->status == 2) {$status = 'In Progress';}
                                                else if ($products->status == 3 && $products->stock > 0) {$status = 'Active';}
                                                else if ($products->status == 3 && $products->stock == 0) {$status = 'Inactive';}
                                                $ratinground = $products->product_rating / 5 * 100;
                                                $rating = round($ratinground / 10) * 10;
                                    
                                            @endphp
                                            
                                                <tr class="font-weight-bold">
                                                    <td class="bg-light text-dark">
                                                        <div class="float-left"><img src="{{$products->product_imgurl}}" class="w100"></div>
                                                    </td>
                                                    <td class="green1 text-dark">{{$products->source}}</td>
                                                    <td class="green1 text-dark">{{$products->sku}}</td>
                                                    <td class="bg-light">{{$products->asin}}</td>
                                                    <td class="green1 text-dark text-wrap"><div class="txtover">{{$products->product_title}}</div><br>
                                                        
                                                        <div class="end"><div class="stars-outer"><div class="stars-inner" style="width:{{$rating}}%"></div></div></div>
                                                        </td>
                                                    <td class="bg-light text-dark">{{$products->brand_name}}</td>
                                                    <td class="green1 text-dark">{{$products->rank1brand}}</td>
                                                    <td class="green1"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->min_price}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->min_price_usd}}</div></div><div>&nbsp;</div><div>&nbsp;</div></td>
                                                    <td class="green1"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->price}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->price_usd}}</div></div><div>&nbsp;</div><div>&nbsp;</div></td>
                                                    <td class="green1"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->max_price}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->max_price_usd}}</div></div><div>&nbsp;</div><div>&nbsp;</div></td>
                                                    <td class="green2"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->product_cost}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->product_cost_usd}}</div></div><div>@if ($products->buy_box_seller_id != '')<u>{{ __('dropshipping.sellid') }}:</u> @endif &nbsp;</div><div class="text-dark">{{$products->buy_box_seller_id}}&nbsp;</div></td>
                                                    <td class="green2"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->lowest_product_cost}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->lowest_product_cost_usd}}</div></div><div>@if ($products->lowest_product_seller_id != '')<u>{{ __('dropshipping.sellid') }}:</u> @endif  &nbsp;</div><div class="text-dark">{{$products->lowest_product_seller_id}}&nbsp;</div></td>
                                                    <td class="green1"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->target_product_cost}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->target_product_cost_usd}}</div></div><div>@if ($products->target_buy_box_seller_id != '')<u>{{ __('dropshipping.sellid') }}:</u> @endif  &nbsp;</div><div class="text-dark">{{$products->target_buy_box_seller_id}}&nbsp;</div></td>
                                                    <td class="green1"><div class="row border-bottom"><div class="col-6">{{$products->currency_code}}</div><div class="col-6 text-dark">{{$products->target_lowest_product_cost}}</div></div><div class="row"><div class="col-6">USD</div><div class="col-6 text-dark">{{$products->target_lowest_product_cost_usd}}</div></div><div>@if ($products->target_lowest_product_seller_id != '')<u>{{ __('dropshipping.sellid') }}:</u> @endif  &nbsp;</div><div class="text-dark">{{$products->target_lowest_product_seller_id}}&nbsp;</div></td>
                                                    <td class="green1 text-dark">%&nbsp;{{$products->amazon_fee}}</td>
                                                    <td class="green1 fontgreen">{{$products->stock}}</td>
                                                    <td class="green3 text-dark">{{$status}}</td>
                                                    <td class="green1 text-dark">{{$products->created_at}}</td>
                                                </tr>
                                            @endforeach
                                            @endisset
                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>
<style>

    .stars-outer {
        position: relative;
        display: inline-block;
    }

    .stars-inner {
        position: absolute;
        top: 0;
        left: 0;
        white-space: nowrap;
        overflow: hidden;
        width: 0;
    }

    .stars-outer::before {
        content: "\2605 \2605 \2605 \2605 \2605";
        font-size: 15px;
        font-weight: 900;
        color: #ccc;
    }

    .stars-inner::before {
        content: "\2605 \2605 \2605 \2605 \2605";
        font-size: 15px;
        font-weight: 900;
        color: #3e0bf8;
    }

    .checked {
        color: orange;
    }

    .green1{background-color: rgb(170, 211, 207);}
    .green2{background-color: rgb(137, 216, 208);}
    .green3{background-color: rgb(78, 219, 205);}
    .fontgreen{color:rgb(10, 148, 134);}
    .row{flex-wrap: nowrap;}

    div.txtover {
        white-space: nowrap; 
        width: 200px; 
        overflow: hidden;
        text-overflow: ellipsis;
        border: 1px solid #000000;
      }
      
      div.txtover:hover {
        overflow: visible;
        white-space: normal; 
        width: 400px; 
      }    
</style>
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>
<script>
    $(document).ready(function() {
        $("#asin").val('{{$data['asiny']}}');
        $("#duration").val('{{$data['duration']}}');
    });
</script>

    
@stop
