@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.bestselling'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
               
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">

            <div class="col row align-items-end">
           
                
                        <div>
           
                    </div>
            </div>
           
            <div class="row align-items-end">

            <div class="col">
            <h4>{{__('dropshipping.bestsellings')}}</h4>
                </div>
<div class="col-auto pr-0">
<form action="/switch-daterange" id="SwitchDateRange" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="SC1P1ewPQgKuhLWK3WIgr9PSUsDw8gE4e1b4yw-xOFPCCECzaIW8HrZpw9YU5uWwnwPpXHsUvHDb0TFe6k2fs63oa3Qzb8q-MlgzdyEvtv7P92KQ_0LfKvhcPKgN4DVEBdOE3AzqV-_XEM0a_SD92g2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><select class="form-control" data-val="true" data-val-number="The field DefaultSelectedDateRange must be a number." data-val-required="The DefaultSelectedDateRange field is required." id="daterange-selection" name="DefaultSelectedDateRange"><option selected="selected" value="0">{{__('dashboard.0')}}</option>
<option value="1">{{__('dashboard.1')}}</option>
<option value="3">{{__('dashboard.3')}}</option>
<option value="7">{{__('dashboard.7')}}</option>
<option value="14">{{__('dashboard.14')}}</option>
<option value="30">{{__('dashboard.30')}}</option>
<option value="60">{{__('dashboard.60')}}</option>
<option value="90">{{__('dashboard.90')}}</option>
<option value="180">{{__('dashboard.180')}}</option>
<option value="365">{{__('dashboard.365')}}</option>
<option value="36500">{{__('dashboard.alltime')}}</option>
</select></form></div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                    <option value="2" data-name="mexico" > Mexico</option>
                    <option value="3" data-name="uae"> U.A.E.</option>

                <option value="-1" data-name="allcountries">All Countries</option>
            </select>
</form>    </div>
</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
                    <option value="2" data-name="immanuel" > immanuel</option>
                    <option value="3" data-name="Sirius">Sirius</option>
                    <option value="-1" data-name="allstores">All Stores</option>
            </select>
</form>    </div>
</div>
           </div>

<hr>



<div class="col-md-12">
        <div class="card">
            <div class="header">
                <h3></h3>
                <ul class="header-dropdown dropdown">
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <ul class="nav nav-tabs3">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#product-new2">{{__('dashboard.products')}}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#cat-new2">{{__('dashboard.cats')}}</a></li>
            </ul>
            <div class="tab-content mt-0">
                <div class="tab-pane active show" id="product-new2">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                    <th>{{__('dashboard.country')}}</th>        
                                    <th>{{__('dashboard.store')}}</th>
                                    <th>{{__('dashboard.image')}}</th>
                                    <th>{{__('dashboard.description')}}</th>
                                    <th>{{__('dashboard.asin')}}</th>
                                    <th>{{__('dashboard.category')}}</th>
                                    <th>{{__('dashboard.quantity')}}</th>
                                    <th>{{__('dashboard.estcost')}}</th>
                                    <th>{{__('dashboard.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dashboard.view')}}</th>
                        </thead>


                            <tfoot>
                            <th>{{__('dashboard.country')}}</th>        
                                    <th>{{__('dashboard.store')}}</th>
                                    <th>{{__('dashboard.image')}}</th>
                                    <th>{{__('dashboard.description')}}</th>
                                    <th>{{__('dashboard.asin')}}</th>
                                    <th>{{__('dashboard.category')}}</th>
                                    <th>{{__('dashboard.quantity')}}</th>
                                    <th>{{__('dashboard.estcost')}}</th>
                                    <th>{{__('dashboard.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dashboard.view')}}</th>
                            </tfoot>
                            <tbody>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td>  
                            <td><span>Lacin's Store</span></td>
                                <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>{{__('dashboard.view')}}</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Immanuel Store</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Neverland</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>

                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/nl.svg " class="w35 rounded-circle">
                            </td> <td><span>Yonja Street</span></td>
                                    <td class="w60"><img src="https://m.media-amazon.com/images/I/81y89-PxPUL._AC_UY679_.jpg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Virtual Reality Headset, OPTOSLON 3D VR...</p></td>
                                    <td><span>B00V9XLMV8</span></td>
                                    <td><span>Clothing, Shoes & Jewelry	</span></td>
                                    <td><span class="badge badge-success">25</span></td>
                                    <td><span class="badge badge-success">$125</span></td>
                                    <td class="w100 text-info"><strong>$35,59</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td><button type="button" class="btn btn-default btn-sm mb-0"><span>View</span></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="cat-new2">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <tbody>
                                <thead>
<th>{{__('dashboard.country')}}</th>
<th>{{__('dashboard.store')}}</th>
<th></th>
<th>{{__('dashboard.category')}}</th>
<th>{{__('dashboard.products')}}</th>
<th>{{__('dashboard.orquentity')}}</th>
<th>{{__('dashboard.badget')}}</th>
<th>{{__('dashboard.profit')}}</th>
<th>%</th>

                                </thead>
                                <tfoot>
<th>{{__('dashboard.country')}}</th>
<th>{{__('dashboard.store')}}</th>
<th></th>
<th>{{__('dashboard.category')}}</th>
<th>{{__('dashboard.products')}}</th>
<th>{{__('dashboard.orquentity')}}</th>
<th>{{__('dashboard.badget')}}</th>
<th>{{__('dashboard.profit')}}</th>
<th>%</th>

                                </tfoot>
                                <tr><td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Yonja Street</span></td>
                                    <td class="w60"><img src="../assets/images/coin/BTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="../assets/images/coin/ETH.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/us.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Immanuel</span></td>
                                    <td class="w60"><img src="../assets/images/coin/XRP.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/ca.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Lacin</span></td>
                                    <td class="w60"><img src="../assets/images/coin/qtum.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Angelo</span></td>
                                    <td class="w60"><img src="../assets/images/coin/BTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/gb.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Sirius</span></td>
                                    <td class="w60"><img src="../assets/images/coin/neo.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                                <tr>
                                <td class="w40">
                                <img src="../assets/images/flag/nl.svg " class="w35 rounded-circle">
                            </td> 
                            <td><span>Neverland</span></td>
                                    <td class="w60"><img src="../assets/images/coin/LTC.svg" alt="" class="w30 rounded"></td>
                                    <td><p class="mb-0">Home & Kitchen</p></td>
                                    <td><span>2758</span></td>
                                    <td><span>125</span></td>
                                    <td><span class="badge badge-success w100 text-info">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$425</strong></td>
                                    <td class="w100 text-info"><strong>$33</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>






            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>
<script src="{{ asset('assets/js/index2.js') }}"></script>

@stop