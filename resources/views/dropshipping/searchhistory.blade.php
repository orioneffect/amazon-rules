@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.searchhistory'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
            <ul class="header-dropdown dropdown">
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
            </div>


            <div class="body">
                    <div class="float-left">
                        <h4>{{__('dropshipping.searchhistory')}} <a href="#" class="badge badge-primary"><strong>{{ $data['search_count'] }}</strong></a></h4>
                    </div>

                    <div>
                        <form method="GET" action="{{ route('dropshipping.searchhistory') }}">
                            <div class="float-left pl-3 pr-3">
                            <select class="form-control" id="duration" name="duration">
                                <option value="0">{{__('dashboard.0')}}</option>
                                <option value="1">{{__('dashboard.1')}}</option>
                                <option value="3">{{__('dashboard.3')}}</option>
                                <option value="7">{{__('dashboard.7')}}</option>
                                <option value="14">{{__('dashboard.14')}}</option>
                                <option value="30">{{__('dashboard.30')}}</option>
                                <option value="60">{{__('dashboard.60')}}</option>
                                <option value="90">{{__('dashboard.90')}}</option>
                                <option value="180">{{__('dashboard.180')}}</option>
                                <option value="365">{{__('dashboard.365')}}</option>
                                <option value="36500">{{__('dashboard.alltime')}}</option>
                            </select>
                            </div>


                            <div>
                            <button type="submit" class="btn btn-primary">{{ __('dropshipping.search') }}</button>
                            </div>
                        </form>
                        </div>
                    </div>


<hr>
<div class="alert alert-warning alert-dismissible" role="alert">
<i class="fa fa-warning"></i> {{__('dropshipping.historydescription')}}
</div>
        <div class="row clearfix">
            @isset($data["search"])
            @foreach ($data["search"] as $list)


                <div class="col-lg-4 col-md-6">
                    <div class="card bg-green c_grid c_yellow">
                    <div class="card-header text-center">


                        <div class="float-left">
                            {{__('dropshipping.sname')}}: {{$list->name}}<input type="hidden" id="sname{{$list->id}}" value="{{$list->name}}">
                        </div>
                        <span id="addnewname" class="float-right">
                            &nbsp;
                            <a href="#" onclick="clone_search({{$list->id}})" data-toggle="tooltip" data-placement="top" data-original-title="Click to Clone" class="bg-dark text-white p-1"><i class="fa fa-files-o"></i></a>
                        </span>
                        @if ($list->name == null)
                            <span id="addnewname" class="float-right">
                                &nbsp;
                                <a href="#" onclick="updatesname({{$list->id}},{{$list->name}})"  data-toggle="tooltip" data-placement="top" data-original-title="Click to Add Search Name" class="bg-dark text-info p-1"><i class="fa fa-plus-square"></i></a>
                            </span>
                            @else
                            <span id="updatename" class="float-right">
                                &nbsp;
                                <a href="#" onclick="updatesname({{$list->id}},$('#sname{{$list->id}}').val())"  data-toggle="tooltip" data-placement="top" data-original-title="Click to Update Search Name" class="bg-dark text-info p-1"><i class="fa fa-pencil-square-o"></i></a>
                            </span>
                        @endif



                        <div class="modal fade" id="changename" tabindex="-1"
                            aria-labelledby="changenametitle" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="changenametitle">
                                            {{ __('dropshipping.addsname') }}</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group demo-masked-input">
                                            <div class="input-group mb-3">
                                                <input type="hidden" id="search_id">
                                                <input type="text" class="form-control"
                                                    id="search_name" oninput="$('#snameValidation').hide();"
                                                    placeholder="">
                                            </div>

                                        </div>
                                        <div class="alert alert-warning displaynone" id="snameValidation">
                                            {{ __('dropshipping.validsname') }}
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            onclick="$('#snameValidation').hide();"
                                            data-dismiss="modal">{{ __('settings.close') }}</button>
                                        <button type="button" class="btn btn-primary"
                                            onclick="savename($('#search_name').val(),$('#search_id').val())">{{ __('settings.save') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="float-left mt-1">
                            {{__('dropshipping.sdate')}}: {{$list->created_at}}
                        </div>

                        <div class="text text-white float-right  mt-1">
                            {{__('dropshipping.sid')}} : {{$list->id}}
                        </div>
                        <br>


                    </div>
                        <div class="body text-center p-4">
                            <div class="row">
                                <div class="circle text text-bold text-primary"><br>{{$list->source_flag}}</div>
                                <div class="circle">
                                    <img class="rounded-circle" src="../assets/images/flag/store/128/{{$list->target_flag}}" >
                                </div>
                        </div><br>
                        <div class="d-flex justify-content-between">
                                <div>
                                    <a href="{{$list->amazon_url}}" target="_blank">
                                    <i class="fa fa-paper-plane"></i> {{__('dropshipping.findnumber')}} <p>{{$list->pages}} pages</p>
                                    </a>
                                </div>
                                <div>
                                    <a href="../dropshipping/pendingapproval/?id={{$list->id}}">
                                    <i class="fa fa-filter"></i> {{__('dropshipping.foundnumber')}}<p>{{$list->product_count}} ASIN</p>
                                    </a>

                                </div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div><i class="fa fa-arrow-circle-o-up"></i> {{__('dropshipping.sdate')}} <p>{{$list->picked_at}}</div>
                                <div><i class="fa fa-arrow-circle-down"></i> {{__('dropshipping.enddate')}}<p>{{$list->target_end_time}}</div>
                            </div><!--
                            <h6 class="mt-3 mb-0">URL</h6>
                            <span><a class="p-3" target="_blank" href="">{{__('dropshipping.sourcelink')}}</a>-->
                            <hr>
                            @if ($list->status==2)
                                <div class="alert alert-success alert-dismissible" role="alert" id="success">
                                        <i class="fa fa-primary"></i>{{__('dropshipping.addedinventory')}}
                                </div>
                                @elseif($list->status==1)
                                    <div class="alert alert-primary alert-dismissible" role="alert" id="primary">
                                            <i class="fa fa-primary"></i>{{__('dropshipping.inprogressinventory')}}
                                    </div>
                                @elseif($list->status==3)
                                    <div class="alert alert-danger alert-dismissible" role="alert" id="danger">
                                            <i class="fa fa-danger"></i> {{__('dropshipping.notaddedinventory')}}
                                    </div>
                                @elseif($list->status==0)
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                            <i class="fa fa-danger"></i> {{__('dropshipping.pendinginventory')}}
                                    </div>
                            @endif

                        </div>
                    </div>
                </div>
                @endforeach
                @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script>


    function updatesname(id,name) {
        console.log(name);
        $('#changename').modal('toggle');
        $('#search_id').val(id);
        $('#search_name').val(name);
    }


    function savename(name,id) {
        //$('#addnewname').hide();
        //$('#updatename').show();
        $('#snameValidation').hide();
        if (name == '') {
            $('#snameValidation').show();
            $('#search_name').val('');
        } else if (name != '') {
            $('#phoneValidation').hide();
                var ENDPOINT = "{{ url('/') }}";
                $.ajax({
                        url: ENDPOINT + "/dropshipping/update_searchrequest?id=" + id + "&name=" + name,
                        datatype: "json",
                        type: "get",
                    })
                    .done(function(response) {
                        $('#changename').modal('toggle');
                        window.location.reload();
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError) {
                        console.log('Server error occured');
                    });
        }

    }

    var duration='{{$data["duration"]}}';
    $("#duration").val(duration);

    function clone_search(id) {
        var ENDPOINT = "{{ url('/') }}";
        $.ajax({
                url: ENDPOINT + "/dropshipping/clonesearch?id=" + id,
                datatype: "json",
                type: "get",
            })
            .done(function(response) {
                console.log(response);
                window.location.reload();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
    }

</script>


@stop
