@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.duplicatelist'))


@section('content')
<div class="row clearfix">
    <div class="card">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#list"><i class="fa fa-list"></i>  {{__('dropshipping.cateblacklist')}}</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#addnew"><i class="fa fa-plus-square"></i> {{__('dropshipping.add_cateblacklist')}}</a></li>
        </ul>
        <div class="tab-content mt-0">
            <div class="tab-pane show active" id="list">

            <div class="col-12">
                <div class="alert alert-info alert-dismissible" role="alert" >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-info-circle"></i> {{__('dropshipping.blacklist_description')}}
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-5 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('dropshipping.catename')}}">
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <select class="form-control" id="searchstore">
                                        <option value="0">--{{__('dropshipping.allstore')}}--</option>
                                        @isset($maindata["storesNav"])
                                            @foreach ($maindata["storesNav"] as $store)
                                                <option value="{{$store->id}}">{{$store->store_name}}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i class="fa fa-search"></i> {{__('dropshipping.search')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-custom spacing8">
                        <thead>
                            <tr>
                                <th>{{__('dropshipping.store')}}</th>
                                <th>{{__('dropshipping.catename')}}</th>
                                <th><i class="fa fa-calendar"></i> {{__('dropshipping.created')}}</th>
                                <th>{{__('dropshipping.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody id="data-wrapper">
                        </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5"  class="auto-load text-center displaynone">
                                <div>
                                    <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px" y="0px" height="60" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                        <path fill="#000"
                                            d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s"
                                                from="0 50 50" to="360 50 50" repeatCount="indefinite" />
                                        </path>
                                    </svg>
                                </div>
                            </th>
                        </tr>
                        <tr >
                            <td colspan="5" id="nextload">
                                &nbsp;
                            </td>
                        </tr>
                        <tr >
                            <td colspan="5" id="stopload" display="none">
                            </td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="addnew">
            <div class="col-12 displaynone" id="successmsg"  >
                <div class="alert alert-success" role="alert" >
                    <i class="fa fa-check-circle"></i> <span id="insertedcount"></span>
                </div>
                <div class="alert alert-warning displaynone" role="alert" id="duplicatecountdisplay">
                    <i class="fa fa-warning"></i> <span id="duplicatecount"></span>
                </div>
            </div>
            <form id="frm">
                @csrf
                <div class="body mt-2">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('dropshipping.selectcate')}}</label>
                            <div class="form-control overflow-auto pt-1 pb-2 pl-2" style="height: 200px">
                            @isset($data["category"])
                                @foreach ($data["category"] as $cate)




                                    <div>
                                        <label class="switch">
                                            <input onclick="addtocategory(this.checked, '{{$cate->rank1brand}}')" type="checkbox" id="" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                        <span class="ml-2">
                                            {{$cate->rank1brand}}
                                        </span>
                                    </div>




                                @endforeach
                            @endisset




                            </div>
                        </div>
                    </div>
                    <div class="row clearfix mt-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('dropshipping.catename')}}</label>
                            <div class="form-group">
                                <textarea required rows="10" class="form-control" placeholder="{{__('dropshipping.enter_catename')}}" id="cate"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label>{{__('dropshipping.store')}}</label>
                            <div class="form-group">
                                <select class="form-control show-tick" required name="store" id="store">
                                    <option value="0">ALL</option>
                                    @isset($maindata["storesNav"])
                                        @foreach ($maindata["storesNav"] as $store)
                                            <option value="{{$store->id}}">{{$store->store_name}}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="submit" id="btn" class="btn btn-primary"><i class="fa fa-plus-square"></i> {{__('dropshipping.create_blacklist')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
@stop
@include('dropshipping.dropshippingscripts')
@section('page-script')
<script src="{{ asset('js/dropshipping/dropshipping.js?v=1.2') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script>


    var loaded=0;
    var ENDPOINT = "{{ url('/') }}";

    function search() {
        loaded=0;
        $("#data-wrapper").html('');
        $('#nextload').show();
        $('#stopload').hide();
        infinteLoadMore();
    }

    function dellist(id) {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Confirm',
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true,
        }, function(isConfirm) {
            if (isConfirm) {
                var URL= "deleteitem?id="+id;
                deleteitem(URL);
            }
        });
    }    

    function infinteLoadMore() {
        var storeid=$("#searchstore").val();
        var searchval=$("#searchval").val();
        var ENDPOINT = "{{ url('/') }}";

        var URL=ENDPOINT + "/dropshipping/categorysearch?search="+searchval+"&storeid="+storeid

        loadmore(URL,"{{__('dropshipping.no_more_data')}}")
    }

    $('#frm').submit(function (e) {

        e.preventDefault();
        var $form = $(this);

        // check if the input is valid using a 'valid' property
        if (!$form.valid) {
            console.log("form invalid")
        }
        $("#successmsg").hide();
        $("#duplicatecountdisplay").hide();
        var cate=$("#cate").val();
        var cate = cate.replace(/(\r\n|\n|\r)/gm, "|");//remove those line breaks

        $.ajax({
            /* the route pointing to the post function */
            url: ENDPOINT +'/dropshipping/addcategory',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: "{{ csrf_token() }}", cate:cate, store:$("#store").val()},
            dataType: 'JSON',
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
                data=JSON.parse(data);
                if(data.status=="success") {
                    showresults(data);
                    $("#cate").val('');
                    search();
                } else {
                    alert('Error');
                }
            }
        });
    });

    function addtocategory(ischecked, txt) {
        var text;
        if (ischecked == true) {
            text = $("#cate").val() + txt + "\n";
            $("#cate").val(text);
        } else {
            text = $("#cate").val();
            text = text.replace(txt + "\n", "");
            $("#cate").val(text);
        }
    }


$(document).ready(function() {
    infinteLoadMore();
});
</script>
@stop
