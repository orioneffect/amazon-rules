@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.bulkupload'))


@section('content')
<div class="row clearfix">
    <div class="card">

    <div class="body">
<ul class="nav nav-tabs">
<li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Home-withicon"><i class="fa fa-list"></i> {{__('dropshipping.addnewbulk')}}</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-withicon"><i class="fa fa-plus-square"></i> {{__('dropshipping.bulklist')}}</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane show active" id="Home-withicon">
<div class="col-12 displaynone" id="successmsg"  >
            <div class="alert alert-success" role="alert" >
                <i class="fa fa-check-circle"></i> {{__('dropshipping.success')}}
            </div>
        </div>

        <form method="POST" id="frm" action="#a" validate onsubmit="document.getElementById('btn').setAttribute('disabled','disabled');">
            @csrf
            <div class="body mt-2">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <label><b>{{__('dropshipping.asin')}}</b></label>
                        <div class="form-group">
                            <input type="file" name="file" class="dropify" data-max-file-size="1028K" required data-allowed-file-extensions="txt" >
                            <p>{{__('dropshipping.bulkuploadescription')}}</p>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <label><b>{{__('dropshipping.country')}}</b></label>
                        <div class="form-group">
                            <select class="form-control show-tick" required name="store" id="store">
                                <option value="ALL">ALL</option>
                                <option value="ALL">USA</option>
                                <option value="ALL">Canada</option>
                                <option value="ALL">Turkey</option>
                                <option value="ALL">India</option>
                                <option value="ALL">Germany</option>

                               </select>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <label><b>{{__('dropshipping.store')}}</b></label>
                        <div class="form-group">
                            <select class="form-control show-tick" required name="store" id="store">
                                <option value="ALL">ALL</option>

                                @isset($maindata["storesNav"])
                                    @foreach ($maindata["storesNav"] as $store)
                                        <option value="{{$store->store_name}}">{{$store->store_name}}</option>
                                    @endforeach
                                @endisset

                            </select>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <button type="button" id="btn" class="btn btn-primary">{{__('dropshipping.upload_asin')}}</button>
                    </div>
                </div>
            </div>
        </form>


</div>
<div class="tab-pane" id="Profile-withicon">



<div class="col row align-items-end">
           
                
           <div>

       </div>
</div>

<div class="row align-items-end">

<div class="col">
<h4></h4>
   </div>
<div class="col-auto pr-0">

</div>

<div class="col-auto">
<div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

       <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
       <option value="2" data-name="mexico" > Mexico</option>
       <option value="3" data-name="uae"> U.A.E.</option>

   <option value="-1" data-name="allcountries">All Countries</option>
</select>
</form>    </div>
</div>

<div class="col-auto">
<div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

       <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
       <option value="2" data-name="immanuel" > immanuel</option>
       <option value="3" data-name="Sirius">Sirius</option>
       <option value="-1" data-name="allstores">All Stores</option>
</select>
</form>    </div>
</div>
</div>

<hr>
<div class="alert alert-warning alert-dismissible" role="alert">
<i class="fa fa-warning"></i> {{__('dropshipping.bulklistdescription')}}
</div>

<h5>{{__('settings.prule')}}</h5>
<table>
  <thead>
    <tr>
    <th style="width: 14%">{{__('settings.from')}}</th>
    <th style="width: 14%">{{__('settings.to')}}</th>
    <th style="width: 17%">{{__('settings.min')}}</th>
    <th style="width: 12%">{{__('settings.stan')}}</th>
    <th style="width: 12%">{{__('settings.max')}}</th>
    <th style="width: 12%">{{__('settings.onesel')}}</th>
    <th style="width: 12%">{{__('settings.cstock')}}</th>
    <th style="width: 12%">{{__('settings.pr')}}</th>
</tr>
</thead>
</table>
<div id="form-placeholder"></div>
<button id="btn-add" type="button" class="btn btn-primary">{{__('settings.add')}}</button>
<button id="btn-add" type="button" class="btn btn-success">{{__('settings.save')}}</button>
<hr/>


</div>
</div>
</div>












       
    </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
<script>
var ENDPOINT = "{{ url('/') }}";

$('#btn').on("click",function() {
    $("#successmsg").hide();
    var asin=$("#asin").val();
    var asin = asin.replace(/(\r\n|\n|\r)/gm, "|");//remove those line breaks

    $.ajax({
        /* the route pointing to the post function */
        url: ENDPOINT +'/dropshipping/addnewproduct',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: "{{ csrf_token() }}", asin:asin, sku:$("#sku").val()},
        dataType: 'JSON',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) {
            if(data>0) {
                $("#successmsg").show();
                $("#asin").val('');
            } else {
                alert('Error');
            }
        }
    });
});

</script>
@stop
