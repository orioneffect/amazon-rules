@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.repbuy'))


@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
                <h2></h2>
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <h4></h4>

<div class="col row align-items-end">
           
                
                        <div>
           
                    </div>
            </div>
           
            <div class="row align-items-end">

            <div class="col">
            <h4>{{__('dropshipping.repbuy')}} <a href="#" class="badge badge-primary"><strong>1358</strong></a></h4>
                </div>
<div class="col-auto pr-0"> </div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                    <option value="2" data-name="mexico" > Mexico</option>
                    <option value="3" data-name="uae"> U.A.E.</option>

                <option value="-1" data-name="allcountries">All Countries</option>
            </select>
</form>    </div>
</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
                    <option value="2" data-name="immanuel" > immanuel</option>
                    <option value="3" data-name="Sirius">Sirius</option>
                    <option value="-1" data-name="allstores">All Stores</option>
            </select>
</form>    </div>
</div>
           </div>

<hr>
<div class="alert alert-warning alert-dismissible" role="alert">
<i class="fa fa-warning"></i> {{__('dropshipping.buydescription')}}
</div>

<div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                    <th>{{__('dropshipping.country')}}</th>        
                                    <th>{{__('dropshipping.store')}}</th>
                                    <th>{{__('dropshipping.sellerid')}}</th>
                                    <th>{{__('dropshipping.productcount')}}</th>
                                    <th>{{__('dropshipping.orderquantity')}}</th>
                                    <th>{{__('dropshipping.total_cost')}}</th>
                                    <th>{{__('dropshipping.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dropshipping.view')}}</th>
                        </thead>


                            <tfoot>
                                    <th>{{__('dropshipping.country')}}</th>        
                                    <th>{{__('dropshipping.store')}}</th>
                                    <th>{{__('dropshipping.sellerid')}}</th>
                                    <th>{{__('dropshipping.productcount')}}</th>
                                    <th>{{__('dropshipping.orderquantity')}}</th>
                                    <th>{{__('dropshipping.total_cost')}}</th>
                                    <th>{{__('dropshipping.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dropshipping.view')}}</th>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/us.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Immanuel Store</span></td>
                                    <td><a href="https://www.amazon.ca/sp?seller=A3DWYIK6Y9EEQB">A3DWYIK6Y9EEQB</a></td>
                                    <td><span class="badge badge-success">925</span></td>
                                    <td><span class="badge badge-warning">116</span></td>
                                    <td><span class="badge badge-primary">$12.500</span></td>
                                    <td class="w100 text-info"><strong>$4.375</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonamazon')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewpr')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewsold')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Angelo</span></td>
                                    <td><a href="https://www.amazon.ca/sp?seller=A1EJ6HC61GBHKX">A1EJ6HC61GBHKX</a></td>
                                    <td><span class="badge badge-success">877</span></td>
                                    <td><span class="badge badge-warning">96</span></td>
                                    <td><span class="badge badge-primary">$11.500</span></td>
                                    <td class="w100 text-info"><strong>$4.075</strong></td>
                                    <td class="w100 text-info"><strong>%34</strong></td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonamazon')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewpr')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewsold')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Sirius Global</span></td>
                                    <td><a href="https://www.amazon.ca/sp?seller=A3HJH5NMZ4Q8OZ">A3HJH5NMZ4Q8OZ</a></td>
                                    <td><span class="badge badge-success">755</span></td>
                                    <td><span class="badge badge-warning">90</span></td>
                                    <td><span class="badge badge-primary">$10.500</span></td>
                                    <td class="w100 text-info"><strong>$3.775</strong></td>
                                    <td class="w100 text-info"><strong>%30</strong></td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonamazon')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewpr')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewsold')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/au.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Orion</span></td>
                                    <td><a href="https://www.amazon.ca/sp?seller=A39MSQO8X001IJ">A39MSQO8X001IJ</a></td>
                                    <td><span class="badge badge-success">692</span></td>
                                    <td><span class="badge badge-warning">85</span></td>
                                    <td><span class="badge badge-primary">$8.500</span></td>
                                    <td class="w100 text-info"><strong>$3.375</strong></td>
                                    <td class="w100 text-info"><strong>%35</strong></td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonamazon')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewpr')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewsold')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/nl.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Orion Effect</span></td>
                                    <td><a href="https://www.amazon.ca/sp?seller=A2T8O4WPVSBGF8">A2T8O4WPVSBGF8</a></td>
                                    <td><span class="badge badge-success">652</span></td>
                                    <td><span class="badge badge-warning">82</span></td>
                                    <td><span class="badge badge-primary">$8.300</span></td>
                                    <td class="w100 text-info"><strong>$3.275</strong></td>
                                    <td class="w100 text-info"><strong>%25</strong></td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonamazon')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewpr')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewsold')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/us.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Sirius Global</span></td>
                                    <td><a href="https://www.amazon.ca/sp?seller=A30HM80T26CN0A">A30HM80T26CN0A</a></td>
                                    <td><span class="badge badge-success">591</span></td>
                                    <td><span class="badge badge-warning">80</span></td>
                                    <td><span class="badge badge-primary">$7.850</span></td>
                                    <td class="w100 text-info"><strong>$2.375</strong></td>
                                    <td class="w100 text-info"><strong>%32</strong></td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonamazon')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewpr')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewsold')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>

                                
                            </tbody>
                        </table>
                    </div>











            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop