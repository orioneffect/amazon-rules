@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('dropshipping.salesreports'))

@section('content')

<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">
            <h2>{{__('dropshipping.salesreports')}}</h2>               
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
            <div class="col row align-items-end">
                        <div>
                    </div>
            </div>
            <div class="row align-items-end">
            <div class="col-lg-4 col-md-3 col-sm-12">
                                <div class="input-group">
                                    <input type="text" id="searchval" class="form-control" placeholder="{{__('dropshipping.searchid')}}">
                                </div>
                            </div>
<div class="col-auto pr-0">
<form action="/switch-daterange" id="SwitchDateRange" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="SC1P1ewPQgKuhLWK3WIgr9PSUsDw8gE4e1b4yw-xOFPCCECzaIW8HrZpw9YU5uWwnwPpXHsUvHDb0TFe6k2fs63oa3Qzb8q-MlgzdyEvtv7P92KQ_0LfKvhcPKgN4DVEBdOE3AzqV-_XEM0a_SD92g2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><select class="form-control" data-val="true" data-val-number="The field DefaultSelectedDateRange must be a number." data-val-required="The DefaultSelectedDateRange field is required." id="daterange-selection" name="DefaultSelectedDateRange"><option selected="selected" value="0">{{__('dashboard.0')}}</option>
<option value="1">{{__('dashboard.1')}}</option>
<option value="3">{{__('dashboard.3')}}</option>
<option value="7">{{__('dashboard.7')}}</option>
<option value="14">{{__('dashboard.14')}}</option>
<option value="30">{{__('dashboard.30')}}</option>
<option value="60">{{__('dashboard.60')}}</option>
<option value="90">{{__('dashboard.90')}}</option>
<option value="180">{{__('dashboard.180')}}</option>
<option value="365">{{__('dashboard.365')}}</option>
<option value="36500">{{__('dashboard.alltime')}}</option>
</select></form></div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="canada" selected="&quot;selected&quot;"  data-select2-id="2"> Canada</option>
                    <option value="2" data-name="mexico" > Mexico</option>
                    <option value="3" data-name="uae"> U.A.E.</option>

                <option value="-1" data-name="allcountries">All Countries</option>
            </select>
</form>    </div>
</div>

<div class="col-auto">
    <div class="m-selection-list">
<form action="/switch-marketplace" id="SwitchMarketplace" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="EbQqS8P9LMWWAC2dy76qTv_YwzmJntB6gPRwc4Ape54vB8fRBkXtU-qmVh9I4n_kSXYE4L62IjegjPqhq6zfOrc40RP0sXFYjpzZtHf1Glxc6NvuRotY3lgLYffJDY_IR-n3AaXX4ItFePyja64noA2"><input class="amzr-returnurl" id="ReturnUrl" name="ReturnUrl" type="hidden" value=""><input data-val="true" data-val-number="The field DefaultSelectedMarketplaceId must be a number." data-val-required="The DefaultSelectedMarketplaceId field is required." id="DefaultSelectedMarketplaceId" name="DefaultSelectedMarketplaceId" type="hidden" value="2">            <select id="marketplace-selection" name="SwitchedMarketplaceId" class="form-control select2-hidden-accessible" data-toggle="select" data-minimum-results-for-search="-1" data-select2-id="marketplace-selection" tabindex="-1" aria-hidden="true">

                    <option value="1" data-name="Lacin's Store" selected="&quot;selected&quot;"  data-select2-id="2"> Lacin's Store</option>
                    <option value="2" data-name="immanuel" > immanuel</option>
                    <option value="3" data-name="Sirius">Sirius</option>
                    <option value="-1" data-name="allstores">All Stores</option>
            </select>
</form>    </div>
</div>
           
   <div class="col-lg-2 col-md-12 col-sm-12">
                                <a href="javascript:search();" class="btn btn-sm btn-primary btn-block" title=""><i class="fa fa-search"></i> {{__('dropshipping.search')}}</a>
                            </div>

</div>         
</div>

<hr>




<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card planned_task">
            <div class="header">             
                <ul class="header-dropdown dropdown">                                
                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
<div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                    <th>{{__('dropshipping.country')}}</th>        
                                    <th>{{__('dropshipping.store')}}</th>
                                    <th>{{__('dropshipping.ordernum')}}</th>
                                    <th>{{__('dropshipping.buyeronum')}}</th>
                                    <th>{{__('dropshipping.status')}}</th>
                                    <th>{{__('dropshipping.price')}}</th>
                                    <th>{{__('dropshipping.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dropshipping.fee')}}</th>
                                    <th>{{__('dropshipping.orderdate')}}</th>
                                    <th>{{__('dropshipping.view')}}</th>
                        </thead>


                            <tfoot>
                                    <th>{{__('dropshipping.country')}}</th>        
                                    <th>{{__('dropshipping.store')}}</th>
                                    <th>{{__('dropshipping.ordernum')}}</th>
                                    <th>{{__('dropshipping.buyeronum')}}</th>
                                    <th>{{__('dropshipping.status')}}</th>
                                    <th>{{__('dropshipping.price')}}</th>
                                    <th>{{__('dropshipping.profit')}}</th>
                                    <th>%</th>
                                    <th>{{__('dropshipping.fee')}}</th>
                                    <th>{{__('dropshipping.orderdate')}}</th>
                                    <th>{{__('dropshipping.view')}}</th>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/us.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Immanuel Store</span></td>
                                    <td><span class="badge badge-primary">702-4740186-4793849</span></td>
                                    <td><span class="badge badge-warning">105-4740186-4793849</span></td>
                                    <td><span class="badge badge-primary">Delivered Fri, Feb 12</span></td>
                                    <td><span class="text-primary"><strong>$125,00</strong></span></td>
                                    <td><span class="text-success"><strong>$45</strong></td>
                                    <td><span class="text-primary"><strong>%35</strong></td>
                                    <td><span class="text-danger"><strong>CN$25</strong></td>
                                    <td><span class="text-orange">15.08.2021</td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.vieworder')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonsource')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Angelo</span></td>
                                    <td><span class="badge badge-primary">702-4740186-4793849</span></td>
                                    <td><span class="badge badge-warning">105-4740186-4793849</span></td>
                                    <td><span class="badge badge-danger">Cancelled Fri, Aug 12</span></td>
                                    <td><span class="text-primary"><strong>$350,00</strong></span></td>
                                    <td><span class="text-success"><strong>$45</strong></td>
                                    <td><span class="text-primary"><strong>%35</strong></td>
                                    <td><span class="text-danger"><strong>CN$45</strong></td>
                                    <td><span class="text-orange">13.08.2021</td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.vieworder')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonsource')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/ca.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Sirius Global</span></td>
                                    <td><span class="badge badge-primary">702-4740186-4793849</span></td>
                                    <td><span class="badge badge-warning">105-4740186-4793849</span></td>
                                    <td><span class="badge badge-primary">Delivered Fri, Aug 22</span></td>
                                    <td><span class="text-primary"><strong>$125,00</strong></span></td>
                                    <td><span class="text-success"><strong>$45</strong></td>
                                    <td><span class="text-primary"><strong>%35</strong></td>
                                    <td><span class="text-danger"><strong>CN$25</strong></td>
                                    <td><span class="text-orange">22.08.2021</td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.vieworder')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonsource')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/au.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Orion</span></td>
                                    <td><span class="badge badge-primary">702-4740186-4793849</span></td>
                                    <td><span class="badge badge-warning">105-4740186-4793849</span></td>
                                    <td><span class="badge badge-danger">Cancelled Fri, Aug 29</span></td>
                                    <td><span class="text-primary"><strong>$725,00</strong></span></td>
                                    <td><span class="text-success"><strong>$235</strong></td>
                                    <td><span class="text-primary"><strong>%35</strong></td>
                                    <td><span class="text-danger"><strong>CN$55</strong></td>
                                    <td><span class="text-orange">29.08.2021</td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.vieworder')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonsource')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/nl.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Orion Effect</span></td>
                                    <td><span class="badge badge-primary">702-4740186-4793849</span></td>
                                    <td><span class="badge badge-warning">105-4740186-4793849</span></td>
                                    <td><span class="badge badge-primary">Delivered Fri, Feb 12</span></td>
                                    <td><span class="text-primary"><strong>$125,00</strong></span></td>
                                    <td><span class="text-success"><strong>$45</strong></td>
                                    <td><span class="text-primary"><strong>%35</strong></td>
                                    <td><span class="text-danger"><strong>CN$25</strong></td>
                                    <td><span class="text-orange">15.08.2021</td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.vieworder')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonsource')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>
                                <tr>
                                    <td class="w40"><img src="../assets/images/flag/us.svg " class="w35 rounded-circle"></td>  
                                    <td><span>Sirius Global</span></td>
                                    <td><span class="badge badge-primary">702-4740186-4793849</span></td>
                                    <td><span class="badge badge-warning">105-4740186-4793849</span></td>
                                    <td><span class="badge badge-primary">Delivered Fri, Feb 12</span></td>
                                    <td><span class="text-primary"><strong>$125,00</strong></span></td>
                                    <td><span class="text-success"><strong>$45</strong></td>
                                    <td><span class="text-primary"><strong>%35</strong></td>
                                    <td><span class="text-danger"><strong>CN$25</strong></td>
                                    <td><span class="text-orange">15.08.2021</td>
                                    <td>
                                    <ul class="header-dropdown dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);">{{__('dropshipping.vieworder')}}</a></li>
                            <li><a href="javascript:void(0);">{{__('dropshipping.viewonsource')}}</a></li>
                        </ul>
                    </li>
                </ul>
                                </td>
                                </tr>

                                
                            </tbody>
                        </table>

</div>
</div>
</div>








@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop