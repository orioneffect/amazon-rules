@extends('layout.master')
@section('parentPageTitle', __('dropshipping.dropshipping'))
@section('title', __('menus.product_details'))


@section('content')
    <div class="row clearfix">
        <div class="col-12 displaynone" id="successmsg">
            <div class="alert alert-success" role="alert">
                <i class="fa fa-check-circle"></i><span id="success_button"></span>
            </div>
        </div>
        <div class="card">
            <div class="col-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 text-center d-flex">
                                <img src="{{ $details->product_imgurl }}" alt="" class="img-rounded" />
                            </div>
                            <div class="col-lg-8 col-md-12 col-sm-12">
                                <h6>{{ __('dropshipping.productname') }}</h6>
                                <p> {{ $details->product_title }}</p>
                                <p>
                                    <span class="ml-0 h5 font-weight-bold">{{ __('dropshipping.price') }} :
                                    </span><span>{{ $details->product_cost }} {{ $details->curr }}</span>
                                </p>
                                <p>
                                    <span><strong>{{ __('dropshipping.asin') }} : </strong>
                                    </span><span></span><span>{{ $details->asin }}</span>
                                </p>
                                @php
                                    $ratinground = ($details->product_rating / 5) * 100;
                                    $rating = round($ratinground / 10) * 10;
                                @endphp
                                <p>
                                <div class="end">
                                    <div class="stars-outer">
                                        <div class="stars-inner" style="width:{{ $rating }}%"></div>
                                    </div> {{ $details->total_ratings }}
                                </div>
                                </p>
                                <p>
                                    <span><strong>{{ __('dropshipping.sku_caps') }} : </strong> </span><span>{{ $details->sku }}</span>
                                </p>
                                <p class="nav-item">

                                    @php

                                        $new_url = '';
                                        $source = '';
                                        $url = $details->url;
                                        $parts = explode('/', $url);
                                        $new_url = $parts[0].'/'.$parts[1].'/'.$parts[2].'/';
                                        $source = parse_url($url);

                                    @endphp

                                    <a class="nav-link py-0" href="{{ $new_url }}dp/{{ $details->asin }}" rel="" target="_blank">
                                        <i class="fa fa-external-link"></i> {{ __('dropshipping.view_on') }}{{ $source['host'] }}
                                    </a>
                                </p>
                                <p class="nav-item">
                                    <a class="nav-link py-0"  href="https://www.{{ $details->url_dest }}/dp/{{ $details->asin }}" rel="" target="_blank">
                                        <i class="fa fa-external-link"></i> {{ __('dropshipping.view_on') }}www.{{$details->url_dest}}
                                    </a>
                                </p>
                                <!--<p class="nav-item">
                                    <a class="nav-link py-0" href="" rel="" target="_blank">
                                        <i class="fa fa-external-link"></i> {{ __('dropshipping.viewoff_amazon_ca') }}
                                    </a>
                                </p>-->
                                <p class="nav-item">
                                    <!--<a class="nav-link py-0" href="" rel="" target="_blank">-->
                                        <i class="fa fa-external-link"></i> {{ __('dropshipping.view_orders') }}
                                        <!--</a>-->
                                </p>
                                <p class="nav-item">
                                    <!--<a class="nav-link py-0" href="" rel="" target="_blank">-->
                                        <i class="fa fa-external-link"></i> {{ __('dropshipping.view_sellercentral') }}
                                        <!--</a>-->
                                </p>
                                <p class="nav-item">
                                    <!--<a class="nav-link py-0" href="" rel="" target="_blank">-->
                                        <i class="fa fa-external-link"></i> {{ __('dropshipping.view_updatehistory') }}
                                        <!--</a>-->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="status_update">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <p class="mb-0">{{ __('dropshipping.prod_details_def1') }}</p>
                                <p class="mb-0">{{ __('dropshipping.prod_details_def2') }}</p>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12">
                                <a href="javascript:product_approve_reject(1,{{ $details->id }});"
                                    class="btn btn-sm btn-success btn-block js-sweetalert" data-type="confirm" title=""><i class="fa fa-plus-circle"></i>
                                    {{ __('dropshipping.approve') }}</a>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12">
                                <a href="javascript:product_approve_reject(2,{{ $details->id }});"
                                    class="btn btn-sm btn-danger btn-block" title=""><i class="fa fa-times-circle"></i>
                                    {{ __('dropshipping.remove') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.purchaseprice') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->product_cost }} {{ $details->curr }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.shippingcost') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->shipping_fee }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.minallowedprice') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->lowest_rate }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.maxallowedprice') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            @php
                            $seller = str_replace('www.a', 'A', $source['host']);   
                            @endphp
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{$seller}} {{ __('dropshipping.seller') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>
                                        {{ $details->buy_box_seller }}
                                </p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.review') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <div class="stars-outer">
                                    <div class="stars-inner" style="width:{{ $rating }}%"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-12 font-weight-bold">
                                <hr />
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.brand') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->brand_name }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.trademarkstatus') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.category1') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->rank1brand }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.category2') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->rank2brand }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.handlingtime') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.can_sentto_customer') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-12 font-weight-bold">
                                <hr />
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.fullimportfee') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.fullshippingcost') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.unavailable') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.fba_amazonproduct') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.price_appears_basket') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.addonproduct') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.discountedproduct') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.primeexclusiveproduct') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.chineseseller') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.mainasin') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->asin }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.salesrank') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-12 font-weight-bold">
                                <hr />
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.inventoryupdate') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>-</p>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 font-weight-bold">
                                {{ __('dropshipping.productupdate') }}
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <p>{{ $details->created_at }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>
    <style>
        .stars-outer {
            position: relative;
            display: inline-block;
        }

        .stars-inner {
            position: absolute;
            top: 0;
            left: 0;
            white-space: nowrap;
            overflow: hidden;
            width: 0;
        }

        .stars-outer::before {
            content: "\2605 \2605 \2605 \2605 \2605";
            font-size: 15px;
            font-weight: 900;
            color: #ccc;
        }

        .stars-inner::before {
            content: "\2605 \2605 \2605 \2605 \2605";
            font-size: 15px;
            font-weight: 900;
            color: #f8ce0b;
        }

        .checked {
            color: orange;
        }

    </style>
@stop

@section('page-script')
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('js/dropshipping/dropshipping.js?v=1.3') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        function product_approve_reject(status, id) {
            if (status == '1') {
                alert = "Are you sure to Approve?";
                succ_msg = " This item is Approved Successfully";
            } else {
                alert = "Are you sure to Reject?";
                succ_msg = " This item is Rejected Successfully";
            }
            if (confirm(alert) == true) {

                var items = document.getElementsByName("chkbox");
                var selectedItems = "";
                if (id > 0) {
                    selectedItems = id;
                } else {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].type == "checkbox" && items[i].checked == true) selectedItems += items[i].value + ",";
                    }
                }
                var ENDPOINT = "{{ url('/') }}";
                $.ajax({
                        url: ENDPOINT + "/dropshipping/updateStatus?id=" + selectedItems + "&status=" + status,
                        datatype: "json",
                        type: "get",
                        beforeSend: function() {
                            $('.auto-load').show();
                        }
                    })
                    .done(function(response) {
                        console.log(response);
                        $('#success_button').html(succ_msg);
                        $('#successmsg').show();
                        $('#status_update').hide();
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError) {
                        console.log('Server error occured');
                    });
            }
        }
    </script>
@stop
