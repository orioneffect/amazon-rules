@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('settings.generalsettings'))

@section('content')

    <!-- css and coding area-->
    <ul class="nav nav-tabs " id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#fly" role="tab" aria-controls="fly"
                aria-selected="true" onclick="tab_storeinfo('1')">{{ __('settings.orion') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#store" role="tab" aria-controls="store"
                aria-selected="false" onclick="tab_storeinfo('2')">{{ __('settings.store') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab"
                aria-controls="messages" aria-selected="false"
                onclick="tab_storeinfo('3');sms_no('{{ $data['load_user_main_config']->phone_number }}')">{{ __('settings.messages') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#filters" role="tab" aria-controls="filters"
                aria-selected="false" onclick="tab_storeinfo('4')">{{ __('settings.filters') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#billings" role="tab"
                aria-controls="billing" aria-selected="false"
                onclick="tab_storeinfo('5');load_bill_details();load_bill_history();load_ccp_list();">{{ __('settings.billings') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#Maincredits" role="tab"
                aria-controls="maincredits" aria-selected="false"
                onclick="tab_storeinfo('6'); load_cclist();load_credits();load_credit_type();">Credits</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#prices" role="tab" aria-controls="prices"
                aria-selected="false" onclick="tab_storeinfo('7')">{{ __('settings.prices') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#orion" role="tab" aria-controls="orion"
                aria-selected="false" onclick="tab_storeinfo('8')">{{ __('settings.shipping') }}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#more" role="tab" aria-controls="more"
                aria-selected="false" onclick="tab_storeinfo('9');postpone_details();">{{ __('settings.more') }}</a>
        </li>
        <li>
            <input type="hidden" id="currenttab" value="1">

            <div class="input-group mb-3">
                <div id="countrylist_onload" class="displaynone">
                    <select class="form-control" id="country_info" name="country_info"
                        onchange="load_store_list(this.value);load_state_list(this.value)">
                        @isset($maindata['countriesNav'])
                            @foreach ($maindata['countriesNav'] as $country)
                                <option value="{{ $country->country_id }}"
                                    {{ $data['user']->default_store_id == $country->country_id ? 'selected' : '' }}>
                                    {{ $country->country_name }}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>
                <div id="storelist_onload" class="displaynone">
                    <select class="form-control" id="store_main" name="store_main"
                        onchange="storeinfo_loadstore(this.value);loadstore(this.value);ship_loadstore(this.value);pricer_loadstore(this.value)">
                        @isset($maindata['storesNav'])
                            @foreach ($maindata['storesNav'] as $store)
                                <option value="{{ $store->id }}"
                                    {{ $data['user']->default_store_id == $store->id ? 'selected' : '' }}>
                                    {{ $store->store_name }}
                                </option>
                            @endforeach
                        @endisset
                    </select>
                </div>
                <div id="storelist_load">
                </div>
            </div>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">

        <div class="tab-pane active" id="fly" role="tabpanel" aria-labelledby="fly-orion">
            <div class="row clearfix">
                <div class="col-12">
                    <div class="card text-white bg-green">
                        <div class="card-header">Orion FLY</div>
                        <div class="body">
                            <div class="col-12">
                                <div class="alert alert-success" role="alert">
                                    <h3 class="alert-heading">{{ __('settings.mesheader') }}</h3>
                                    <p>
                                    <h6>{{ __('settings.description') }}</h6>
                                    </p>
                                </div>
                                <hr>
                            </div>
                            <!--25k pakete kadar günlük 20k ürün atabilirsin -->
                            <!--10k pakete kadar günlük 3k tarama 6k uygun ürün -->
                            <table class="table table-success">
                                <tbody>
                                    <tr>
                                        <td><label class="switch">
                                                <input type="checkbox" name="stockupdate" value=""
                                                    {{ $data['load_user_main_config']->orionfly == '1' ? 'checked' : '' }}
                                                    onclick="updateusertbl('orionfly',this.checked?'1':'0')">
                                                <span class="slider round"></span>
                                            </label></td>
                                        <td><label class="mb-1 mt-1 h3"
                                                for="OrionFly">{{ __('settings.activate') }}</label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="store" role="tabpanel" aria-labelledby="store-tab">
            <div class="row clearfix">
                <div class="col-12">
                    <div class="card">
                        <div class="body">
                            <label>
                                <h3>{{ __('settings.storefront') }}</h3>
                            </label>
                            <form id="storeinfo_frm">
                                @csrf

                                <table class="table table-hover">
                                    <input type="hidden" id="updatestore" name="updatestore" value="">
                                    <thead>
                                        <tr>
                                            <th style="width: 25%">
                                                <h6 class="text-warning">
                                                    <label class="switch">
                                                        <input type="checkbox" name="updateallstores" id="updateallstores"
                                                            value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    {{ __('settings.updateallstores') }}
                                                </h6>

                                            </th>
                                            <th style="width: 10%"></th>
                                            <th style="width: 35%"></th>
                                            <th style="width: 30%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ __('settings.maxstock') }}</td>
                                            <td colspan="2"><input type="text" name="maxstock" id="maxstock" value=""
                                                    class="form-control" placeholder="{{ __('settings.maxstock') }} *"
                                                    required></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.comm') }}* </td>
                                            <td>
                                                <div><input type="text" name="amazon_commission" id="amazon_commission"
                                                        value="20" class="form-control float-left" required></div>
                                                <div class="mt-2">&nbsp;(%)</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.curr') }}</td>
                                            <td>
                                                <div>
                                                    <select name="currency_update_o" id="currency_update_o"
                                                        class="form-control show-tick" required onchange="store_option('1', 'currency_update_o','currency_update')">
                                                        <option value="A" selected>{{ __('settings.oto') }}
                                                        </option>
                                                        <option value="F">{{ __('settings.fixed') }}</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <span name="currency_update_a" id="currency_update_a"  class="text-info displaynone"></span>
                                                <span name="currency_update_c" id="currency_update_c"  class="text-warning displaynone"></span>
                                                <input type="text" name="currency_update_t" id="currency_update_t" value=""
                                                    class="form-control displaynone" placeholder="{{ __('settings.parity') }} *"
                                                    ></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.salestax') }}</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox" name="sales_tax_c" id="sales_tax_c" value="1"
                                                        onclick="store_option('2',this.id,'sales_tax_t')">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td><input type="text" name="sales_tax_t" id="sales_tax_t" value=""
                                                    class="form-control displaynone"
                                                    placeholder="{{ __('settings.saletax') }} % *">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.impfee') }}</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox" name="import_fee_c" id="import_fee_c" value="1"
                                                        onclick=store_option('2',this.id,'import_fee_t')>
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td><input type="text" name="import_fee_t" id="import_fee_t" value=""
                                                    class="form-control displaynone"
                                                    placeholder="{{ __('settings.impfees') }} % *">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.reskeyword') }}</td>
                                            <td>
                                                <label class="switch">
                                                    <input type="checkbox" name="restricted_keywords"
                                                        id="restricted_keywords" value="1">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.resprod') }}</td>
                                            <td><label class="mb-1 mt-1" for="restricted_products"></label>
                                                <label class="switch">
                                                    <input type="checkbox" name="restricted_products"
                                                        id="restricted_products" value="1">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.brandblack') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="blacklist" id="blacklist" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.brandwhite') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="whitelist" id="whitelist" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.combrand') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="common_pools_for_brand"
                                                        id="common_pools_for_brand" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.comprod') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="common_pools_for_product"
                                                        id="common_pools_for_product" value="1">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.handling') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="handling_time_c" id="handling_time_c"
                                                        value="1" onclick=store_option('2',this.id,'handling_time_t')>
                                                    <span class="slider round"></span>
                                                </label></td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="handling_time_t" id="handling_time_t"
                                                        class="form-control show-tick displaynone">
                                                        <option value="" Selected>{{ __('settings.select') }}</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.comfirm') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="order_confirmation" id="order_confirmation"
                                                        value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.precomfirm') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="pre_order_confirmation_c"
                                                        id="pre_order_confirmation_c" value="1"
                                                        onclick=store_option('2',this.id,'pre_order_confirmation_t')>
                                                    <span class="slider round"></span>
                                                </label></td>
                                            <td><input type="text" name="pre_order_confirmation_t"
                                                    id="pre_order_confirmation_t" value="" class="form-control displaynone"
                                                    placeholder="{{ __('settings.prehour') }} *">
                                                <span class="slider round"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.priceup') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="priceupdate" id="priceupdate" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.stockup') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="stockupdate" id="stockupdate" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.remove') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="autodeletepr" id="autodeletepr" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('settings.precomfirm') }}</td>
                                            <td><label class="switch">
                                                    <input type="checkbox" name="pre_order_confirmation"
                                                        id="pre_order_confirmation" value="1">
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <button type="submit" id="btn"
                                            class="btn btn-primary">{{ __('settings.update') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">

            <div class="alert alert-success" role="alert">
                <h3 class="alert-heading">{{ __('settings.mesheader') }}</h3>
                <p>
                <h6>{{ __('settings.mestext') }}</h6>
                </p>
                <hr>
                <p class="mb-0">{{ __('settings.mestitle1') }}</p>
            </div>
            <hr>

            <div class="card">
                <div class="card-body">
                    <table table table-hover table-dark>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <div class="alert alert-light" role="alert">
                                        <h6>{{ __('settings.phonenumber') }}</h6>
                                    </div>
                                </td>
                                <td>
                                    <div id="phnumber">
                                        <span class="badge badge-success">
                                            <h6 id="load_user_main_config">
                                                {{ $data['load_user_main_config']->phone_number }}</h6>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div id="updatenumber">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#changenumber"
                                            data-whatever="changenumber">{{ __('settings.cnumber') }}</button>
                                    </div>
                                    <div id="addnewnumber" class="displaynone">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#changenumber"
                                            data-whatever="changenumber">{{ __('settings.addnumber') }}</button>
                                    </div>

                                    <div class="modal fade" id="changenumber" tabindex="-1"
                                        aria-labelledby="changenumbertitle" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="changenumbertitle">
                                                        {{ __('settings.cnumber') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group demo-masked-input">
                                                        <label for="phone_number" class="col-form-label">Phone
                                                            Number:</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i
                                                                        class="fa fa-phone"></i></span>
                                                            </div>
                                                            <input type="text" class="form-control phone-number"
                                                                id="phone_number" oninput="$('#phoneValidation').hide();"
                                                                placeholder="Ex: +00 (000) 000-00-00">
                                                        </div>

                                                    </div>
                                                    <div class="alert alert-warning displaynone" id="phoneValidation">
                                                        {{ __('settings.please_enter_valid_phone_number') }}
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        onclick="$('#phoneValidation').hide();"
                                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                                    <button type="button" class="btn btn-primary"
                                                        onclick="savephoneno($('#phone_number').val());">{{ __('settings.save') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <ul class="list-group">
                        <li class="list-group-item">
                            {{ __('settings.enable_free_text_msg') }}
                            <div class="float-right">
                                <label class="switch">
                                    <input type="checkbox" id="enable_free_text_msg"
                                        {{ $data['load_user_main_config']->enable_free_text_msg ? 'checked' : '' }}
                                        onclick="disable_all_msgs(this.checked?'1':'0')">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </li>
                        <li class="list-group-item">
                            {{ __('settings.msg_when_orion_cannot_reach_amazon_seller_acc') }}
                            <div class="float-right">
                                <label class="switch">
                                    <input type="checkbox" id="msg_when_orion_cannot_reach_amazon_seller_acc"
                                        {{ $data['load_user_main_config']->msg_when_orion_cannot_reach_amazon_seller_acc ? 'checked' : '' }}
                                        onclick="update_mainconfig('msg_when_orion_cannot_reach_amazon_seller_acc', this.checked?'1':'0')">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </li>
                        <li class="list-group-item">
                            {{ __('settings.msg_when_package_lost') }}
                            <div class="float-right">
                                <label class="switch">
                                    <input type="checkbox" id="msg_when_package_lost"
                                        {{ $data['load_user_main_config']->msg_when_package_lost ? 'checked' : '' }}
                                        onclick="update_mainconfig('msg_when_package_lost', this.checked?'1':'0')">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </li>
                        <li class="list-group-item">
                            {{ __('settings.msg_when_customer_leaves_negative_feedback') }}
                            <div class="float-right">
                                <label class="switch">
                                    <input type="checkbox" id="msg_when_customer_leaves_negative_feedback"
                                        {{ $data['load_user_main_config']->msg_when_customer_leaves_negative_feedback ? 'checked' : '' }}
                                        onclick="update_mainconfig('msg_when_customer_leaves_negative_feedback', this.checked?'1':'0')">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </li>
                        <li class="list-group-item">
                            {{ __('settings.msg_when_automatic_bill_payment_failed') }}
                            <div class="float-right">
                                <label class="switch">
                                    <input type="checkbox" id="msg_when_automatic_bill_payment_failed"
                                        {{ $data['load_user_main_config']->msg_when_automatic_bill_payment_failed ? 'checked' : '' }}
                                        onclick="update_mainconfig('msg_when_automatic_bill_payment_failed', this.checked?'1':'0')">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="tab-pane" id="filters" role="tabpanel" aria-labelledby="filters">

            <div class="alert alert-primary" role="alert">
                <h3 class="alert-heading">{{ __('settings.filheader') }}</h3>
                <p>
                <h6>{{ __('settings.filtext') }}</h6>
                </p>
                <hr>
                <p class="mb-0">{{ __('settings.filtitle1') }}</p>
            </div>

            <hr>

            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-warning"
                        onclick="filters_update()">{{ __('settings.updateallstores') }}</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table table table-hover>
                        <tbody>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.fbamz') }}<span
                                            class="badge badge-primary">{{ __('settings.recomend') }}</span></h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" id="fbamazonpr" name="fbamazonpr" value=""
                                            onclick="updatetbl('only_fba_amazon_products',this.checked?$('#onlyfba').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="onlyfba" id="onlyfba"
                                            onchange="updatetbl('only_fba_amazon_products',this.value);$('#fbamazonpr').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.chinese') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" id="chinise" name="chinise" value=""
                                            onclick="updatetbl('chinese_sellers_in_the_sale_store',this.checked?$('#chinese').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="chinese" id="chinese"
                                            onchange="updatetbl('chinese_sellers_in_the_sale_store',this.value);$('#chinise').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.amzseller') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="amzown" id="amzown" value=""
                                            onclick="updatetbl('amazon_sellers_in_the_sale_store',this.checked?$('#amzseller').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="amzseller" id="amzseller"
                                            onchange="updatetbl('amazon_sellers_in_the_sale_store',this.value);$('#amzown').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.mystore') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="myown" id="myown" value=""
                                            onclick="updatetbl('products_from_my_own_stores',this.checked?$('#myownn').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="myownn" id="myownn"
                                            onchange="updatetbl('products_from_my_own_stores',this.value);$('#myown').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.filterstore') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="sellerid" id="sellerid" value=""
                                            onclick="updatetbl('stores_i_ve_added',this.checked?$('#sellerids').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="sellerids" id="sellerids"
                                            onchange="updatetbl('stores_i_ve_added',this.value);$('#sellerid').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.trademark') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="trademark" id="trademark" value=""
                                            onclick="updatetbl('trademark_protected_products',this.checked?$('#trademark1').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="trademark1" id="trademark1"
                                            onchange="updatetbl('trademark_protected_products',this.value);$('#trademark').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.discount') }}<span
                                            class="badge badge-primary">{{ __('settings.norecom') }}</span></h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="discount" id="discount" value=""
                                            onclick="updatetbl('discounted_products',this.checked?$('#discount1').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="discount1" id="discount1"
                                            onchange="updatetbl('discounted_products',this.value);$('#discount').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.cantaddress') }}<span
                                            class="badge badge-primary">{{ __('settings.recomend') }}</span></h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="cantaddress" id="cantaddress" value=""
                                            onclick="updatetbl('cannot_be_sent_to_the_address',this.checked?$('#cantadres').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="cantadres" id="cantadres"
                                            onchange="updatetbl('cannot_be_sent_to_the_address',this.value);$('#cantaddress').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <h6>{{ __('settings.misinfo') }}<span
                                            class="badge badge-primary">{{ __('settings.recomend') }}</span></h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="misship" id="misship" value=""
                                            onclick="updatetbl('missing_shipping_info_products',this.checked?$('#misshipping').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="misshipping" id="misshipping"
                                            onchange="updatetbl('missing_shipping_info_products',this.value);$('#misship').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.unavailable') }}<span
                                            class="badge badge-primary">{{ __('settings.recomend') }}</span></h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="unavail" id="unavail" value=""
                                            onclick="updatetbl('unavailable_products',this.checked?$('#unavailable').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="unavailable" id="unavailable"
                                            onchange="updatetbl('unavailable_products',this.value);$('#unavail').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.noimport') }}<span
                                            class="badge badge-primary">{{ __('settings.norecom') }}</span></h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="noimport" id="noimport" value=""
                                            onclick="updatetbl('no_import_fee',this.checked?$('#nofee').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="nofee" id="nofee"
                                            onchange="updatetbl('no_import_fee',this.value);$('#noimport').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.sameseller') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="samebrand" id="samebrand" value=""
                                            onclick="updatetbl('same_brand_and_seller_name',this.checked?$('#brand').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="brand" id="brand"
                                            onchange="updatetbl('same_brand_and_seller_name',this.value);$('#samebrand').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.primeseller') }}</h6>
                                </td>
                                <td><label class="switch">
                                        <input type="checkbox" name="primeseller" id="primeseller" value=""
                                            onclick="updatetbl('prime_sellers_in_the_sale_store',this.checked?$('#primesel').val():'')">
                                        <span class="slider round"></span>
                                    </label></td>
                                <td>
                                    <div class="form-group">
                                        <select name="primesel" id="primesel"
                                            onchange="updatetbl('prime_sellers_in_the_sale_store',this.value);$('#primeseller').prop('checked', true);"
                                            class="form-control show-tick" required>
                                            <option value="R">{{ __('settings.removee') }}</option>
                                            <option value="O">{{ __('settings.outofstock') }}</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.maxseller') }}</h6>
                                </td>
                                <td>
                                    <!--  <label class="switch">
                                                <input type="checkbox" name="maxsel" value="1" class="displaynone">
                                                <span class="slider round"></span>
                                                </label> -->
                                </td>
                                <td><input type="text" name="maxseller" id="maxseller" value="" class="form-control"
                                        placeholder="MaxSeller" oninput="updatetbl('max_number_of_sellers',this.value)">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>{{ __('settings.minseller') }}</h6>
                                </td>
                                <td>
                                    <!--  <label class="switch">
                                                <input type="checkbox" name="minsel" value="1">
                                                <span class="slider round"></span>
                                                </label> -->
                                </td>
                                <td><input type="text" name="minseller" id="minseller" value="" class="form-control"
                                        placeholder="MinSeller" oninput="  ('min_number_of_sellers',this.value)"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="tab-pane" id="billings" role="tabpanel" aria-labelledby="billings">

            <div class="alert alert-warning" role="alert">
                <h6 class="alert-heading">Dear username "Erkan LAÇİN", Your Subscription type is <strong>"Beginner 5000
                        ASIN"</h6>
                </p>
                <p><a href="subscription.php" title="">Upgrade Your Subscription</a></strong></p>
                <p>10 days left until your subscription expires. Your subscription will automatically renew.</p>
                <h3><strong>
                        <p><a href="../process.php" title="">Freeze My Subscription</a></p>
                    </strong></h3>
            </div>
            <div id="ccpay">
                <form id="paycc">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="form-group" id="list_cc">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 col-sm-12">
                                    <h2>$ <span id="paycc_total"></span></h2>
                                    <input type="hidden" id="paycc_id" name="paycc_id" value="">
                                    <input type="hidden" id="paycc_totalamt" name="paycc_totalamt" value="">
                                </div>
                                <div class="col-lg-2 col-md-6 col-sm-12">
                                    <div>
                                        <button type="submit" id="paycc_btn"
                                            class="btn btn-primary">{{ __('settings.paynow') }}</button>
                                    </div>
                                </div>
                                <div class="alert alert-warning displaynone" id="payValidation">
                                    {{ __('settings.please_select_invoice') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div id="pending_list">
                        <h5>{{ __('settings.payment_pending') }}</h5>
                        <div class="table-responsive invoice_list mb-4">
                            <table class="table table-hover table-custom spacing8">
                                <thead>
                                    <tr>
                                        <th style="width: 20px;">Select all
                                            <br>
                                            <label class="switch">
                                                <input type="checkbox" id="selectall" checked value="1"
                                                    onchange="pending_payment()">
                                                <span class="slider round"></span>
                                            </label>
                                        </th>
                                        <th style="width: 200px;">{{ __('settings.type') }}</th>
                                        <th style="width: 200px;">{{ __('settings.invoice_number') }}</th>
                                        <th style="width: 100px;">{{ __('settings.amount') }}</th>
                                        <th style="width: 100px;">{{ __('settings.status') }}</th>
                                        <th style="width: 110px;">{{ __('settings.invoicedate') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="bill_payment_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="history">
                        <h5>{{ __('settings.payment_history') }}</h5>
                        <div class="table-responsive invoice_list mb-4">
                            <table class="table table-hover table-custom spacing8">
                                <thead>
                                    <tr>
                                        <th style="width: 20px;">#
                                        </th>
                                        <th style="width: 100px;">{{ __('settings.type') }}</th>
                                        <th style="width: 200px;">{{ __('settings.invoice_number') }}</th>
                                        <th style="width: 100px;">{{ __('settings.amount') }}</th>
                                        <th style="width: 100px;">{{ __('settings.status') }}</th>
                                        <th style="width: 100px;">{{ __('settings.paiddate') }}</th>
                                        <th style="width: 100px;">{{ __('settings.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="bill_history_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="Maincredits">
            <div class="row clearfix">
                <div class="col-12">
                    <div class="card text-white bg-orange">
                        <div class="card-header">{{ __('settings.credits') }}</div>
                        <div class="card-body">
                            <h5 class="card-title">{{ __('settings.your_payment_details') }}</h5>
                            <p class="card-text">
                            <div class="d-flex align-items-center">
                                <div class="ml-3">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#changecreditcardshipping"
                                        data-whatever="changecreditcardshipping">{{ __('settings.addnew') }}</button>
                                    <div class="modal fade" id="changecreditcardshipping" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="changecreditcardshipping">
                                                        {{ __('settings.bankdet') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        @csrf
                                                        <div class="form-group demo-masked-input">
                                                            <label for="ccardnumber"
                                                                class="col-form-label">{{ __('settings.ccnum') }}</label>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i
                                                                            class="fa fa-credit-card"></i></span>
                                                                </div>
                                                                <input type="text" class="form-control credit-card"
                                                                    name="ccardnumber" id="ccardnumber"
                                                                    oninput="$('#ccalert').hide();">
                                                            </div>
                                                        </div>
                                                        <div class="form-group demo-masked-input">
                                                            <label for="expiredate"
                                                                class="col-form-label">{{ __('settings.epdate') }}</label>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i
                                                                            class="icon-calendar"></i></span>
                                                                </div>
                                                                <input type="month" class="form-control month"
                                                                    name="expiredate" id="expiredate"
                                                                    oninput="$('#ccalert').hide();">
                                                            </div>
                                                        </div>
                                                        <div class="form-group demo-masked-input">
                                                            <label for="CVVV"
                                                                class="col-form-label">{{ __('settings.cvv') }}</label>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i
                                                                            class="fa fa-credit-card-alt"></i></span>
                                                                </div>
                                                                <input type="number" name="CVVV" class="form-control month"
                                                                    required data-parsley-min="3" value="" id="CVVV"
                                                                    oninput="$('#ccalert').hide();">
                                                            </div>
                                                        </div>
                                                        <div class="alert alert-warning displaynone" id="ccalert"></div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        onclick="$('#ccalert').hide();"
                                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                                    <button type="button" class="btn btn-primary"
                                                        onclick="save_cclist();">{{ __('settings.save') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </p>

                            <div class="row clearfix">
                                <div class="col-lg-6">
                                    <table>
                                        <thead>
                                            <th style="width: 10%">{{ __('settings.type') }}</th>
                                            <th style="width: 20%">{{ __('settings.ccnum') }}</th>
                                            <th style="width: 10%">{{ __('settings.expire') }}</th>
                                            <th style="width: 5%">{{ __('settings.delete') }}</th>
                                            <th style="width: 5%">{{ __('settings.sedef') }}</th>
                                        </thead>
                                        <tbody id="cclist-wrapper">
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <br>
                            <hr>
                            <h4><span class="badge badge-success"><strong>{{ __('settings.orcredit') }} $ <span
                                            id="tot_credit"></span></strong></span></h4>
                            <br>

                            <div class="row clearfix">
                                <div class="col-lg-6">
                                    <table>
                                        <thead>
                                            <th style="width: 50%;">{{ __('settings.addusd') }}</th>
                                            <th style="width: 50%;">{{ __('settings.crtype') }}</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="addbalance" id="addbalance"
                                                            class="form-control show-tick" required>
                                                            <option value="20" Selected>$ 20</option>
                                                            <option value="25">$ 25</option>
                                                            <option value="35">$ 35</option>
                                                            <option value="40">$ 40</option>
                                                            <option value="50">$ 50</option>
                                                            <option value="75">$ 75</option>
                                                            <option value="100">$ 100</option>
                                                            <option value="150">$ 150</option>
                                                            <option value="200">$ 200</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group" id="credit_type">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><button type="submit" class="btn btn-primary"
                                                        onclick="save_credits();">{{ __('settings.addc') }}</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <br>
                            <hr>

                            <div class="col-lg-6">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <th style="width: 15%;">{{ __('settings.addedorusd') }}</th>
                                            <th style="width: 15%;">{{ __('settings.paytyp') }}</th>
                                        </thead>
                                        <tbody id="credits-wrapper">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="prices" role="tabpanel" aria-labelledby="prices">

            <div class="row clearfix">
                <div class="col-12">

                    <div class="card">
                        <h5 class="card-header">{{ __('settings.repheader') }}</h5>
                        <div class="card-body">
                            <h5 class="card-title">{{ __('settings.title1') }}</h5>
                            <p class="card-text">{{ __('settings.reptext') }}</p>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#profit">
                                {{ __('settings.profit_maximizer') }}
                            </button>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#dynamic">
                                {{ __('settings.dynamic_profit_generator') }}
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hybrid">
                                {{ __('settings.hybrid_profit') }}
                            </button>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#balanced">
                                {{ __('settings.balanced_sales') }}
                            </button>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#agressive">
                                {{ __('settings.aggressive_selling') }}
                            </button>
                            <button type="button" class="btn btn-light" data-toggle="modal" data-target="#careful">
                                {{ __('settings.careful_sales') }}
                            </button>
                        </div>
                    </div>

                    <hr />

                    <!-- Modal -->
                    <div class="modal fade" id="profit" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('settings.profititle') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('settings.profitdesc') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="dynamic" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('settings.dynamictitle') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('settings.dynamicdesc') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="hybrid" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('settings.hybridtitle') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('settings.hybriddesc') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="balanced" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        {{ __('settings.balancedtitle') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('settings.balancedesc') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="agressive" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        {{ __('settings.aggressivetitle') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('settings.aggressivedesc') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="careful" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('settings.carefultitle') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('settings.carefuldesc') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary"
                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <table class="table table-hover">

                        <tbody>

                            <tr>
                                <td>
                                    <h5>{{ __('settings.title2') }}</h5>
                                </td>

                                <td>
                                    <select name="repricer" id="repricer" class="form-control show-tick" required
                                        onchange="pricer_update_repricer('repricer',this.value)">
                                        <option value="" Selected>{{ __('settings.repmethod') }}</option>
                                        <option value="0">{{ __('settings.stop_running_repricer') }}</option>
                                        <option value="1">{{ __('settings.profit_maximizer') }}</option>
                                        <option value="2">{{ __('settings.dynamic_profit_generator') }}</option>
                                        <option value="3">{{ __('settings.hybrid_profit') }}</option>
                                        <option value="4">{{ __('settings.balanced_sales') }}</option>
                                        <option value="5">{{ __('settings.aggressive_selling') }}</option>
                                        <option value="6">{{ __('settings.careful_sales') }}</option>
                                    </select>
                                </td>

                                <!--

                                <td>
                                    <h5>{{ __('settings.interval') }}</h5>
                                </td>
                                <td>
                                    <select name="interval" id="interval" class="form-control show-tick" required
                                        onchange="pricer_update_repricer('repricer_interval',this.value)">
                                        <option value="" Selected>{{ __('settings.select_interval') }}</option>
                                        <option value='0.01'>0.01</option>
                                        <option value='0.05'>0.05</option>
                                        <option value='0.15'>0.15</option>
                                        <option value='0.20'>0.20</option>
                                        <option value='0.25'>0.25</option>
                                        <option value='0.30'>0.30</option>
                                        <option value='0.35'>0.35</option>
                                        <option value='0.40'>0.40</option>
                                        <option value='0.45'>0.45</option>
                                        <option value='0.50'>0.50</option>
                                        <option value='1.00'>1.00</option>
                                    </select>
                                </td>-->

                            </tr>

                            <tr>
                                <td><h5>{{ __('settings.match_buy_box') }}</h5></td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox" name="match_buy_box" id="match_buy_box" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td><h5>{{ __('settings.under_buy_box') }}</h5></td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox" name="under_buy_box" id="under_buy_box" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                            </tr>


                            <tr>
                                <td><h5>{{ __('settings.over_buy_box') }}</h5></td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox" name="over_buy_box" id="over_buy_box" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td><h5>{{ __('settings.match_low_price') }}</h5></td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox" name="match_low_price" id="match_low_price" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                            </tr>


                            <tr>
                                <td><h5>{{ __('settings.under_low_price') }}</h5></td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox" name="under_low_price" id="under_low_price" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td><h5>{{ __('settings.1_perc_low_price') }}</h5></td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox" name="1_perc_low_price" id="1_perc_low_price" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                            </tr>

                        </tbody>
                    </table>

                    <!--
                                                <a href="#" class="btn btn-success"
                                                    onclick="pricer_update_repricer('repricer',$('#repricer').val())">{{ __('settings.save') }}</a>-->

            <hr />
            <h5>{{ __('settings.prule') }}</h5>
            <div class="row mb-3">
                <div class="col-1">{{ __('settings.from') }}</div>
                <div class="col-1">{{ __('settings.to') }}</div>
                <div class="col-1">{{ __('settings.min') }}</div>
                <div class="col-1">{{ __('settings.stan') }}</div>
                <div class="col-1">{{ __('settings.max') }}</div>
                <div class="col-1">{{ __('settings.onesel') }}</div>
                <div class="col-1">{{ __('settings.cstock') }}</div>
                <div class="col-1">{{ __('settings.interval') }}</div>
                <div class="col-1">{{ __('settings.delete') }}</div>
            </div>

            <form id="repircedsave">
                @csrf
                <input type="hidden" id="updateprice" name="updateprice" value="">
                <div id="form-placeholder"></div>
                <button id="btn-add" type="button" class="btn btn-primary">{{ __('settings.add') }}</button>
                <button id="btn-save" type="submit" class="btn btn-success">{{ __('settings.save') }}</button>
                <input type="hidden" id="totalRows" name="totalRows">
            </form>

            <hr />

        </div>
    </div>

    </div>

    <div class="tab-pane" id="orion" role="tabpanel" aria-labelledby="orion-shipping">

        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs mb-2">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab"
                                href="#Shipping_Settings">Shippings</a></li>
                        <!--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Messages">Messages</a></li>-->
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile">Profile</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Labels">Labels</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Invoice">Invoice</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Credits">Credits
                            </a></li>
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane active show" id="Shipping_Settings">
                            <div class="card text-white bg-info">
                                <div class="card-header">Shipping Settings</div>
                                <div class="card-body">
                                    <div class="alert alert-success" role="alert">
                                        <h3 class="alert-heading">{{ __('settings.shipheader') }}</h3>
                                        <p>
                                        <h6>{{ __('settings.shiptext') }}</h6>
                                        </p>
                                        <hr>
                                        <p class="mb-0">{{ __('settings.shiptitle') }}</p>
                                    </div>
                                    <hr>

                                    <div class="row clearfix">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="alert alert-warning" role="alert">
                                                        {{ __('settings.shipalert') }}
                                                    </div>
                                                    <table>
                                                        <tr>
                                                            <td><label class="switch">
                                                                    <input type="checkbox" name="globalshipment"
                                                                        id="globalshipment" value=""
                                                                        onclick="this.checked?updatetbl(this.id, 1):updatetbl(this.id, 0)">
                                                                    <span class="slider round"></span>
                                                                </label></td>
                                                            <td>
                                                                <h6>{{ __('settings.globalcancel') }}</h6>
                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <form method="POST" action="{{ route('settings.shippingsave') }}" validate>
                                        @csrf

                                        <div class="row clearfix">
                                            @if (Session::has('success'))
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="alert alert-success alert-dismissible">
                                                        {{ __('settings.success') }}</div>
                                                </div>
                                            @endif
                                            <div class="col sm-12">
                                                <div class="card">
                                                    <div class="accordion  mb-0" id="accordion">
                                                        <div>
                                                            @php
                                                                $prevpath = '';
                                                                $i = 0;
                                                            @endphp
                                                            @foreach ($data['shippings'] as $shipping)
                                                                @if ($prevpath == '' || $prevpath != $shipping->server_name)

                                                                    @if ($prevpath != $shipping->server_name)
                                                        </div>
                                                        @endif

                                                        @php
                                                            $i++;
                                                        @endphp
                                                        <div class="card-header" id="headingOne{{ $i }}">
                                                            <h5 class="mb-0">
                                                                <button class="btn btn-link" type="button"
                                                                    data-toggle="collapse"
                                                                    data-target="#collapseOne{{ $i }}"
                                                                    aria-expanded="true"
                                                                    aria-controls="collapseOne{{ $i }}">
                                                                    <img src="../assets/images/shipping/{{ $shipping->logo_path }}"
                                                                        alt="Avatar" class="w200 rounded mr-2">
                                                                </button>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseOne{{ $i }}"
                                                            class="collapse mb-0 @if ($i == 1) show @endif"
                                                            aria-labelledby="headingOne{{ $i }}"
                                                            data-parent="#accordion">

                                                            @endif
                                                            <div class="row clearfix  ">
                                                                <div class="col sm-12 ml-5 mt-3 mb-3 mr-3">
                                                                    <label class="fancy-checkbox margin-1 mt-3 ">
                                                                        <input type="radio" name="shipping_sett"
                                                                            id="shipping_sett"
                                                                            value="{{ $shipping->id }}"
                                                                            class="checkbox-tick" style="zoom: 150%"
                                                                            required
                                                                            onclick="updatetbl('shipping',this.value)">
                                                                        <span>
                                                                            <img src="../assets/images/shipping/{{ $shipping->company_logo }}"
                                                                                alt="Avatar" class="w100 rounded mr-2">
                                                                            <!--<span>{{ $shipping->server_name }}</span>-->
                                                                            Delivery in
                                                                            {{ $shipping->delivery_timeline }}
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @php
                                                                $prevpath = $shipping->server_name;
                                                            @endphp

                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="Messages">
                            <div class="card text-white bg-green">
                                <div class="card-header">Message Settings</div>
                                <div class="card-body">
                                    <form>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Default Country</label>
                                                    <select class="form-control">
                                                        <option selected="selected">USA</option>
                                                        <option>United Kingdom</option>
                                                        <option>India</option>
                                                        <option>French</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Date Format</label>
                                                    <select class="form-control">
                                                        <option value="d/m/Y">15/05/2016</option>
                                                        <option value="d.m.Y">15.05.2016</option>
                                                        <option value="d-m-Y">15-05-2016</option>
                                                        <option value="m/d/Y">05/15/2016</option>
                                                        <option value="Y/m/d">2016/05/15</option>
                                                        <option value="Y-m-d">2016-05-15</option>
                                                        <option value="M d Y">May 15 2016</option>
                                                        <option selected="selected" value="d M Y">15 May 2016
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Timezone</label>
                                                    <select class="form-control">
                                                        <option>10:45am Chicago (GMT-6)</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Default Language</label>
                                                    <select class="form-control">
                                                        <option selected="selected">English</option>
                                                        <option>Russian</option>
                                                        <option>Arabic</option>
                                                        <option>French</option>
                                                        <option>Hindi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Currency Code</label>
                                                    <select class="form-control">
                                                        <option selected="selected">USD</option>
                                                        <option>Pound</option>
                                                        <option>EURO</option>
                                                        <option>Ringgit</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Currency Symbol</label>
                                                    <input class="form-control" value="$" type="text">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 text-right m-t-20">
                                                <button type="button"
                                                    class="btn btn-primary">{{ __('settings.saves') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="Profile">
                            <div class="card text-white bg-pink">
                                <div class="card-header">{{ __('settings.profiles') }}</div>
                                <div class="card-body">

                                    <div class="col-6">
                                        <div class="alert alert-info" role="alert">
                                            <h3 class="alert-heading">{{ __('settings.wareaddress') }}</h3>
                                            <p>
                                            <h6 id="s_fulladdress"></h6>
                                            </p>
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <p id="s_address1"></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p id="s_city"></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p id="s_state"></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p id="s_country_code"></p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <hr>

                                    </div>

                                    <form id="storefrm" method="POST" action="{{ route('settings.updatestore') }}">
                                        @csrf
                                        <div class="row">
                                            <input type="hidden" name="ustore_id" id="ustore_id">


                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label>Company Name <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="text" value="" id="company"
                                                        name="company">
                                                </div>
                                            </div>
                                            <!--<div class="col-md-6 col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>Amazon Store Name</label>
                                                                            <input class="form-control" value="Amazon Store Name" type="text">
                                                                        </div>
                                                                    </div>-->
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label>Contact Person</label>
                                                    <input class="form-control" value="" type="text" id="contact"
                                                        name="contact">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12 form-group demo-masked-input">
                                                <div class="form-group">
                                                    <label>Mobile Number <span class="text-danger">*</span></label>
                                                    <input class="form-control mobile-phone-number" value="" type="text"
                                                        id="mobile_number" name="mobile_number">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea class="form-control" placeholder=""
                                                        aria-label="With textarea" id="address" name="address"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 form-group demo-masked-input">
                                                <div class="form-group">
                                                    <label>Email <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="icon-envelope"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control email"
                                                            value="support@orioneffect.com" type="email" id="email"
                                                            name="email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Website Url</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="icon-globe"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control" placeholder=""
                                                            id="weburl" name="weburl">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <label>{{ __('settings.country') }}</label>
                                                    <select class="form-control" id="country_id" name="country_id"
                                                        onchange="load_state_list(this.value)">
                                                        <option value="">{{ __('settings.seco') }}</option>
                                                        @isset($maindata['country'])
                                                            @foreach ($maindata['country'] as $country)
                                                                <option value="{{ $country->id }}">
                                                                    {{ $country->country_name }}</option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <label>{{ __('settings.stapro') }}</label>
                                                    <select class="form-control" id="state_id" name="state_id">
                                                        <option value="">{{ __('settings.sestate') }}</option>
                                                        @isset($maindata['state'])
                                                            @foreach ($maindata['state'] as $state)
                                                                <option value="{{ $state->id }}">
                                                                    {{ $state->state_name }}</option>
                                                            @endforeach
                                                        @endisset
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <input class="form-control" value="" type="text" id="city"
                                                        name="city">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <label>Postal Code</label>
                                                    <input class="form-control" value="" type="text" id="postalcode"
                                                        name="postalcode">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 form-group demo-masked-input">
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <input class="form-control phone-number" value="" type="text"
                                                        id="sphonenumber" name="phone_number">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 form-group demo-masked-input">
                                                <div class="form-group">
                                                    <label>Fax</label>
                                                    <input class="form-control phone-number" value="" type="text" id="fax"
                                                        name="fax">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 text-right m-t-20">
                                                <button type="submit"
                                                    class="btn btn-primary">{{ __('settings.saves') }}</button>
                                            </div>
                                        </div>
                                    </form>







                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="Labels">
                            <div class="card text-white bg-cyan">
                                <div class="card-header">{{ __('settings.warlab') }}</div>
                                <div class="card-body">
                                    <form>


                                        <div class="col-12">
                                            <div class="card">
                                                <div class="body">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-md-3 col-sm-12">
                                                            <div class="input-group">
                                                                <input type="text" id="searchval" class="form-control"
                                                                    placeholder="Search any Orion Order Id, Amazon Order Id">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12">
                                                            <a href="javascript:search();"
                                                                class="btn btn-sm btn-primary btn-block"
                                                                title="">Search</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-custom spacing8">
                                                    <thead>
                                                        <tr>
                                                            <th>Orion Order Id</th>
                                                            <th>Order Id</th>
                                                            <th>Upload Date</th>
                                                            <th>Download</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="data-wrapper">
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="5" class="auto-load text-center displaynone">
                                                                <div>
                                                                    <svg version="1.1" id="L9"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                                        y="0px" height="60" viewBox="0 0 100 100"
                                                                        enable-background="new 0 0 0 0"
                                                                        xml:space="preserve">
                                                                        <path fill="#000"
                                                                            d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                                                            <animateTransform attributeName="transform"
                                                                                attributeType="XML" type="rotate" dur="1s"
                                                                                from="0 50 50" to="360 50 50"
                                                                                repeatCount="indefinite" />
                                                                        </path>
                                                                    </svg>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" id="nextload">
                                                                <button type="button" href="#a"
                                                                    class="btn btn-primary btn-lg btn-block mb-3"
                                                                    onclick="infinteLoadMore()">{{ __('masterlang.showmore') }}</button>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>




                                    </form>
                                </div>
                            </div>
                        </div>











                        <div class="tab-pane" id="Invoice">
                            <div class="card text-white bg-purple">
                                <div class="card-header">{{ __('settings.') }}Invoice Settings</div>
                                <div class="card-body">
                                    <form>
                                        <div class="row">
                                            <div class="row clearfix">
                                                <div class="col-lg-12">
                                                    <div class="table-responsive invoice_list mb-4">

                                                        <table class="table table-hover table-custom spacing8">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 20px;">#</th>
                                                                    <th style="width: 200px;">
                                                                        {{ __('settings.type') }}</th>
                                                                    <th style="width: 200px;">
                                                                        {{ __('settings.invoice_number') }}</th>
                                                                    <th style="width: 100px;">
                                                                        {{ __('settings.amount') }}</th>
                                                                    <th style="width: 100px;">
                                                                        {{ __('settings.status') }}</th>
                                                                    <th style="width: 110px;">
                                                                        {{ __('settings.action') }}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                @php $norec=0; @endphp
                                                                @isset($data['invoice_ship'])
                                                                    @foreach ($data['invoice_ship'] as $invoiceship)
                                                                        @php $norec++; @endphp
                                                                        <tr>
                                                                            <td>
                                                                                <span>{{ $norec }}</span>
                                                                            </td>
                                                                            <td>
                                                                                <div class="d-flex align-items-center">
                                                                                    <div class="avtar-pic w35 bg-indigo"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="top" title="Shippping">
                                                                                        <span>S</span>
                                                                                    </div>
                                                                                    <div class="ml-3">
                                                                                        {{ $invoiceship->credit_type_text }}
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td><strong>{{ $invoiceship->invoice_no }}</strong>
                                                                            </td>
                                                                            <td>{{ $invoiceship->amount }}</td>
                                                                            <td>
                                                                                @if ($invoiceship->status == '0')
                                                                                    <span
                                                                                        class="badge badge-warning ml-0 mr-0">{{ __('settings.pending') }}</span>
                                                                                @else
                                                                                    <span
                                                                                        class="badge badge-success ml-0 mr-0">{{ __('settings.paid') }}</span>
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                <a href="{{ route('settings.invoicedetails') }}"
                                                                                    title="">
                                                                                    <button type="button"
                                                                                        class="btn btn-sm btn-default "
                                                                                        title="Print" data-toggle="tooltip"
                                                                                        data-placement="top"><i
                                                                                            class="icon-printer"></i></button>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endisset


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        <div class="tab-pane" id="Credits">
                            <div class="row clearfix">
                                <div class="col-12">
                                    <div class="card text-white bg-orange">
                                        <div class="card-header">{{ __('settings.credits') }}</div>
                                        <div class="card-body">

                                            <h5 class="card-title">{{ __('settings.paydetails') }}</h5>
                                            <p class="card-text">
                                            <div class="d-flex align-items-center">
                                                <div class="avtar-pic w35" data-toggle="tooltip" data-placement="top"
                                                    title="Subscription Payment"><span><img
                                                            src="../assets/images/mastercard2.png" data-toggle="tooltip"
                                                            data-placement="top" title="Payment Logo"
                                                            class="w35 h35 rounded"></span>
                                                </div>
                                                <div class="ml-3">
                                                    **** **** **** 2773 Expires in 7/2024

                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#changecreditcardshipping"
                                                        data-whatever="changecreditcardshipping">Change Credit
                                                        Card</button>
                                                    <div class="modal fade" id="changecreditcardshipping" tabindex="-1"
                                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"
                                                                        id="changecreditcardshipping">
                                                                        {{ __('settings.bankdet') }}</h5>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form>
                                                                        <div class="form-group">
                                                                            <label for="ccardnumber"
                                                                                class="col-form-label">{{ __('settings.ccnum') }}</label>
                                                                            <input type="text" class="form-control"
                                                                                id="ccardnumber">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="expiredate"
                                                                                class="col-form-label">{{ __('settings.epdate') }}</label>
                                                                            <input type="text" class="form-control"
                                                                                id="cexpdate">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="CVVV"
                                                                                class="col-form-label">{{ __('settings.cvv') }}</label>
                                                                            <input type="text" class="form-control"
                                                                                id="CVVV">
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">{{ __('settings.close') }}</button>
                                                                    <button type="submit"
                                                                        class="btn btn-primary">{{ __('settings.save') }}</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    </p>

                                                </div>
                                            </div> <br>
                                            <hr>
                                            <h4><span class="badge badge-success"><strong>Your Total Shipping
                                                        Credit :
                                                        $ 10</strong></span></h4>
                                            <h4><span class="badge badge-danger"><strong>You Need To Add Credit : $
                                                        10</strong></span></h4>
                                            <hr>
                                            <br>
                                            <div class="row clearfix">
                                                <div class="col-lg-12">
                                                    <table>
                                                        <thead>
                                                            <th style="width: 15%;">Add Credit (USD $)</th>
                                                            <th style="width: 15%;">Store Name</th>
                                                            <th style="width: 15%;">Order ID</th>
                                                            <th style="width: 15%;">ASIN</th>
                                                            <th style="width: 30%;">Product Name</th>
                                                            <th style="width: 10%;">Price</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <select name="addbalance" id="addbalance"
                                                                            class="form-control show-tick" required>
                                                                            <option value="20" Selected>$ 20
                                                                            </option>
                                                                            <option value="25">$ 25</option>
                                                                            <option value="35">$ 35</option>
                                                                            <option value="40">$ 40</option>
                                                                            <option value="50">$ 50</option>
                                                                            <option value="75">$ 75</option>
                                                                            <option value="100">$ 100</option>
                                                                            <option value="150">$ 150</option>
                                                                            <option value="200">$ 200</option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <select name="orderid1" id="orderid1"
                                                                            class="form-control show-tick" required>
                                                                            <option value="1" Selected>Immanuel
                                                                                Store
                                                                            </option>
                                                                            <option value="2">Lacin s Store</option>
                                                                            <option value="3">Angelo</option>
                                                                            <option value="4">NoNameSus</option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <select name="orderid1" id="orderid1"
                                                                            class="form-control show-tick" required>
                                                                            <option value="1" Selected>Orderid
                                                                                115-5468-12564</option>
                                                                            <option value="2">Orderid 115-152-12564
                                                                            </option>
                                                                            <option value="3">Orderid 705-5468-8963
                                                                            </option>
                                                                            <option value="4">Orderid 190-2564-1460
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" value=""
                                                                            placeholder="">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" value=""
                                                                            placeholder="">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" value=""
                                                                            placeholder="">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><button type="submit" class="btn btn-primary">Add
                                                                        Credit</button></td>
                                                            </tr>
                                                        </tbody>

                                                    </table>

                                                </div>
                                            </div>

                                            <br>
                                            <hr>
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <th style="width: 15%;">Added Credit (USD $)</th>
                                                        <th style="width: 15%;">Store Name</th>
                                                        <th style="width: 15%;">Order ID</th>
                                                        <th style="width: 15%;">ASIN</th>
                                                        <th style="width: 30%;">Product Name</th>
                                                        <th style="width: 10%;">Price</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>35</td>
                                                            <td>Lacin's Store</td>
                                                            <td>Orderid 115-5468-12564</td>
                                                            <td>Orderid 115-5468-12564</td>
                                                            <td>Lacin Extra Soft Pillow</td>
                                                            <td>25</td>
                                                        </tr>
                                                        <tr>
                                                            <td>25</td>
                                                            <td>Immanuel's Store</td>
                                                            <td>Orderid 115-152-12564</td>
                                                            <td>Orderid 115-5468-12564</td>
                                                            <td>Logitech Mouse</td>
                                                            <td>25</td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>










                    </div>
                </div>
            </div>
        </div>





    </div>


    <div class="tab-pane" id="more" role="tabpanel" aria-labelledby="more">

        <div class="alert alert-success" role="alert">
            <h3 class="alert-heading">{{ __('settings.posheader') }}</h3>
            <p>
            <h6>{{ __('settings.postext') }}</h6>
            </p>
        </div>
        <hr>


        <table table table-hover>
            <tbody>
                <tr>
                    <td>
                        <h4><span class="badge badge-success">{{ __('settings.post') }}</span></h4>
                    </td>

                    <td>
                        <div class="form-group">
                            <select name="postsel" id="postsel" class="form-control show-tick" required>
                                <option value="" Selected>{{ __('settings.postselect') }}</option>
                                <option value="15">{{ __('settings.15') }}</option>
                                <option value="30">{{ __('settings.30') }}</option>
                                <option value="45">{{ __('settings.45') }}</option>
                                <option value="60">{{ __('settings.60') }}</option>
                                <option value="90">{{ __('settings.90') }}</option>
                                <option value="120">{{ __('settings.2h') }}</option>
                                <option value="180">{{ __('settings.3h') }}</option>
                                <option value="360">{{ __('settings.6h') }}</option>
                                <option value="720">{{ __('settings.12h') }}</option>
                                <option value="1440">{{ __('settings.24h') }}</option>
                            </select>
                        </div>
                    </td>
                    <td><button id="btn-ad" onclick="savepotpone()" type="button"
                            class="btn btn-success">{{ __('settings.save') }}</button>

                        &nbsp;
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rollbacktr">
                            {{ __('settings.rollsave') }}
                        </button>



                    </td>
                </tr>


                <!--<tr>
                                <td><h4><span class="badge badge-danger">{{ __('settings.rollbackpost') }}</span></h4></td>
                                <td>
                                <div class="form-group">
                                <select name="roll" id="rollpost" class="form-control show-tick" required>
                                <option value="0" Selected>{{ __('settings.rollback') }}</option>
                                <option value="insert">{{ __('settings.insert') }}</option>
                                <option value="update">{{ __('settings.update') }}</option>
                                <option value="delete">{{ __('settings.delete') }}</option>
                                <option value="all">{{ __('settings.all') }}</option>
                                </select>
                                </div>
                                </td>
                                <td>-->
                <!-- Button trigger modal -->

                <!-- Modal -->
                <div class="modal fade" id="rollbacktr" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">
                                    {{ __('settings.roltrans') }}
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h3>{{ __('settings.attention') }}</h3><br>
                                <h6>{{ __('settings.rollmodtext') }}</h6>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">{{ __('settings.cancel') }}</button>
                                <button type="button" class="btn btn-success"
                                    onclick=rollback(1,2)>{{ __('settings.rollbackall') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                                </td>
                                </tr>
                                -->
            </tbody>
        </table>


        <br>
        <hr>

        <div class="col-lg-8">
            <div class="table-responsive">
                <h5>
                    {{ __('settings.queuelist') }}
                </h5>
                <table class="table table-hover">
                    <thead>
                        <th style="width: 15%;">{{ __('settings.asin') }}</th>
                        <th style="width: 15%;">{{ __('settings.sync') }}</th>
                        <th style="width: 15%;">{{ __('settings.discard') }}</th>
                    </thead>
                    <tbody id="queue-wrapper">
                    </tbody>
                </table>
            </div>
        </div>

    </div>





    </div>


@stop

@section('page-styles')

    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/nouislider/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
    <style>
        .demo-card label {
            display: block;
            position: relative;
        }

        .demo-card .col-lg-4 {
            margin-bottom: 30px;
        }

    </style>

@stop

@section('page-script')

    <script src="{{ asset('assets/bundles/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/ui/dialogs.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <!-- Bootstrap Colorpicker Js -->
    <script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
    <!-- Input Mask Plugin Js -->
    <script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="{{ asset('assets/vendor/nouislider/nouislider.js') }}"></script>
    <!-- noUISlider Plugin Js -->
    <script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/advanced-form-elements.js') }}"></script>
    <script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>

    <script>
        function tab_storeinfo(tabno) {

            $("#currenttab").val(tabno);

            country_id = $("#country_info").val();
            store_id = $("#store_main").val();
            if (tabno == '1') { //Fly
                $("#storelist_onload").hide();
                $("#countrylist_onload").hide();
            } else if (tabno == '2') { //store
                load_store_list(country_id);
                storeinfo_loadstore(store_id);
                $("#countrylist_onload").show();
                $("#storelist_onload").show();
            } else if (tabno == '3') { //Messages
                $("#storelist_onload").hide();
                $("#countrylist_onload").hide();
            } else if (tabno == '4') { //filters
                load_store_list(country_id);
                loadstore(store_id);
                $("#countrylist_onload").show();
                $("#storelist_onload").show();
            } else if (tabno == '5') { //Billing
                $("#storelist_onload").hide();
                $("#countrylist_onload").hide();
            } else if (tabno == '6') { //Credits
                $("#storelist_onload").hide();
                $("#countrylist_onload").hide();
            } else if (tabno == '7') { //prices
                $("#form-placeholder").html('');
                load_store_list(country_id);
                pricer_loadstore(store_id);
                $("#countrylist_onload").show();
                $("#storelist_onload").show();
            } else if (tabno == '8') { //Shipment
                //$("#storelist_onload").hide();
                //$("#countrylist_onload").hide();
                load_state_list(country_id);
                load_store_list(country_id);
                ship_loadstore(store_id);
                $("#countrylist_onload").show();
                $("#storelist_onload").show();
            } else if (tabno == '9') { //Postpone
                $("#storelist_onload").hide();
                $("#countrylist_onload").hide();
                getqueue();

            }
        }


        function filters_update() {
            var store_id = $("#store_main").val();
            var ENDPOINT = "{{ url('/') }}";
            if (parseInt(store_id) == 0) {
                alert('Please select a store')
                return false;
            }
            $.ajax({
                    url: ENDPOINT + "/settings/filters_update?store_id=" + store_id,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    swal("Updated!", "Data successfully Updated!", "success");
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }


        function updatetbl(optionid, optionid_val) {

            var storeid = $("#store_main").val();
            var ENDPOINT = "{{ url('/') }}";
            if (parseInt(storeid) == 0) {
                alert('Please select a store')
                return false;
            }
            $.ajax({
                    url: ENDPOINT + "/settings/updatetable?optionid=" + optionid + "&optionid_val=" + optionid_val +
                        "&storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    //swal("Updated!", "Data successfully Updated!", "success");
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function updateusertbl(optionid, optionid_val) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/updateusertable?optionid=" + optionid + "&optionid_val=" + optionid_val,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {})
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function loadstore(storeid) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/filterbystore?storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    if (response.only_fba_amazon_products != null && response.only_fba_amazon_products != "") {
                        $("#fbamazonpr").prop('checked', true);
                        $("#onlyfba").val(response.only_fba_amazon_products);
                    } else {
                        $("#fbamazonpr").prop('checked', false);
                    }
                    if (response.chinese_sellers_in_the_sale_store != null && response
                        .chinese_sellers_in_the_sale_store != "") {
                        $("#chinise").prop('checked', true);
                        $("#chinese").val(response.chinese_sellers_in_the_sale_store);
                    } else {
                        $("#chinise").prop('checked', false);
                    }
                    if (response.amazon_sellers_in_the_sale_store != null && response
                        .amazon_sellers_in_the_sale_store != "") {
                        $("#amzown").prop('checked', true);
                        $("#amzseller").val(response.amazon_sellers_in_the_sale_store);
                    } else {
                        $("#amzown").prop('checked', false);
                    }
                    if (response.products_from_my_own_stores != null && response.products_from_my_own_stores != "") {
                        $("#myown").prop('checked', true);
                        $("#myownn").val(response.products_from_my_own_stores);
                    } else {
                        $("#myown").prop('checked', false);
                    }
                    if (response.stores_i_ve_added != null && response.stores_i_ve_added != "") {
                        $("#sellerid").prop('checked', true);
                        $("#sellerids").val(response.stores_i_ve_added);
                    } else {
                        $("#sellerid").prop('checked', false);
                    }
                    if (response.trademark_protected_products != null && response.trademark_protected_products != "") {
                        $("#trademark").prop('checked', true);
                        $("#trademark1").val(response.trademark_protected_products);
                    } else {
                        $("#trademark").prop('checked', false);
                    }
                    if (response.discounted_products != null && response.discounted_products != "") {
                        $("#discount").prop('checked', true);
                        $("#discount1").val(response.discounted_products);
                    } else {
                        $("#discount").prop('checked', false);
                    }
                    if (response.cannot_be_sent_to_the_address != null && response.cannot_be_sent_to_the_address !=
                        "") {
                        $("#cantaddress").prop('checked', true);
                        $("#cantadres").val(response.cannot_be_sent_to_the_address);
                    } else {
                        $("#cantaddress").prop('checked', false);
                    }
                    if (response.missing_shipping_info_products != null && response.missing_shipping_info_products !=
                        "") {
                        $("#misship").prop('checked', true);
                        $("#misshipping").val(response.missing_shipping_info_products);
                    } else {
                        $("#misship").prop('checked', false);
                    }
                    if (response.unavailable_products != null && response.unavailable_products != "") {
                        $("#unavail").prop('checked', true);
                        $("#unavailable").val(response.unavailable_products);
                    } else {
                        $("#unavail").prop('checked', false);
                    }
                    if (response.no_import_fee != null && response.no_import_fee != "") {
                        $("#noimport").prop('checked', true);
                        $("#nofee").val(response.no_import_fee);
                    } else {
                        $("#noimport").prop('checked', false);
                    }
                    if (response.same_brand_and_seller_name != null && response.same_brand_and_seller_name != "") {
                        $("#samebrand").prop('checked', true);
                        $("#brand").val(response.same_brand_and_seller_name);
                    } else {
                        $("#samebrand").prop('checked', false);
                    }
                    if (response.prime_sellers_in_the_sale_store != null && response.prime_sellers_in_the_sale_store !=
                        "") {
                        $("#primeseller").prop('checked', true);
                        $("#primesel").val(response.prime_sellers_in_the_sale_store);
                    } else {
                        $("#primeseller").prop('checked', false);
                    }
                    $("#maxseller").val(response.max_number_of_sellers);
                    $("#minseller").val(response.min_number_of_sellers);
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }


        function load_state_list(country) {
            var ENDPOINT = "{{ url('/') }}";
            $("#state_id").html('');
            $.ajax({
                    url: ENDPOINT + "/settings/loadstate_list?country_id=" + country,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    response.forEach(function(val, key) {
                        var selected = '';
                        $("#state_id").append('<option value="' + val.id + '"  ' + selected + '  >' + val
                            .state_name + '</option>');
                    });
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function getStates(country) {
            var userstate = '{{ $data['user']->state }}';
            $("#storeinfo_storename").html('');
            $.ajax({
                url: "{{ url('api/fetch-states') }}",
                type: "POST",
                data: {
                    country_id: country,
                    _token: '{{ csrf_token() }}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#storeinfo_storename').html(
                        '<option value="">{{ __('masterlang.select_state') }} *</option>');
                    $.each(result.states, function(key, value) {
                        var selected = '';
                        if (userstate == value.id) {
                            selected = 'selected';
                        }
                        $("#storeinfo_storename").append('<option value="' + value.id + '"  ' +
                            selected + '  >' + value.state_name + '</option>');
                    });
                }
            });
        }






        function load_store_list(country) {
            var ENDPOINT = "{{ url('/') }}";
            $("#store_main").html('');
            $.ajax({
                    url: ENDPOINT + "/settings/loadstore_list?country_id=" + country,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    response.forEach(function(val, key) {
                        var selected = '';
                        $("#store_main").append('<option value="' + val.id + '"  ' + selected + '  >' + val
                            .store_name + '</option>');
                    });

                    if ($("#currenttab").val() == 2) {
                        storeinfo_loadstore($("#store_main").val());
                    } else if ($("#currenttab").val() == 4) {
                        loadstore($("#store_main").val());
                    } else if ($("#currenttab").val() == 7) {
                        storeinfo_loadstore($("#store_main").val());
                        pricer_loadstore($("#store_main").val());
                    } else if ($("#currenttab").val() == 8) {
                        ship_loadstore($("#store_main").val());
                        ship_loadstore_shipping($("#store_main").val());
                    }

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function ship_loadstore(storeid) {
            var store_id = $("#store_main").val();
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/filterbystore?storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("input[name=shipping_sett][value='" + response.shipping + "']").prop('checked', true);
                    if (response.globalshipment == 1) {
                        $("#globalshipment").prop('checked', true);
                    }
                    $("#ustore_id").val(response.id);
                    $("#company").val(response.company);
                    $("#contact").val(response.contact);
                    $("#mobile_number").val(response.mobile_number);
                    $("#address").val(response.address);
                    $("#email").val(response.email);
                    $("#weburl").val(response.weburl);
                    $("#country_id").val(response.country_id);
                    $("#state_id").val(response.state_id);
                    $("#city").val(response.city);
                    $("#postalcode").val(response.postalcode);
                    $("#sphonenumber").val(response.phone_number);
                    $("#fax").val(response.fax);
                    ship_loadstore_shipping(store_id);


                    console.log(response.shipping);
                    console.log(response);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function ship_loadstore_shipping(storeid) {
            var a1 = '';
            var a2 = '';
            var city = '';
            var state = '';
            var country = '';
            var code = '';

            var store_id = $("#store_main").val();
            var ENDPOINT = "{{ url('/') }}";

            $.ajax({
                    url: ENDPOINT + "/settings/shipfilterbystore?storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    if (response.address1 != null) {
                        a1 = response.address1
                    };
                    if (response.address2 != null) {
                        a2 = response.address2
                    };
                    if (response.city != null) {
                        city = response.city
                    };
                    if (response.state_name != null) {
                        state = response.state_name
                    };
                    if (response.country_name != null) {
                        country = response.country_name
                    };
                    if (response.postalcode != null) {
                        code = response.postalcode
                    };

                    full = a1 + ", " + a2 + ", " + city + ", " + state + ", " + country + ", " + code;
                    $("#s_fulladdress").html(full);
                    address = a1 + ", " + a2;
                    $("#s_address1").html(address);
                    $("#s_city").html(city);
                    $("#s_state").html(state);
                    postalcode = country + ", " + code;
                    $("#s_country_code").html(postalcode);


                    console.log(response.shipping);
                    console.log(response);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }



        function storeinfo_loadstore(storeid) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/filterbystoreinfo?storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#updatestore").val(response.id);
                    $("#updateprice").val(response.id);
                    $("#maxstock").val(response.stock);
                    $("#amazon_commission").val(response.amazon_commission);
                    $("#currency_update_o").val(response.currency_update_o);
                    if (response.currency_update_o == 'F') {
                      $("#currency_update_t").show();
                      $("#currency_update_a").hide();
                      $("#currency_update_c").hide();
                    } else if (response.currency_update_o == 'A') {
                        $("#currency_update_a").show();
                        $("#currency_update_c").show();
                        $("#currency_update_t").hide();
                    }
                    $("#currency_update_t").val(response.currency_update_t);
                    $("#currency_update_a").html(response.currency_update_a);
                    $("#currency_update_c").html(response.currency_code);

                    if (response.sales_tax_c == '1') {
                        $("#sales_tax_c").prop('checked', true);
                        $("#sales_tax_t").show();
                    } else {
                        $("#sales_tax_c").prop('checked', false);
                        $("#sales_tax_t").hide();
                    }
                    $("#sales_tax_t").val(response.sales_tax_t);

                    if (response.import_fee_c == '1') {
                        $("#import_fee_c").prop('checked', true);
                        $("#import_fee_t").show();
                    } else {
                        $("#import_fee_c").prop('checked', false);
                        $("#import_fee_t").hide();
                    }
                    $("#import_fee_t").val(response.import_fee_t);

                    if (response.restricted_keywords == '1') {
                        $("#restricted_keywords").prop('checked', true);
                    } else {
                        $("#restricted_keywords").prop('checked', false);
                    }
                    if (response.restricted_products == '1') {
                        $("#restricted_products").prop('checked', true);
                    } else {
                        $("#restricted_products").prop('checked', false);
                    }
                    if (response.blacklist == '1') {
                        $("#blacklist").prop('checked', true);
                    } else {
                        $("#blacklist").prop('checked', false);
                    }
                    if (response.whitelist == '1') {
                        $("#whitelist").prop('checked', true);
                    } else {
                        $("#whitelist").prop('checked', false);
                    }
                    if (response.common_pools_for_brand == '1') {
                        $("#common_pools_for_brand").prop('checked', true);
                    } else {
                        $("#common_pools_for_brand").prop('checked', false);
                    }
                    if (response.common_pools_for_product == '1') {
                        $("#common_pools_for_product").prop('checked', true);
                    } else {
                        $("#common_pools_for_product").prop('checked', false);
                    }
                    if (response.handling_time_c == '1') {
                        $("#handling_time_c").prop('checked', true);
                        $("#handling_time_t").show();
                    } else {
                        $("#handling_time_c").prop('checked', false);
                        $("#handling_time_t").hide();
                    }
                    $("#handling_time_t").val(response.handling_time_t);

                    if (response.order_confirmation == '1') {
                        $("#order_confirmation").prop('checked', true);
                    } else {
                        $("#order_confirmation").prop('checked', false);
                    }
                    if (response.pre_order_confirmation_c == '1') {
                        $("#pre_order_confirmation_c").prop('checked', true);
                        $("#pre_order_confirmation_t").show();
                    } else {
                        $("#pre_order_confirmation_c").prop('checked', false);
                        $("#pre_order_confirmation_t").hide();
                    }
                    $("#pre_order_confirmation_t").val(response.pre_order_confirmation_t);

                    if (response.price_update_flag == '1') {
                        $("#priceupdate").prop('checked', true);
                    } else {
                        $("#priceupdate").prop('checked', false);
                    }
                    if (response.stock_update_flag == '1') {
                        $("#stockupdate").prop('checked', true);
                    } else {
                        $("#stockupdate").prop('checked', false);
                    }
                    if (response.product_removal_flag == '1') {
                        $("#autodeletepr").prop('checked', true);
                    } else {
                        $("#autodeletepr").prop('checked', false);
                    }
                    if (response.pre_order_confirmation == '1') {
                        $("#pre_order_confirmation").prop('checked', true);
                    } else {
                        $("#pre_order_confirmation").prop('checked', false);
                    }
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function update_mainconfig(optionid, optionid_val) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/update_user_main_config?optionid=" + optionid + "&optionid_val=" +
                        optionid_val,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {})
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function store_option(val, main, sub) {
          if (val == 1) {
              if ($("#"+main+"").val() == "A") {
                $("#"+sub+"_t").hide();
                $("#"+sub+"_a").show();
                $("#"+sub+"_c").show();
                $("#"+sub+"_t").prop('required', false);
            } else if ($("#"+main+"").val() == "F") {
                $("#"+sub+"_t").show();
                $("#"+sub+"_a").hide();
                $("#"+sub+"_c").hide();
                $("#"+sub+"_t").prop('required', true);
            }
          } else if (val == 2) {
            console.log(main);
            if ($("#"+main+"").is(':checked')) {
                    $("#" + sub + "").show();
                    $("#" + sub + "").prop('required', true);
                } else {
                    $("#" + sub + "").hide();
                    $("#" + sub + "").prop('required', false);
                }
            }
        }

        function disable_all_msgs(val) {
            if (val > 0) {
                update_mainconfig('enable_free_text_msg', '1');
                $('#msg_when_orion_cannot_reach_amazon_seller_acc').prop('disabled', false);
                $('#msg_when_package_lost').prop('disabled', false);
                $('#msg_when_customer_leaves_negative_feedback').prop('disabled', false);
                $('#msg_when_automatic_bill_payment_failed').prop('disabled', false);
                $('#msg_when_automatic_bill_payment_failed').prop('disabled', false);
            } else if (val == 0) {
                update_mainconfig('enable_free_text_msg', '0');
                update_mainconfig('msg_when_orion_cannot_reach_amazon_seller_acc', '0');
                $('#msg_when_orion_cannot_reach_amazon_seller_acc').prop('checked', false);
                $('#msg_when_orion_cannot_reach_amazon_seller_acc').prop('disabled', true);
                update_mainconfig('msg_when_package_lost', '0');
                $('#msg_when_package_lost').prop('checked', false);
                $('#msg_when_package_lost').prop('disabled', true);
                update_mainconfig('msg_when_customer_leaves_negative_feedback', '0');
                $('#msg_when_customer_leaves_negative_feedback').prop('checked', false);
                $('#msg_when_customer_leaves_negative_feedback').prop('disabled', true);
                update_mainconfig('msg_when_automatic_bill_payment_failed', '0');
                $('#msg_when_automatic_bill_payment_failed').prop('checked', false);
                $('#msg_when_automatic_bill_payment_failed').prop('disabled', true);
            }
        }

        function savephoneno(val) {
            $('#addnewnumber').hide();
            $('#phnumber').show();
            $('#updatenumber').show();
            $('#enable_free_text_msg').prop('disabled', false);

            $('#phoneValidation').hide();
            if (val == '') {
                $('#phoneValidation').show();
                $('#phone_number').val('');
            } else if (val != '') {
                $('#phoneValidation').hide();
                update_mainconfig('phone_number', val);
                $('#changenumber').modal('toggle');
                $('#load_user_main_config').html(val);
                $('#phone_number').val('');
            }

        }

        function appendUserRow(id, user) {
            var html = "<div id=\"opt-row." + id + "\" class=\"form-group row\">\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"price_a" + id +
                "\" name=\"price_a" + id + "\" placeholder=\"Price1\" value=\"" + user.price_a + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"price_b" + id +
                "\" name=\"price_b" + id + "\" placeholder=\"Price2\" value=\"" + user.price_b + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" onblur=\"validateMin(this.value)\" id=\"min" +
                id + "\" name=\"min" + id + "\" placeholder=\"Min Profit\" value=\"" + user.min + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"standard" + id +
                "\" name=\"standard" + id + "\" placeholder=\"Standard\" value=\"" + user.standard + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"max" + id + "\" name=\"max" +
                id + "\" placeholder=\"Maximum\" value=\"" + user.max + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"oneseller" + id +
                "\" name=\"oneseller" + id + "\" placeholder=\"One Seller\" value=\"" + user.oneseller + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"stock" + id + "\" name=\"stock" +
                id + "\" placeholder=\"Critic Stock\" value=\"" + user.stock + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"interval" + id + "\" name=\"interval" +
                id + "\" placeholder=\"Repricer Interval\" value=\"" + user.repricer_interval + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "             <button type=\"button\" onclick=\"delRow(" + id +
                ")\" class=\"btn btn-sm btn-default js-sweetalert\" title=\"Delete\" data-type=\"confirm\"><i class=\"fa fa-trash-o text-danger\"></i></button>\n" +
                "        </div>";
            $("#form-placeholder").append(html);
        }

        function delRow(id) {
            var element = document.getElementById("opt-row." + id);
            element.parentNode.removeChild(element);
        }

        var count = 0;

        $(document).ready(function() {
            $("#btn-add").click(function() {
                appendUserRow(count++, {
                    price_a: "",
                    price_b: "",
                    min: "",
                    standard: "",
                    max: "",
                    oneseller: "",
                    stock: "",
                    repricer_interval:""

                });

                $("#totalRows").val(count);

            });
        });

        $('#repircedsave').submit(function(e) {
            var ENDPOINT = "{{ url('/') }}";

            e.preventDefault();
            var $form = $(this);

            // check if the input is valid using a 'valid' property
            if (!$form.valid) {
                console.log("form invalid");
                //return false;
            }
            //    $("#successmsg").hide();

            var data = $form.serialize();
            console.log(data);

            //return false;
            $.ajax({
                /* the route pointing to the post function */
                url: ENDPOINT + '/settings/update_pricingdetails',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: data + "&store_id=" + $("#store_main").val(),
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function(data) {
                    swal("Updated!", "Data successfully Updated!", "success");
                    return false;
                }
            });
            return false;
        });

        function validateMin(e) {

            if (e < 20 && getCookie("ignore") == "") {
                swal({
                    title: "You have entered less than 20%!",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, Confirm',
                    cancelButtonText: "Dont show this again!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm == true) {
                        swal.close()
                    } else {
                        setCookie("ignore", "1", 1000)
                        swal.close()
                    }
                });

            }

        }

        function setCookie(cname, cvalue, exdays) {
            const d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            let expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            let name = cname + "=";
            let ca = document.cookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function pricer_update_repricer(optionid, optionid_val) {


            var ENDPOINT = "{{ url('/') }}";
            var storeid = $("#store_main").val();
            $.ajax({
                    url: ENDPOINT + "/settings/update_repricer?optionid=" + optionid + "&optionid_val=" + optionid_val +
                        "&storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    //swal("Updated!", "Repricer Method successfully Updated!", "success");
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function pricer_loadstore(storeid) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/pricerloadstore?storeid=" + storeid,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#repricer").val(response.repricer.repricer);
                    $("#interval").val(response.repricer.repricer_interval);
                    parseRepricer(response.pricing_details)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function parseRepricer(data) {
            $("#form-placeholder").html('');

            count = 0;
            for (i = 0; i < data.length; i++) {
                appendUserRow(count++, {
                    price_a: data[i].price_1,
                    price_b: data[i].price_2,
                    min: data[i].min_profit,
                    standard: data[i].standard,
                    max: data[i].maximum,
                    oneseller: data[i].one_seller,
                    stock: data[i].critic_stock,
                    repricer_interval: data[i].repricer_interval
                });
                $("#totalRows").val(count);
            }
        }

        $('#storeinfo_frm').submit(function(e) {

            var ENDPOINT = "{{ url('/') }}";

            e.preventDefault();
            var $form = $(this);

            // check if the input is valid using a 'valid' property
            if (!$form.valid) {
                console.log("form invalid");
                //return false;
            }
            //    $("#successmsg").hide();

            var data = $form.serialize();
            console.log(data);
            $.ajax({
                /* the route pointing to the post function */
                url: ENDPOINT + '/settings/storeinfo_update',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: data,
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function(data) {
                    swal("Updated!", "Data successfully Updated!", "success");
                }
            });
        });

        function save_cclist() {
            var cardno = $('#ccardnumber').val();
            var expdt = $('#expiredate').val();
            var cvv = $('#CVVV').val();
            var re16digit = /^\d{16}$/;
            var re3digit = /^\d{3}$/;
            var wscardno = cardno.replace(/ /g, "");
            $('#ccalert').hide();
            if (cardno == '' || !re16digit.test(wscardno)) {
                $('#ccalert').html("{{ __('settings.please_enter_valid_cc_number') }}");
                $('#ccalert').show();
            } else if (expdt == '') {
                $('#ccalert').html("{{ __('settings.please_enter_valid_exp_date') }}");
                $('#ccalert').show();
            } else if (cvv == '' || !re3digit.test(cvv)) {
                $('#ccalert').html("{{ __('settings.please_enter_valid_cvv') }}");
                $('#ccalert').show();
            } else {
                addcclist(cardno, expdt, cvv);
                $('#ccalert').hide();
                $('#changecreditcardshipping').modal('toggle');
            }
        }

        function addcclist(cardno, expdt, cvv) {
            var ENDPOINT = "{{ url('/') }}";
            var data = {
                cardno: cardno,
                expdt: expdt,
                cvv: cvv,
                _token: "{{ csrf_token() }}"
            }
            $.ajax({
                    url: ENDPOINT + "/settings/add_cclist",
                    datatype: "json",
                    data: data,
                    type: "post",
                })
                .done(function(response) {
                    $("#cclist-wrapper").html('');
                    load_cclist();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {});
        }

        function load_cclist() {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/view_cclist",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#cclist-wrapper").html(response);
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function del_cc(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }, function(isConfirm) {
                if (isConfirm) {
                    var ENDPOINT = "{{ url('/') }}";
                    $.ajax({
                            url: ENDPOINT + "/settings/delete_cc?id=" + id,
                            datatype: "json",
                            type: "get",
                        }).done(function(response) {
                            console.log(response);
                            if (response == 0) {
                                swal("Sorry! cant delete default", {
                                    icon: "warning",
                                });
                            } else {
                                swal("Deleted!", {
                                    icon: "success",
                                });
                            }
                            $("#cclist-wrapper").html('');
                            load_cclist();
                        })
                        .fail(function(jqXHR, ajaxOptions, thrownError) {
                            console.log('Server error occured');
                        });
                }
            });
        }

        function set_default_cc(id) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/update_default_cc?id=" + id,
                    datatype: "json",
                    type: "get",

                })
                .done(function(response) {
                    $("#cclist-wrapper").html('');
                    load_cclist();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function save_credits() {
            var addbalance = $('#addbalance').val();
            var credits_feetype = $('#credits_feetype').val();
            var ENDPOINT = "{{ url('/') }}";
            var data = {
                addbalance: addbalance,
                credits_feetype: credits_feetype,
                _token: "{{ csrf_token() }}"
            }
            $.ajax({
                    url: ENDPOINT + "/settings/add_credits",
                    datatype: "json",
                    data: data,
                    type: "post",
                })
                .done(function(response) {
                    $("#credits-wrapper").html('');
                    load_credits();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {});
        }

        function load_credits() {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/view_credits",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    let totalcreditamt = 0;
                    response.forEach(function(val, key) {
                        totalcreditamt = totalcreditamt + val.add_credit;
                        $("#credits-wrapper").append("<tr><td>" + val.add_credit + "</td><td>" + val
                            .credit_type_text + "</td></tr>");
                    });
                    $("#tot_credit").html('');
                    $("#tot_credit").html(totalcreditamt);
                    console.log(totalcreditamt);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function load_credit_type() {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/view_credit_type",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#credit_type").html(response);
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function load_bill_details() {
            status = 0;
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/bill_details_list?status=" + status,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#bill_payment_list").html(response);
                    if (response == '') {
                        $('#ccpay').html('');
                        $("#bill_payment_list").html('No Pending Payments');
                    }
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function load_bill_history() {
            status = 1;
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/bill_details_list?status=" + status,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#bill_history_list").html(response);
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function load_ccp_list() {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/ccp_list",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#list_cc").html(response);
                    console.log(response);
                    pending_payment();
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function pending_payment() {
            $('#payValidation').hide();

            var items = document.getElementsByName("chkbox");
            var selectedItems = "";
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == "checkbox" && items[i].checked == true) selectedItems += items[i].value + ",";
            }
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/totalpayment?id=" + selectedItems,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#paycc_total").html(response);
                    $("#paycc_totalamt").val(response);
                    $("#paycc_id").val(selectedItems);
                    console.log(response);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        $('#selectall').on("click", function() {
            if ($("#selectall").is(':checked')) {
                $(".exclude_item").prop('checked', true);
            } else {
                $(".exclude_item").prop('checked', false);
            }
            pending_payment();
        });

        $('#paycc').submit(function(e) {
            val = $('#paycc_totalamt').val();
            $('#payValidation').hide();
            if (val == '0') {
                $('#payValidation').show();
                return false;
            }
            var ENDPOINT = "{{ url('/') }}";

            e.preventDefault();
            var $form = $(this);

            // check if the input is valid using a 'valid' property
            if (!$form.valid) {
                console.log("form invalid");
                //return false;
            }
            //    $("#successmsg").hide();

            var data = $form.serialize();
            console.log(data);
            $.ajax({
                /* the route pointing to the post function */
                url: ENDPOINT + '/settings/cc_pay',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: data,
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function(data) {
                    swal("Updated!", "Data successfully Updated!", "success");
                    load_bill_details();
                    load_bill_history();
                    load_ccp_list();
                }
            });
        });

        function sms_no(val) {
            if (val == null) {
                $('#phnumber').hide();
                $('#updatenumber').hide();
                $('#addnewnumber').show();
                $('#enable_free_text_msg').prop('disabled', true);
                $('#msg_when_orion_cannot_reach_amazon_seller_acc').prop('disabled', true);
                $('#msg_when_package_lost').prop('disabled', true);
                $('#msg_when_customer_leaves_negative_feedback').prop('disabled', true);
                $('#msg_when_automatic_bill_payment_failed').prop('disabled', true);
                $('#msg_when_automatic_bill_payment_failed').prop('disabled', true);

            } else {
                $('#addnewnumber').hide();
                $('#phnumber').show();
                $('#updatenumber').show();
            }
        }

        function savepotpone() {

            if ($("#postsel").val() == "") {
                swal("Error!", "Please select a valid value", "error");
                return false;
            }

            var ENDPOINT = "{{ url('/') }}";

            var data = {
                postsel: $("#postsel").val(),
                _token: "{{ csrf_token() }}"
            }

            $.ajax({
                /* the route pointing to the post function */
                url: ENDPOINT + '/settings/postpone',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: data,
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function(data) {
                    swal("Updated!", "Data successfully Updated!", "success");
                }
            });
        }

        function getqueue() {
            var ENDPOINT = "{{ url('/') }}";
            $("#queue-wrapper").html('');
            $.ajax({
                    url: ENDPOINT + "/settings/getqueue",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    response.forEach(function(val, key) {
                        $("#queue-wrapper").append("<tr><td>" + val.asin +
                            "</td><td><button type=\"button\" class=\"btn btn-success\">Committ</button></td><td><button type=\"button\" class=\"btn btn-danger\" onclick=rollback(" +
                            val.id + ",1)>Rollback</button></td></tr>");
                    });
                    console.log(totalcreditamt);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function postpone_details() {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/postpone_details",
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    $("#postsel").val(response.postpone);
                    console.log(response)
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }

        function rollback(id, num) {
            var ENDPOINT = "{{ url('/') }}";
            $.ajax({
                    url: ENDPOINT + "/settings/rollback?id=" + id + "&num=" + num,
                    datatype: "json",
                    type: "get",
                })
                .done(function(response) {
                    getqueue();
                    console.log(response);
                    $("#rollbacktr").modal('hide');
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });
        }
    </script>

@stop
