@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('menus.mailaccounts'))

@section('content')

<div class="row clearfix">

    <div class="col-lg-12 col-md-12">

        <div class="card planned_task">

            <div class="header">

                <h2>{{ __('settings.mailforward') }}</h2>

                <ul class="header-dropdown dropdown">                                

                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                    <li class="dropdown">

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>

                        <ul class="dropdown-menu">

                            <li><a href="javascript:void(0);">Action</a></li>

                            <li><a href="javascript:void(0);">Another Action</a></li>

                            <li><a href="javascript:void(0);">Something else</a></li>

                        </ul>

                    </li>

                </ul>

            </div>

            <div class="body">
            <div class="alert alert-success" role="alert"><strong>{{ __('settings.oemails') }} : oeuser4501@amazon.orioneffect.com</strong></div>
            <br><hr>
               <form method="post" id="transfer">
                    @csrf 

                    <table class="table-responsive  text-white">
                        <thead>
<th>{{ __('settings.buyermail') }}</th>
<th>{{ __('settings.sellermail') }}</th>
<th>{{ __('settings.matchaccount') }}</th>
                        </thead>
                   
                   <tbody>
                        <tr>
                 
                   <td><input type="text" name="buyeremail" class="form-control email" placeholder="Ex: example@example.com" required></td>
                    
                    <td><input type="text" name="selleremail" class="form-control email" placeholder="Ex: example@example.com" required></td>
                    <!--<select class="form-control" id="mail_info" name="mail_info">
                    <option value="buyer" selected="">{{ __('settings.buyermail') }}</option>
                    <option value="seller">{{ __('settings.sellermail') }}</option>
                    </select> -->
                    </td>
                  
                    <td>
                    
                        <select class="form-control" id="store" name="store">
                        <option value="ImmanStore" selected="">ImmanStore</option>
                        <option value="LacinStore">LacinStore</option>
                        <option value="SiriusGlobal">SiriusGlobal</option>
                        <option value="SametBae">SametBae</option>
                        <option value="SametSG">SametSG</option>
                        </select>
                    
                    </td>
                    
                    <td><button type="submit" class="btn btn-primary">{{__('settings.save')}}</button></td>
                    </tr>
                    </tbody>
                    </table>
               </form> 
               <br><hr><br>
                <strong><h3>{{ __('settings.mailaccounts') }}</h3></strong>


<div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable text-white">
                        <thead>
                            <tr>
                            <th>{{ __('settings.buyermail') }}</th>
                            <th>{{ __('settings.sellermail') }}</th>
                            <th>{{ __('settings.matchedstores') }}</th>
                            <th>{{ __('settings.date') }}</th>     
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>{{ __('settings.buyermail') }}</th>
                            <th>{{ __('settings.sellermail') }}</th>
                            <th>{{ __('settings.matchedstores') }}</th>
                            <th>{{ __('settings.date') }}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        @foreach ($list as $li)
<tr>
<td>{{$li->buyermail}}</td>
<td>{{$li->sellermail}}</td>
<td>{{$li->stores}}</td>
<td>{{$li->created_at}}</td>
</tr>
@endforeach

                        </tbody>
                    </table>









            </div>

        </div>

    </div>

</div>

@stop



@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

<link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}">

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
@stop



@section('page-script')

<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/chartist.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/index2.js') }}"></script>

@stop