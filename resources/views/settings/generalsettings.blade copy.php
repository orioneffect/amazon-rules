@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('settings.generalsettings'))

@section('content')

<!-- css and coding area-->
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#fly" role="tab" aria-controls="fly" aria-selected="true">{{__('settings.orion')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#store" role="tab" aria-controls="store" aria-selected="false">{{__('settings.store')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">{{__('settings.messages')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#filters" role="tab" aria-controls="filters" aria-selected="false">{{__('settings.filters')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#billings" role="tab" aria-controls="billing" aria-selected="false">{{__('settings.billings')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#prices" role="tab" aria-controls="prices" aria-selected="false">{{__('settings.prices')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#orion" role="tab" aria-controls="orion" aria-selected="false">{{__('settings.shipping')}}</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#more" role="tab" aria-controls="more" aria-selected="false">{{__('settings.more')}}</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

  <div class="tab-pane active" id="fly" role="tabpanel" aria-labelledby="fly-orion">
 <div class="row clearfix">
            <div class="col-12">
                <div class="card">
                    <div class="body">

                    <div class="col-12">
            <div class="alert alert-success" role="alert">
  <h3 class="alert-heading">{{__('settings.mesheader')}}</h3>
  <p><h6>{{__('settings.description')}}</h6></p>
</div>
<hr>               

            </div>
<!--25k pakete kadar günlük 20k ürün atabilirsin -->
<!--10k pakete kadar günlük 3k tarama 6k uygun ürün -->


<table class="table table-success">

  <tbody>
    <tr>
      <td>    <label class="switch">
                        <input type="checkbox" name="stockupdate" value="1">
                        <span class="slider round"></span>
                    </label></td>
      <td><label class="mb-1 mt-1 h3" for="OrionFly">{{__('settings.activate')}}</label></td>
    </tr>
   
  </tbody>
</table>

</div>
</div>
</div>
</div>
</div>



  
  
<div class="tab-pane" id="store" role="tabpanel" aria-labelledby="store-tab">
 <div class="row clearfix">
            <div class="col-12">
                <div class="card">
                    <div class="body">

                        <label><h3>{{__('settings.storefront')}}</h3></label>

                        <table class="table table-hover">
                        <thead>
    <tr>
    <th style="width: 25%"></th>
    <th style="width: 10%"></th>
    <th style="width: 35%"></th>
    <th style="width: 30%"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{{__('settings.storename')}}</td>
      <td colspan="2"><input type="text" name="storename"  value="" class="form-control" placeholder="{{__('settings.storefront')}} *" required></td>
    </tr>
    <tr>
      <td>{{__('settings.maxstock')}}</td>
      <td colspan="2"><input type="text" name="maxstock"  value="" class="form-control" placeholder="{{__('settings.maxstock')}} *" required></td>
    </tr>
    <tr>
      <td>{{__('settings.comm')}}</td>
      <td><input type="text" name="amzcomission"  value="" class="form-control" placeholder="{{__('settings.comm')}} *" required></td>
    </tr>


    <tr>
      <td>{{__('settings.curr')}}</td>
      <td><div class="form-group">
<select name="currency" id="currency" class="form-control show-tick" required>
<option value="Automatic" Selected>{{__('settings.oto')}}</option>
<option value="Fixed">{{__('settings.fixed')}}</option>
</select>
</div></td>
      <td><input type="text" name="currselect"  value="" class="form-control" placeholder="{{__('settings.parity')}} *" required></td>
    </tr>
   
    <tr>
      <td>{{__('settings.salestax')}}</td>
      <td>
      <label class="switch">
<input type="checkbox" name="salestax" value="1">
<span class="slider round"></span>
</label></td>
<td><input type="text" name="amzcomission"  value="" class="form-control" placeholder="{{__('settings.saletax')}} % *" required> </td>
    </tr>  

<tr>
<td>{{__('settings.impfee')}}</td>
<td> 
<label class="switch">
<input type="checkbox" name="importfee" value="1">
<span class="slider round"></span>
</label></td>
<td><input type="text" name="importfee"  value="" class="form-control" placeholder="{{__('settings.impfees')}} % *" required></td>
</tr>

<tr>
    <td>{{__('settings.reskeyword')}}</td>
    <td>
<label class="switch">
<input type="checkbox" name="reskeywords" value="1">
<span class="slider round"></span>
</label></td>
</tr>

<tr>

<td>{{__('settings.resprod')}}</td>
<td><label class="mb-1 mt-1" for="resproducts"></label>
<label class="switch">
<input type="checkbox" name="resproducts" value="1">
<span class="slider round"></span>
</label></td>
</tr>

<tr>
<td>{{__('settings.brandblack')}}</td>
<td><label class="switch">
<input type="checkbox" name="brandblack" value="1">
<span class="slider round"></span>
</label></td>

</tr>

<tr>

<td>{{__('settings.brandwhite')}}</td>
<td><label class="switch">
<input type="checkbox" name="brandwhite" value="1">
<span class="slider round"></span>
</label></td>
</tr>

<tr>
<td>{{__('settings.combrand')}}</td>
<td><label class="switch">
<input type="checkbox" name="commonbrand" value="1">
<span class="slider round"></span>
</label></td>

</tr>

<tr>
<td>{{__('settings.comprod')}}</td>

<td><label class="switch">
<input type="checkbox" name="commonproduct" value="1">
<span class="slider round"></span>
</label>
</td>

</tr>

<tr>
<td>{{__('settings.handling')}}</td>
<td><label class="switch">
<input type="checkbox" name="handling" value="1">
<span class="slider round"></span>
</label></td>
<td><div class="form-group">
<select name="currency" id="handlingtime" class="form-control show-tick" required>
<option value="Otomatik" Selected>{{__('settings.select')}}</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
</select>
</div></td>

</tr>

<tr><td>{{__('settings.comfirm')}}</td>
<td><label class="switch">
<input type="checkbox" name="autocomfirm" value="1">
<span class="slider round"></span>
</label></td>

</tr>


<tr>
<td>{{__('settings.precomfirm')}}</td>
<td><label class="switch">
<input type="checkbox" name="precomfirm" value="1">
<span class="slider round"></span>
</label></td>
<td><input type="text" name="percomfirmtime"  value="" class="form-control" placeholder="{{__('settings.prehour')}} *" required>    
<span class="slider round"></span></td>

</tr>

<tr><td>{{__('settings.priceup')}}</td>
<td><label class="switch">
<input type="checkbox" name="priceupdate" value="1">
<span class="slider round"></span>
</label></td>
</tr>


<tr>
<td>{{__('settings.stockup')}}</td>
<td><label class="switch">
<input type="checkbox" name="stockupdate" value="1">
<span class="slider round"></span>
</label></td>
</tr>


<tr>
<td>{{__('settings.remove')}}</td>
<td><label class="switch">
<input type="checkbox" name="autodeletepr" value="1">
<span class="slider round"></span>
</label></td>
</tr>


<tr>
<td>{{__('settings.precomfirm')}}</td>
<td><label class="switch">
<input type="checkbox" name="autoaddpr" value="1">
<span class="slider round"></span>
</label></td>

</tr>


</tbody>
</table>

<div class="row clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <button type="button" id="btn" class="btn btn-primary">{{__('settings.save')}}</button>
                    </div>


</div>
</div>
</div>
</div>
</div>
</div>


                                    
  <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">


<div class="alert alert-success" role="alert">
  <h3 class="alert-heading">{{__('settings.mesheader')}}</h3>
  <p><h6>{{__('settings.mestext')}}</h6></p>
  <hr>
  <p class="mb-0">{{__('settings.mestitle1')}}</p>
</div>
<hr>


<div class="card">
  <div class="card-body">

  <table table table-hover table-dark>
<tbody>
  <tr>
<td colspan="2"><div class="alert alert-light" role="alert"><h6>
{{__('settings.phonenumber')}}</h6>
</div></td> <td><span class="badge badge-success"><h6>"nondb 905055965467"</h6></span></td>
<td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changenumber" data-whatever="changenumber">{{__('settings.cnumber')}}</button>
<div class="modal fade" id="changenumber" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="changenumber">{{__('settings.cnumber')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="phonenumber" class="col-form-label">Phone Number:</label>
            <input type="text" class="form-control" id="phonenumber">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('settings.close')}}</button>
        <button type="submit" class="btn btn-primary">{{__('settings.save')}}</button>
      </div>
    </div>
  </div>
</div>


</td>
</tr>
</tbody>
</table>


  <table table table-hover table-dark>
<tbody>
  <tr>
<td><label class="switch">
<input type="checkbox" name="enablemessages" value="1">
<span class="slider round"></span>
</label></td>
<td colspan="2"><h5>Enable free text messages</h5></td>
<td></td>
</tr>

<tr>
<td></td>
<td><label class="switch">
<input type="checkbox" name="cantreach" value="1">
<span class="slider round"></span>
</label></td>
<td>Text me when Orion Effect cannot reach your Amazon Seller account</td>
</tr>

<tr>
<td></td>
<td><label class="switch">
<input type="checkbox" name="lost" value="1">
<span class="slider round"></span>
</label></td>
<td>Text me when package may be lost</td>
</tr>

<tr>
<td></td>
<td><label class="switch">
<input type="checkbox" name="negfeed" value="1">
<span class="slider round"></span>
</label></td>
<td>Text me when customer leaves negative feedback</td>
</tr>

<tr>
<td></td>
<td><label class="switch">
<input type="checkbox" name="outofstock" value="1">
<span class="slider round"></span>
</label></td>
<td>Text me when the product in Amazon.com is running out of stock</td>
</tr>

<tr>
<td></td>
<td><label class="switch">
<input type="checkbox" name="payfail" value="1">
<span class="slider round"></span>
</label></td>
<td>Text me when automatic bill payment failed</td>
</tr>

</tbody>
</table>
</div>
</div>
</div>


  <div class="tab-pane" id="filters" role="tabpanel" aria-labelledby="filters">
    


<div class="alert alert-primary" role="alert">
  <h3 class="alert-heading">{{__('settings.filheader')}}</h3>
  <p><h6>{{__('settings.filtext')}}</h6></p>
  <hr>
  <p class="mb-0">{{__('settings.filtitle1')}}</p>
</div>


<hr>

<div class="card">
  <div class="card-body">
<table table table-hover>
<tbody>
  <tr><td><h6>{{__('settings.fbamz')}}<span class="badge badge-primary">{{__('settings.recomend')}}</span></h6></td><td><label class="switch">
<input type="checkbox" name="fbamazonpr" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="onlyfba" id="onlyfba" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.chinese')}}</h6></td><td><label class="switch">
<input type="checkbox" id="chinise 
"name="chinise" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="chinese" id="chinese" class="form-control show-tick" required>
<option value="Remove">{{__('settings.removee')}}</option>
<option value="OutofStock" Selected>{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.amzseller')}}</h6></td><td><label class="switch">
<input type="checkbox" name="amzown" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="amzseller" id="amzseller" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.mystore')}}</h6></td><td><label class="switch">
<input type="checkbox" name="myown" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="myownn" id="myownn" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.filterstore')}}</h6></td><td><label class="switch">
<input type="checkbox" name="sellerid" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="sellerids" id="sellerids" class="form-control show-tick" required>
<option value="Remove">{{__('settings.removee')}}</option>
<option value="OutofStock" selected>{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.trademark')}}</h6></td><td><label class="switch">
<input type="checkbox" name="trademark" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="trademark1" id="trademark1" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.discount')}}<span class="badge badge-primary">{{__('settings.norecom')}}</span></h6></td><td><label class="switch">
<input type="checkbox" name="discount" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="discount1" id="discount1" class="form-control show-tick" required>
<option value="Remove">{{__('settings.removee')}}</option>
<option value="OutofStock" Selected>{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.cantaddress')}}<span class="badge badge-primary">{{__('settings.recomend')}}</span></h6></td><td><label class="switch">
<input type="checkbox" name="cantaddress" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="cantadres" id="cantadres" class="form-control show-tick" required>
<option value="Remove">{{__('settings.removee')}}</option>
<option value="OutofStock" Selected>{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.misinfo')}}<span class="badge badge-primary">{{__('settings.recomend')}}</span></h6></td><td><label class="switch">
<input type="checkbox" name="misship" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="misshipping" id="misshipping" class="form-control show-tick" required>
<option value="Remove">{{__('settings.removee')}}</option>
<option value="OutofStock" Selected>{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.unavailable')}}<span class="badge badge-primary">{{__('settings.recomend')}}</span></h6></td><td><label class="switch">
<input type="checkbox" name="unavail" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="unavailable" id="unavailable" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.noimport')}}<span class="badge badge-primary">{{__('settings.norecom')}}</span></h6></td><td><label class="switch">
<input type="checkbox" name="noimport" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="nofee" id="nofee" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.sameseller')}}</h6></td><td><label class="switch">
<input type="checkbox" name="samebrand" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="brand" id="brand" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.primeseller')}}</h6></td><td><label class="switch">
<input type="checkbox" name="primeseller" value="1">
<span class="slider round"></span>
</label></td><td><div class="form-group">
<select name="primesel" id="primesel" class="form-control show-tick" required>
<option value="Remove" Selected>{{__('settings.removee')}}</option>
<option value="OutofStock">{{__('settings.outofstock')}}</option>
</select>
</div></td></tr>

<tr><td><h6>{{__('settings.maxseller')}}</h6></td><td><label class="switch">
<input type="checkbox" name="maxsel" value="1">
<span class="slider round"></span>
</label></td>
<td><input type="text" name="maxseller"  value="" class="form-control" placeholder="MaxSeller"></td>
</tr>


<tr><td><h6>{{__('settings.minseller')}}</h6></td><td><label class="switch">
<input type="checkbox" name="minsel" value="1">
<span class="slider round"></span>
</label></td>
<td><input type="text" name="minseller"  value="" class="form-control" placeholder="MinSeller"></td>
</tr>


</tbody>
</table>
</div>
</div>




  </div>
  
  
  
  <div class="tab-pane" id="billings" role="tabpanel" aria-labelledby="billings">Billing</div>
 
 
 
  <div class="tab-pane" id="prices" role="tabpanel" aria-labelledby="prices">
  <div class="row clearfix">
            <div class="col-12">

                    <div class="card">
  <h5 class="card-header">{{__('settings.repheader')}}</h5>
  <div class="card-body">
    <h5 class="card-title">{{__('settings.title1')}}</h5>
    <p class="card-text">{{__('settings.reptext')}}</p>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#profit">
  Profit Maximizer
</button>

<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#dynamic">
Dynamic Profit Generator
</button>

<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hybrid">
Hybrid Profit
</button>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#balanced">
Balanced Sales
</button>

<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agressive">
Aggressive Selling
</button>

<button type="button" class="btn btn-light" data-toggle="modal" data-target="#careful">
Careful Sales
</button>

  </div>
</div>
    <hr/>

<!-- Modal -->
<div class="modal fade" id="profit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('settings.profititle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {{__('settings.profitdesc')}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('settings.close')}}</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="dynamic" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('settings.dynamictitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {{__('settings.dynamicdesc')}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('settings.close')}}</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="hybrid" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('settings.hybridtitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {{__('settings.hybriddesc')}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('settings.close')}}</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="balanced" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('settings.balancedtitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {{__('settings.balancedesc')}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('settings.close')}}</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="agressive" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('settings.aggressivetitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {{__('settings.aggressivedesc')}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('settings.close')}}</button>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="careful" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('settings.carefultitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {{__('settings.carefuldesc')}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{__('settings.close')}}</button>
      </div>
    </div>
  </div>
</div>
        

            <div class="card">
  <div class="card-body">
    <h5 class="card-title">{{__('settings.title2')}}</h5>
    <p class="card-text">
    <div class="form-group">
<select name="currency" id="repmethod" class="form-control show-tick" required>
<option value="Otomatik" Selected>{{__('settings.repmethod')}}</option>
<option value="0">Stop Running Repricer</option>
<option value="1">Profit Maximizer</option>
<option value="2">Dynamic Profit Generator</option>
<option value="3">Hybrid Profit</option>
<option value="4">Balanced Sales</option>
<option value="5">Aggressive Selling</option>
<option value="6">Careful Sales</option>

</select>
</div>



    </p>
    <a href="#" class="btn btn-success">{{__('settings.save')}}</a>
  </div>
</div>
            <hr/>
            
            
<h5>Pricing Rules</h5>
<table>
  <thead>
    <tr>
    <th style="width: 14%">From</th>
    <th style="width: 14%">To</th>
    <th style="width: 17%">Min</th>
    <th style="width: 12%">Stan</th>
    <th style="width: 12%">Max</th>
    <th style="width: 12%">One Seller</th>
    <th style="width: 12%">C Stock</th>
    <th style="width: 12%">PR</th>
</tr>
</thead>
</table>
<div id="form-placeholder"></div>
<button id="btn-add" type="button" class="btn btn-primary">{{__('settings.add')}}</button>
<button id="btn-add" type="button" class="btn btn-success">{{__('settings.save')}}</button>
<hr/>




</div>
</div>


</div>


  <div class="tab-pane" id="orion" role="tabpanel" aria-labelledby="orion-shipping">

  <div class="alert alert-success" role="alert">
  <h3 class="alert-heading">{{__('settings.shipheader')}}</h3>
  <p><h6>{{__('settings.shiptext')}}</h6></p>
  <hr>
  <p class="mb-0">{{__('settings.shiptitle')}}</p>
</div>
<hr>

<div class="row clearfix">
<div class="col-12">
<div class="card">
<div class="card-body">
<div class="alert alert-warning" role="alert">
{{__('settings.shipalert')}}
</div>
<table>
<tr><td><label class="switch">
<input type="checkbox" name="cangloshi" value="1">
<span class="slider round"></span>
</label></td><td><h6>{{__('settings.globalcancel')}}</h6></td></tr>

</table>

</div>
</div>
</div>
</div>


  <form  method="POST" action="{{ route('settings.shippingsave') }}" validate>
@csrf

<div class="row clearfix">
    @if (Session::has('success'))
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="alert alert-success alert-dismissible">{{ __('settings.success') }}</div>
    </div>
    @endif
    <div class="col sm-12">
        <div class="card">
            <div class="accordion  mb-0" id="accordion">
                <div>
                    @php
                    $prevpath="";
                    $i=0;
                    @endphp
                    @foreach ($shippings as $shipping)
                        @if ($prevpath=="" || $prevpath!=$shipping->server_name)

                        @if ($prevpath!=$shipping->server_name)
                        </div>
                        @endif

                        @php
                            $i++;
                        @endphp
                        <div class="card-header" id="headingOne{{$i}}">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$i}}" aria-expanded="true" aria-controls="collapseOne{{$i}}">
                                    <img src="../assets/images/shipping/{{ $shipping->logo_path}}" alt="Avatar" class="w200 rounded mr-2">
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne{{$i}}" class="collapse mb-0 @if($i==1) show @endif" aria-labelledby="headingOne{{$i}}" data-parent="#accordion">

                        @endif
                            <div class="row clearfix  ">
                                <div class="col sm-12 ml-5 mt-3 mb-3 mr-3">
                                    <label class="fancy-checkbox margin-1 mt-3 ">
                                        <input @if ($user->shipping==$shipping->id)
                                            checked
                                        @endif type="radio" name="shipping" value="{{ $shipping->id}}" class="checkbox-tick" style="zoom: 150%" required>
                                        <span>
                                            <img src="../assets/images/shipping/{{ $shipping->company_logo}}" alt="Avatar" class="w100 rounded mr-2"> <!--<span>{{ $shipping->server_name}}</span>-->  Delivery in {{ $shipping->delivery_timeline}}
                                        </span>
                                    </label>
                                </div>
                            </div>
                        @php
                            $prevpath=$shipping->server_name;
                        @endphp

                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col sm-12 text align-center">
                <button type="submit" id="btn" class="btn btn-success">{{__('Save')}}</button>
            </div>
        </div>
    </div>
</div>
</form>

  </div>


  <div class="tab-pane" id="more" role="tabpanel" aria-labelledby="more">

  <div class="alert alert-success" role="alert">
  <h3 class="alert-heading">{{__('settings.posheader')}}</h3>
  <p><h6>{{__('settings.postext')}}</h6></p>
</div>
<hr>


<table table table-hover>
<tbody>
<tr>
<td><h4><span class="badge badge-success">{{__('settings.post')}}</span></h4></td>

<td>
<div class="form-group">
<select name="currency" id="postsel" class="form-control show-tick" required>
<option value="0" Selected>{{__('settings.postselect')}}</option>
<option value="15">{{__('settings.15')}}</option>
<option value="30">{{__('settings.30')}}</option>
<option value="45">{{__('settings.45')}}</option>
<option value="60">{{__('settings.60')}}</option>
<option value="90">{{__('settings.90')}}</option>
<option value="120">{{__('settings.2h')}}</option>
<option value="180">{{__('settings.3h')}}</option>
<option value="360">{{__('settings.6h')}}</option>
<option value="720">{{__('settings.12h')}}</option>
<option value="1440">{{__('settings.24h')}}</option>
</select>
</div>
</td>
<td><button id="btn-ad" type="submit" class="btn btn-success">{{__('settings.save')}}</button></td>
</tr>
<tr>
<td><h4><span class="badge badge-danger">{{__('settings.rollbackpost')}}</span></h4></td>
<td>
<div class="form-group">
<select name="roll" id="rollpost" class="form-control show-tick" required>
<option value="0" Selected>{{__('settings.rollback')}}</option>
<option value="insert">{{__('settings.insert')}}</option>
<option value="update">{{__('settings.update')}}</option>
<option value="delete">{{__('settings.delete')}}</option>
<option value="all">{{__('settings.all')}}</option>
</select>
</div>
</td>
<td>
<!-- Button trigger modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rollbacktr">
{{__('settings.rollsave')}}
</button>

<!-- Modal -->
<div class="modal fade" id="rollbacktr" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Rollback Transactions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <h3>{{__('settings.attention')}}</h3><br>
      <h6>{{__('settings.rollmodtext')}}</h6>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('settings.close')}}</button>
        <button type="button" class="btn btn-success">{{__('settings.save')}}</button>
      </div>
    </div>
  </div>
</div>

</td>
</tr>

</tbody>
</table>


  </div>



</div>


@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@stop
@section('page-script')

<script>

function appendUserRow(id, user) {
            var html = "<div id=\"opt-row." + id + "\" class=\"form-group row\">\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"price1" + id + "\" name=\"price1" + id + "\" placeholder=\"Price1\" value=\"" + user.price1 + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"price2." + id + "\" name=\"price2" + id + "\" placeholder=\"Price2\" value=\"" + user.price2 + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"min" + id + "\" name=\"min" + id + "\" placeholder=\"Min Profit\" value=\"" + user.min + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"standard" + id + "\" name=\"standard" + id + "\" placeholder=\"Standard\" value=\"" + user.standard + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"max" + id + "\" name=\"max" + id + "\" placeholder=\"Maximum\" value=\"" + user.max + "\">\n" +
                "            </div>\n" +
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"oneseller" + id + "\" name=\"oneseller" + id + "\" placeholder=\"One Seller\" value=\"" + user.oneseller + "\">\n" +
                "            </div>\n" +   
                "            <div class=\"col-1\">\n" +
                "                <input required type=\"text\" class=\"form-control\" id=\"stock" + id + "\" name=\"stock" + id + "\" placeholder=\"Critic Stock\" value=\"" + user.stock + "\">\n" +
                "            </div>\n" +                                                
                "             <button type=\"button\" onclick=\"delRow(" + id + ")\" class=\"btn btn-sm btn-default js-sweetalert\" title=\"Delete\" data-type=\"confirm\"><i class=\"fa fa-trash-o text-danger\"></i></button>\n" +
                "        </div>";
            $("#form-placeholder").append(html);
        }


function delRow(id) {
            var element = document.getElementById("opt-row." + id);
            element.parentNode.removeChild(element);
        }

$(document).ready(function () {
            var count = 0;
$("#btn-add").click(function () {
                appendUserRow(count++, {
                    price1: "",
                    price2: "",
                    min: "",
                    standard: "",
                    max: "",
                    oneseller: "",
                    stock: ""
                })
            });
});
  </script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/bundles/bootstrap.min.js') }}"></script>
  @stop