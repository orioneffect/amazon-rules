@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('menus.vdsserver'))

@section('content')

<div class="row clearfix">

    <div class="col-lg-12 col-md-12">

        <div class="card planned_task">

            <div class="header">

                <ul class="header-dropdown dropdown">                                

                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                    <li class="dropdown">

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>

                        <ul class="dropdown-menu">

                            <li><a href="javascript:void(0);">Action</a></li>

                            <li><a href="javascript:void(0);">Another Action</a></li>

                            <li><a href="javascript:void(0);">Something else</a></li>

                        </ul>

                    </li>

                </ul>

            </div>

            <div class="body">

                <div class="tab-pane" id="vds" role="tabpanel" aria-labelledby="vds">

<div class="alert alert-primary" role="alert">
    <h6 class="alert-heading">{{ __('settings.vdsinf') }}</h6>
    <p>
    <strong>{{ __('settings.vdsdesc') }}</strong>
    </p>
</div>
<hr>
<strong><h4 class="text-white">{{ __('settings.vdstitle') }} :</h4> You have 2 active VDS Server(s)</strong>
<table class="table table-info table-responsive bg-orange text-white">
<thead>
    <th style="width: 10%"><strong>{{ __('settings.oemail') }}</strong></th>
    <th style="width: 10%"><strong>{{ __('settings.buyermail') }}</strong></th>
    <th style="width: 10%"><strong>{{ __('settings.sellermail') }}</strong></th>
    <th style="width: 15%"><strong>{{ __('settings.servercode') }}</strong></th>
    <th style="width: 5%"><strong>{{ __('settings.ipadress') }}</strong></th>
    <th style="width: 5%"><strong>{{ __('settings.registerdate') }}</strong></th>
    <th style="width: 5%"><strong>{{ __('settings.expiredate') }}</strong></th>
    <th style="width: 5%"><strong>{{ __('settings.vdsos') }}</strong></th>
    <th style="width: 10%"><strong>{{ __('settings.osusername') }}</strong></th>
    <th style="width: 10%"><strong>{{ __('settings.ospassword') }}</strong></th>
    <th style="width: 5%"><strong>{{ __('settings.localip') }}</strong></th>
    <th style="width: 5%"><strong>{{ __('settings.macid') }}</strong></th>
</thead>

<tbody>
@foreach ($list as $li)
<tr>
<td>{{$li->orionemail}}</td>
<td>{{$li->buyermail}}</td>
<td>{{$li->sellermail}}</td>
<td>{{$li->servercode}}</td>
<td>{{$li->ipaddressv4}}</td>
<td>{{$li->registerdate}}</td>
<td>{{$li->expiredate}}</td>
<td>{{$li->os}}</td>
<td>{{$li->vdsusername}}</td>
<td>{{$li->vdspwd}}</td>
<td>{{$li->vdslocalipv4}}</td>
<td>{{$li->vdslocalmac}}</td>
</tr>
@endforeach

</tbody>
</table>
<hr>
<br>
<strong><h4 class="text-white">{{ __('settings.iphistorydesc') }}</h4></strong>
<table class="table table-responsive table-dark table-striped bg-purple text-white">
<thead>
    <th>{{ __('settings.iphistory') }}</th>
    <th>{{ __('settings.iphistorylocal') }}</th>
    <th>{{ __('settings.iphistorymac') }}</th>
    <th>{{ __('settings.iphistoryos') }}</th>
    <th>{{ __('settings.iphistorybrowser') }}</th>
</thead>
<tbody>
    <tr>
        <td>212.156.4.5</td>
        <td>10.10.10.123</td>
        <td>00-28-6E-55-0A-52</td>
        <td>Windows 10</td>
        <td>Chrome</td>
    </tr>
    <tr>
        <td>192.156.8.195</td>
        <td>10.10.10.123</td>
        <td>00-28-6E-55-0A-52</td>
        <td>Windows 10</td>
        <td>Vivaldi</td>
    </tr>
    <tr>
        <td>172.125.4.5</td>
        <td>10.10.10.123</td>
        <td>00-28-6E-55-0A-52</td>
        <td>Windows 10</td>
        <td>Firefox</td>
    </tr>
</tbody>


</table>



    </div>

<!--



< ?php

$user_agent = $_SERVER['HTTP_USER_AGENT'];

function getOS() { 

    global $user_agent;

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
}

function getBrowser() {

    global $user_agent;

    $browser        = "Unknown Browser";

    $browser_array = array(
                            '/msie/i'      => 'Internet Explorer',
                            '/firefox/i'   => 'Firefox',
                            '/safari/i'    => 'Safari',
                            '/chrome/i'    => 'Chrome',
                            '/edge/i'      => 'Edge',
                            '/opera/i'     => 'Opera',
                            '/netscape/i'  => 'Netscape',
                            '/maxthon/i'   => 'Maxthon',
                            '/konqueror/i' => 'Konqueror',
                            '/mobile/i'    => 'Handheld Browser'
                     );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}


$user_os        = getOS();
$user_browser   = getBrowser();

$device_details = "<strong>Browser: </strong>".$user_browser."<br /><strong>Operating System: </strong>".$user_os."";

print_r($device_details);

echo("<br /><br /><br />".$_SERVER['HTTP_USER_AGENT']."");

?>
-->




            </div>

        </div>

    </div>

</div>

@stop



@section('page-styles')

@stop



@section('page-script')

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

@stop