@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('menus.mailread'))

@section('content')



<div class="row clearfix">
<div class="col-md-12">
<div class="card">
<div class="mail-inbox">
<div class="mobile-left">
<a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i class="fa fa-bars"></i></a>
</div>
<div class="body mail-left">
<!--
    <div class="mail-compose m-b-20">
<a href="https://oculux.nsdbytes.com/laravel/public/email/compose" class="btn btn-danger btn-block btn-round">Compose</a>
</div>
-->
<div class="mail-side">
<ul class="nav">
    <li class="active">
    <select class="form-control" id="mail_info" name="mail_info">
                    <option value="All" selected="">AllStores</option>
                    <option value="ImmanStore">ImmanStore</option>
                    <option value="LacinsStore">LacinsStore</option>
                    <option value="SiriusGlobal">SiriusGlobal</option>
                    <option value="SametsStore">SametsStore</option>
                    <option value="SametSingapore">SametSingapore</option>
                    </select>
    </li>
    <br>
    <li>
    <select class="form-control" id="mail_info" name="mail_info">
                    <option value="buyer" selected="">{{ __('settings.allemails') }}</option>
                    <option value="buyer">{{ __('settings.buyermail') }}</option>
                    <option value="seller">{{ __('settings.sellermail') }}</option>
                    </select>
    </li>
<!--
<li><a href="app-inbox.html"><i class="icon-drawer"></i>Inbox<span class="badge badge-primary float-right">6</span></a></li>
<li><a href="javascript:void(0);"><i class="icon-cursor"></i>Sent<span class="badge badge-warning float-right">6</span></a></li>
<li><a href="javascript:void(0);"><i class="icon-envelope-open"></i>Draft</a></li>
<li><a href="javascript:void(0);"><i class="icon-action-redo"></i>Outbox</a></li>
<li><a href="javascript:void(0);"><i class="icon-trash"></i>Trash</a></li>
-->
</ul>
<h3 class="label">{{ __('settings.filters') }}</h3>
<ul class="nav">
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-success"></i>{{ __('settings.newsold') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-light"></i>{{ __('settings.ordercancel') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-warning"></i>{{ __('settings.warnings') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-success"></i>{{ __('settings.amzpayments') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-warning"></i>{{ __('settings.amzattention') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-danger"></i>{{ __('settings.suspend') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-danger"></i>{{ __('settings.atoz') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-danger"></i>{{ __('settings.sellingaccount') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-purple"></i>{{ __('settings.listdeact') }}</a></li>
<li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>{{ __('settings.pagesremoved') }}</a></li>
</ul>
</div>
</div>
<div class="body mail-right check-all-parent">
<div class="mail-action clearfix m-l-15">
<div class="pull-left">
<div class="fancy-checkbox d-inline-block">
<label>
<input class="check-all" type="checkbox" name="checkbox">
<span></span>
 </label>
</div>
<a href="javascript:void(0);" class="btn btn-warning btn-sm"><i class="icon-trash"></i></a>
<a href="javascript:void(0);" class="btn btn-light btn-sm hidden-sm"><i class="icon-refresh"></i></a>
<div class="btn-group">
<button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-tag"></i></button>
<div class="dropdown-menu">
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.newsold') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.ordercancel') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.warnings') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.amzpayments') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.amzattention') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.suspend') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.atoz') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.sellingaccount') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.listdeact') }}</a>
<a class="dropdown-item" href="javascript:void(0);">{{ __('settings.pagesremoved') }}</a>
</div>
</div>
</div>
<div class="pull-right ml-auto">
<div class="pagination-email d-flex">
<p>1-50/295</p>
<div class="btn-group m-l-20">
<button type="button" class="btn btn-default btn-sm"><i class="fa fa-angle-left"></i></button>
<button type="button" class="btn btn-default btn-sm"><i class="fa fa-angle-right"></i></button>
</div>
</div>
</div>
</div>
<div class="mail-list">
<ul class="list-unstyled">
<li class="clearfix">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star active"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon Services <payments-messages@amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[Sold]</span>Sold, ship now: AMZR-OREF-1221430864 New BN59-01241A Replaced Remote fit for Samsung TV UN49KS8000F UN49KS8000FXZA UN49KS8500F UN65KS8000FXZA UN65KS8500F UN65KS8500FXZA UN65KS9000F UN65KS</p>
<span class="time">23 Jun</span>
</div>
</li>
<li class="clearfix">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon Services <payments-messages@amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[Sold]</span>Sold, ship now: AMZR-OREF-1221430694 New AKB73896401 Replace Remote for LG BLU RAY DISC DVD Player BP135 BP145 BP155 BP175 BP255 BP300 BP335W BP340 BP350 BPM25 BPM35 UP870 UP875 BP550</p>
<span class="time"><i class="fa fa-paperclip"></i> 22 Jun</span>
</div>
</li>
<li class="clearfix unread">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">PETER GUERIN <ntdpt296b9tycjr+A03533292YFPS6SO03UVI@marketplace.amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[Order cancellation]</span>Order cancellation request from Amazon customer PETER GUERIN(Order: 701-7809580-2109830)</p>
<span class="time">20 Jun</span>
</div>
</li>
<li class="clearfix">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon Services <seller-notification@amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[Payment]</span>Your payment is on its way</p>
<span class="time">14 Jun</span>
</div>
</li>
<li class="clearfix">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon Seller Central notifications (do not reply) <donotreply@amazon.com></a></p>
<p class="dep"><span class="m-r-10">[Action required]</span>: Important information about your product listings</p>
<span class="time"><i class="fa fa-paperclip"></i> 11 Jun</span>
</div>
</li>
<li class="clearfix">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon.com Product Safety Team <noreply@amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[Attention]</span>Attention: Notification of Product Removal by Amazon Product Safety</p>
<span class="time">29 May</span>
</div>
</li>
<li class="clearfix">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon Seller Central Notifications <donotreply@amazon.com></a></p>
<p class="dep"><span class="m-r-10">[Listings deactivated]</span>Listings deactivated due to potential pricing error</p>
<span class="time"><i class="fa fa-paperclip"></i> 22 Jun</span>
</div>
</li>
<li class="clearfix unread">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon Services <seller-info@amazon.com></a></p>
<p class="dep"><span class="m-r-10">[Pages Removed]</span>Your Amazon seller detail pages have been temporarily removed</p>
<span class="time">20 Jun</span>
</div>
</li>
<li class="clearfix unread">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">Amazon <no-replies-appeals@amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[Selling Account]</span>Your Amazon.ca Selling Account</p>
<span class="time">20 Jun</span>
</div>
</li>
<li class="clearfix unread">
<div class="md-left">
<label class="fancy-checkbox">
<input type="checkbox" name="checkbox" class="checkbox-tick">
<span></span>
</label>
<a href="javascript:void(0);" class="mail-star"><i class="fa fa-star"></i></a>
</div>
<div class="md-right">
<img class="rounded" src="../assets/images/logo/2.jpg" alt="">
<p class="sub"><a href="https://oculux.nsdbytes.com/laravel/public/email/inboxdetail" class="mail-detail-expand">A-to-Z<atoz-guarantee-no-reply@amazon.ca></a></p>
<p class="dep"><span class="m-r-10">[A-to-z Guarantee Claim]</span>Your Amazon A-to-z Guarantee Claim for Order 701-6470228-1989827</p>
<span class="time">20 Jun</span>
</div>
</li>
</ul>
</div>
<ul class="pagination mb-0">
<li class="page-item"><a class="page-link" href="javascript:void(0);">Previous</a></li>
<li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
<li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li>
</ul>
</div>
</div>
</div>
</div>


@stop



@section('page-styles')

@stop



@section('page-script')

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

@stop