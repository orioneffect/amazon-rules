@extends('layout.master')

@section('parentPageTitle', __('menus.settings'))

@section('title', __('menus.balancetransfer'))

@section('content')

<div class="row clearfix">

    <div class="col-lg-12 col-md-12">

        <div class="card planned_task">

            <div class="header">

                <h2>{{ __('settings.easytransfer') }}</h2>

                <ul class="header-dropdown dropdown">                                

                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                    <li class="dropdown">

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>

                        <ul class="dropdown-menu">

                            <li><a href="javascript:void(0);">Action</a></li>

                            <li><a href="javascript:void(0);">Another Action</a></li>

                            <li><a href="javascript:void(0);">Something else</a></li>

                        </ul>

                    </li>

                </ul>

            </div>

            <div class="body">

            <div class="alert alert-primary" role="alert">
    <h6 class="alert-heading">{{ __('settings.balancetransfertitle') }}</h6>
    <p>
    <strong>{{ __('settings.balancetransferdesc') }}</strong>
    </p>
</div>
<hr>

<strong class="text-warning"><h3>Your balance : 250 $ / 1.250 TRY</h3></strong>

<hr>
<form method="post" id="transfer">
@csrf
<table class="table table-responsive text-white">
<tbody>
    <tr><td></td></tr>
<tr>
    <td>{{ __('settings.transfermail') }}</td><td><input type="text" id="targetemail" name="targetemail" class="form-control email" placeholder="Ex: example@example.com"></td>
<td>{{ __('settings.transferamount') }}</td>
<td>
<select class="form-control" id="amount" name="amount">
            <option value="5" selected="">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="35">35</option>
            <option value="40">40</option>
            <option value="45">45</option>
            <option value="50">50</option>
            <option value="55">55</option>
            <option value="60">60</option>
            <option value="65">65</option>
            <option value="70">70</option>
            <option value="75">75</option>
            <option value="80">80</option>
            <option value="85">85</option>
            <option value="90">90</option>
            <option value="95">95</option>
            <option value="100">100</option>
            </select>
</td>
<td>{{ __('settings.transfercurrency') }}</td>
<td>
<select class="form-control" id="currency" name="currency">
            <option value="TRY" selected="">TRY</option>
            <option value="USD">USD</option>
</select>
</td>
</tr>
<tr><td><button type="submit" id="btn" class="btn btn-success">{{ __('settings.save') }}</button></td></tr>
</tbody>
</table>
</form>
<br>
<hr>
<br>
<strong><h3>{{ __('settings.yourtransfers') }}</h3></strong>


<div class="body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover dataTable js-exportable text-white">
                        <thead>
                            <tr>
                            <th>{{ __('settings.transferreduser') }}</th>
                            <th>{{ __('settings.amount') }}</th>
                            <th>{{ __('settings.currency') }}</th>
                            <th>{{ __('settings.date') }}</th>     
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>{{ __('settings.transferreduser') }}</th>
                            <th>{{ __('settings.amount') }}</th>
                            <th>{{ __('settings.currency') }}</th>
                            <th>{{ __('settings.date') }}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        @foreach ($list as $li)
<tr>
<td>{{$li->targetemail}}</td>
<td>{{$li->amount}}</td>
<td>{{$li->currency}}</td>
<td>{{$li->curdate}}</td>
</tr>
@endforeach

                        </tbody>
                    </table>

</div>
        </div>
    </div>
</div>

@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}"/>

<link rel="stylesheet" href="{{ asset('assets/vendor/c3/c3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}">

<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
@stop



@section('page-script')

<script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-datatable/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}"></script>

<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/chartist.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/knob.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/index2.js') }}"></script>

@stop