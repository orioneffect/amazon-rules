@extends('layout.master')

@section('parentPageTitle', __('menus.settings'))

@section('title', __('menus.invoiceintegration'))

@section('content')

<div class="row clearfix">

    <div class="col-lg-12 col-md-12">

        <div class="card planned_task">

            <div class="header">

                <ul class="header-dropdown dropdown">                                

                    <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>

                    <li class="dropdown">

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>

                        <ul class="dropdown-menu">

                            <li><a href="javascript:void(0);">Action</a></li>

                            <li><a href="javascript:void(0);">Another Action</a></li>

                            <li><a href="javascript:void(0);">Something else</a></li>

                        </ul>

                    </li>

                </ul>

            </div>

            <div class="body">

            <div class="alert alert-primary" role="alert">
    <h6 class="alert-heading">{{ __('settings.paratitle') }}</h6>
    <p>
    <strong>{{ __('settings.paradesc') }}</strong>
    </p>
</div>
<hr>


<strong><h4 class="text-white">{{ __('settings.paratitle') }}</h4></strong>
<form method="post" id="invoice">
@csrf
<table class="table table-responsive text-white">
<tbody>
<tr>
<td>{{ __('settings.paraclientid') }}</td>
<td><input type="text" name="parclientid" id="parclientid" class="form-input" placeholder="HTK***"></td>
<td>{{ __('settings.paraclientsecret') }}</td>
<td><input type="text" class="form-input" id="parclientsecret" name="parclientsecret" placeholder="HTK***" >
<td>{{ __('settings.paracallbackurls') }}</td>
<td><td><input type="text" class="form-input" id="parcallbackurls" name="parcallbackurls" placeholder="urn:***"></td>
</tr>
<tr>
<td>{{ __('settings.parausername') }}</td>
<td><input type="text" name="parusername" id="parusername" class="form-input" ></td>
<td>{{ __('settings.parauserpwd') }}</td>
<td><input type="text" class="form-input" id="paruserpwd" name="paruserpwd"></td>
<td>{{ __('settings.accounttype') }}</td>
<td>
            <select class="form-control" id="acctype" name="acctype">
            <option value="0" selected="">{{ __('settings.earchive') }}</option>
            <option value="1">{{ __('settings.einvoice') }}</option>
            </select>
</td>

</tr>
<tr><td>{{ __('settings.parainvdesc') }}</td><td><input type="textarea" name="parainvdesc" id="parainvdesc" class="form-input" ></td></tr>
<tr><td> <button type="submit" id="btn" class="btn btn-success">{{ __('settings.save') }}</button></td></tr>
</table>
</form>
<hr>
<br>


<strong><h4 class="text-white">{{ __('settings.parainfo') }}</h4></strong>
<table class="table table-info table-responsive bg-purple text-white">
<thead>
                                        <th style="width: 5%">{{ __('settings.paractive') }}
                                            <hr />
                                        </th>
                                        <th style="width: 20%">{{ __('settings.paraclientid') }}
                                            <hr />
                                        </th>
                                        <th style="width: 20%">{{ __('settings.paraclientsecret') }}
                                            <hr />
                                        </th>
                                        <th style="width: 20%">{{ __('settings.paracallbackurls') }}
                                            <hr />
                                        </th>
                                        <th style="width: 15%">{{ __('settings.parausername') }}
                                            <hr />
                                        </th>
                                        <th style="width: 15%">{{ __('settings.parauserpwd') }}
                                            <hr />
                                        </th>
                                        <th style="width: 15%">{{ __('settings.accounttype') }}
                                            <hr />
                                        </th>
                                    </thead>

<tbody>


@foreach ($list as $li)
<tr>
<td>
<label class="switch">
<input type="checkbox" id="selectall" checked value="0">
<span class="slider round"></span>
</label>
</td>
<td><input type="text" name="paraclientid" value="{{$li->client_id}}" id="paraclientid" class="form-input" Placeholder="HTK***"  disabled></td>
<td><input type="text" class="form-input" value="{{$li->client_secret}}" id="paraclientsecret" name="paraclientsecret" Placeholder="HTK***" disabled></td>
<td><input type="text" class="form-input" value="{{$li->callback_urls}}"id="paracallbackurls" name="paracallbackurls" Placeholder="urn:***" disabled></td>
<td><input type="text" name="parausername" value="{{$li->parusername}}"id="parausername" class="form-input" disabled></td>
<td><input type="text" class="form-input" value="{{$li->paruserpwd}}" id="parauserpwd" name="parauserpwd"  disabled></td>
<td><input type="text" class="form-input" 
@if($li->invoicetype == 0)
    value="E-Archive"
@else
    value="E-invoice"
@endif

id="acctype" name="acctype" placeholder="E-Invoice" disabled></td>

</tr>
@endforeach

</table>
@stop
@section('page-styles')
@stop
@section('page-script')

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

@stop