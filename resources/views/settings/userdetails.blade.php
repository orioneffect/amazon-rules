@extends('layout.master')
@section('parentPageTitle', __('menus.users'))
@section('title', __('menus.userdet'))

@section('content')

@if(session()->has('success'))
    <div class="alert alert-success">
        @if (session()->get('success')==53)
            {{__('settings.pwdchanged')}}
        @elseif (session()->get('success')==54)
            {{__('settings.locchanged')}}
        @elseif (session()->get('success')==55)
            {{__('settings.compsettchanged')}}
        @endif
    </div>
@endif

@if($errors->any())
        {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
@endif

@if(session()->has('failure'))
    <div class="alert alert-danger">
        @if (session()->get('failure')==51)
            {{__('settings.newpwdmismatch')}}
        @endif
    </div>
@endif


<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <ul class="nav nav-tabs mb-2">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Company_Settings">{{__('settings.company')}}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Localization">{{__('settings.localization')}}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Email_Settings">{{__('settings.email')}}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Notifications">{{__('settings.notify')}} </a></li>

                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#vds">{{ __('settings.vds') }}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#billinteg">{{ __('settings.billinteg') }}</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Change_Password">{{__('settings.chpwd')}} </a></li>
            </ul>
            <div class="tab-content mt-0">
                <div class="tab-pane active show" id="Company_Settings">
                    <div class="card text-white bg-info">
                        <div class="card-header">{{__('settings.comset')}}</div>
                        <div class="card-body">
                            <form id="companyfrm" method="POST" action="{{ route('settings.updatecompany') }}">
                            @csrf
                            <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('settings.comname')}} <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" value="{{$user["company_name"]}}" required name="company_name">
                                        </div>
                                    </div>

                                    <!--
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('settings.amzstname')}}</label>
                                            <input class="form-control" value="Amazon Store Name" type="text">
                                        </div>
                                    </div>
                                    -->


                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('settings.conper')}}</label>
                                            <input class="form-control" value="{{$user["contact_person"]}}" type="text" name="contact_person">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 form-group demo-masked-input">

                                        <label>{{__('settings.mobnum')}} <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-mobile-phone"></i></span>
                                            </div>
                                            <input type="text" value="{{$user["mobile_numb"]}}" name="mobile_numb" class="form-control mobile-phone-number" placeholder="Ex: +00 (000) 000-00-00" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('settings.address')}}</label>
                                            <textarea name="address1" class="form-control" placeholder="" aria-label="With textarea">{{$user["address1"]}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 form-group demo-masked-input">
                                        <label>{{__('settings.email')}} <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
                                            </div>
                                            <input type="text" value="{{$user["email"]}}" name="email" class="form-control email" placeholder="Ex: example@example.com" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label>{{__('settings.web')}}</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="icon-globe"></i></span>
                                                </div>
                                                <input type="text" value="{{$user["website_url"]}}" name="website_url" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="form-group">
                                            <label>{{__('settings.country')}}</label>
                                            <select class="form-control" id="country" name="country" onchange="load_state_list(this.value)">
                                                <option value="">{{__('settings.seco')}}</option>
                                                @isset($maindata["country"])
                                                @foreach ($maindata["country"] as $country)
                                                <option value="{{$country->id}}" {{$user["country"] == $country->id?'selected':''}}>{{$country->country_name}}</option>
                                                @endforeach
                                                @endisset
                                              </select>

                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="form-group">
                                            <label>{{__('settings.stapro')}}</label>
                                            <select class="form-control" id="state" name="state">
                                                <option value="">{{__('settings.sestate')}}</option>
                                                @isset($maindata["state"])
                                                @foreach ($maindata["state"] as $state)
                                                <option value="{{$state->id}}" {{$user["state"] == $state->id?'selected':''}}>{{$state->state_name}}</option>
                                                @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="form-group">
                                            <label>{{__('settings.city')}}</label>
                                            <input name="city" class="form-control" value="{{$user["city"]}}" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3">
                                        <div class="form-group">
                                            <label>{{__('settings.postc')}}</label>
                                            <input name="pincode" class="form-control" value="{{$user["pincode"]}}" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group demo-masked-input">
                                        <label>{{__('settings.phnum')}}</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" value="{{$user["phone"]}}" name="phone" class="form-control phone-number" placeholder="Ex: +00 (000) 000-00-00">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 form-group demo-masked-input">
                                        <label>{{__('settings.fax')}}</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-fax"></i></span>
                                            </div>
                                            <input type="text" value="{{$user["fax"]}}" name="fax" class="form-control phone-number" placeholder="Ex: +00 (000) 000-00-00">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-right m-t-20">
                                        <button type="submit" class="btn btn-primary">{{__('settings.save')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="Localization">
                    <div class="card text-white bg-green">
                        <div class="card-header">{{__('settings.baset')}}</div>
                        <div class="card-body">
                            <form id="localizationfrm" method="POST" action="{{ route('settings.updatelocal') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.defcount')}}</label>
                                            <select class="form-control" id="country_info" name="country_info" onchange="load_store_list(this.value)">
                                                @isset($maindata["countriesNav"])
                                                @foreach ($maindata["countriesNav"] as $country)
                                                <option value="{{$country->country_id}}" {{$user["default_country_id"] == $country->country_id?'selected':''}}>{{$country->country_name}}</option>
                                                @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.defstore')}}</label>
                                            <select class="form-control" id="store_main" name="store_main" onchange="storeinfo_loadstore(this.value);loadstore(this.value)">
                                                @isset($maindata["storesNav"])
                                                @foreach ($maindata["storesNav"] as $store)
                                                <option value="{{$store->id}}" {{$user["default_store_id"] == $store->id?'selected':''}}>{{$store->store_name}}</option>
                                                @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>


                                    <!--
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>{{__('settings.datefor')}}</label>
                                            <select class="form-control">
                                                <option value="d/m/Y">15/05/2016</option>
                                                <option value="d.m.Y">15.05.2016</option>
                                                <option value="d-m-Y">15-05-2016</option>
                                                <option value="m/d/Y">05/15/2016</option>
                                                <option value="Y/m/d">2016/05/15</option>
                                                <option value="Y-m-d">2016-05-15</option>
                                                <option value="M d Y">May 15 2016</option>
                                                <option selected="selected" value="d M Y">15 May 2016</option>
                                            </select>
                                        </div>
                                    </div>
                                    -->

                                    <!--
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.timezone')}}</label>
                                            <select class="form-control">
                                                <option>10:45am Chicago (GMT-6)</option>
                                            </select>
                                        </div>
                                    </div>
                                    -->


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.deflan')}}</label>
                                            <select class="form-control" id="language_info" name="language_info">
                                                <option value="">-- {{__('settings.selectdefaultlang')}} --</option>
                                                @isset($maindata["languageNav"])
                                                @foreach ($maindata["languageNav"] as $language)
                                                    <option value={{$language->id}} {{$user["default_language_id"] == $language->id?'selected':''}}>{{$language->language}}</option>
                                                @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.curcode')}}</label>
                                            <select class="form-control" id="currency_info" name="currency_info">
                                                <option value="">-- {{__('settings.selectcurr')}} --</option>
                                                @isset($maindata["currencyNav"])
                                                @foreach ($maindata["currencyNav"] as $currency)
                                                    <option value={{$currency->currency_code}} {{$user["currency"] == $currency->currency_code?'selected':''}}>{{$currency->currency_code}}</option>
                                                @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>

                                    <!--
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.cursym')}}</label>
                                            <input class="form-control" value="$" type="text">
                                        </div>
                                    </div>
                                    -->

                                    <div class="col-sm-12 text-right m-t-20">
                                        <button type="submit" class="btn btn-primary">{{__('settings.save')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="Email_Settings">
                    <div class="card text-white bg-cyan">
                        <div class="card-header">{{__('settings.smtp')}}</div>
                        <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="PHP Mail" type="radio" checked><span><i></i>{{__('settings.phm')}}</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="SMTP" type="radio"><span><i></i>SMTP</span></label>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.eadres')}}</label>
                                            <input class="form-control" type="email">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.ename')}}</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>SMTP HOST</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.smuser')}}</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.smpwd')}}</label>
                                            <input class="form-control" type="password">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>SMTP PORT</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.smsec')}}</label>
                                            <select class="form-control">
                                                <option>None</option>
                                                <option>SSL</option>
                                                <option>TLS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>{{__('settings.smautdo')}}</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 m-t-20 text-right">
                                        <button type="button" class="btn btn-primary">{{__('settings.save')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="Notifications">
                    <div class="card text-white bg-orange">
                        <div class="card-header">{{__('settings.notset')}}</div>
                        <div class="card-body">
                           <!-- <h6>{{ __('settings.mestext') }}</h6>-->
                            <ul class="list-group">
                                <li class="list-group-item">
                                {{ __('settings.enable_free_text_msg') }}
                                    <div class="float-right">
                                        <label class="switch">
                                            <input type="checkbox" checked="">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                {{ __('settings.msg_when_orion_cannot_reach_amazon_seller_acc') }}
                                    <div class="float-right">
                                        <label class="switch">
                                            <input type="checkbox" checked="">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                {{ __('settings.msg_when_package_lost') }}
                                    <div class="float-right">
                                        <label class="switch">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                {{ __('settings.msg_when_customer_leaves_negative_feedback') }}
                                    <div class="float-right">
                                        <label class="switch">
                                            <input type="checkbox" checked="">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                {{ __('settings.msg_when_automatic_bill_payment_failed') }}
                                    <div class="float-right">
                                        <label class="switch">
                                            <input type="checkbox" checked="">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>








                <div class="tab-pane" id="vds" role="tabpanel" aria-labelledby="vds">

<div class="alert alert-primary" role="alert">
    <h6 class="alert-heading">{{ __('settings.vdsinf') }}</h6>
    <p>
    <strong>{{ __('settings.vdsdesc') }}</strong>
    </p>
</div>
<hr>
<strong><h4 class="text-white">{{ __('settings.vdstitle') }}</h4></strong>
<table class="table table-info table-responsive bg-orange text-white">

<thead></thead>


                                <tbody>
<tr>

<td>{{ __('settings.servercode') }}</td>
<td><input type="text" name="servercode" id="servercode" class="form-control" value="Orion4501" disabled ></td>

<td>{{ __('settings.ipadress') }}</td>
<td><input type="text" class="form-input" id="ipv4" name="ipv4" value="212.156.8.5" disabled>

<td>{{ __('settings.expiredate') }}</td>
<td><td><input type="text" class="form-input" id="expiredate" name="expiredate" value="25.12.2021"disabled></td>

</tr>

<tr>

<td>{{ __('settings.vdsos') }}</td>
<td><input type="text" name="vdsos" id="vdsos" class="form-control" value="Windows 10 Pro" disabled ></td>

<td>{{ __('settings.osusername') }}</td>
<td><input type="text" class="form-input" id="osusername" name="osusername" value="OrionEffect" disabled>

<td>{{ __('settings.ospassword') }}</td>
<td><td><input type="text" class="form-input" id="ospassword" name="ospassword" value="-]*aXzq?."disabled></td>

</tr>

<tr>

<td>{{ __('settings.localip') }}</td>
<td><input type="text" name="localip" id="localip" class="form-control" value="10.10.10.123" disabled ></td>

<td>{{ __('settings.macid') }}</td>
<td><input type="text" class="form-input" id="macid" name="macid" value="00-28-6E-55-0A-52" disabled>

</tr>
</table>
<hr>
<br>
<strong><h4 class="text-white">{{ __('settings.iphistorydesc') }}</h4></strong>
<table class="table table-responsive table-dark table-striped bg-purple text-white">
<thead>
    <th>{{ __('settings.iphistory') }}</th>
    <th>{{ __('settings.iphistorylocal') }}</th>
    <th>{{ __('settings.iphistorymac') }}</th>
    <th>{{ __('settings.iphistoryos') }}</th>
    <th>{{ __('settings.iphistorybrowser') }}</th>
</thead>
<tbody>
    <tr>
        <td>212.156.4.5</td>
        <td>10.10.10.123</td>
        <td>00-28-6E-55-0A-52</td>
        <td>Windows 10</td>
        <td>Chrome</td>
    </tr>
    <tr>
        <td>192.156.8.195</td>
        <td>10.10.10.123</td>
        <td>00-28-6E-55-0A-52</td>
        <td>Windows 10</td>
        <td>Vivaldi</td>
    </tr>
    <tr>
        <td>172.125.4.5</td>
        <td>10.10.10.123</td>
        <td>00-28-6E-55-0A-52</td>
        <td>Windows 10</td>
        <td>Firefox</td>
    </tr>
</tbody>


</table>



    </div>

<!--



< ?php

$user_agent = $_SERVER['HTTP_USER_AGENT'];

function getOS() { 

    global $user_agent;

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
}

function getBrowser() {

    global $user_agent;

    $browser        = "Unknown Browser";

    $browser_array = array(
                            '/msie/i'      => 'Internet Explorer',
                            '/firefox/i'   => 'Firefox',
                            '/safari/i'    => 'Safari',
                            '/chrome/i'    => 'Chrome',
                            '/edge/i'      => 'Edge',
                            '/opera/i'     => 'Opera',
                            '/netscape/i'  => 'Netscape',
                            '/maxthon/i'   => 'Maxthon',
                            '/konqueror/i' => 'Konqueror',
                            '/mobile/i'    => 'Handheld Browser'
                     );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}


$user_os        = getOS();
$user_browser   = getBrowser();

$device_details = "<strong>Browser: </strong>".$user_browser."<br /><strong>Operating System: </strong>".$user_os."";

print_r($device_details);

echo("<br /><br /><br />".$_SERVER['HTTP_USER_AGENT']."");

?>
-->


<div class="tab-pane" id="billinteg" role="tabpanel" aria-labelledby="billinteg">

<div class="alert alert-primary" role="alert">
    <h6 class="alert-heading">{{ __('settings.paratitle') }}</h6>
    <p>
    <strong>{{ __('settings.paradesc') }}</strong>
    </p>
</div>
<hr>


<strong><h4 class="text-white">{{ __('settings.paratitle') }}</h4></strong>
<form action="#" method="post">
@csrf
<table class="table table-responsive text-white">
                                <tbody>
<tr>

<td>{{ __('settings.paraclientid') }}</td>
<td><input type="text" name="parclientid" id="parclientid" class="form-input" placeholder="HTK***"></td>

<td>{{ __('settings.paraclientsecret') }}</td>
<td><input type="text" class="form-input" id="parclientsecret" name="parclientsecret" placeholder="HTK***" >

<td>{{ __('settings.paracallbackurls') }}</td>
<td><td><input type="text" class="form-input" id="parcallbackurls" name="parcallbackurls" placeholder="urn:***"></td>

</tr>

<tr>

<td>{{ __('settings.parausername') }}</td>
<td><input type="text" name="parusername" id="parusername" class="form-input" ></td>

<td>{{ __('settings.parauserpwd') }}</td>
<td><input type="text" class="form-input" id="paruserpwd" name="paruserpwd"></td>

</tr>
<tr><td> <button type="submit" id="btn" class="btn btn-success">{{ __('settings.save') }}</button></td></tr>

</table>
</form>
<hr>
<br>


<strong><h4 class="text-white">{{ __('settings.parainfo') }}</h4></strong>
<table class="table table-info table-responsive bg-purple text-white">
<thead>
                                        <th style="width: 5%">{{ __('settings.paractive') }}
                                            <hr />
                                        </th>
                                        <th style="width: 20%">{{ __('settings.paraclientid') }}
                                            <hr />
                                        </th>
                                        <th style="width: 20%">{{ __('settings.paraclientsecret') }}
                                            <hr />
                                        </th>
                                        <th style="width: 20%">{{ __('settings.paracallbackurls') }}
                                            <hr />
                                        </th>
                                        <th style="width: 15%">{{ __('settings.parausername') }}
                                            <hr />
                                        </th>
                                        <th style="width: 15%">{{ __('settings.parauserpwd') }}
                                            <hr />
                                        </th>
                                    </thead>

<tbody>
<tr>
<td>
<label class="switch">
                                                <input type="checkbox" id="selectall" checked value="0">
                                                <span class="slider round"></span>
                                            </label>

</td>
<td><input type="text" name="paraclientid" id="paraclientid" class="form-input" Placeholder="HTK***"  ></td>
<td><input type="text" class="form-input" id="paraclientsecret" name="paraclientsecret" Placeholder="HTK***" ></td>
<td><input type="text" class="form-input" id="paracallbackurls" name="paracallbackurls" Placeholder="urn:***" ></td>
<td><input type="text" name="parausername" id="parausername" class="form-input" ></td>
<td><input type="text" class="form-input" id="parauserpwd" name="parauserpwd"  ></td>

</tr>
</table>

    </div>




















                <div class="tab-pane" id="Change_Password">
                    <form id="frm" method="POST" action="{{ route('settings.changepwd') }}" data-parsley-validate validate>
                        @csrf
                        <div class="card text-white bg-blue">
                            <div class="card-header">{{__('settings.chpwd')}}</div>
                                <div class="card-body">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <input  type="hidden" name="tabno" value="4">
                                                <input type="text" class="form-control" value={{$maindata['username']}} disabled="" placeholder="{{__('settings.username')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <input type="email" class="form-control" value={{$maindata['email']}} disabled="" placeholder="{{__('settings.email')}}">
                                            </div>
                                        </div>
                                        <!--<div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="{{__('settings.phnum')}}">
                                            </div>
                                        </div>-->
                                        <div class="col-lg-12 col-md-12">
                                            <hr>
                                            <h6>{{__('settings.chpwd')}}</h6>
                                            <!--<div class="form-group">
                                                <input type="password" id="curpwd" name="curpwd" class="form-control" placeholder="{{__('settings.curpwd')}}">
                                            </div>-->
                                            <div class="form-group">
                                                <input type="password" id="password" name="password" class="form-control" placeholder="{{__('settings.newpwd')}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="{{__('settings.conpwd')}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 m-t-20 text-right">
                                            <button type="submit" class="btn btn-primary">{{__('settings.saves')}}</button> &nbsp;
                                            <button type="button" class="btn btn-default">{{__('settings.cancel')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
       
    </div>


@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/nouislider/nouislider.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/parsleyjs/css/parsley.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/sweetalert.css') }}">
<style>
    .demo-card label{ display: block; position: relative;}
    .demo-card .col-lg-4{ margin-bottom: 30px;}
</style>


@stop
@section('page-script')
<script src="{{ asset('assets/vendor/dropify/js/dropify.js') }}"></script>
<script src="{{ asset('assets/vendor/parsleyjs/js/parsley.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script><!-- Input Mask Plugin Js -->
<script src="{{ asset('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/js/pages/forms/advanced-form-elements.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
<script src="{{ asset('assets/bundles/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/ui/dialogs.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script><!-- Bootstrap Colorpicker Js -->
<script src="{{ asset('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script><!-- Input Mask Plugin Js -->
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ asset('assets/vendor/nouislider/nouislider.js') }}"></script><!-- noUISlider Plugin Js -->







@if(session()->has('failure') || $errors->any())
    <script>
        @if (session()->get('failure')==51 || old('tabno')==4)
            $('[href="#Change_Password"]').tab('show');
            $("#newpwd").focus();
        @else
            $('[href="#Change_Password"]').tab('show');
            $("#newpwd").focus();
        @endif
    </script>
@endif

@if(session()->has('success'))
    <script>
        @if (session()->get('success')==53)
            $('[href="#Change_Password"]').tab('show');
        @elseif (session()->get('success')==54)
            $('[href="#Localization"]').tab('show');
        @endif
    </script>
@endif

<script>
    $('#frm').parsley();

    function load_store_list(country) {
        var ENDPOINT = "{{ url('/') }}";
        $("#store_main").html('');
        $.ajax({
            url: ENDPOINT + "/settings/loadstore_list?country_id="+country,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
          response.forEach(function (val, key) {
                var selected='';
                $("#store_main").append('<option value="' + val.id + '"  '+selected+'  >' + val.store_name + '</option>');
            });
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
          console.log('Server error occured');
        });
      }
      function load_state_list(country) {
        var ENDPOINT = "{{ url('/') }}";
        $("#state").html('');
        $.ajax({
            url: ENDPOINT + "/settings/loadstate_list?country_id="+country,
            datatype: "json",
            type: "get",
        })
        .done(function (response) {
          response.forEach(function (val, key) {
                var selected='';
                $("#state").append('<option value="' + val.id + '"  '+selected+'  >' + val.state_name + '</option>');
            });
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
          console.log('Server error occured');
        });
      }

</script>
@stop
