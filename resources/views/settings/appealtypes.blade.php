@extends('layout.master')

@section('parentPageTitle', __('menus.settings'))

@section('title', __('menus.appealtypes'))



@section('content')

<div class="row clearfix">

    <div class="col-md-12">


        <div class="card">

            <div class="mail-inbox">

                <div class="mobile-left">

                    <a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i class="fa fa-bars"></i></a>

                </div>

                <div class="body mail-left">



                    <div class="mail-side">

                        <h3 class="label">{{ __('settings.policycompliance2') }}</h3>

                        <ul class="nav">

                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-danger"></i>Suspected Intellectual Property</a></li>

                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-info"></i>Received Intellectual Property</a></li>

                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-dark"></i>Product Authenticity Customer</a></li>

                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Product Condition Customer</a></li>

                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Food and Product Safety Issues</a></li>

                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Listing Policy Violations</a></li>
                            
                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Restricted Product Policy Violations</a></li>
                           
                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Customer Product Reviews Policy Violations</a></li>
                            
                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Other Policy Violations</a></li>
                            
                            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-primary"></i>Policy violation warning</a></li>


                        </ul>

                    </div>

                </div>

                <div class="body mail-right check-all-parent">

                    <div class="mail-compose">

                        <form>

                            <div class="form-group">
            <select class="form-control" id="policy" name="amount">
            <option value="0" selected="">--Select Your Policy Compliance--</option>
            <option value="Suspected Intellectual Property Violations">Suspected Intellectual Property Violations</option>
            <option value="Received Intellectual Property Complaints">Received Intellectual Property Complaints</option>
            <option value="Product Authenticity Customer Complaints">Product Authenticity Customer Complaints</option>
            <option value="Product Condition Customer Complaints">Product Condition Customer Complaints</option>
            <option value="Food and Product Safety Issues">Food and Product Safety Issues</option>
            <option value="Listing Policy Violations">Listing Policy Violations</option>
            <option value="Restricted Product Policy Violations">Restricted Product Policy Violations</option>
            <option value="Customer Product Reviews Policy Violations">Customer Product Reviews Policy Violations</option>
            <option value="Other Policy Violations">Other Policy Violations</option>
            <option value="Policy violation warning">Policy violation warning</option>
            </select>

                            </div>

                        

                        </form>

                        <div class="summernote">

                            
                        </div>

                        <div class="m-t-30 text-right">

                            <button type="button" class="btn btn-success btn-round">Send Message</button>

                            <button type="button" class="btn btn-outline-secondary btn-round">Draft</button>

                            <a href="app-inbox.html" class="btn btn-outline-secondary btn-round">Cancel</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@stop



@section('page-styles')

<link rel="stylesheet" href="{{ asset('assets/vendor/summernote/dist/summernote.css') }}">

@stop



@section('page-script')

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

<script src="{{ asset('assets/vendor/summernote/dist/summernote.js') }}"></script>

<script>

    $('.toggle-email-nav').on('click', function() {

		$('.mail-left').toggleClass('open');

	});

</script>

@stop