@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('settings.shipping'))

@section('content')

<div class="row clearfix">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-orange text-white rounded-circle"><i class="fa fa-users"></i></div>
                    <div class="ml-4">
                        <span>Total Cost</span>
                        <h4 class="mb-0 font-weight-medium">{{$vieworder["order_details"]["order_total"]["amount"]}} {{$vieworder["order_details"]["order_total"]["currency_code"]}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-pink text-white rounded-circle"><i class="fa fa-life-ring"></i></div>
                    <div class="ml-4">
                        <span>Total Quantity</span>
                        <h4 class="mb-0 font-weight-medium">1</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-pink text-white rounded-circle"><i class="fa fa-truck"></i></div>
                    <div class="ml-4">
                        <h4 class="mb-0 font-weight-medium"><img src="../assets/images/shipping/{{ $company->company_logo}}" alt="Avatar" class="w100 rounded mr-2"></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="body">
                <div class="d-flex align-items-center">
                    <div class="icon-in-bg bg-pink text-white rounded-circle"><i class="fa fa-envelope"></i></div>
                    <div class="ml-4">
                        <span>New Messages</span>
                        <h4 class="mb-0 font-weight-medium">1</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table header-border table-hover table-custom spacing5">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th class="w200">Product</th>
                        <th>Quantity</th>
                        <th>ASIN</th>
                        <th>Dimention</th>
                        <th>Weight</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="w60">1</th>
                        <td>
                            <img class="rounded" id="productImage" src="{{$vieworder["productURL"]}}" alt="">
                        </td>
                        <td class="w200"><textarea class="form-control" rows="5" cols="10"  readonly >{{$vieworder["Title"]}}</textarea>
                        </td>
                        <td>{{$vieworder["qty"]}}</td>
                        <td>{{$vieworder["asin"]}}</td>
                        <td>{{$vieworder["productSize"]}}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="body ribbon">
                <div class="ribbon-box pink">Order Summary</div>
                <div class="row text-center">
                    <div class="col-lg-4 col-sm-12 border-right pb-4 pt-4">
                        <label class="mb-0">Ship By</label>
                        @php
                            $shipdt=new DateTime($vieworder["order_details"]["latest_ship_date"]);
                            $deldt=new DateTime($vieworder["order_details"]["latest_delivery_date"]);
                            $purdt=new DateTime($vieworder["order_details"]["purchase_date"]);
                        @endphp
                        <h4 class="font-15 font-weight-bold text-col-blue">{{$shipdt->format('Y-m-d H:i')}}</h4>
                    </div>
                    <div class="col-lg-4 col-sm-12 border-right pb-4 pt-4">
                        <label class="mb-0">Deliver By</label>
                        <h4 class="font-15 font-weight-bold text-col-blue">{{$deldt->format('Y-m-d H:i')}}</h4>
                    </div>
                    <div class="col-lg-4 col-sm-12 pb-4 pt-4">
                        <label class="mb-0">Purchase Date</label>
                        <h4 class="font-15 font-weight-bold text-col-blue">{{$purdt->format('Y-m-d H:i')}}</h4>
                    </div>
                </div>
                <hr>
                <div class="mb-4 mt-3">
                    <label class="d-block">Buyer Name<span class="float-right">{{$vieworder["buyername"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="mb-4 mt-3">
                    <label class="d-block">Address 1<span class="float-right">{{$vieworder["address1"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>

                <div class="mb-4 mt-3">
                    <label class="d-block">Address 2<span class="float-right">{{$vieworder["address2"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="mb-4 mt-3">
                    <label class="d-block">Municipality<span class="float-right">{{$vieworder["municipality"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>

                <div class="mb-4 mt-3">
                    <label class="d-block">City<span class="float-right">{{$vieworder["city"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>

                <div class="mb-4 mt-3">
                    <label class="d-block">County/State<span class="float-right">{{$vieworder["country"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>

                <div class="mb-4 mt-3">
                    <label class="d-block">Country Code<span class="float-right">{{$vieworder["countrycode"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>

                <div class="mb-4 mt-3">
                    <label class="d-block">Postal<span class="float-right">{{$vieworder["postalcode"]}}</span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="body ribbon">
                <div class="ribbon-box green">Dropshipping Messages</div>
                <div class="row text-center">
                    &nbsp;
                </div>
                <div class="row text-center">
                    <textarea rows="5" class="form-control"></textarea>
                </div>

                <div class="row float-right">
                    <input type="button" class="btn btn-success " value="Send">
                </div>

                <div class="mb-4 mt-3">
                    <label class="d-block">Message 1<span class="float-right"></span></label>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-styles')
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
@stop

