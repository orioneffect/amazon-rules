@extends('layout.master')
@section('parentPageTitle', __('menus.settings'))
@section('title', __('settings.shipping'))

@section('content')

<form  method="POST" action="{{ route('settings.shippingsave') }}" validate>
@csrf

<div class="row clearfix">
    @if (Session::has('success'))
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="alert alert-success alert-dismissible">{{ __('settings.success') }}</div>
    </div>
    @endif
    <div class="col sm-12">
        <div class="card">
            <div class="accordion  mb-0" id="accordion">
                <div>
                    @php
                    $prevpath="";
                    $i=0;
                    @endphp
                    @foreach ($shippings as $shipping)
                        @if ($prevpath=="" || $prevpath!=$shipping->server_name)

                        @if ($prevpath!=$shipping->server_name)
                        </div>
                        @endif

                        @php
                            $i++;
                        @endphp
                        <div class="card-header" id="headingOne{{$i}}">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$i}}" aria-expanded="true" aria-controls="collapseOne{{$i}}">
                                    <img src="../assets/images/shipping/{{ $shipping->logo_path}}" alt="Avatar" class="w200 rounded mr-2">
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne{{$i}}" class="collapse mb-0 @if($i==1) show @endif" aria-labelledby="headingOne{{$i}}" data-parent="#accordion">

                        @endif
                            <div class="row clearfix  ">
                                <div class="col sm-12 ml-5 mt-3 mb-3 mr-3">
                                    <label class="fancy-checkbox margin-1 mt-3 ">
                                        <input @if ($user->shipping==$shipping->id)
                                            checked
                                        @endif type="radio" name="shipping" value="{{ $shipping->id}}" class="checkbox-tick" style="zoom: 150%" required>
                                        <span>
                                            <img src="../assets/images/shipping/{{ $shipping->company_logo}}" alt="Avatar" class="w100 rounded mr-2"> <!--<span>{{ $shipping->server_name}}</span>-->  Delivery in {{ $shipping->delivery_timeline}}
                                        </span>
                                    </label>
                                </div>
                            </div>
                        @php
                            $prevpath=$shipping->server_name;
                        @endphp

                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col sm-12 text align-center">
                <button type="submit" id="btn" class="btn btn-success">{{__('Save')}}</button>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">
@stop

@section('page-script')
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script><!-- Bootstrap Tags Input Plugin Js -->
@stop
