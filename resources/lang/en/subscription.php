<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Subscription Language Lines - English
    |--------------------------------------------------------------------------
    |
    |
    */
    'personal' => 'Personal Details',
    'bank' => 'Bank  Details',
    'amount' => 'Subscription Amount',
    'choose_plan' => 'Choose plan',
    'subscription' => 'Subscription',
    'gotopaymentpage' => 'Go to payment page',
    'subscription_list' => 'Subscription List',
    'no_of_store' => 'Number of stores',
    'repricer' => 'Repricer',

    'unlimited_stores' => 'Unlimited',
    'advanced_filters' => 'Advanced Filter',
    'shipping_usage' => "Shipping Usage",
    'max_asin_count' => "Maximum ASIN count",
    'unlimited_asin_limit' => "Unlimted",
    'stock_control' => "Stock control",
    'price_control' => "Price control",
    'trademark_control' => "Trademark control",
    'dropshipping_flag' => "Dropshipping",
    'fba_flag' => "FBA",
    'ebay_etsy_flag' => "EBay, ETSY search",
    'supplier_wholesale_america_flag' => "Supplier Wholesale America",
    'supplier_wholesale_turkey_flag' => "Supplier Wholesale Turkey",
    'invalidCreditCard' => 'Invalid Credit card.',
    'invalidExpDate' => 'Invalid Expiry date.',
    'totalamount' => 'Total Amount',
    'discount' => 'Discount',
    'balance' => 'Final Payable Amount',
    'coupon_success' => 'Yeah!! You have got :1: discount!',
    'coupon_failed' => 'No! You have entered an :1: coupon',
    'confirm_subscription_personal' => 'Confirm Subscription - Personal Detail',
    'confirm_subscription_bank' => 'Confirm Subscription - Bank Details',
    'confirm_payment' => 'Confirm Payment',
    'selplan' => 'Plan Name',
    'refid' => 'Transaction ID',
    'paysucc' => 'Payment Success',
    'payment' => 'Payment',
    'confirm_bankdetails_pay' => 'Confirm Payment - Bank Details',

];
