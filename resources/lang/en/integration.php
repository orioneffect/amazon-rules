<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'store_list' => 'Store List',
    'owner' => 'Owner Name',
    'store_name' => 'Store Name',
    'add_store' => 'Add Store',
    'add_newstore' => 'Add New Store',
    'stock' => 'Stock',
    'store_changed' =>'Your current store changed successfully.',
    'store_added' =>'New store integrated successfully.',
    'error'=>'You have to select a store to continue',
    'basic_details' => 'Basic Details',
    'add_on' => 'Add On',
    'integration' => 'Integration',
    'budget' => 'Budget',
    'price_update' => 'Price Update',
    'stock_update' => 'Stock Update',
    'product_remove' => 'Product removal',
    'product_insert' => 'Product insert',
    'select_cuntry' => 'Please select a country.',
    'current_store' => 'Current Store.',
    'merchant'=>'Amazon Merchant ID',
    'sku'=>'SKU Prefix'



];
