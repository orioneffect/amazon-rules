<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    | add commonly using languages
    */

    'country' => 'Country Name',
    'status' => 'Status',
    'select_country' => 'Select Country',
    'select_state' => 'Select State/ProvinceCode',
    'cancel' => 'Cancel',
    'create' => 'Create',
    'name' => 'Name',
    'full_name' => 'Your Full Name',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'select' => 'Selct',
    'state' => 'State/City',
    'city' => 'City',
    'address' => 'Address',
    'email' => 'Email',
    'phone' => 'Phone',
    'pincode' => 'Pincode',
    'credit_card' => 'Credit Card',
    'exp_mon_year' => 'Expiration Month & Year',
    'exp_mon' => 'Expiration Month',
    'exp_year' => 'Expiration Year',
    'cvv' => 'CVV',
    'confirm' => 'Confirm',
    'showmore' => 'Show More',
    'couponcode' => 'Discount Code',
    'apply' => 'Apply',
    'nameoncard' => 'Card Holder Name',
    'cardnumber' => 'Card Number',
    'installment' => 'No. of Installments',


];
