<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Options Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'options'=>'Options',
    'search'=>'Search By Name/Phone/Email',
    'suppliers_list'=>'Suppliers List',
    'addnew_suppliers'=>'Add New Suppliers',
    'companyname'=>'Company Name',
    'phone'=>'Phone',
    'email'=>'Email',
    'contact'=>'Contact Name',
    'user'=>'User Name',
    'status'=>'status',
    'created'=>'Created At',
    'xml'=>'XML Link',
    'create'=>'Create Supplier',
    'delete'=>'Delete',
    'image'=>'Image',
    'product'=>'Product',
    'price'=>'Price',
    'category'=>'Category',
    'quantity'=>'Quantity',
    'details'=>'Details',
    'select'=>'Select',
    'pushintoamazon' => 'Push into Amazon store',
    'filelist'=>'File List',
    'uploadnew'=>'Upload New File',
    'uploadsearch'=>'Search File',
    'createnew'=>'Create New File',
    'documentname'=>'Document Name',
    'download'=>'Download',
    'notes'=>'Notes',
    'uploadmessagefailed'=>'Your have reached maximum limit.',
    'uploadmessagesuccess'=>'Your file has been successfully uploaded into database.',
    'selectstore'=>'Select Store',
    'generalfile'=>'General info file',
    'storename'=>'Store Name',

    'productprice'=>'Product Price',
    'amazoncommission20'=>'Amazon Commission (%20)',
    'importfee'=>'Import Fee',
    'salestax25'=>'Sales Tax (%25)',
    'shippingprice'=>'Shipping Price',
    'profit20'=>'Profit % 20',
    'totalamount'=>'Total Amount',
    'profitloss'=>'Profit / Loss',
    'totalcost'=>'Total Cost',
    'toplamsatış'=>'Toplam Satış',


];
