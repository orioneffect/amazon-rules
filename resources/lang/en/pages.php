<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'country'=>'Country',
    'store'=>'Stores',
    'address'=>'Address',
    'emailaddress'=>'Email address',
    'phone'=>'Phone',
    'mobile'=>'Mobile',
    'fax'=>'Fax',
    'contactperson'=>'Contact Person',
    'website'=>'Website',
    'social'=>'Social',
    'basicinfo'=>'Basic Information',
    'fname'=>'First Name',
    'lname'=>'Last Name',
    'faddress'=>'Address 1',
    'saddress'=>'Address 2',
    'city'=>'City',
    'state'=>'State',
    'pincode'=>'Pincode',
    'created'=>'Created',
    'shipping'=>'Shipping',
    'currency'=>'Currency',
    'defaultstore'=>'Default Store',
    'defaultlan'=>'Default Language',
    'defaultcountry'=>'Default Country',
    'name'=>'Name',


];
