<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Help Desk Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'createticket' => 'Create Ticket',
    'ticketlist' => 'Ticket List',
    'title' => 'Title',
    'desc' => 'Description',
    'priority' => 'Priority',
    'create_ticket' => 'Create Ticket',
    'msg' => 'Message',
    'send_msg' => 'Send Message',
    'screenshots' => 'Screenshots',
    'addsshot' => 'Add Screenshot',
    'upload' => 'Upload',
    'close'=>'Close',
    'totaltickets'=>'Total Tickets',
    'responded'=>'Responded',
    'resolve'=>'Resolve',
    'pending'=>'Pending',
    'selectpriority'=>'Select Priority',
    'selectdate'=>'Select Date',
    'selectstatus'=>'Select Status',
    'search'=>'Search',
    'ticketdetail'=>'Ticket Detail',
    'activity'=>'Activity',
    'id'=>'ID',
    'title'=>'Title',
    'priority'=>'Priority',
    'user'=>'User',
    'downloads'=>'Downloads',
    'date'=>'Date',
    'to'=>'to',
    'addlogo'=>'Add Logo',
    'ticketdetails'=>'Ticket Details',
    'ticketinfo'=>'Ticket Info',
    'noneupload'=>'None Uploaded',
    'sts'=>'Status:',
    'ticketreplies'=>'Ticket Replies',
    'sendmessage'=>'Send Message',
    'admin'=>'Admin',
    'setstatus'=>'Set Status',

];
