<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logintoaccount' => 'Login to your account',
    'email' => 'Email',
    'password' => 'Password',
    'rememberme' => 'Remember me',
    'forgotpwd' => 'Forgot password?',
    'register' => 'Register',
    'login' => 'Login',
    'donthaveaccount' => 'Don\' t have an account?',
    'oops' => 'Oops',
    'forgotsomething' => 'forgot something?',
    'type_email_to_rec_pwd' => 'Type email to recover password.',
    'reset_pwd' => 'RESET PASSWORD',
    'know_your_pwd' => 'Know your password?',
    'createaccount' => 'Create an account',
    'createdsuccessfully' => 'Your account has been created successfully. Please check your emailbox to verify the email address',
    'your_name' => 'Your Name',
    'your_email' => 'Your Email',
    'your_phone' => 'Your Phone',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'register' => 'Register',
    'already_have_account' => 'Already have an account?',
    'login' => 'Login',

];
