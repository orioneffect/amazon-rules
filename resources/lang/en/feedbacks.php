<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Feedbacks Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'feedbacks'=>'Feedbacks',
    'amazonorderid'=>'{{AmazonOrderId}}',
    'asin'=>'{{ASIN}}',
    'BuyerEmail'=>'{{BuyerEmail}}',
    'DeliveredDate'=>'{{DeliveredDate}}',
    'FeedbackLink'=>'{{FeedbackLink}}',
    'FLinkWithProduct'=>'{{FLinkWithProduct}}',
    'ProductName'=>'{{ProductName}}',
    'ProductUrl'=>'{{ProductUrl}}',
    'ProductUrlWithName'=>'{{ProductUrlWithName}}',
    'PurchaseDate'=>'{{PurchaseDate}}',
    'SenderName'=>'{{SenderName}}',
    'ShipDate'=>'{{ShipDate}}',
    'StoreName'=>'{{StoreName}}',
    'campaign'=>'Campaign',
    'allcampaign'=>'All Campaigns',
    'activecampaign'=>'Active Campaigns',
    'passivecampaign'=>'Passive Campaigns',
    'title'=>'Title',   
    'sentdate'=>'Sent Date',
    'readingrate'=>'Reading Rate',
    'statistics'=>'Statistics',
    'credate'=>'Created Date',
    'actions'=>'Actions',
    'delivered'=>'Delivered',
    'sent'=>'Mail Sent',
    'read'=>'Mail Read',
    'variables'=>'Variables',
    'readesc'=>'Read Description',
    'createnew'=>'Create New Campaign',
    'close'=>'Close',
    'cancel'=>'Cancel',
    'save'=>'Save Template',
    'confirm'=>'Confirmation',
    'shipped'=>'Shipped',
    'delivered'=>'Delivered',
    'feedback'=>'Feedback',
    'review'=>'Review',
    'allinone'=>'All in One',
    'messagetittle' => 'Select the message type you want to send.',
       
    'modaltitle' => 'How do I request reviews from my customers?',
    'modaldesc' => 'How do I request reviews from my customers? This is probably the most commonly asked question for many Amazon sellers. Due to Amazon’s strict policies, sellers become very careful and sensitive when asking their customers to leave feedback and reviews. If you violate the policies, your account may get suspended.<p>

    <p>If you are struggling to get more product reviews, this checklist can help you to understand the basics of buyer-seller messages according to Amazon compliance guidelines.
    
    <p>* Don’t offer anyone a discount, a free or discounted product, or other rewards for a review.
    <p>* Don’t offer to provide a refund or reimbursement after the buyer writes a review (including reimbursement via a non-Amazon payment method).
    <p>* Don’t link to a third-party website including in your messages.
    <p>* Don’t offer a coupon code for a future purchase.
    <p>* Don’t ask for positive reviews.
    <p>* Don’t use “if or then” language to send unhappy buyers to customer service and happy buyers to the review page.
    <p>* Don’t only ask for reviews from buyers who had a positive experience
     
    
    <p>Now, let’s start writing an email that makes buyers read and want to leave you a review.',

    'orderconfirmsub'=>'{{BuyerEmail}}',  
    'orderconfirmdesc'=>'
    Hi Buyer’s First Name<p>

<p>Thank you for your purchasing (product name) from (Brand Name). Your order should be with you shortly.

<p>We went to great lengths to ensure your product name was made from the finest materials. I hope you’ll enjoy it!

<p>If you have any questions or experience difficulties while using your (product name), please get in touch and we’ll resolve it as quickly as possible.

<p>You can reach us by replying to this email or (insert contact link)

 
<p>Regards

<p>Name of Head of Customer Support', 



'deliveryconfirmsub'=>'{{BuyerEmail}}',  
    'deliveryconfirmdesc'=>'
    Hi (Buyer’s First Name)<p>
    <p>Thank you for your order of (product name). How are you enjoying your (product name) so far? Hopefully, without any issues.
    <p> The Amazon community relies on people like you for feedback on (product name) so potential customers have more information when making a buying decision. Two minutes of your time and a few words goes a long way.
    <p> You can leave a product review here (insert review link).
    <p>Don’t forget to let us know if there’s anything wrong with your order, and we’ll fix it immediately.
     
    <p> Regards
    <p> (Head of Customer Support Name)
    ',
    
    


    'feedbacksub'=>'{{BuyerEmail}}',  
    'feedbackdesc'=>'
    Hi (Buyer’s First Name)<p>
    <p>Thank you so much for your recent order. We wanted to check if the order met your expectations and see if you are completely satisfied.

    <p>If there are any concerns with the order, please let us know directly before leaving feedback so we can more quickly address them.
    
    <p> If the order met or superceded your expectations, please take a moment to leave us positive feedback.
    <p>Regards

    <p>Name of Head of Customer Support', 





    'feedbacksub2'=>'{{BuyerEmail}}',  
    'feedbackdesc2'=>'
    <p> Hi XXX,

    <p>Thank you very much for your recent order. I hope you are enjoying your copy of XXXX. If you have a moment, I would greatly appreciate your feedback on whether or not the book arrived on time and as you expected it.

    <p>Feedback helps me to be a better seller and lets other customers know what to expect from me. It also gives me a chance to address any unexpected issues that might come up during the warehousing and shipping process. Thanks again.

    <p>Warm regards,
    <p> (Head of Customer Support Name)
',



'shippedsub' =>'{{BuyerEmail}}',
'shippeddesc' => '
<p>Hello Amazon Valued Customer,

<p>Great news! Your order has been shipped. It left our warehouse earlier today on its way to you! You can expect it to be on your doorstep within {{days of delivery}} days.
<p>
                                <ul>
                                    <li>Product name 1</li>
                                    <li>Product name 2</li>
                                    <li>Product name 3</li>
                                </ul>
 

<p>You made a great customer choice shopping with us. At {{Your Brand}}, we truly care about your customer experience and, we are 100% dedicated to your complete satisfaction. Your feedback will help us to improve our services.

 

<p>Thank you for shopping at {{Your Brand}}. If you have any questions or concerns please let us know. If you do not receive your item within {{days of delivery}} days, then please contact Amazon support here: Amazon Customer Support

 

<p>Best Regards,
<p>{{Your Brand}}
<p>Customer Care Team
',

'feedandreviewsub' =>'{{BuyerEmail}}',
'feedandreviewdesc' => '
<p>Dear {{Buyer’s full name}},

<p>{{Your Brand}} from Amazon would like to thank you for your recent purchase.

<p>Our record indicates that your order has been delivered. We want to make sure you are completely satisfied with the product you have purchased. Please let us know if there are any issues, questions or concerns. Our customer support team is standing by to assist you.
<p>If you are satisfied with the product, please take a moment to submit a product review here:

<ul>
<li>Leave Product Review: Product name 1</li>
<li>Leave Product Review: Product name 2</li>
<li>Leave Product Review: Product name 3</li>
</ul>

<p>If you’ve had a pleasant buying experience from our store on Amazon, please leave us feedback by clicking on the following link:

<p>Leave Feedback

 

<p>Thank You,
<p>Customer Service Team',




'3in1sub' => '{{BuyerEmail}}',
'3in1desc' => '
<p>Dear {{Buyer’s full name}},

<p>Our records indicate that you have recently purchased a

<ul>
<li>Product name 1</li>
<li>Product name 2</li>
<li>Product name 3</li>
</ul>
 

<p>from us on Amazon.com. In our endeavor to provide the best quality cases to customers, we would love to hear your thoughts and opinions on our product. To do so, we would encourage you to leave a review of the product on Amazon.com.

 

<p>To leave a product review, please sign in to Amazon.com and click the following link:
<ul>
<li>Leave Product Review: Product name 1</li>
<li>Leave Product Review: Product name 2</li>
<li>Leave Product Review: Product name 3</li>
 

<p>Your comments will ultimately aid us in continually improving our products and services. We want to continue to meet and exceed your expectations. Additionally, you may Leave Seller Feedback for {{Your Brand}} on Amazon.

<p>We genuinely hope that your experience with our store was one that was positive and memorable. Thank you in advance for your valuable feedback.

 

<p>Warmest regards,

<p>{{Your Brand}} Customer Service
',

];
