<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    | add menus languages
    */

    'amazon' => 'Amazon-Formulare',
    'inventory' => 'Inventar',
    'orders' => 'Bestellungen',
    'dropshipping' => 'Dropshipping',
    'add_new_product' => 'Neues Produkt hinzufügen',
    'restricted_keywords' => 'Eingeschränkte Schlüsselwörter',
    'brand_list' => 'Markenliste',
    'black_list' => 'Marke - Schwarze Liste',
    'white_list' => 'Marke - Weiße Liste',
    'restricted_products' => 'Eingeschränkte Produkte',
    'excluded_products' => 'Ausgeschlossene Produkte',
    'fixed_price_products' => 'Festpreisprodukte',
    'common_pools' => 'Gemeinsame Pools',
    'product_list' => 'Produktliste',
    'common_product_pool' => 'Gemeinsamer Produktpool',
    'common_brand_pool' => 'Gemeinsamer Markenpool',
    'fbm' => 'FBM',
    'fba' => 'FBA',
    'fbalist' => 'FBA-Liste',
    'fbashipment' => 'FBA-Sendung',
    'add_new' => 'Neu hinzufügen',
    'shipping_plan' => 'Versandplan',
    'best_selling_products' => 'Meistverkaufte Produkte',
    'duplicate_listings' => 'Doppelte Einträge',
    'stranded' => 'Gestrandet/Unterdrückt',
    'manage_orders' => 'Bestellungen verwalten',
    'sold' => 'verkauft',
    'placed' => 'Platziert',
    'confirmed' => 'Bestätigt',
    'delivered' => 'geliefert',
    'cancelled' => 'Abgesagt',
    'returned' => 'zurückgegeben',
    'refunded' => 'erstattet',
    'late' => 'spät',
    'reports' => 'Berichte',
    'account_health' => 'Kontozustand',
    'feedback' => 'Feedback',
    'feedback_status' => 'Feedback Status',
    'feedback_design' => 'Feedback Design',
    'sent_messages' => 'Gesendete Nachrichten',
    'options' => 'Optionen',
    'suppliers' => 'Lieferanten',
    'add_new_supplier' => 'Neuen Lieferanten hinzufügen',
    'supplier_list' => 'Lieferantenliste',
    'xml_adding' => 'XML-Hinzufügen',
    'fileupload' => 'Dateien hochladen',
    'addnew' => 'Neuen Shop hinzufügen',
    'mystores' => 'Meine Stores',
    'showall' => 'Alle Geschäfte anzeigen',
    'consultant_control_msg'=>'Kontrolle zulassen',
    'profitloss'=>'Gewinnverlust',
    'bulkupload'=>'Bulk-Upload',
    'newplan'=>'Neuer Plan',
    'productsearch'=>'Produktsuche'
];
