<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen mindestens sechs Zeichen lang sein und mit der Bestätigung übereinstimmen.',
    'reset' => 'Ihr Passwort wurde zurückgesetzt!',
    'sent' => 'Wir haben Ihren Link zum Zurücksetzen Ihres Passworts per E-Mail gesendet!',
    'token' => 'Dieser Token zum Zurücksetzen des Passworts ist ungültig.',
    'user' => "Wir können keinen Benutzer mit dieser E-Mail-Adresse finden.",

];
