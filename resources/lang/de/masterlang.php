<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    | add commonly using languages
    */

    'country' => 'Ländername',
    'status' => 'Status',
    'select_country' => 'Land auswählen',
    'select_state' => 'Staat/Provinzcode auswählen',
    'cancel' => 'Abbrechen',
    'create' => 'erstellen',
    'name' => 'Name',
    'full_name' => 'Ihr vollständiger Name',
    'first_name' => 'Vorname',
    'last_name' => 'Nachname',
    'select' => 'wählen',
    'state' => 'Staat/Stadt',
    'city' => 'Stadt',
    'address' => 'Adresse',
    'email' => 'E-mail',
    'phone' => 'Telefon',
    'pincode' => 'Pincode',
    'credit_card' => 'Kreditkarte',
    'exp_mon_year' => 'Ablauf Monat & Jahr',
    'cvv' => 'CVV',
    'confirm' => 'Bestätigen',
    'showmore' => 'Mehr anzeigen',
    'couponcode' => 'Rabattcode',
    'apply' => 'Bewerben',

];
