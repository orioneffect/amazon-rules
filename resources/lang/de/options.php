<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Options Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'options'=>'Optionen',
    'search'=>'Suche nach Name/Telefon/E-Mail',
    'suppliers_list'=>'Lieferantenliste',
    'addnew_suppliers'=>'Neue Lieferanten hinzufügen',
    'companyname'=>'Firmenname',
    'phone'=>'Telefon',
    'email'=>'E-mail',
    'contact'=>'Kontaktname',
    'user'=>'Benutzername',
    'status'=>'status',
    'created'=>'Erstellt am',
    'xml'=>'XML Link',
    'create'=>'Lieferanten erstellen',
    'delete'=>'Löschen',
    'image'=>'Bild',
    'product'=>'Produkt',
    'price'=>'Preis',
    'category'=>'Kategorie',
    'quantity'=>'Menge',
    'details'=>'Details',
    'select'=>'Select',
    'pushintoamazon' => 'In den Amazon-Shop Schieben',
    'filelist'=>'Dateiliste',
    'uploadnew'=>'Neue Datei hochladen',
    'uploadsearch'=>'Datei suchen',
    'createnew'=>'Neue Datei erstellen',
    'documentname'=>'Dokumentname',
    'download'=>'Herunterladen',
    'notes'=>'Notizen',
    'uploadmessagefailed'=>'Sie haben das maximale Limit erreicht.',
    'uploadmessagesuccess'=>'Ihre Datei wurde erfolgreich in die Datenbank hochgeladen.',
    'selectstore'=>'Store auswählen',
    'generalfile'=>'Allgemeine Info-Datei',
    'storename'=>'Storename',

    'productprice'=>'Produktpreis',
    'amazoncommission20'=>'Amazon-Kommission (%20)',
    'importfee'=>'Importgebühr',
    'salestax25'=>'Umsatzsteuer (%25)',
    'shippingprice'=>'Versandpreis',
    'profit20'=>'Gewinn % 20',
    'totalamount'=>'Gesamtbetrag',
    'profitloss'=>'Gewinn / Verlust',
    'totalcost'=>'Gesamtkosten',
    'toplamsatış'=>'Gesamtumsatz',


];
