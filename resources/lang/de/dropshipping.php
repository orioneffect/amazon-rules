<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dropshipping Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'dropshipping' => 'Dropshipping',
    'blacklist_description' => 'Die Produkte der Marken, die Sie der schwarzen Liste hinzugefügt haben, werden niemals in Ihren Shop übertragen, wenn sie in Ihrem Shop vorhanden sind, werden sie sofort gelöscht. Die gleiche Marke sollte jedoch nicht auf der Whitelist stehen. Stellen Sie daher sicher, dass Sie nicht in der Whitelist aufgeführt sind, bevor Sie sie zur Blacklist hinzufügen.',
    'whitelist_description' => 'Produkte von Marken, die dieser Liste hinzugefügt wurden, auch wenn Sie den Markenschutz aktivieren, bleiben Ihre Produkte in Ihrem Shop und werden regelmäßig aktualisiert, unabhängig davon, ob eine Markenregistrierung vorliegt.',
    'restricted_description' => 'Ihre Produkte auf der Liste der eingeschränkten Produkte werden vollständig aus Ihrem Shop entfernt und werden Ihrem Shop nie wieder hinzugefügt, wenn Sie sie nicht aus der Liste der eingeschränkten Produkte entfernen.',
    'common_pool_brand_description' => 'Wir lesen die Produktwarnungs-E-Mails von Amazon an die Benutzer und fügen sie dem gemeinsamen Markenpool hinzu. Auf diese Weise möchten wir künftig möglicherweise von Amazon kommende „Policy Warning“-Benachrichtigungen minimieren und Ihr Konto sicherer machen, indem problematische Marken/Produkte dank anderer Benutzer im Voraus erkannt werden, auch wenn diese nicht in Ihrem Inventar sind. Sie können Marken zu Ihrer schwarzen Liste für Marken hinzufügen, indem Sie die Marken aus dem Pool auswählen. ',
    'common_pool_warning' => 'Hinweis: Bereits hinzugefügte Marken werden automatisch ausgeschlossen, wenn Sie versuchen, dieselbe Marke erneut in die schwarze Liste aufzunehmen',
    'common_pool_product_description' => 'Wir lesen die Produktwarnungs-E-Mails, die von Amazon an die Benutzer gesendet werden, und fügen sie dem gemeinsamen eingeschränkten Produktpool hinzu. Auf diese Weise möchten wir zukünftig möglicherweise von Amazon kommende „Policy Warning“-Benachrichtigungen minimieren und Ihr Konto sicherer machen, indem problematische Produkte dank anderer SellerRunning-Benutzer im Voraus erkannt werden, auch wenn diese nicht in Ihrem Inventar sind. Sie können Produkte zu Ihrer eingeschränkten Produktliste hinzufügen, indem Sie die Produkte aus dem Pool auswählen. ',
    'exclude_already_added' => 'Bereits hinzugefügte Elemente ausschließen',
    'blacklist' => 'Schwarze Liste',
    'whitelist' => 'Whitelist',
    'restrictedlist' => 'Eingeschränkte Produktliste',
    'id' => 'ID',
    'type' => 'Typ',
    'asin' => 'ASIN',
    'brandname' => 'Markenname',
    'enter_brandname' => 'Markennamen eingeben, einen pro Zeile',
    'enter_asin' => 'ASIN eingeben, eine pro Zeile',
    'trademark' => 'Warenzeichen',
    'delete' => 'Löschen',
    'select' => 'wählen',
    'select_all' => 'Alles Auswählen',
    'country' => 'Land',
    'no_more_data' => 'Keine weiteren Daten zum Anzeigen',
    'add_blacklist' => 'Neue schwarze Liste hinzufügen',
    'add_whitelist' => 'Neue weiße Liste hinzufügen',
    'add_restricted_keywords' => 'Neue eingeschränkte Keywords hinzufügen',
    'add_restricted' => 'Neue eingeschränkte Produktliste hinzufügen',
    'create_blacklist' => 'Schwarze Liste erstellen',
    'create_whitelist' => 'Weiße Liste erstellen',
    'create_restricted' => 'Eingeschränktes Produkt erstellen',
    'create_restricted_keywords' => 'Liste erstellen',
    'restricted_keywords' => 'Eingeschränkte Schlüsselwörter',
    'keywords' => 'Schlüsselwörter',
    'enter_keywords' => 'Schlüsselwörter eingeben',
    'restricted_keywords_description'=>'Sie können Warnungen zu mutmaßlichen Verletzungen des geistigen Eigentums minimieren, indem Sie bestimmte Wörter im Titel der Produkte blockieren. Wenn die Keywords, die Sie dieser Liste hinzufügen, im Titel Ihrer Produkte auf Amazon.com und in Ihrem Shop enthalten sind, werden diese Produkte aus Ihrem Shop entfernt. Die von Ihnen hinzugefügten Keywords oder Produkte werden nach einer bestimmten Zeit aus Ihrem Shop entfernt.',
    'success' => 'Erfolgreich in die Datenbank aufgenommen',
    'search' => 'Suchen',
    'add_to_brand_black_list' => 'Zur Marken-Schwarzen Liste hinzufügen',
    'asin_note' => 'Sie können bis zu 5.000 ASINs oder Produkt-URLs gleichzeitig eingeben.',
    'addproduct_note1' => 'Sie können ASINs der Produkte, die Sie zu Ihrem Shop hinzufügen möchten, in unserem System hinzufügen.',
    'addproduct_note2' => 'Nachdem Sie die ASIN-Liste übermittelt haben, werden Ihre Produkte innerhalb von 60 Minuten in Ihren Shop hochgeladen.',
    'addproduct_bulk' => 'Produkt in großen Mengen hinzufügen',
    'sku_note'=>'Legen Sie ein Präfix für Ihre SKUs fest. Beispiel AMZR-XXXX-123456',
    'sku' => 'SKU-Präfix',
    'create_products' => 'Produkte erstellen',
    'upload_asin' => 'ASIN hochladen',
    'store' => 'Laden',
    'itemname' => 'Artikelname',
    'manufacturer'=>'Hersteller',
    'productsearch'=>'Suchen Sie Ihr Produkt'


];
