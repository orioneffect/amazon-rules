<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Subscription Language Lines - English
    |--------------------------------------------------------------------------
    |
    |
    */
    'personal' => 'Persönliche Daten',
    'bank' => 'Bankverbindung',
    'amount' => 'Abo-Betrag',
    'choose_plan' => 'Plan wählen',
    'subscription' => 'Abonnement',
    'confirm_subscription' => 'Abonnement bestätigen',
    'subscription_list' => 'Abonnementliste',
    'no_of_store' => 'Anzahl der Geschäfte',
    'repricer' => 'Repricer',

    'unlimited_stores' => 'Unbegrenzt',
    'advanced_filters' => 'Erweiterter Filter',
    'shipping_usage' => "Versandnutzung",
    'max_asin_count' => "Maximale ASIN-Anzahl",
    'unlimited_asin_limit' => "Unbegrenzt",
    'stock_control' => "Bestandskontrolle",
    'price_control' => "Preiskontrolle",
    'trademark_control' => "Markenkontrolle",
    'dropshipping_flag' => "Dropshipping",
    'fba_flag' => "FBA",
    'ebay_etsy_flag' => "EBay, ETSY Suche",
    'supplier_wholesale_america_flag' => "Lieferanten Großhandel Amerika",
    'supplier_wholesale_turkey_flag' => "Lieferanten Großhandel Türkei",
    'invalidCreditCard' => 'Ungültige Kreditkarte.',
    'invalidExpDate' => 'Ungültiges Ablaufdatum.',
    'totalamount' => 'Gesamtbetrag',
    'discount' => 'Rabatt',
    'balance' => 'Endgültiger zu zahlender Betrag',
    'coupon_success' => 'Ja!! Sie haben :1: Rabatt!',
    'coupon_failed' => 'Nein! Sie haben einen :1: Gutschein eingegeben',
];
