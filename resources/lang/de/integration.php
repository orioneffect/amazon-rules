<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'store_list' => 'Store-Liste',
    'owner' => 'Eigentümername',
    'store_name' => 'Store-Name',
    'add_store' => 'Shop hinzufügen',
    'add_newstore' => 'Neuen Shop hinzufügen',
    'stock' => 'Lager',
    'store_changed' =>'Ihr aktueller Shop wurde erfolgreich geändert.',
    'store_added' =>'Neuer Shop erfolgreich integriert.',
    'error'=>'Sie müssen einen Shop auswählen, um fortzufahren',
    'basic_details' => 'Grundlegende Details',
    'add_on' => 'Add-On',
    'integration' => 'Integration',
    'budget' => 'Budget',
    'price_update' => 'Preisaktualisierung',
    'stock_update' => 'Aktienaktualisierung',
    'product_remove' => 'Produkt entfernen',
    'product_insert' => 'Produktbeilage',
    'select_cuntry' => 'Bitte wählen Sie ein Land.',
    'current_store' => 'Aktueller Store.',
];
