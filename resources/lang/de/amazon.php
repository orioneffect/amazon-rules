<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'amazon' => 'Amazon',
    'orders' => 'Bestellungen',
    'reports' => 'Berichte',
    'addreport' => 'Neuen Bericht hinzufügen',
    'type' => 'Berichtstyp',
    'from' => 'Von Datum',
    'to' => 'Bis Datum',
    'status' => 'Status',
    'action' => 'Aktion',
    'created' => 'Erstellungsdatum',
    'new_report_added' => 'Ihre Anfrage ist eingegangen. Bitte erlauben Sie 5-10 Minuten, um den Bericht zu erstellen.',
    'in_progress' => 'Ihre Berichterstellung läuft noch. Bitte erlauben Sie 5-10 Minuten, um den Bericht zu erstellen.',
    'retry' => 'Klicken Sie hier, um es erneut zu versuchen.',
    'deleted' => 'Bericht erfolgreich gelöscht..',
    'health' => 'Gesundheitsbericht',
    'performance_metrix' => 'Leistungsmetrix',
    'lateShipment' => 'Später Versand',
    'target' => 'Ziel',
    'count' => 'Metrix Count',
    'rate' => 'bewerten',
    'metrix' => 'Metrix',
    'account_health_status'=>'Kontozustand',
    'lateShipmentRate'=>'Rate für verspätete Lieferung',
    'onTimeDeliveryRate' =>'Pünktliche Lieferrate',
    'invoiceDefectRate'=>'Rechnungsfehlerrate',
    'orderDefectRate_afn'=>'Bestellfehlerrate Amazon Fullfilment',
    'orderDefectRate_mfn'=>'Bestellfehlerrate Herstellererfüllung',
    'validTrackingRate'=>'Gültige Tracking-Rate',
    'preFulfillmentCancellationRate'=>'Pre-Fulfillment-Stornierungsrate',
    'accountHealthRating'=>'Kontozustandsbewertung',
    'listingPolicyViolations'=>'Richtlinienverstöße auflisten',
    'productAuthenticityCustomerComplaints'=>'Produktauthentizitäts-Kundenbeschwerden',
    'productConditionCustomerComplaints'=>'Produktbedingungs-Kundenbeschwerden',
    'receivedIntellectualPropertyComplaints'=>'Received Intellectual Property Complaints',
    'restrictedProductPolicyViolations'=>'RVerstöße gegen eingeschränkte Produktrichtlinien',
    'suspectedIntellectualPropertyViolations'=>'Vermutete Verletzungen des geistigen Eigentums',
    'foodAndProductSafetyIssues'=>'Lebensmittel- und Produktsicherheitsprobleme',
    'customerProductReviewsPolicyViolations'=>'Verstöße gegen die Richtlinien für Kundenbewertungen',
    'otherPolicyViolations'=>'Andere Richtlinienverstöße',
    'documentRequests'=>'Dokumentanfragen',
    'maximum_allowed'=>'Maximal zulässig',
    'policy_violation'=>'Richtlinienverstoß',
    'productSafetyCustomerComplaints'=>'Produktsicherheit Kundenbeschwerden',
    'none'=>'Keine',
    'warningstatus'=>'Warnungsstatus',
    'no_of_violation'=>'Anzahl der Verstöße',
    'last_generated_on' => 'Ihr letzter Gesundheitsbericht erstellt am ',
    'generate_new' => 'Klicken Sie hier, um einen neuen Bericht zu generieren.',
    'no_more_data'=>'Wir haben keine weiteren Daten zum Anzeigen.',
    'price'=>'Bestellpreis',
    'purchasedate'=>'Kaufdatum',
    'deliverydate'=>'Lieferdatum',
    'orderid' => 'Amazon-Bestell-ID',


];
