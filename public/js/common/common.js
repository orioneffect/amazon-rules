function demolish(id) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    },function(isConfirm){
      if (isConfirm) {
        $.ajax({
          url: "../common/demolish?id="+id+"&refname="+$("#refname").val(),
          datatype: "json",
          type: "get",
        }).done(function (response) {
          console.log(response);
            swal("Deleted!", {
              icon: "success",
            });
            window.location.reload();
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            console.log('Server error occured');
        });
      }
    });
}
