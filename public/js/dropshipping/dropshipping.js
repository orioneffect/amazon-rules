    $('#selectall').on("click",function() {
        if($("#selectall").is(':checked')) {
        $(".exclude_item").prop('checked', true);
        } else {
            $(".exclude_item").prop('checked', false);
        }
    });

    function deleteitem(url) {
            $.ajax({
                url: url,
                datatype: "json",
                type: "get",
                beforeSend: function () {
                    $('.auto-load').show();
                }
            })
            .done(function (response) {

                console.log(response);
                search();
            
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });

    }


    function loadmore(url,nomoredata) {
            $.ajax({
                url: url,
                datatype: "html",
                type: "get",
                beforeSend: function () {
                    $('.auto-load').show();
                }
            })
            .done(function (response) {
                loaded = loaded  + 50;
                //console.log(response);
                $('.auto-load').hide();
                $("#data-wrapper").append(response);
                if(response == '') {
                    $('#stopload').show();
                    $('#stopload').html(nomoredata);
                    $('#nextload').hide();
                }
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });
    }

    